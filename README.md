# itp-2023-24-students

This is the official Git repository for the course material of the ITP course of the academic year of 2023/2024.

Homework exercises are due each Friday evening. The exact deadlines will be announced on the exercise sheets and on Canvas.

