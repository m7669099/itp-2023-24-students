theory Auto_Proof_Demo
imports Main
begin

section\<open>Logic and sets\<close>

lemma "\<forall>x. \<exists>y. x=y"
  by auto
  
lemma "\<exists>x y::nat. x*y>n"
  thm exI
  apply (rule exI[where x="n+1"])
  apply (rule exI[where x=2])
  by simp
  
    

lemma "A \<subseteq> B \<inter> C \<Longrightarrow> A \<subseteq> B \<union> C"
  by auto

text\<open>Note the bounded quantification notation:\<close>

lemma "\<lbrakk> \<forall>xs \<in> A. \<exists>ys. xs = ys @ ys;  us \<in> A \<rbrakk> \<Longrightarrow> \<exists>n. length us = n+n"
  by fastforce
  
text\<open>
 Most simple proofs in FOL and set theory are automatic.
 Example: if T is total, A is antisymmetric
 and T is a subset of A, then A is a subset of T.
\<close>

lemma AT:
  "\<lbrakk> \<forall>x y. T x y \<or> T y x;
     \<forall>x y. A x y \<and> A y x \<longrightarrow> x = y;
     \<forall>x y. T x y \<longrightarrow> A x y \<rbrakk>
   \<Longrightarrow> \<forall>x y. A x y \<longrightarrow> T x y"
by blast

lemma "(\<forall>x. P x \<longrightarrow> Q x)"
  apply auto
  oops

prop "\<And>x. P \<Longrightarrow> Q"

definition  "foo P Q x \<longleftrightarrow> (\<forall>x. P x \<longrightarrow> Q x)"

section\<open>Sledgehammer\<close>

lemma "R\<^sup>* \<subseteq> (R \<union> S)\<^sup>*" 
  by (simp add: rtrancl_mono)

(* Find a suitable P and try sledgehammer: *)
lemma lemma1: "a # xs = ys @ [a] \<Longrightarrow> xs=[] \<and> ys=[] \<or> (\<exists>zs. xs=zs@[a] \<and> ys=a#zs)"
  thm append_eq_Cons_conv
  by (metis append_eq_Cons_conv list.inject)
    
(* Can you also show equivalence ? *)    
lemma "a # xs = ys @ [a] \<longleftrightarrow> (xs=[] \<and> ys=[] \<or> (\<exists>zs. xs=zs@[a] \<and> ys=a#zs))"
  by (auto dest: lemma1)
  (*by (meson Cons_eq_append_conv lemma1)*)


end
