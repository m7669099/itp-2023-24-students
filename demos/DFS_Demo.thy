section \<open>A verified DFS algorithm\<close>
(**
  This file is form interactive development during lecture/tutorial.
  
  See DFS_Demo_Doc for an annotated file explaining all the intermediate steps!
*)


theory DFS_Demo
imports Option_Monad_Tools
begin

text \<open>
  In this theory, I leave all intermediate development steps, 
  and add an explanation in which order I did what steps, and why, to finally 
  arrive at a clean and concise verification.
\<close> 


subsection \<open>Graph by successor Function\<close>

type_synonym 'v sg = "('v \<Rightarrow> 'v list)"

definition sg_\<alpha> :: "'v sg \<Rightarrow> 'v rel"
  where "sg_\<alpha> sg \<equiv> {(u,v). v \<in> set (sg u)}"

definition sg_succs :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v list"
  where "sg_succs sg v \<equiv> sg v"

lemma sg_succs_correct[simp]: "set (sg_succs sg v) = sg_\<alpha> sg `` {v}"
  unfolding sg_succs_def sg_\<alpha>_def
  by auto


subsection \<open>Problem Specification\<close>  
text \<open>It's good to start with a specification of what your algorithm shall compute\<close>

(*
  
  Compute set of nodes reachable from start node v:

    E\<^sup>*``{v}

  (if this set is finite)
*)  
  
  

  
subsection \<open>Pseudocode Stage\<close>  
text \<open>Next, we sketch out the algorithm in pseudocode.
  For more complex algorithms, it actually makes sense to implement the 
  algorithm in your favorite programming language first! 
  
  When implementing (in pseudocode or real code), already try to think about the abstract structure. 
  And try to only use features you know how to express in Isabelle 
  (E.g., if you use loops with break, do you know how to model that in Isabelle?).
\<close>  
  
(*
  Pseudocode:

  dfs g v:
    wl = [v]  // Worklist
    V = {}    // Visited set
    
    while worklist \<noteq> []
      remove first element v from worklist
      
      if v\<notin>V 
        V = {v} \<union> V
        wl = succs g v @ wl
    
    return V    

*)

subsection \<open>Implementation in Isabelle\<close>
text \<open>
  Only use data-structures via their interfaces!
  We'll use lists (hd,tl,\<noteq>[]) and sg_succs.
\<close>


definition dfs :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v set option" where
  "dfs g v \<equiv> do {
    (_,V) \<leftarrow> while (\<lambda>(wl,V). wl\<noteq>[]) (\<lambda>(wl,V). do {
      undefined
    }) ([v],{});
    Some V
  }"

  
  
  
  
  
  
(** Back to Lecture **)  
  
  
  
  
  
  
  
  
  
  
  
  
subsection \<open>Correctness proof (soundness) (\<open>\<subseteq>\<close> direction)\<close>   
  
text \<open>We want to prove  

  result = reachable nodes

  we start with the easier direction, namely result \<open>\<subseteq>\<close> reachable nodes.
  this is also called soundness, as every node that is found is actually reachable.
  the symmetric \<open>completeness\<close> property (\<open>\<supseteq>\<close>) then states that every reachable node is actually found.
  
  We also prove termination already.
\<close> 


text \<open>Let's declare our assumptions in a context, so we don't have to restate them, over and over again\<close>  
context
  fixes g :: "'v sg" and v\<^sub>0 :: "'v"
  assumes finite_reachable: "finite ((sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
begin

  abbreviation "E \<equiv> sg_\<alpha> g" (* Shortcut for the edges *)

  subsubsection \<open>Invariants\<close>
  (*
    Invariant (soundness)
    
      V and wl contain only reachable nodes
    
      \<rightarrow> immediately yields V \<subseteq> reachable nodes at end of loop
  
    Termination:
      either we discover a new node, or worklist gets shorter
  
      number of undiscovered nodes <*lex*> length worklist
  
  *)
  
  text \<open>Note that we define the invariants as their own constants. 
    This will separate the verification condition generation part from
    the rest of the proof.
  \<close>
  definition dfs_invar_sound :: "'v list \<times> 'v set \<Rightarrow> bool" where
    "dfs_invar_sound \<equiv> \<lambda>(wl,V). undefined"  
  
  definition dfs_var :: "('v list \<times> 'v set) rel" where
    "dfs_var \<equiv> inv_image (inv_image finite_psubset (\<lambda>V. E\<^sup>*``{v\<^sub>0}-V) <*lex*> measure length) prod.swap"

    
    
  subsubsection \<open>Auxiliary Lemmas\<close>

  (** To be filled in *)    
    
  subsubsection \<open>Soundness lemma\<close>
  (*
    We first start exploring the proof, to find suitable auxiliary lemmas.
    Afterwards, we clean up the proof.
  *)
    
  (* Proof exploration. *)  
  lemma dfs_sound: "wp (dfs g v\<^sub>0) (\<lambda>V. V \<subseteq> (sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
    unfolding dfs_def
    apply (intro wp_intro wp_while[where I="dfs_invar_sound" and V="dfs_var"])
    apply simp_all
    oops
    
    (* Don't forget to clean up! *)
  
    
    
    
(** Back to Lecture **)  
    
    
    
    
    
    
    
    
    
    
    
        
  subsection \<open>Correctness proof (completeness) (\<open>\<supseteq>\<close> direction)\<close>    
    
  subsubsection \<open>Invariants\<close>  
  
  (*
    The basic idea for the completeness direction is to show that every step that we make 
    from a visited node brings us to a visited node, or into the worklist.
  
    Intuitively, the worklist is a "search frontier", separating the already explored space V from
    the unexplored space.
    
    Re-use old invar
    Don't forget v\<^sub>0!
  *)
    

  definition "dfs_invar \<equiv> \<lambda>(wl,V). undefined"

  subsubsection \<open>Auxiliary Lemmas\<close>

  (* Generate from aux-lemmas developed for soundness direction. 
    Structure looks the same!
  *)
    
  subsubsection \<open>Correctness lemma\<close>
  
  (* We try the same structure as for the soundness lemma *)
  lemma dfs_correct: "wp (dfs g v\<^sub>0) (\<lambda>V. V = (sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
    unfolding dfs_def
    apply (intro wp_intro wp_while[where I="dfs_invar" and V="dfs_var"])
    oops

  lemmas [wp_intro] = wp_cons[OF dfs_correct]
      
    
end  
  
end
