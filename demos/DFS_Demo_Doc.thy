section \<open>Documentation how to develop a verified DFS algorithm\<close>
theory DFS_Demo_Doc
imports Option_Monad_Tools
begin

text \<open>
  In this theory, I leave all intermediate development steps, 
  and add an explanation in which order I did what steps, and why, to finally 
  arrive at a clean and concise verification.
\<close> 


subsection \<open>Graph by successor Function\<close>

type_synonym 'v sg = "('v \<Rightarrow> 'v list)"

definition sg_\<alpha> :: "'v sg \<Rightarrow> 'v rel"
  where "sg_\<alpha> sg \<equiv> {(u,v). v \<in> set (sg u)}"

definition sg_succs :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v list"
  where "sg_succs sg v \<equiv> sg v"

lemma sg_succs_correct[simp]: "set (sg_succs sg v) = sg_\<alpha> sg `` {v}"
  unfolding sg_succs_def sg_\<alpha>_def
  by auto


subsection \<open>Problem Specification\<close>  
text \<open>It's good to start with a specification of what your algorithm shall compute\<close>

(*
  
  Compute set of nodes reachable from start node v:

    E\<^sup>*``{v}

  (if this set is finite)
*)  
  
  

  
subsection \<open>Pseudocode Stage\<close>  
text \<open>Next, we sketch out the algorithm in pseudocode.
  For more complex algorithms, it actually makes sense to implement the 
  algorithm in your favorite programming language first! 
  
  When implementing (in pseudocode or real code), already try to think about the abstract structure. 
  And try to only use features you know how to express in Isabelle 
  (E.g., if you use loops with break, do you know how to model that in Isabelle?).
\<close>  
  
(*
  Pseudocode:

  dfs g v:
    wl = [v]  // Worklist
    V = {}    // Visited set
    
    while worklist \<noteq> []
      remove first element v from worklist
      
      if v\<notin>V 
        V = {v} \<union> V
        wl = succs g v @ wl
    
    return V    

*)

subsection \<open>Implementation in Isabelle\<close>
text \<open>
  Now you are ready for the first implementation in Isabelle.
  
  Try to use data structures with clean interfaces for the operations.
  Or the abstract data types directly, if possible!

  Here, we decided to use the abstract data type set directly for the visited set V.
  However, as we don't have nondeterminism yet, we cannot easily model the operation 
    "select some node from workset"
  That's why we decided to model on the level of worklist. 
  
  Thus, we also had to model the successors of a node as a list, that we could easily prepend
  to the worklist. Again, prepending a set of successors to a list cannot be easily modeled, as the 
  set has no order.
    
  Thus, we used the graph by successor function representation, but made sure to only
  used well-defined operations, for which we have abstraction lemmas.
  The only operation we needed is get successors (sg_succs):
\<close>


definition dfs :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v set option" where
  "dfs g v \<equiv> do {
    (_,V) \<leftarrow> while (\<lambda>(wl,V). wl\<noteq>[]) (\<lambda>(wl,V). do {
      let (v,wl) = (hd wl, tl wl);
      
      if v\<notin>V then do {
        let V = insert v V;
        let wl = sg_succs g v @ wl;
        Some (wl,V)
      } else Some (wl,V)
    
    
    }) ([v],{});
    Some V
  }"

  
  
  
  
  
  
(** Back to Lecture **)  
  
  
  
  
  
  
  
  
  
  
  
  
subsection \<open>Correctness proof (soundness) (\<open>\<subseteq>\<close> direction)\<close>   
  
text \<open>We want to prove  

  result = reachable nodes

  we start with the easier direction, namely result \<open>\<subseteq>\<close> reachable nodes.
  this is also called soundness, as every node that is found is actually reachable.
  the symmetric \<open>completeness\<close> property (\<open>\<supseteq>\<close>) then states that every reachable node is actually found.
  
  We also prove termination already.
\<close> 


text \<open>Let's declare our assumptions in a context, so we don't have to restate them, over and over again\<close>  
context
  fixes g :: "'v sg" and v\<^sub>0 :: "'v"
  assumes finite_reachable: "finite ((sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
begin

  abbreviation "E \<equiv> sg_\<alpha> g" (* Shortcut for the edges *)

  subsubsection \<open>Invariants\<close>
  (*
    Invariant (soundness)
    
      V and wl contain only reachable nodes
    
      \<rightarrow> immediately yields V \<subseteq> reachable nodes at end of loop
  
    Termination:
      either we discover a new node, or worklist gets shorter
  
      number of undiscovered nodes <*lex*> length worklist
  
  *)
  
  text \<open>Note that we define the invariants as their own constants. 
    This will separate the verification condition generation part from
    the rest of the proof.
  \<close>
  definition dfs_invar_sound :: "'v list \<times> 'v set \<Rightarrow> bool" where
    "dfs_invar_sound \<equiv> \<lambda>(wl,V). set wl \<union> V \<subseteq> (E)\<^sup>*``{v\<^sub>0}"  
  
  definition dfs_var :: "('v list \<times> 'v set) rel" where
    "dfs_var \<equiv> inv_image (inv_image finite_psubset (\<lambda>V. E\<^sup>*``{v\<^sub>0}-V) <*lex*> measure length) prod.swap"

    
    
  (********* GOTO dfs_sound lemma! *********)  
    
  subsubsection \<open>Auxiliary Lemmas\<close>
  (* The following lemmas have been generated during exploring proof of dfs_sound further down.
    Start reading from there!
  *)  

  (* Generated during exploring proof of dfs_sound further down *)  
  lemma dfs_var_wf: "wf (dfs_var)"  
    unfolding dfs_var_def
    by auto
    
  (* Generated during exploring proof of dfs_sound further down *)  
  lemma dfs_invar_sound_init: "dfs_invar_sound ([v\<^sub>0], {})" 
    unfolding dfs_invar_sound_def
    by auto
    
  (* Generated during exploring proof of dfs_sound further down *)  
  lemma dfs_invar_sound_pres_newnode_EXPLORATION:
    assumes "dfs_invar_sound (wl, V)" "wl \<noteq> []" "hd wl \<notin> V"
    shows "dfs_invar_sound (sg_succs g (hd wl) @ tl wl, insert (hd wl) V)"
      and "((sg_succs g (hd wl) @ tl wl, insert (hd wl) V), wl, V) \<in> dfs_var"
      
    (* First step after phrasing aux lemma: 
      use sorry, and check if aux-lemma solves your goal!
    *)  
      
    (* Proof exploration: *)  
    subgoal (* Invar *) 
      (* Unfold invar to find relevant lemmas over fully abstract concepts *)
      using assms
      unfolding dfs_invar_sound_def
      apply (cases wl) (* Split wl intro hd and tl *)
      apply auto (* We are lucky, everything got proved automatically *)
      done
    subgoal (* variant *)  
      using assms
      unfolding dfs_var_def dfs_invar_sound_def
      apply (cases wl) (* Split wl intro hd and tl *)
      apply auto
      (* analyze subgoals. we can alsop sledgehammer at this point to get inspirations  *)
      (* 
      subgoal sledgehammer sorry
      subgoal sledgehammer sorry
      
      only finite_reachable seems to be missing.
      *)
      by (auto simp: finite_reachable) (* Trying that*)
    done
    
  (* Note: the above was only a documentation how I found a proof. Now, this is typically cleaned up,
    either into an Isar proof if you want to document certain steps, or into a clean 
    apply-style proof, in particular not using auto before other methods. 
  *)  
  lemma dfs_invar_sound_pres_newnode:
    assumes "dfs_invar_sound (wl, V)" "wl \<noteq> []" "hd wl \<notin> V"
    shows "dfs_invar_sound (sg_succs g (hd wl) @ tl wl, insert (hd wl) V)"
      and "((sg_succs g (hd wl) @ tl wl, insert (hd wl) V), wl, V) \<in> dfs_var"
    subgoal (* Invar *) 
      using assms
      unfolding dfs_invar_sound_def
      by (cases wl) auto
    subgoal (* variant *)  
      using assms
      unfolding dfs_var_def dfs_invar_sound_def
      by (cases wl) (auto simp: finite_reachable)
    done
    
  (* Generated during exploring proof of dfs_sound further down *)  
  lemma dfs_invar_sound_pres_exnode_EXPLORE:  
    assumes "dfs_invar_sound (wl, V)" "wl \<noteq> []" "hd wl \<in> V"
    shows "dfs_invar_sound (tl wl, V)" and "((tl wl, V), wl, V) \<in> dfs_var"
    (* Let's try what we have learned from proof exploration for _newnode lemma first *)
    subgoal
      using assms
      unfolding dfs_invar_sound_def
      apply (cases wl)
      apply auto
      (* Perfect! ;) *)
      done
    
    subgoal
      using assms
      unfolding (*dfs_invar_sound_def*) dfs_var_def
      apply (cases wl)
      apply auto
      (* Perfect! ;) ... actually, we don't need the invariant here, so I have removed its unfolding. *)
      done
    done        
    
  (* Clean up: *)  
  lemma dfs_invar_sound_pres_exnode:
    assumes "dfs_invar_sound (wl, V)" "wl \<noteq> []" "hd wl \<in> V"
    shows "dfs_invar_sound (tl wl, V)" and "((tl wl, V), wl, V) \<in> dfs_var"
    subgoal (* Invar *) 
      using assms
      unfolding dfs_invar_sound_def
      by (cases wl) auto
    subgoal (* variant *)  
      using assms
      unfolding dfs_var_def
      by (cases wl) auto
    done
    
  lemma dfs_invar_sound_final:
    assumes "dfs_invar_sound ([], V)" "v \<in> V"  
    shows "(v\<^sub>0, v) \<in> E\<^sup>*"  
    using assms
    unfolding dfs_invar_sound_def by auto
    
    
  subsubsection \<open>Soundness lemma\<close>
  (*
    We first start exploring the proof, to find suitable auxiliary lemmas.
    Afterwards, we clean up the proof.
    
    Typically, you would not leave the exploration step in your theory, but
    this theory is meant to document the process, not only the final result.
  
  *)
    
  (* Proof exploration. *)  
  lemma dfs_sound_EXPLORATION: "wp (dfs g v\<^sub>0) (\<lambda>V. V \<subseteq> (sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
    unfolding dfs_def
    apply (intro wp_intro wp_while[where I="dfs_invar_sound" and V="dfs_var"])
    apply simp_all
    
    (* generated aux-lemmas for 1st and second subgoal *)
    subgoal by (simp add: dfs_var_wf)
    subgoal by (simp add: dfs_invar_sound_init)
    
    subgoal for s wl V 
      (* This looks like two cases *)
      apply (intro conjI)
      subgoal (* case: new node *)
        (* Explore aux-lemmas *)
        apply clarsimp
        (* Aux-lemma generated from this subgoal *)
        by (auto simp: dfs_invar_sound_pres_newnode)
      subgoal (* case: existing node *)  
        apply clarsimp
        (* Aux-lemma generated from this subgoal *)
        apply (auto simp: dfs_invar_sound_pres_exnode)
        done
      done
    subgoal for s wl V
      apply clarsimp
      (* aux-lemma generated from here *)
      by (auto simp: dfs_invar_sound_final)
    done
  
  (* Clean up: *)  
  lemma dfs_sound: "wp (dfs g v\<^sub>0) (\<lambda>V. V \<subseteq> (sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
    unfolding dfs_def
    apply (intro wp_intro wp_while[where I="dfs_invar_sound" and V="dfs_var"])
    by (auto simp:
      dfs_var_wf dfs_invar_sound_init dfs_invar_sound_pres_newnode dfs_invar_sound_pres_exnode dfs_invar_sound_final
    )

    
    
    
    
    
(** Back to Lecture **)  
    
    
    
    
    
    
    
    
    
    
    
        
  subsection \<open>Correctness proof (completeness) (\<open>\<supseteq>\<close> direction)\<close>    
    
  (* At this point, we have proved soundness. And we also have set up some boilerplate to
    structure our proofs.
  
    We can now follow two routes:
    
      either we keep adding things to the invariant, until it is strong enough to also
      prove completeness, or we re-use the invariants for soundness when phrasing a new
      set of invariants for completeness. 
      
      The former approach is less boilerplate, but the complexity of the invariant proofs will increase.
      The latter approach is more boilerplate, but allows for reusing the existing invariant lemmas.
      
      We illustrate the latter approach here.
  
    *)
  
  subsubsection \<open>Invariants\<close>  
  
  (*
    The basic idea for the completeness direction is to show that every step that we make 
    from a visited node brings us to a visited node, or into the worklist.
  
    Intuitively, the worklist is a "search frontier", separating the already explored space V from
    the unexplored space.
    
    At the end, the worklist is empty, meaning the V is closed under edges. 
    Together with the knowledge that v\<^sub>0 is in V (we have to add this to the invariant), 
    we can conclude that V must contain all reachable nodes.
    
    
    Note that, if you do not know the invariant yet, you first try to find as much as possible
    by sketching out the proof on paper. 
    Still, you might have missed something, at which point you will have to analyze why your proof does
    not work as expected, and modify the invariant. This is iterated until everything goes through.
    
    (Fortunately, I discovered (remembered) all parts of the invariant immediately: I actually
      forgot the crucial v\<^sub>0\<in>V \<union> set wl initially, but realized that when I sketched out the 
      intuition of the invariant.
    )
  *)
    

  definition "dfs_invar \<equiv> \<lambda>(wl,V).
    dfs_invar_sound (wl,V)    \<comment> \<open>Soundness invariant holds\<close>
  \<and> v\<^sub>0\<in>V\<union> set wl             \<comment> \<open>we don't loose the start node\<close>
  \<and> E``V \<subseteq> V \<union> set wl       \<comment> \<open>search frontier (see comment above)\<close>
  "

  subsubsection \<open>Auxiliary Lemmas\<close>
    
  (* We can now copy the auxiliary lemmas from above, changing dfs_invar_sound to dfs_invar. 
    Note that we have already proved termination
  *)    
  
  lemma dfs_invar_init: "dfs_invar ([v\<^sub>0], {})" 
    unfolding dfs_invar_def
    by (simp add: dfs_invar_sound_init) (* Reuse lemma for soundness invariant! *)
    
    
      
  lemma dfs_invar_pres_newnode_EXPLORATION:
    assumes "dfs_invar (wl, V)" "wl \<noteq> []" "hd wl \<notin> V"
    shows "dfs_invar (sg_succs g (hd wl) @ tl wl, insert (hd wl) V)"
      and "((sg_succs g (hd wl) @ tl wl, insert (hd wl) V), wl, V) \<in> dfs_var"
    subgoal (* Invariant *)  
      using assms
      unfolding dfs_invar_def
      apply (simp add: dfs_invar_sound_pres_newnode) (* Reuse lemma! *)
      (* Explore remaining proof *)
      unfolding dfs_invar_sound_def
      apply (cases wl)
      apply auto
      done
    subgoal (* Variant *)  
      using assms
      unfolding dfs_invar_def
      apply (simp add: dfs_invar_sound_pres_newnode) (* Reuse lemma! *)
      done
    done      

  (* Clean-up *)  
  lemma dfs_invar_pres_newnode:
    assumes "dfs_invar (wl, V)" "wl \<noteq> []" "hd wl \<notin> V"
    shows "dfs_invar (sg_succs g (hd wl) @ tl wl, insert (hd wl) V)"
      and "((sg_succs g (hd wl) @ tl wl, insert (hd wl) V), wl, V) \<in> dfs_var"
    subgoal (* Invariant *)  
      using assms
      unfolding dfs_invar_def
      apply (simp add: dfs_invar_sound_pres_newnode) (* Reuse lemma! *)
      by (cases wl) auto
    subgoal (* Variant *)  
      using assms
      unfolding dfs_invar_def
      by (simp add: dfs_invar_sound_pres_newnode) (* Reuse lemma! *)
    done
            
  lemma dfs_invar_pres_exnode:
    assumes "dfs_invar (wl, V)" "wl \<noteq> []" "hd wl \<in> V"
    shows "dfs_invar (tl wl, V)" and "((tl wl, V), wl, V) \<in> dfs_var"
    subgoal (* Invar *) 
      using assms
      unfolding dfs_invar_def
      apply (simp add: dfs_invar_sound_pres_exnode) (* Reuse lemma! *)
      by (cases wl) auto (* Just tried pattern from above \<dots> it worked *)
    subgoal (* variant *)  
      using assms
      unfolding dfs_var_def
      by (cases wl) auto
    done
    (* No need to clean that up! *)
    
    
  lemma dfs_invar_final:
    assumes "dfs_invar ([], V)" 
    shows "V = E\<^sup>*``{v\<^sub>0}"  
  proof
    show "V \<subseteq> local.E\<^sup>* `` {v\<^sub>0}" (* This direction follows from dfs_invar_sound *)
      using assms unfolding dfs_invar_def
      by (auto simp: dfs_invar_sound_final)
      
  next
    (* We're looking for something about sets closed under relation *)
    find_theorems "_``?V \<subseteq> ?V"
    thm Image_closed_trancl (* That looks right! *)
  
    show "local.E\<^sup>* `` {v\<^sub>0} \<subseteq> V"
      using assms unfolding dfs_invar_def
      (* Proof exploration
      apply auto
      
        Sledgehammer finds:
          using Image_closed_trancl by blast
        
        playing around, we find that the following also works
          apply (blast dest: Image_closed_trancl)
        
        and even:  
      apply (auto dest: Image_closed_trancl)
      
      So we try our luck just with the auto:
      *)
      apply (auto dest: Image_closed_trancl) (* Yay! *)
      done
  qed  

  
  subsubsection \<open>Correctness lemma\<close>
  
  (* We try the same structure as for the soundness lemma *)
  lemma dfs_correct: "wp (dfs g v\<^sub>0) (\<lambda>V. V = (sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
    unfolding dfs_def
    apply (intro wp_intro wp_while[where I="dfs_invar" and V="dfs_var"])
    by (auto simp:
      dfs_var_wf dfs_invar_init dfs_invar_pres_newnode dfs_invar_pres_exnode dfs_invar_final
    ) (* Works!, no more cleaning up required *)

  lemmas [wp_intro] = wp_cons[OF dfs_correct]
      
    
end  

  subsection \<open>Conclusion\<close>  
    
  (* At this point, we have proved correct the DFS algorithm. 
  
    We have set up the proof in a systematic way, defining constants and using auxiliary lemmas
    to introduce abstraction barriers and keep the different steps of the proof separate.
    
    We also split the correctness lemma in two parts.
  

    This approach, together with splitting the algorithm into smaller functions that are proved 
    correct separately, actually scales to quite complex algorithms. 
    Of course, the proofs of the invariant lemmas will get more complicated, 
    requiring more structuring and auxiliary lemmas.
    
    
    For even larger algorithms, it makes sense to use 
    locales (named contexts) for the ADTs and the invariants (see Documentation/Isabelle Tutorials/locales).
  *)  
    
    
  
  
  
end
