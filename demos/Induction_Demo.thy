theory Induction_Demo
imports Main
begin

subsection \<open> Generalization \<close>
  
fun itrev :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"itrev [] ys = ys" |
"itrev (x#xs) ys = itrev xs (x#ys)"


lemma "itrev xs [] = rev xs" 
  apply (induction xs)
  apply auto
  (* Stuck here! Generalize!*)
  oops
  
lemma "itrev xs ys = rev xs @ ys" 
  apply (induction xs arbitrary: ys)
  apply auto
  done


(**
  Back to lecture
*)  
  



































  
  
  
  
  
  
  
  

subsection\<open> Computation Induction \<close>

fun sep :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"sep a [] = []" |
"sep a [x] = [x]" |
"sep a (x#y#zs) = x # a # sep a (y#zs)"

lemma "map f (sep a xs) = sep (f a) (map f xs)"
  apply (induction a xs rule: sep.induct)
  apply auto
  done

  
(* Common mistake: choosing the 'wrong' induction rule *)  

lemma "map f (sep a xs) = sep (f a) (map f xs)"
  apply (induction xs)
  apply auto
  (* And we are stuck, as there is no rule to rewrite sep a (aa # xs) *)
  oops
  
lemma "map f (sep a xs) = sep (f a) (map f xs)"
  apply (induction xs)
  subgoal by simp
  subgoal for x xs  (* Often, we can 'make up' for the wrong induction rule by manual case splitting.  *)
    apply (cases xs)
    by auto
  done
  
  (* Can get complicated for larger examples.
  
     \<^bold>A\<^bold>p\<^bold>p\<^bold>r\<^bold>o\<^bold>p\<^bold>r\<^bold>i\<^bold>a\<^bold>t\<^bold>e \<^bold>i\<^bold>n\<^bold>d\<^bold>u\<^bold>c\<^bold>t\<^bold>i\<^bold>o\<^bold>n \<^bold>r\<^bold>u\<^bold>l\<^bold>e\<^bold>s \<^bold>\<Longrightarrow> \<^bold>s\<^bold>i\<^bold>m\<^bold>p\<^bold>l\<^bold>e\<^bold>r \<^bold>p\<^bold>r\<^bold>o\<^bold>o\<^bold>f\<^bold>s\<^bold>!
  *)
  
  
  
  
  
  
  
  
  
  
  
(**
  Back to lecture
*)  
  







  
  
  
  
  
  
  
  
  
  
  
  
  
  
subsection \<open> Auxiliary Lemmas \<close>
  
hide_const rev  (* Get predefined rev of standard lib out of way! *)  
fun rev where 
  "rev [] = []"  
| "rev (x#xs) = rev xs @ [x]"  
  

lemma rev_append: "rev (xs@ys) = rev ys @ rev xs"
  apply (induction xs)
  by auto

lemma "rev (rev xs) = xs"  
  apply (induction xs)
  apply (auto simp: rev_append)
  done
  (* We get stuck here! Auxiliary lemma needed! *)
  
  
  
  
  
end
