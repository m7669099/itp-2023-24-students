theory Induction_Examples
imports Main
begin  
  
(* When to use what induction? 
  \<^item> structural induction + arbitrary: 
    \<^item> function is defined over the structure of a datatype
    \<^item> some other variables change, but are not pattern-matched
  \<^item> computation induction: 
    \<^item> non-structural recursion (e.g. quicksort)
    \<^item> multiple pattern-matched variables (you can get away with case-distinctions, but less nice)

  \<^item> rule induction:   
    \<^item> inductively defined predicate in assumptions
    
*)  


context fixes E :: "'a rel" begin
  (* Example: structural recursion, but other argument changes, too *)

  fun path where
    "path u [] v \<longleftrightarrow> u=v"
  | "path u (x#xs) v \<longleftrightarrow> (u,x)\<in>E \<and> path x xs v"    


  lemma "path u xs v \<Longrightarrow> set xs \<subseteq> Range E"
    apply (induction xs)
    apply auto
    oops (* Stuck here! Try to understand why! *)
    
  (* Compare the following two proofs: *)  
  lemma "path u xs v \<Longrightarrow> set xs \<subseteq> Range E"
    apply (induction xs arbitrary: u)
    by auto
  
  lemma "path u xs v \<Longrightarrow> set xs \<subseteq> Range E"
    apply (induction u xs v rule: path.induct) (* Disadvantage: v also needs to be a variable. *)
    by auto
    
end

  
(* Example: non-structural recursion *)

fun qs :: "'a::linorder list \<Rightarrow> 'a list" where
  "qs [] = []"
| "qs (p#xs) = qs (filter ((<)p) xs) @ p # qs (filter ((\<ge>)p) xs)"

lemma "set (qs xs) = set xs"
  (* Naive attempt: induction on xs *)
  apply (induction xs)
  apply simp_all
  (* stuck here: IH allows us to reason about "set (qs xs)", but recursion expands to "set (qs (filter \<dots> xs))" *)
  oops

lemma "set (qs xs) = set xs"
  (* Better: computation induction on qs *)
  apply (induction xs rule: qs.induct)
  apply simp_all
  by fastforce
      

(* Example: multiple pattern-matched variables *)  

fun zip :: "'a list \<Rightarrow> 'b list \<Rightarrow> ('a\<times>'b) list" where
  "zip [] ys = []"
| "zip xs [] = []"  
| "zip (x#xs) (y#ys) = (x,y) # zip xs ys"

lemma "length (zip xs ys) = min (length xs) (length ys)"  
  (* Easy approach: computation induction *)
  apply (induction xs ys rule: zip.induct) 
  (* Try to understand the subgoals here! And see how they nicely match the function definition: 
    You'll have an IH for exactly the recursive call that the "zip (x # xs) (y # ys)" in the last subgoal unfold to!
  *)
  apply auto
  done

lemma "length (zip xs ys) = min (length xs) (length ys)"  
  (* More complicated: induction on xs, case distinction on ys *)
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case by simp
next
  case (Cons a xs)
  then show ?case 
    apply simp
    thm zip.simps
    (* Note: There is no rule to unfold zip (a # xs) ys. 
      But there are rules for "zip (_#_) []" and "zip (_#_) (_#_)"
      So you can do a case-distinction on the shape of ys. 
      
      Note that ys must be arbitrary for this to work. Try to understand why!
    *)
    apply (cases ys)
    apply simp_all
    done
  
  
qed
  
(* While computation induction is simpler, sometimes it get's more complicated.
  Look at the following subgoal, for example.
*)
lemma "set xs \<subseteq> Q \<Longrightarrow> set (zip xs (filter (\<lambda>x. x\<in>Q) ys)) \<subseteq> Q\<times>Q"
  (* We can prove that directly by induction on xs. *)
proof (induction xs arbitrary: ys)
  case Nil
  then show ?case by auto
next
  case (Cons a xs)
  then show ?case 
    apply clarsimp
    (* To progress from here, we need to unfold "zip (a # xs) (filter (\<lambda>x. x \<in> Q) ys)".
      I.e., we need a case distinction on "filter (\<lambda>x. x \<in> Q) ys"
    *)
    apply (cases "filter (\<lambda>x. x \<in> Q) ys")
    apply simp
    apply clarsimp
    (* At this point, we have to express 'list' as a filter again, to apply the IH! 
      Luckily, we have:
    *)
    
    find_theorems "filter _ _ = _#_"
    apply (drule filter_eq_ConsD)
    by auto
  
  
qed  
  

(* However, in most cases, if you see complicated patterns like this, there is an easier proof by
  understanding and generalizing the lemma. 
  
  This will replace one big and complicated proof by several easy proofs of lemmas 
  that may even be re-usable!
  
  In this case, there are several ways to reason about the set of a zip (disregarding the filter).
  One approach might be to realize that:
*)
lemma zip_subset: "set (zip xs ys) \<subseteq> set xs \<times> set ys" by (induction xs ys rule: zip.induct) auto

(* And this makes the main proof straightforward. Here an Isar-version to explain what happens: *)
lemma "set xs \<subseteq> Q \<Longrightarrow> set (zip xs (filter (\<lambda>x. x\<in>Q) ys)) \<subseteq> Q\<times>Q"
proof -
  assume A: "set xs \<subseteq> Q"

  have "set (zip xs (filter (\<lambda>x. x\<in>Q) ys)) \<subseteq> set xs \<times> set (filter (\<lambda>x. x\<in>Q) ys)"
    by (rule zip_subset)
  also have "\<dots> \<subseteq> Q \<times> Q" using A by auto 
    (* Note: that used a lemma about "set (filter _ _)" from the standard library *)
    find_theorems "set (filter _ _) = _"
  finally show ?thesis .
qed

(* Obviously, there are several ways to do the proof as a one-liner now \<dots> but these obfuscate the idea of the proof.*)
lemma "set xs \<subseteq> Q \<Longrightarrow> set (zip xs (filter (\<lambda>x. x\<in>Q) ys)) \<subseteq> Q\<times>Q"
  (* Sledgehammer finds *)
  by (smt (verit) Sigma_mono filter_set member_filter order_trans subsetI zip_subset)
  
lemma "set xs \<subseteq> Q \<Longrightarrow> set (zip xs (filter (\<lambda>x. x\<in>Q) ys)) \<subseteq> Q\<times>Q"
  (* Straightforward, manually found proof *)
  using zip_subset by fastforce

lemma "set xs \<subseteq> Q \<Longrightarrow> set (zip xs (filter (\<lambda>x. x\<in>Q) ys)) \<subseteq> Q\<times>Q"
  (* Trying to at least hint at what happens *)
  apply (rule order_trans[OF zip_subset])
  by auto
    

(* Rule induction *)  

inductive multiple :: "nat set \<Rightarrow> nat \<Rightarrow> bool" for F where
  base: "f\<in>F \<Longrightarrow> multiple F f"
| step: "f\<^sub>1\<in>F \<Longrightarrow> multiple F f\<^sub>2 \<Longrightarrow> multiple F (f\<^sub>1*f\<^sub>2)"
  
lemma "multiple F a \<Longrightarrow> \<exists>x\<in>F. a\<ge>x"  (* Inductively defined predicate in assumptions *)
proof (induction rule: multiple.induct)
  case (base f)
  then show ?case by auto
next
  case (step f\<^sub>1 f\<^sub>2)
  
  from step.IH obtain x where "x\<in>F" "x\<le>f\<^sub>2" by blast
  
  show ?case
  proof (cases f\<^sub>1)
    case 0 
    with \<open>f\<^sub>1\<in>F\<close> show ?thesis by blast
  next
    case (Suc n) 
    with \<open>x\<le>f\<^sub>2\<close> have "x\<le>f\<^sub>1*f\<^sub>2" by simp
    with \<open>x\<in>F\<close> show ?thesis ..
  qed
qed


end
  
  
  
  
  
  
  