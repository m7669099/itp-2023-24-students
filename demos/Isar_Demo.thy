theory Isar_Demo
imports Complex_Main
begin

section\<open>An introductory example\<close>


lemma "\<not> surj(f :: 'a \<Rightarrow> 'a set)"
proof (rule notI)
  assume 0: "surj f"
  from 0 have 1: "\<forall>A. \<exists>a. A = f a" by(simp add: surj_def)
  from 1 have 2: "\<exists>a. {x. x \<notin> f x} = f a" by blast
  from 2 show "False" by blast
qed

text\<open>A bit shorter:\<close>

lemma "\<not> surj(f :: 'a \<Rightarrow> 'a set)"
proof
  assume 0: "surj f"
  from 0 have 1: "\<exists>a. {x. x \<notin> f x} = f a" by(auto simp: surj_def)
  from 1 show "False" by blast
qed





lemma "\<not> surj(f :: 'a \<Rightarrow> 'a set)"
proof (rule notI)
  assume "surj f"
  hence "\<forall>A. \<exists>a. A = f a" by(simp add: surj_def)
  hence "\<exists>a. {x. x \<notin> f x} = f a" by blast
  thus "False" by blast
qed



(** BACK TO LECTURE *)


























subsection\<open>Abbreviations\<close>

text\<open>Avoid labels, use this:\<close>

lemma "\<not> surj(f :: 'a \<Rightarrow> 'a set)"
proof
  assume "surj f"
  from this have "\<exists>a. {x. x \<notin> f x} = f a" by(auto simp: surj_def)
  from this show "False" by blast
qed

text\<open>then = from this\<close>

lemma "\<not> surj(f :: 'a \<Rightarrow> 'a set)"
proof
  assume "surj f"
  then have "\<exists>a. {x. x \<notin> f x} = f a" by(auto simp: surj_def)
  then show "False" by blast
qed

text\<open>hence = then have, thus = then show\<close>
lemma "\<not> surj(f :: 'a \<Rightarrow> 'a set)"
proof (rule notI)
  assume "surj f"
  hence "\<exists>a. {x. x \<notin> f x} = f a" by(auto simp: surj_def)
  thus "False" by blast
qed


subsection\<open>Structured statements: fixes, assumes, shows\<close>

lemma cantor_nth_proof: 
  fixes f :: "'a \<Rightarrow> 'a set"
  assumes s: "surj f"
  shows "False"
proof -  \<comment> \<open>no automatic proof step!\<close>
  from s have "\<exists> a. {x. x \<notin> f x} = f a" 
    by(auto simp: surj_def)
  thus "False" by blast
qed









(** BACK TO LECTURE *)



































section\<open>Proof patterns\<close>

thm iffI

lemma "P \<longleftrightarrow> Q"
proof
  assume "P"
  show "Q" sorry
next
  assume "Q"
  show "P" sorry
qed

  
thm equalityI  
  
lemma "A = (B::'a set)"
proof
  show "A \<subseteq> B" sorry
next
  show "B \<subseteq> A" sorry
qed

thm subsetI
  
lemma "A \<subseteq> B"
proof
  fix a
  assume "a \<in> A"
  show "a \<in> B" sorry
qed

(* Sometimes, you want to combine equalityI and subsetI *)  

lemma "A = (B::'a set)"
proof (intro equalityI subsetI)
  fix x assume "x\<in>A" show "x\<in>B" sorry
next
  fix x assume "x\<in>B" show "x\<in>A" sorry
qed
  
lemma "{x. \<exists>y. x=y+y} = {x::nat. even x}"
proof (intro equalityI subsetI)
  fix x::nat assume "x\<in>{x. \<exists>y. x=y+y}" 
  hence "\<exists>y. x=y+y" by simp
  thus "x\<in>{x. even x}" by auto
next
  fix x::nat assume "x\<in>{x. even x}" thus "x\<in>{x. \<exists>y. x=y+y}" sorry
qed
  
  
lemma "A = (B::'a set)"
proof
  show "A \<subseteq> B" proof
    fix x assume "x \<in> A" show "x\<in>B" sorry
  qed      
next
  show "B \<subseteq> A" proof
    fix x assume "x \<in> B" show "x\<in>A" sorry
  qed      
qed


  
  
text\<open>Contradiction\<close>

lemma P 
proof (rule ccontr)
  assume "\<not>P"
  show "False" sorry
qed

text\<open>Case distinction\<close>

lemma "R"
proof cases
  assume "P"
  show "R" sorry
next
  assume "\<not> P"
  show "R" sorry
qed

  
lemma "R"
proof (cases "P::bool")
  case True
  then show ?thesis sorry
next
  case False
  then show ?thesis sorry
qed

lemma "R"
proof (cases "P::bool")
  assume "P"
  show "R" sorry
next
  assume "\<not> P"
  show "R" sorry
qed
  
  
lemma "R"
proof -
  have "P \<or> Q" sorry
  then show "R"
  proof
    assume "P"
    show "R" sorry
  next
    assume "Q"
    show "R" sorry
  qed
qed

lemma X
proof -
  fix n :: nat
  consider 
    (LESS) "n<0" 
  | (EQ) "n=0" 
  | (GREATER) "n>0" 
    by blast
  then show X proof cases
    print_cases
oops

lemma X
proof -
  fix xs :: "nat list"
  consider 
    (empty) "xs=[]"
  | (single) x where "xs=[x]"
  | (multi) x y xs' where "xs=x#y#xs'"
    by (meson remdups_adj.cases)
  then show X proof cases
    case empty
  then show ?thesis sorry
  next
    case single
  then show ?thesis sorry
  next
    case multi
    then show ?thesis sorry
  qed
  oops 


text \<open>\<open>\<forall>\<close> introduction\<close>
  
lemma "\<forall>x. x < Suc x"
proof
  fix x
  show "x<Suc x" by simp
qed      

text \<open>\<open>\<exists>\<close> elimination\<close>  
  
lemma "\<exists>x. Suc x < 3"  
proof
  show "Suc 0 < 3" by simp
qed  
  
text\<open>obtain example\<close>

lemma "\<not> surj(f :: 'a \<Rightarrow> 'a set)"
proof
  assume "surj f"
  hence "\<exists>a. {x. x \<notin> f x} = f a" by(auto simp: surj_def)
  then obtain a where "{x. x \<notin> f x} = f a" by blast
  hence "a \<notin> f a \<longleftrightarrow> a \<in> f a" by blast
  thus "False" by blast
qed

lemma foo
proof -
  fix xs :: "nat list"
  assume "xs\<noteq>[]"
  then obtain x xs' where "xs=x#xs'" by (cases xs) simp
  oops



text\<open>Interactive exercise:\<close>

lemma assumes A: "\<exists>x. \<forall>y. P x y" shows "\<forall>y. \<exists>x. P x y"
proof
  oops

  
(* Witness for existential can only contain variables that were 
  fixed BEFORE exI rule was applied! 
  
  Try starting proof above with 
    proof (intro allI exI)
  
*)
  
  




(** BACK TO LECTURE *)


























section\<open>Streamlining proofs\<close>


subsection\<open>Pattern matching and ?-variables\<close>

text\<open>Show EX\<close>

lemma "\<exists> xs. length xs = 0" (is "\<exists> xs. ?P xs")
proof
  show "?P []" by simp
qed

text\<open>Multiple EX\<close>

lemma "\<exists> x y :: int. x < z \<and> z < y" (is "\<exists> x y. ?P x y")
proof (intro exI)
  show "?P (z - 1) (z + 1)" by arith
qed

(* Forward proof *)
lemma "\<exists> x y :: int. x < z & z < y" (is "\<exists> x y. ?P x y")
proof -
  have "?P (z - 1) (z + 1)" by arith
  thus ?thesis by blast
qed


subsection\<open>Quoting facts:\<close>

lemma assumes "x < (0::int)" shows "x*x > 0"
proof -
  thm \<open>x<0\<close>
  from \<open>x<0\<close> show ?thesis by(metis mult_neg_neg)
qed





(** BACK TO LECTURE *)



























subsection \<open>Example: Top Down Proof Development\<close>

lemma "(\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs) \<or>
  (\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs + 1)"
  (is \<open>(\<exists>ys zs. ?EV ys zs) \<or> (\<exists>ys zs. ?ODD ys zs)\<close>)
  oops
  
  

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
(* SOLUTION *)  
  

























(*  
proof cases
  assume "even (length xs)"
  then obtain k where "length xs = k+k"    
    by (metis add_self_div_2 div_add)
  hence "?EV (take k xs) (drop k xs)" by auto  
  thus ?thesis by blast
next
  assume "odd (length xs)"
  then obtain k where "length xs = k + 1 + k" 
    by (metis add.commute left_add_twice oddE)
  hence "(?ODD (take (k+1) xs) (drop (k+1) xs))" 
    by auto
  thus ?thesis by blast
qed  

*)  
  
  
  
  
  
  
  
  
    
  
  



(** BACK TO LECTURE *)
  
  
  
  
  
  
  
  
  
  
subsection \<open>moreover - ultimately\<close>

lemma assumes "A \<and> B" shows "B \<and> A"
proof -
  from \<open>A \<and> B\<close> have "A" by auto
  moreover
  from \<open>A \<and> B\<close> have "B" by auto
  ultimately show "B \<and> A" by auto
qed


(* also - finally *)
lemma fixes x y :: real assumes "x \<ge> y" "y > 0"
shows "(x - y) ^ 2 \<le> x^2 - y^2"
proof -
  have "(x - y) ^ 2 = x^2 + y^2 - 2*x*y"
    by(simp add: numeral_eq_Suc algebra_simps)
  also have "\<dots> \<le> x^2 + y^2 - 2*y*y"
    using assms by(simp)
  also have "\<dots> = x^2 - y^2"
    by(simp add: numeral_eq_Suc)
  finally show ?thesis .
qed


subsection\<open>Raw proof blocks\<close>

lemma fixes k :: int assumes "k dvd (n+k)" shows "k dvd n"
proof -
  { fix a assume a: "n+k = k*a"
    have "EX b. n = k*b"
    proof
      show "n = k*(a - 1)" using a by(simp add: algebra_simps)
    qed 
  } note aux=this  
    
  thm aux
    
  with assms show ?thesis by (auto simp add: dvd_def)
qed

subsection\<open>Local Lemmas. Shortcut for raw proof block with name\<close>

lemma fixes k :: int assumes "k dvd (n+k)" shows "k dvd n"
proof -
  have AUX: "EX b. n = k*b" if a: "n+k = k*a" for a
  proof
    show "n = k*(a - 1)" using a by(simp add: algebra_simps)
  qed 
  with assms show ?thesis by (auto simp add: dvd_def)

  thm AUX (* Local lemma also available by its name *)
qed
















(** BACK TO LECTURE *)














section\<open>Solutions to interactive exercises\<close>

lemma assumes "\<exists>x. \<forall>y. P x y" shows "\<forall>y. \<exists>x. P x y"
proof
  fix b
  from assms obtain a where 0: "\<forall>y. P a y" by blast
  show "\<exists>x. P x b"
  proof
    show "P a b" using 0 by blast
  qed
qed

lemma fixes x y :: real assumes "x \<ge> y" "y > 0"
shows "(x - y) ^ 2 \<le> x^2 - y^2"
proof -
  have "(x - y) ^ 2 = x^2 + y^2 - 2*x*y"
    by(simp add: numeral_eq_Suc algebra_simps)
  also have "\<dots> \<le> x^2 + y^2 - 2*y*y"
    using assms by(simp)
  also have "\<dots> = x^2 - y^2"
    by(simp add: numeral_eq_Suc)
  finally show ?thesis .
qed

subsection \<open>Example: Top Down Proof Development\<close>

text\<open>The key idea: case distinction on length:\<close>

lemma "(\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs) \<or>
  (\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs + 1)"
proof cases
  assume "EX n. length xs = n+n"
  show ?thesis sorry
next
  assume "\<not> (EX n. length xs = n+n)"
  show ?thesis sorry
qed

text\<open>A proof skeleton:\<close>

lemma "(\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs) \<or>
  (\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs + 1)"
proof cases
  assume "\<exists>n. length xs = n+n"
  then obtain n where "length xs = n+n" by blast
  let ?ys = "take n xs"
  let ?zs = "take n (drop n xs)"
  have "xs = ?ys @ ?zs \<and> length ?ys = length ?zs" sorry
  thus ?thesis by blast
next
  assume "\<not> (\<exists>n. length xs = n+n)"
  then obtain n where "length xs = Suc(n+n)" sorry
  let ?ys = "take (Suc n) xs"
  let ?zs = "take n (drop (Suc n) xs)"
  have "xs = ?ys @ ?zs \<and> length ?ys = length ?zs + 1" sorry
  then show ?thesis by blast
qed

text\<open>The complete proof:\<close>

lemma "(\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs) \<or>
  (\<exists>ys zs. xs = ys @ zs \<and> length ys = length zs + 1)"
proof cases
  assume "\<exists>n. length xs = n+n"
  then obtain n where "length xs = n+n" by blast
  let ?ys = "take n xs"
  let ?zs = "take n (drop n xs)"
  have "xs = ?ys @ ?zs \<and> length ?ys = length ?zs"
    by (simp add: \<open>length xs = n + n\<close>)
  thus ?thesis by blast
next
  assume "\<not> (\<exists>n. length xs = n+n)"
  hence "\<exists>n. length xs = Suc(n+n)" by arith
  then obtain n where l: "length xs = Suc(n+n)" by blast
  let ?ys = "take (Suc n) xs"
  let ?zs = "take n (drop (Suc n) xs)"
  have "xs = ?ys @ ?zs \<and> length ?ys = length ?zs + 1" by (simp add: l)
  thus ?thesis by blast
qed

end
