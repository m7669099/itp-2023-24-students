chapter \<open>Nres Monad\<close> 
theory Nres_Monad_Tools
imports 
  Main
  "HOL-Library.Monad_Syntax" (* To get the do-notation in Isabelle *)
  "HOL-Library.Complete_Partial_Order2"
begin



datatype 'a nres = FAIL | RES (the_res: "'a set")

definition "join_nres S \<equiv> if FAIL\<in>S then FAIL else RES {x. \<exists>X. RES X \<in> S \<and> x\<in>X}"

fun bind_nres where
  "bind_nres FAIL f = FAIL"
| "bind_nres (RES S) f = join_nres (f`S)"  

adhoc_overloading bind bind_nres

definition "return x \<equiv> RES {x}"

definition "spec P \<equiv> RES {x. P x}"


subsection \<open>Pointwise Reasoning\<close>

fun is_res where
  "is_res FAIL x = False"
| "is_res (RES S) x \<longleftrightarrow> x\<in>S"


named_theorems pw_intro

lemma pw_eqI[pw_intro]: "\<lbrakk> a=FAIL \<longleftrightarrow> b=FAIL; \<And>x. is_res a x = is_res b x \<rbrakk> \<Longrightarrow> a=b"
  apply (cases a; cases b) by auto

lemma is_res_return[simp]: "is_res (return x) y \<longleftrightarrow> x=y"  
  unfolding return_def by auto
  
lemma is_fail_return[simp]: "return x \<noteq> FAIL"  
  unfolding return_def by auto

lemma is_res_spec[simp]: "is_res (spec P) x \<longleftrightarrow> P x"  
  unfolding spec_def by auto

lemma is_fail_spec[simp]: "spec P \<noteq> FAIL"  
  unfolding spec_def by auto
    
    
lemma is_res_join[simp]: "is_res (join_nres S) x \<longleftrightarrow> (\<forall>m\<in>S. m\<noteq>FAIL) \<and> (\<exists>m\<in>S. is_res m x)"
  unfolding join_nres_def
  apply (rule iffI)
  subgoal by (force split: if_splits) 
  apply clarsimp
  subgoal for m
    apply (cases m)
    by auto
  done

lemma is_fail_join[simp]: "join_nres S = FAIL \<longleftrightarrow> FAIL\<in>S"  
  unfolding join_nres_def by auto
  
lemma is_res_bind[simp]: "is_res (do {x\<leftarrow>m; f x}) a 
  \<longleftrightarrow> (\<forall>x. is_res m x \<longrightarrow> f x \<noteq> FAIL) \<and> (\<exists>x. is_res m x \<and> is_res (f x) a)"
  apply (cases m)
  by auto
  
  
lemma is_fail_bind[simp]: "do {x\<leftarrow>m; f x} = FAIL \<longleftrightarrow> m=FAIL \<or> (\<exists>x. is_res m x \<and> f x = FAIL)"   
  apply (cases m; simp)
  subgoal for S
    apply (rule iffI)
    subgoal by force
    apply clarsimp
    subgoal for x
      apply (cases "f x")
      apply force
      apply force
      done
    done
  done  
  

subsection \<open>Monad Laws\<close>
    
lemma return_bind_nres[simp]: "do { x\<leftarrow>return a; f x } = f a"
  apply (rule pw_intro)
  by auto

lemma bind_return_nres[simp]: "do { x\<leftarrow>m; return x } = m"  
  apply (rule pw_intro)
  by auto

lemma bind_assoc_nres[simp]: "do {y\<leftarrow>do { x\<leftarrow>m; f x }; g y} = do { x\<leftarrow>m; y \<leftarrow> f x; g y }" for m :: "_ nres"
  apply (rule pw_intro)
  by auto

  
section \<open>Assertions\<close> 
  
(* Assert. Return () if condition is true, Fail otherwise. *)
  
definition "assert P \<equiv> if P then return () else FAIL"

lemma assert_simps[simp]:
  "assert False = FAIL"
  "assert True = return ()"
  by (auto simp: assert_def)

lemma is_res_assert[simp]: "is_res (assert P) x \<longleftrightarrow> P"  
  unfolding assert_def by auto
  
lemma is_fail_assert[simp]: "assert P = FAIL \<longleftrightarrow> \<not>P"  
  unfolding assert_def by auto

  
(** Back to lecture *)
  
  
  
  
  
  
    
section \<open>Weakest Precondition\<close>  
  
  fun wp :: "'a nres \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "wp FAIL Q = False"
  | "wp (RES S) Q = (\<forall>x\<in>S. Q x)"  

  lemma wp_alt: "wp m Q \<longleftrightarrow> m\<noteq>FAIL \<and> (\<forall>x. is_res m x \<longrightarrow> Q x)"
    apply (cases m)
    by auto

  lemma wp_return[simp]: "wp (return x) Q \<longleftrightarrow> Q x"  
    unfolding wp_alt
    by auto

  lemma wp_spec[simp]: "wp (spec P) Q \<longleftrightarrow> (\<forall>x. P x \<longrightarrow> Q x)"      
    unfolding wp_alt
    by auto
        
  lemma wp_bind[simp]: "wp (do { x\<leftarrow>m; f x }) Q = wp m (\<lambda>x. wp (f x) Q)"
    unfolding wp_alt
    by auto
  
  
  subsection \<open>Intro-Rules\<close>  
    
  named_theorems wp_intro
  
  lemma wp_returnI[wp_intro]: "Q a \<Longrightarrow> wp (return a) Q" unfolding wp_alt by auto
  lemma wp_specI[wp_intro]: "\<lbrakk>\<And>x. P x \<Longrightarrow> Q x\<rbrakk> \<Longrightarrow> wp (spec P) Q" unfolding wp_alt by auto
  lemma wp_bindI[wp_intro]: "wp m (\<lambda>x. wp (f x) Q) \<Longrightarrow> wp (do { x\<leftarrow>m; f x }) Q" by simp

  lemma wp_letI[wp_intro]: "wp (f v) Q \<Longrightarrow> wp (let x=v in f x) Q" by simp
  
  lemma wp_assertI[wp_intro]: "\<lbrakk>P; Q ()\<rbrakk> \<Longrightarrow> wp (assert P) Q" unfolding wp_alt by auto
  
  (* Consequence rule: Weakening post-condition 
  
    If c ensures Q, and Q \<Longrightarrow> Q', then c ensures Q'
  *)
  lemma wp_cons: 
    assumes "wp c Q"
    assumes "\<And>s. Q s \<Longrightarrow> Q' s"
    shows "wp c Q'"
    using assms by (cases c) auto
  
    
  (* Plus case-splitting rules for better usability  *)
  
  lemma wp_case_prodI[wp_intro]:
    assumes "\<And>a b. \<lbrakk> p=(a,b) \<rbrakk> \<Longrightarrow> wp (f a b) Q"
    shows "wp (case p of (a,b) \<Rightarrow> f a b) Q"
    using assms by (cases p) auto
  

  lemma wp_if[wp_intro]:
    assumes "b \<Longrightarrow> wp c Q"
    assumes "\<not>b \<Longrightarrow> wp d Q"
    shows "wp (if b then c else d) Q"
    using assms by auto

    
(** Back to lecture *)

    
    
    
    

  (* If rule with monadic condition *)  
  definition IfM :: "bool nres \<Rightarrow> 'a nres \<Rightarrow> 'a nres \<Rightarrow> 'a nres" ("(if\<^sub>M (_)/ then (_)/ else (_))" [0, 0, 10] 10)
    where "if\<^sub>M b then c else d \<equiv> do { bb\<leftarrow>b; if bb then c else d }"
  
  lemma wp_ifM[wp_intro]:
    assumes "wp b (\<lambda>bb. wp (if bb then c else d) Q)"
    shows "wp (if\<^sub>M b then c else d) Q"
    using assms unfolding IfM_def
    apply (intro wp_intro)
    by simp
    
    
(** Back to lecture *)


    
    
        
subsection \<open>Partial-Function Setup\<close>
(* Details are beyond material covered in this course *)

interpretation nres:
  partial_function_definitions "flat_ord FAIL" "flat_lub FAIL"
  rewrites "flat_lub FAIL {} \<equiv> FAIL"
  by (rule flat_interpretation)(simp add: flat_lub_def)

abbreviation "nres_ord \<equiv> flat_ord FAIL"
abbreviation "mono_nres \<equiv> monotone (fun_ord nres_ord) nres_ord"


lemma nres_ord_pw_iff: 
  "nres_ord a b \<longleftrightarrow> a=FAIL \<or> (a\<noteq>FAIL \<and> b\<noteq>FAIL \<and> (\<forall>x. is_res a x = is_res b x))"
  apply (cases a; cases b)
  by (auto simp: flat_ord_def)

lemma nres_ord_pwI[pw_intro]: 
  "a=FAIL \<or> (a\<noteq>FAIL \<and> b\<noteq>FAIL \<and> (\<forall>x. is_res a x = is_res b x)) \<Longrightarrow> nres_ord a b"
  apply (cases a; cases b)
  by (auto simp: flat_ord_def)


lemma bind_mono[partial_function_mono]:
assumes mf: "mono_nres B" and mg: "\<And>y. mono_nres (\<lambda>f. C y f)"
shows "mono_nres (\<lambda>f. bind_nres (B f) (\<lambda>y. C y f))"
  using assms
  unfolding monotone_def
  apply (simp add: nres_ord_pw_iff)
  by meson

declaration \<open>Partial_Function.init "nres" \<^term>\<open>nres.fixp_fun\<close>
  \<^term>\<open>nres.mono_body\<close> @{thm nres.fixp_rule_uc} @{thm nres.fixp_induct_uc}
  (NONE)\<close>
  
  
lemma ifM_mono[partial_function_mono]: 
  "\<lbrakk>mono_nres C; mono_nres F; mono_nres G\<rbrakk>
    \<Longrightarrow> mono_nres (\<lambda>f. if\<^sub>M C f then F f else G f)"
  unfolding IfM_def
  by (rule partial_function_mono | assumption)+

subsection \<open>While Loops\<close>  

  context
    fixes b :: "'s \<Rightarrow> bool nres"
    fixes c :: "'s \<Rightarrow> 's nres"
  begin
  
    partial_function (nres) while :: "'s \<Rightarrow> 's nres" where
      "while s = (
        if\<^sub>M b s then do {
          s \<leftarrow> c s;
          while s
        } else return s)" 


    (* We combine the step and consequence rule. 
      This is required as condition may be (nondet) true and false for the same state.
    *)        
    lemma wp_while:
      assumes WF: "wf V"
      assumes I0: "I s"
      assumes STEP: "\<And>s. \<lbrakk> I s \<rbrakk> \<Longrightarrow> wp (b s)
        (\<lambda>bb. 
          (bb \<longrightarrow> wp (c s) (\<lambda>s'. I s' \<and> (s',s)\<in>V))
        \<and> (\<not>bb \<longrightarrow> Q s)  
        )"
      shows "wp (while s) Q"
      using WF I0
    proof (induction s rule: wf_induct_rule)
      case (less s)
      
      note \<open>I s\<close> \<comment> \<open>Invariant holds for s\<close>
      
      show ?case
        apply (subst while.simps) 
        apply (intro wp_intro wp_cons[OF STEP] \<open>I s\<close>)
        subgoal
          apply clarsimp
          apply (erule wp_cons)
          by (rule less.IH; auto)
        subgoal
          by clarsimp
        done
        
    qed
        
          
  end


  lemmas [wp_intro] = impI conjI (* To continue after wp_while rule *)  

  
(** Back to lecture *)  







  
  
  
      
  
section \<open>Monadic Fold\<close>  
  
thm fold_simps  
  
fun mfold :: "('a \<Rightarrow> 's \<Rightarrow> 's nres) \<Rightarrow> 'a list \<Rightarrow> 's \<Rightarrow> 's nres" where  
  "mfold f [] s = return s" 
| "mfold f (x#xs) s = do { s \<leftarrow> f x s; mfold f xs s }"  

(* Invariant rule for monadic fold *)  
lemma wp_mfold:
  assumes I0: "I [] xs s"
  assumes STEP: "\<And>xs\<^sub>1 x xs\<^sub>2 s. \<lbrakk> xs=xs\<^sub>1@x#xs\<^sub>2; I xs\<^sub>1 (x#xs\<^sub>2) s \<rbrakk> \<Longrightarrow> wp (f x s) (\<lambda>s'. I (xs\<^sub>1@[x]) xs\<^sub>2 s')"
  assumes CONS: "\<And>s. I xs [] s \<Longrightarrow> Q s"
  shows "wp (mfold f xs s) Q"
proof -
  (* Generalization over xs\<^sub>1, the elements already processed. 
    We decided to apply the consequence rule later, so we show postcondition "I xs []".
    We could as well do that within the induction, showing Q directly.
  *)
  have "wp (mfold f xs\<^sub>2 s) (I xs [])" if "xs=xs\<^sub>1@xs\<^sub>2" "I xs\<^sub>1 xs\<^sub>2 s" for xs\<^sub>1 xs\<^sub>2 s
    using that
  proof (induction xs\<^sub>2 arbitrary: xs\<^sub>1 s)
    case Nil
    then show ?case by auto (* Note that "wp (Some x) Q \<longleftrightarrow> \<dots>" is in the simpset *)
  next
    case (Cons x xs\<^sub>2)
    
    (* We instantiate the STEP rule and the IH to our split of xs, 
      and register the resulting rules as wp_intro
    *)
    note [wp_intro] = wp_cons[OF STEP, OF \<open>xs=xs\<^sub>1@x#xs\<^sub>2\<close>]
    
    from Cons.IH[of "xs\<^sub>1@[x]"] Cons.prems 
    have IH: "I (xs\<^sub>1 @ [x]) xs\<^sub>2 s' \<Longrightarrow> wp (mfold f xs\<^sub>2 s') (I xs [])" for s' by auto
    note [wp_intro] = wp_cons[OF IH]
    
    show ?case 
      apply simp (* unfolds mfold definition *)
      apply (intro wp_intro) (* applies rule for f (STEP) and "mfold _ xs\<^sub>2" (IH), 
        which we registered as wp_intro. *)
      using Cons.prems by auto
    
  qed
  from this[of "[]" xs] I0 have "wp (mfold f xs s) (I xs [])" (* specialize to I [] xs *)
    by auto
  from wp_cons[OF this] CONS show ?thesis . (* Apply final consequence rule *)
qed





section \<open>Refinement\<close>


definition refines :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a nres \<Rightarrow> 'b nres \<Rightarrow> bool" 
  where "refines R m m' 
    \<equiv> m'=FAIL \<or> (m\<noteq>FAIL \<and> (\<forall>x. is_res m x \<longrightarrow> (\<exists>x'. R x x' \<and> is_res m' x')))"

  
text \<open>Pattern-matching style definition\<close>
lemma 
  "refines R m FAIL \<longleftrightarrow> True"  
  "refines R (RES S) (RES S') \<longleftrightarrow> (\<forall>x\<in>S. (\<exists>y\<in>S'. R x y))"
  "refines R FAIL (RES S) \<longleftrightarrow> False"
  unfolding refines_def
  by auto
  
  
text \<open>Refinement is reflexive and transitive\<close>  
lemma refines_refl[simp]: "refines (=) m m"  
  and refines_trans[trans]: "refines R\<^sub>1 m\<^sub>1 m\<^sub>2 \<Longrightarrow> refines R\<^sub>2 m\<^sub>2 m\<^sub>3 \<Longrightarrow> refines (R\<^sub>1 OO R\<^sub>2) m\<^sub>1 m\<^sub>3"
  unfolding refines_def
  apply simp_all
  by blast


(** Back to lecture *)  
  
  
    
  
  
  
    
  
subsection \<open>Automation\<close>
text \<open>Whenever a syntactic rule introduces a fresh refinement relation, we wrap it
  into a \<open>rel\<close> constant. This is a purely syntactic marker to allow us to infer a suitable 
  refinement relation in a controlled way.
\<close>
definition rel:: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> bool" where "rel R a b \<equiv> R a b"

named_theorems rel_rule \<open>Theorems to instantiate refinement relations\<close>
  
lemma rel_prod[rel_rule]: "rel A a a' \<Longrightarrow> rel B b b' \<Longrightarrow> rel (rel_prod A B) (a,b) (a',b')"
  unfolding rel_def by simp

text \<open>Use this lemma to declare refinement relations\<close>  
lemma relI: "R a b \<Longrightarrow> rel R a b" by (simp add: rel_def)

text \<open>For example, always use identity if possible 
  (if concrete and abstract type are equal)\<close>
lemmas [rel_rule] = relI[of "(=)"]

    
subsection \<open>Syntactic Rules\<close>  
text \<open>We prove syntactic rules to prove refinement goals\<close>

lemma refines_FAIL[wp_intro]: "refines R m FAIL" unfolding refines_def by auto     
  
lemma refines_return[wp_intro]: "rel R x x' \<Longrightarrow> refines R (return x) (return x')"
  unfolding rel_def refines_def by auto

lemma refines_spec[wp_intro]: "\<lbrakk>\<And>x. P x \<Longrightarrow> \<exists>x'. R x x' \<and> P' x'  \<rbrakk> \<Longrightarrow> refines R (spec P) (spec P')"
  unfolding refines_def by auto

  
  
text \<open>The following rules treat assertions specially, 
  in particular, they do not require a corresponding assertion 
  on the other side of the refinement.\<close>
  
text \<open>As we have defined refinement to trivially hold for the failing abstract program,
  we can assume that an abstract assertion does not fail: \<close>  
lemma refines_assert_bind_right[wp_intro]: 
  assumes "P' \<Longrightarrow> refines R m m'"
  shows "refines R m (do { assert P'; m' })"  
  using assms unfolding refines_def by auto

text \<open>Symmetrically, we have to prove that concrete assertions do not fail:\<close>  
lemma refines_assert_bind_left[wp_intro]: 
  assumes "m'\<noteq>FAIL \<Longrightarrow> P"
  assumes "refines R m m'"
  shows "refines R (do { assert P; m }) (m')"  
  using assms unfolding refines_def by auto
  
text \<open>The rules for the assertions are defined \<^emph>\<open>before\<close> the general bind rule. 
  Thus, intro wp_intro tries them first
  \<close>
lemma refines_bind[wp_intro]:
  assumes "refines Rh m m'"
  assumes "\<And>x x'. \<lbrakk> Rh x x'; is_res m x; is_res m' x' \<rbrakk> \<Longrightarrow> refines R (f x) (f' x')"
  shows "refines R (do {x\<leftarrow>m; f x}) (do {x'\<leftarrow>m'; f' x'})"
  using assms
  unfolding refines_def 
  by fastforce
  
  
  
lemma refines_if[wp_intro]: 
  assumes "rel (=) b b'"
  assumes "b' \<Longrightarrow> refines R m\<^sub>1 m\<^sub>1'"
  assumes "\<not>b' \<Longrightarrow> refines R m\<^sub>2 m\<^sub>2'"
  shows "refines R (if b then m\<^sub>1 else m\<^sub>2) (if b' then m\<^sub>1' else m\<^sub>2')"  
  using assms unfolding rel_def by simp

lemma refines_ifM[wp_intro]: 
  assumes "refines (=) b b'"
  assumes "is_res b' True \<Longrightarrow> refines R m\<^sub>1 m\<^sub>1'"
  assumes "is_res b' False \<Longrightarrow> refines R m\<^sub>2 m\<^sub>2'"
  shows "refines R (if\<^sub>M b then m\<^sub>1 else m\<^sub>2) (if\<^sub>M b' then m\<^sub>1' else m\<^sub>2')"  
  using assms unfolding IfM_def
  apply -
  apply (intro wp_intro | assumption | erule relI)+
  by auto
  
  
lemma refines_let[wp_intro]:
  assumes "\<And>x x'. x=v \<Longrightarrow> x'=v' \<Longrightarrow> refines R (f x) (f' x')"
  shows "refines R (let x=v in f x) (let x'=v' in f' x')"
  using assms unfolding Let_def by simp  
  
lemma refines_case_prod[wp_intro]: 
  assumes "\<And>a b a' b'. \<lbrakk> p=(a,b); p'=(a',b') \<rbrakk> \<Longrightarrow> refines R (f a b) (f' a' b')"
  shows "refines R (case p of (a,b) \<Rightarrow> f a b) (case p' of (a',b') \<Rightarrow> f' a' b')"  
  using assms by (auto split: prod.split)  


  
(* A few aux-lemmas for while-refine proof *)  
lemma fun_lub_apply: "fun_lub L A x = L {f x | f. f\<in>A}"  
  unfolding fun_lub_def
  by meson
  
lemma chain_apply:
  assumes "Complete_Partial_Order.chain (fun_ord le) A"
  shows "Complete_Partial_Order.chain le {f x |f. f \<in> A}"
  using assms
  unfolding Complete_Partial_Order.chain_def fun_ord_def
  by blast

lemma chain_cases:
  assumes "Complete_Partial_Order.chain (flat_ord b) M"
  obtains "M={}" | "M={b}" | x where "x\<noteq>b" "M={x}" | x where "x\<noteq>b" "M={b,x}"
  using assms
  unfolding chain_def flat_ord_def
  by fast
  
lemma pointwise_admissible[cont_intro]: "nres.admissible (\<lambda>F. P x (F x))"
  apply (rule ccpo.admissibleI) 
  subgoal for A
    apply (drule chain_apply[where x=x])
    apply (simp add: fun_lub_apply)
    apply (erule chain_cases)
    apply (simp_all add: flat_lub_def)
    subgoal by (smt (verit) mem_Collect_eq singleton_conv2)
    subgoal by fastforce
    subgoal by auto
    done
  done

lemma refines_while[wp_intro]:
  assumes R: "rel R s s'"
  assumes BREF: "\<And>s s'. R s s' \<Longrightarrow> refines (=) (b s) (b' s')"
  assumes CREF: "\<And>s s'. R s s' \<Longrightarrow> is_res (b' s') True \<Longrightarrow> refines R (c s) (c' s')"
  shows "refines R (while b c s) (while b' c' s')"
  using R[unfolded rel_def]
proof (induction arbitrary: s s' rule: while.fixp_induct)
  case (3 while')
  then show ?case
    apply (subst while.simps)
    apply (rule wp_intro BREF CREF | assumption)+
    by (simp add: rel_def)
  
qed (auto simp: refines_FAIL)
  

lemma refines_mfold[wp_intro]:
  assumes "rel Rs s s'"
  assumes "rel (list_all2 Rx) xs xs'"
  assumes STEP: "\<And>x x' s s'. \<lbrakk> Rx x x'; Rs s s' \<rbrakk> \<Longrightarrow> refines Rs (f x s) (f' x' s')"
  shows "refines Rs (mfold f xs s) (mfold f' xs' s')"
  using assms(2,1)
  unfolding rel_def
  apply (induction arbitrary: s s' rule: list_all2_induct)
  subgoal
    apply simp
    apply (rule wp_intro)
    by (simp add: rel_def)
  subgoal for x xs x' xs' s s'
    apply simp
    apply (rule wp_intro STEP)+
    by auto
  done  




subsubsection \<open>Fallback rules\<close>  
text \<open>Rules for non-perfect matches of combinators\<close>
  
(* When refining a spec with (=), we can fall back to our VCG for wp *)
lemma any_refines_eq_spec[wp_intro]: "wp m Q \<Longrightarrow> refines (=) m (spec Q)"
  unfolding wp_alt refines_def rel_def by auto

(* Special case for return *)  
lemma any_refines_eq_return[wp_intro]: "wp m (\<lambda>r. r=x) \<Longrightarrow> refines (=) m (return x)"
  unfolding wp_alt refines_def rel_def by auto


lemma any_refines_spec_iff: "refines R m (spec Q) \<longleftrightarrow> (wp m (\<lambda>r. \<exists>r'. R r r' \<and> Q r'))"  
  unfolding wp_alt refines_def rel_def by auto

lemma any_refines_spec: "wp m (\<lambda>r. \<exists>r'. rel R r r' \<and> Q r') \<Longrightarrow> refines R m (spec Q)"  
  unfolding wp_alt refines_def rel_def by auto

lemma any_refines_return_iff: "refines R m (return x') \<longleftrightarrow> (wp m (\<lambda>r. R r x'))"  
  unfolding wp_alt refines_def rel_def by auto

lemma any_refines_return[wp_intro]: "wp m (\<lambda>r. rel R r x') \<Longrightarrow> refines R m (return x')"  
  unfolding wp_alt refines_def rel_def by auto

    
lemma return_refines_eq_spec: "P x \<Longrightarrow> refines (=) (return x) (spec P)"
  unfolding refines_def by auto
  
lemma return_refines_spec: "\<exists>x'. R x x' \<and> P x' \<Longrightarrow> refines R (return x) (spec P)"
  unfolding refines_def by auto
    
lemma refines_let_bind[wp_intro]: 
  assumes "refines Rh (return v) m'"
  assumes "\<And>x x'. \<lbrakk>Rh x x'; x=v; is_res m' x'\<rbrakk> \<Longrightarrow> refines R (f x) (f' x')"
  shows "refines R (let x=v in f x) (do {x'\<leftarrow>m'; f' x'})"
  using assms
  unfolding refines_def by fastforce

lemma refines_bind_let[wp_intro]: 
  assumes "refines Rh m (return v')"
  assumes "\<And>x x'. \<lbrakk>Rh x x'; is_res m x; x'=v'\<rbrakk> \<Longrightarrow> refines R (f x) (f' x')"
  shows "refines R (do {x\<leftarrow>m; f x}) (do {let x'=v'; f' x'})"
  using assms
  unfolding refines_def by auto
    
subsection \<open>Refinement Relation from Abstraction Function\<close>

definition "br \<alpha> c a \<equiv> a = \<alpha> c"  
lemma br_refl: "br \<alpha> c (\<alpha> c)" unfolding br_def by auto

lemma brI[intro?]: "a = \<alpha> c \<Longrightarrow> br \<alpha> c a"
  unfolding br_def by auto

lemma brD: "br \<alpha> c a \<Longrightarrow> a = \<alpha> c"
  unfolding br_def by auto


(** Back to lecture *)
















subsection \<open>Refinement and Wp\<close>

lemma wp_refines_conv: "wp m Q \<longleftrightarrow> refines (=) m (spec Q)"
  unfolding refines_def wp_alt
  by auto

text \<open>Transitivity can also be applied with a weakest 
  precondition\<close>  
lemma refines_wp_trans[trans]:
  "refines R\<^sub>1 m\<^sub>1 m\<^sub>2 \<Longrightarrow> wp m\<^sub>2 Q \<Longrightarrow> refines R\<^sub>1 m\<^sub>1 (spec Q)"
  unfolding refines_def wp_alt
  by auto
  
  
text \<open>Once transitivity has been used to show that 
  a concrete implementation refines the abstract 
  specification, a Hoare triple for the concrete 
  implementation can be derived.\<close>  
lemma refines_spec_wpI:
  assumes "refines R m (spec Q)"
  assumes "\<And>r r'. Q r' \<Longrightarrow> R r r' \<Longrightarrow> P r"
  shows "wp m P"
  using assms unfolding refines_def wp_alt by auto
  
text \<open>Refinement of a specification can be expressed as wp\<close>      
lemma refines_spec_eq_wp: "refines R m (spec Q) \<longleftrightarrow> wp m (\<lambda>r. \<exists>r'. R r r' \<and> Q r')"  
  unfolding refines_def wp_alt
  by auto

text \<open>This rule is useful to 'switch' to Hoare-logic when a specification is to be refined\<close>
lemma refines_spec_wpD: "wp m (\<lambda>r. \<exists>r'. R r r' \<and> Q r') \<Longrightarrow> refines R m (spec Q)" 
  by (simp add: refines_spec_eq_wp)
  
text \<open>Special case if no data refinement is involved\<close>  
lemma refines_eq_spec_wpD[wp_intro]: "wp m P \<Longrightarrow> refines (=) m (spec P)"
  unfolding refines_def wp_alt by auto
  

(** Back to lecture *)




section \<open>Specifying Hoare-Triples\<close>

(* Specify a program that satisfies \<And>x. {P x} c {Q x}, where the x are logical variables *)
definition "hspec PQ \<equiv> do { c\<leftarrow>spec (\<lambda>c. \<forall>x. fst (PQ x) \<longrightarrow> wp c (snd (PQ x))); c }"

lemma wp_hspec[THEN wp_cons,wp_intro]: "P x \<Longrightarrow> wp (hspec (\<lambda>x. (P x,Q x))) (Q x)"
  unfolding hspec_def
  apply (intro wp_intro)
  by auto

lemma hspec_refines[wp_intro]:
  assumes "\<And>x. P x \<Longrightarrow> wp c (Q x)"
  shows "refines (=) c (hspec (\<lambda>x. (P x, Q x)))"
  using assms
  unfolding refines_def wp_alt hspec_def
  by auto



section \<open>Deterministic Result\<close>


definition "is_det m \<equiv> m=FAIL \<or> (\<exists>x. m = RES {x})"

lemma is_det_alt: "is_det m \<longleftrightarrow> m=FAIL \<or> (\<exists>x. is_res m x \<and> (\<forall>y. is_res m y \<longrightarrow> x=y))"
  unfolding is_det_def 
  apply (cases m; simp)
  by blast
  
lemma det_FAIL[wp_intro]: "is_det FAIL"
  unfolding is_det_alt by auto
  
lemma det_return[wp_intro]: "is_det (return x)" 
  unfolding is_det_alt by auto

lemma det_bind[wp_intro]: "\<lbrakk> is_det m; \<And>x. is_det (f x) \<rbrakk> \<Longrightarrow> is_det (do {x\<leftarrow>m; f x})"
  unfolding is_det_alt 
  apply simp
  by metis

lemma det_If[wp_intro]: "\<lbrakk> is_det c; is_det d \<rbrakk> \<Longrightarrow> is_det (if b then c else d)" by auto  
    
lemma det_IfM[wp_intro]: "\<lbrakk> is_det b; is_det c; is_det d \<rbrakk> \<Longrightarrow> is_det (if\<^sub>M b then c else d)" 
  unfolding IfM_def
  by (intro wp_intro)
  
lemma det_Let[wp_intro]: "\<lbrakk>\<And>x. is_det (f x)\<rbrakk> \<Longrightarrow> is_det (let x=v in f x)"  
  by simp
  
lemma det_prod_case[wp_intro]: "\<lbrakk>\<And>x y. is_det (f x y)\<rbrakk> \<Longrightarrow> is_det (case p of (x,y) \<Rightarrow> f x y)"  
  by (cases p) auto
  
lemma det_while[wp_intro]:  
  assumes "\<And>s. is_det (b s)"
  assumes "\<And>s. is_det (c s)"
  shows "is_det (while b c s)"
  apply (induction arbitrary: s rule: while.fixp_induct)  
  apply simp
  apply (intro wp_intro)
  by (intro wp_intro assms)

lemma det_assert[wp_intro]: "is_det (assert P)"  
  unfolding assert_def
  by (intro wp_intro)

lemma det_mfold[wp_intro]:
  assumes "\<And>x s. is_det (f x s)"
  shows "is_det (mfold f xs s)"
  apply (induction xs arbitrary: s)
  apply simp_all
  apply (intro wp_intro)
  by (intro wp_intro assms)


  
(* Back to Lecture *)  
  


subsection \<open>Extracting Result\<close>

definition "dres m \<equiv> THE x. m = RES {x}"

lemma wp_det_dres: "\<lbrakk> wp m Q; is_det m \<rbrakk> \<Longrightarrow> Q (dres m)"
  unfolding is_det_def dres_def
  by auto



  

(*** Back to lecture *)





  
  


section \<open>Code Export\<close>

(* Technical setup. Not covered in lecture. *)

text \<open>Set up code export for deterministic part of nres monad\<close>
fun nres_of_option where
  "nres_of_option None = FAIL"
| "nres_of_option (Some x) = return x"

declare nres_of_option.simps[code del]

code_datatype nres_of_option

definition "bind1 m f \<equiv> case m of None \<Rightarrow> FAIL | Some x \<Rightarrow> f x"

lemma [code]:
  "FAIL = nres_of_option None"
  "return x = nres_of_option (Some x)"
  "bind (nres_of_option m) f = bind1 m f"
  apply (auto split: Option.bind_split option.split simp: bind1_def)
  done

declare while.simps[code]  

lemma [code]: "dres (nres_of_option (Some x)) = x"
  unfolding dres_def by (auto simp: return_def)



    
end

