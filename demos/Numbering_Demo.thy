theory Numbering_Demo
imports 
  "HOL-Library.RBT"
  (* The next two activate dictionary order on strings, 
     such that we can use them as keys in RBTs
  *)
  "HOL-Library.Char_ord" 
  "HOL-Library.List_Lexorder"  
begin
  
  term \<open>RBT.lookup\<close> 
  
  term RBT.empty thm RBT.lookup_empty
  term RBT.insert thm RBT.lookup_insert
  term RBT.delete thm RBT.lookup_delete
  

  section \<open>Injective Numbering\<close>  
  (*
    Given a list of strings. Generate an injective map from the strings to
    numbers {0..<n}, where n is the number of different strings in the list.
    
    Motivation: many algorithms work better when things (states, etc) are numbered,
      such that properties for state i can be efficiently stored at an array index a[i].
  *)
  
    
  term "m(k\<mapsto>v)" term "m(k := Some v)"
  
  definition addnum :: "'a::linorder \<Rightarrow> nat \<times> ('a,nat) rbt \<Rightarrow> nat \<times> ('a,nat) rbt"
    where "addnum x \<equiv> \<lambda>(n,m). 
      if RBT.lookup m x \<noteq> None then 
        (n,m) 
      else 
        (n+1,RBT.insert x n m)"

  definition "numbering xs = (let 
    (_,m) = fold addnum xs (0::nat,RBT.empty)
  in
    the \<circ> RBT.lookup m
  )"
      
  lemma "the \<circ> m = (\<lambda>x. the (m x))" oops
  
  value "the (Some (3::nat))"
  (* value "the (None::nat option)" *)
  
  
  

    
(********** BACK TO LECTURE *)  
  





















  
  
  
  
  
  

  subsubsection \<open>Invariant Rule for Fold\<close>
  (* 
    The following assumes an iteration over the elements of a list,
    where the order and multiplicity of elements does not matter.
    
    The invariant is parameterised by a set of already processed elements.
  *)
  lemma fold_invariant_rule_aux:
    assumes INIT: "I {} s" \<comment> \<open>Initially, no elements are processed\<close>
    assumes STEP: "\<And>x it s. 
      \<lbrakk> it\<subseteq>set xs; x\<in>set xs; I it s \<rbrakk> \<Longrightarrow> I (insert x it) (f x s)"
      \<comment> \<open>Invariant is preserved by processing an element from the list\<close>
    assumes CONS: "I (set xs) (fold f xs s) \<Longrightarrow> Q"
      \<comment> \<open>Subgoal follows from invariant, if all elements of the list have been processed\<close>
    shows "Q"
  proof -
  
    have "I (it\<union>set xs') (fold f xs' s)" if "I it s" "it \<union> set xs' \<subseteq> set xs" for it xs'
      using that
    proof (induction xs' arbitrary: it s)
      case Nil
      then show ?case by simp
    next
      case (Cons x xs')
      
      note IH' = Cons.IH[of "insert x it", simplified]
      
      show ?case
        apply simp
        apply (rule IH')
        apply (rule STEP)
        using Cons.prems
        by auto
      
    qed
    from this[of "{}", OF INIT] CONS show ?thesis by simp
  qed
  
  lemma fold_invariant_rule:
    assumes INIT: "I {} s" \<comment> \<open>Initially, no elements are processed\<close>
    assumes STEP: "\<And>x it s. \<lbrakk> it\<subseteq>set xs; x\<in>set xs; I it s \<rbrakk> \<Longrightarrow> I (insert x it) (f x s)"
      \<comment> \<open>Invariant is preserved by processing an element from the list\<close>
    assumes CONS: "\<And>s'. I (set xs) s' \<Longrightarrow> Q s'"
      \<comment> \<open>Subgoal follows from invariant, if all elements of the list have been processed\<close>
    shows "Q (fold f xs s)"
    using fold_invariant_rule_aux[of I] assms by blast
    

    
  text \<open>Example Proofs\<close>  
    
  (* Check if list contains a 'match', i.e., an element satisfying P *)
  lemma "fold (\<lambda>x s. P x \<or> s) xs False \<longleftrightarrow> (\<exists>x\<in>set xs. P x)"
    apply (rule fold_invariant_rule[where 
      (* Invariant: state describes if already processed elements contain a match *)
      I="\<lambda>it s. s \<longleftrightarrow> (\<exists>x\<in>it. P x)"
    ])
    by auto
  
    
  (* Check if all elements satisfy P *)    
  lemma "fold (\<lambda>x s. P x \<and> s) xs True \<longleftrightarrow> (\<forall>x\<in>set xs. P x)"    
    apply (rule fold_invariant_rule[where 
      I="\<lambda>it s. s \<longleftrightarrow> (\<forall>x\<in>it. P x)" (* What's a good invariant here? *)
    ])
    by auto
    
  (* Count number of elements that satisfy P *)    
  lemma "fold (\<lambda>x s. if P x then s+1 else s) xs 0 = (length (filter P xs))"  
    apply (rule fold_invariant_rule[where 
      I="\<lambda>it s. s = card ({x\<in>it. P x}) \<comment> \<open>Nope!\<close>" (* What's a good invariant here? *)
    ])
    oops
    
  (* Can you prove this lemma directly by induction? *)
    

          
    
    
  (********** BACK TO LECTURE *)  











    
    
    
    
    
    
    
    
  subsubsection \<open>Invariants\<close>
  
  (*
    The main algorithm folds over the list, using addnum on each element.
    This can be seen as a 'for'-loop over the elements of the list, 
    modifying a 'state' (n,m) in each iteration.
    
    Such loops are best reasoned about by providing an invariant: 
      A property that holds for the initial state (0,Map.empty),
      is maintained by each loop iteration, 
      and, upon completion of the iteration, implies the property we want to prove.
      
      
    The invariant usually describes the idea of the loop. 
    It requires a good understanding of how the algorithm works (and some practice) to
    come up with good invariants.
    
    In our case, we want to express that:
      the current map is an injective numbering of the elements seen so far,
      using the numbers {0..<n}
      
  *)
  

  (* Note: phrased over abstract type: \<open>'a \<Rightarrow> nat option\<close>, rather than \<open>('a,nat) rbt\<close>! *)  
  definition is_numbering_on :: "nat \<times> ('a \<Rightarrow> nat option) \<Rightarrow> 'a set \<Rightarrow> bool"
  where
    "is_numbering_on \<equiv> \<lambda>(n,m) s. 
      ran m = {0..<n} \<comment> \<open>Exactly the numbers \<open>{0..<n}\<close> are used\<close>
    \<and> dom m = s       \<comment> \<open>To map the elements from \<open>s\<close>\<close>
    \<and> inj_on m s      \<comment> \<open>Injectively\<close>
    "  
    
  (** Here's the invariant for the concrete data structure: *)  
  definition is_numbering_oni :: "nat \<times> ('a::linorder,nat) rbt \<Rightarrow> 'a set \<Rightarrow> bool" where
    "is_numbering_oni \<equiv> \<lambda>(n,mi) s. is_numbering_on (n,RBT.lookup mi) s"

  (*
    Presentation (and proof creation) sequence:
      
    \<^item> invar lemma: is_numbering_invar
    \<^item> derived aux lemmas:
      \<^item> is_numbering_init
      \<^item> addnum_pres_numbering
      \<^item> derived aux lemmas:
        \<^item> addnum_pres_numbering_aux1
        \<^item> addnum_pres_numbering_aux2
    \<^item> main lemmas
      \<^item> numbering_inj
      \<^item> numbering_dense
  
  *)  
    
    
  (* Invariant holds initially *)
  lemma is_numbering_init: "is_numbering_oni (0,RBT.empty) {}"
    apply (simp add: is_numbering_oni_def)
    (** Note: only abstract data types here *)
    by (auto simp: is_numbering_on_def)
    
  (* Invariant preservation: addnum preserves the invariant.  *)  
  
  (* Aux-lemmas: extracted from proof below! *)
  (* Case 1: element already numbered *)
  (* We replaced the "RBT.lookup mi" by a single variable m: 
    when abstracted, it's irrelevant how the map is represented! 
  *)
  lemma addnum_pres_numbering_aux1: " \<lbrakk>is_numbering_on (n, m) s; m x = Some i\<rbrakk>
    \<Longrightarrow> is_numbering_on (n, m) (insert x s)"
    unfolding is_numbering_on_def (** Only here we unfold abstract invariant, and do the proof *)
    by (auto simp: domIff inj_onD)
  
  lemma addnum_pres_numbering_aux2: " \<lbrakk>is_numbering_on (n, m) s; m x = None\<rbrakk>
    \<Longrightarrow> is_numbering_on (Suc n, m(x \<mapsto> n)) (insert x s)"  
    unfolding is_numbering_on_def
    (** auto does not terminate \<dots> we are lazy and just use clarsimp + sledgehammer.
      Otherwise: proof exploration mode! Break the thing down, and try to prove parts.
    *)
    apply clarsimp
    by (metis atLeast0_lessThan_Suc atLeastLessThan_iff imageE inj_on_fun_updI nless_le ranI)
  
  lemma addnum_pres_numbering: "is_numbering_oni nm s \<Longrightarrow> is_numbering_oni (addnum x nm) (insert x s)"
    unfolding is_numbering_oni_def addnum_def
    apply (clarsimp split: if_splits)
    (** Note: only abstract data types here (RBT.lookup _) 
      at this point, we extract auxiliary lemmas over the abstract data types
      (Alternative: Isar-proof)
    *)
    subgoal for mi n i (** Use meaningful variable names! *)
      using addnum_pres_numbering_aux1 .
    subgoal for n' n mi  
      using addnum_pres_numbering_aux2 .
    done  
  
  (* Using fold_invariant_rule: *)    
  lemma is_numbering_invar: "is_numbering_oni (fold addnum xs (0,RBT.empty)) (set xs)"  
    supply R = fold_invariant_rule[where I="\<lambda>it s. is_numbering_oni s it"]
    thm R
    apply (rule R)
    (* Extract auxiliary lemmas from here. Alternative: Isar *)
    apply (rule is_numbering_init)
    apply (rule addnum_pres_numbering, assumption)
    .
      
  (* Now we can prove the properties that follow from the invariant, and use them to
    show that the main algorithm is correct. *)  
    
  (* Auxiliary lemma for property implied by invariant \<dots> *)  
  lemma is_numbering_imp_inj: "is_numbering_oni (n,mi) s \<Longrightarrow> inj_on (the o RBT.lookup mi) s"  
    apply (clarsimp simp: is_numbering_oni_def)
    by (auto simp: is_numbering_on_def inj_on_def domI)

  lemma numbering_inj: "inj_on (numbering xs) (set xs)"
    apply (clarsimp simp: numbering_def split: prod.splits)
    (* short proof: using is_numbering_invar[of xs] is_numbering_imp_inj by auto *)
  proof -
    fix n m
    assume "fold addnum xs (0, RBT.empty) = (n, m)"
    with is_numbering_invar[of xs] have "is_numbering_oni (n,m) (set xs)" 
      by simp
    with is_numbering_imp_inj show "inj_on (the o RBT.lookup m) (set xs)" by auto
  qed

  (* \<dots> or prove the implication inside main lemma *)
  lemma numbering_dense: "\<exists>n. numbering xs`set xs = {0..<n}"
  proof (clarsimp simp: numbering_def split: prod.splits)
    fix n m
    assume "fold addnum xs (0, RBT.empty) = (n, m)"
    with is_numbering_invar[of xs] have "is_numbering_oni (n,m) (set xs)"
      by simp
    then show "\<exists>n. (\<lambda>x. the (RBT.lookup m x)) ` set xs = {0..<n}"
      apply (clarsimp simp: is_numbering_oni_def)
      apply (clarsimp simp: is_numbering_on_def)
      
      (* TODO: find a nice proof! (homework?) *)
      apply (rule exI[where x=n])
      apply (auto simp: ran_def)
      apply force
      by (smt (z3) Setcompr_eq_image all_nat_less_eq domI mem_Collect_eq option.sel) 
      
  qed      

  
  (*
    Note that, without disciplined structuring of the proof, separating the different aspects
    of the reasoning, the proof of this rather simple program would likely have become very 
    complicated, and hard to understand.
  
    Even with the additional lemmas, getting the correct structuring 
    into this proof required some effort and knowing of what you are doing. 
    It is too easy to deviate from this structure accidentally, or due to pre-mature 
    proof-optimization attempts.
    
    In the last part of this lecture, we will introduce a more systematic 
    approach to this refinement proof style. This will pave the way to proving 
    correct more complex programs at still manageable effort.
  *)
    

  (*
    More homework ideas:

    LTS from list of triples, as RBT-map. Create from list of triples, succ operation on RBT-map.

    Map LTS with injective function. Language remains the same (do abstractly, maybe implement on triple list)
  *)
  
end
