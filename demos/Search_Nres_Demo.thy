section \<open>A verified search algorithm\<close>

theory Search_Nres_Demo
imports Nres_Monad_Tools "HOL-Library.Multiset"
begin

  
subsection \<open>Pseudocode Stage\<close>  
  
(*
  Pseudocode:

  search g v:
    wl = {v}  // Workset
    V = {}    // Visited set
    
    while wl \<noteq> {}
      remove some element v from worklist
      
      if v\<notin>V 
        V = {v} \<union> V
        wl = succs g v \<union> wl
    
    return V    

*)

subsection \<open>Implementation in Isabelle\<close>

(* Add set to workset. Duplicity of all elements can change arbitrarily, 
   as long as set of elements is correct.
*)
definition ws_add :: "'a set \<Rightarrow> 'a multiset \<Rightarrow> 'a multiset nres" 
  where "ws_add s ws \<equiv> spec (\<lambda>ws'. set_mset ws' = set_mset ws \<union> s)"

(* Get some element from workset. Workset must not be empty.
  The new workset must be smaller, and we must not add elements, and remove at most x
*)  
definition "ws_get ws \<equiv> do { 
  assert (ws\<noteq>{#}); 
  spec (\<lambda>(x,ws'). x\<in>#ws \<and> size ws' < size ws \<and> set_mset ws = {x} \<union> set_mset ws') 
}"

(* These operations nondeterministically cover various implementation options,
  e.g., as set, as list, as multiset, etc. *)


(********* Back to lecture *)  
    
  
  
  
  
  

(* The search algorithm: *)

definition search :: "'v rel \<Rightarrow> 'v \<Rightarrow> 'v set nres" where
  "search E v \<equiv> do {
    (_,V) \<leftarrow> while (\<lambda>(wl,V). return (wl\<noteq>{#})) (\<lambda>(wl,V). do {
      (v,wl) \<leftarrow> ws_get wl;
      if v\<notin>V then do {
        let V = insert v V;
        
        wl \<leftarrow> ws_add (E``{v}) wl;
        return (wl, V)
      } else
        return (wl,V)
    }) ({#v#},{});
    return V
  }"
  
subsection \<open>Correctness proof (soundness) (\<open>\<subseteq>\<close> direction)\<close>   
  

text \<open>Let's declare our assumptions in a context, so we don't have to restate them, over and over again\<close>  
context
  fixes E :: "'v rel" and v\<^sub>0 :: "'v"
  assumes finite_reachable: "finite (E\<^sup>*``{v\<^sub>0})"
begin

  subsubsection \<open>Invariants\<close>
  
  text \<open>Note that we define the invariants as their own constants. 
    This will separate the verification condition generation part from
    the rest of the proof.
  \<close>
  definition search_invar_sound :: "'v set \<times> 'v set \<Rightarrow> bool" where
    "search_invar_sound \<equiv> \<lambda>(wl,V). wl \<union> V \<subseteq> E\<^sup>*``{v\<^sub>0}"  
  
  definition search_var :: "('v multiset \<times> 'v set) rel" where
    "search_var \<equiv> 
      inv_image 
        (inv_image finite_psubset (\<lambda>V. E\<^sup>*``{v\<^sub>0}-V) <*lex*> measure size) 
        prod.swap"

    
  subsubsection \<open>Soundness lemma\<close>
  (*
    We first start exploring the proof, to find suitable auxiliary lemmas.
    Afterwards, we clean up the proof.
  *)
    
  lemma search_var_wf: "wf search_var"
    unfolding search_var_def by auto
  
  
  lemma search_invar_sound_init: "search_invar_sound ({v\<^sub>0}, {})"
    unfolding search_invar_sound_def by auto  

  lemma search_var_sound_new: "\<lbrakk>x \<notin> V; search_invar_sound (insert x wl'', V)\<rbrakk> \<Longrightarrow> ((wl', insert x V), wl, V) \<in> search_var"  
    unfolding search_var_def search_invar_sound_def
    using finite_reachable by auto  
      
  lemma search_invar_sound_new:
    assumes "search_invar_sound (insert v wl, V)" "v \<notin> V" 
    shows "search_invar_sound (wl \<union> E `` {v}, insert v V)"
    using assms unfolding search_invar_sound_def
    by auto
    
  lemma search_var_sound_vis:
    "size wl' < size wl \<Longrightarrow> ((wl', V), wl, V) \<in> search_var"  
    unfolding search_var_def by auto
    
  lemma search_invar_sound_vis:
    assumes "search_invar_sound (insert v wl, V)" "v \<in> V"
    shows "search_invar_sound (wl, V)"
    using assms unfolding search_invar_sound_def by auto
    
    
          
  lemma search_invar_sound_final: "search_invar_sound ({}, V) \<Longrightarrow> V \<subseteq> E\<^sup>* `` {v\<^sub>0}"  
    unfolding search_invar_sound_def by auto
    
  lemma search_sound: "wp (search E v\<^sub>0) (\<lambda>V. V \<subseteq> E\<^sup>*``{v\<^sub>0})"
    (* Note: more elaborate abstractions: invar goes over sets, only variant uses multisets.
      Aux-lemmas are abstracted over set_mset.
      
      We unfold ws_get, and ws_add here, as they are just specifications.
    *)
    unfolding search_def ws_add_def ws_get_def
    apply (intro wp_intro 
      wp_while[where 
            I="\<lambda>(wl,V). search_invar_sound (set_mset wl,V)" 
        and V="search_var"])
    apply (simp_all 
      add: search_var_wf search_invar_sound_init
      add: search_var_sound_new search_invar_sound_new
      add: search_var_sound_vis search_invar_sound_vis
      add: search_invar_sound_final
      )
    done     
    
        
  subsection \<open>Correctness proof (completeness) (\<open>\<supseteq>\<close> direction)\<close>    
    
  subsubsection \<open>Invariants\<close>  

  definition "search_invar \<equiv> \<lambda>(wl,V). 
    search_invar_sound (wl,V)
  \<and> E``V \<subseteq> V \<union> wl
  \<and> v\<^sub>0 \<in> V \<union> wl  
  "

  subsubsection \<open>Auxiliary Lemmas\<close>

  (* Generate from aux-lemmas developed for soundness direction. 
    Structure looks the same!
  *)

  lemma search_invar_init: "search_invar ({v\<^sub>0}, {})"
    unfolding search_invar_def by (auto simp: search_invar_sound_init)  

  lemma search_var_new: "\<lbrakk>x \<notin> V; search_invar (insert x wl'', V)\<rbrakk> \<Longrightarrow> ((wl', insert x V), wl, V) \<in> search_var"  
    unfolding search_invar_def
    by (clarsimp simp: search_var_sound_new)
      
  lemma search_invar_new:
    assumes "search_invar (insert v wl, V)" "v \<notin> V" 
    shows "search_invar (wl \<union> E `` {v}, insert v V)"
    using assms unfolding search_invar_def
    apply (simp add: search_invar_sound_new)
    by auto
    
  lemma search_invar_vis:
    assumes "search_invar (insert v wl, V)" "v \<in> V"
    shows "search_invar (wl, V)"
    using assms unfolding search_invar_def
    apply (clarsimp simp: search_invar_sound_vis) 
    by (auto)
    
    
          
  lemma search_invar_final: "search_invar ({}, V) \<Longrightarrow> V = E\<^sup>* `` {v\<^sub>0}"  
    unfolding search_invar_def using search_invar_sound_final[of V] 
    by (auto dest: Image_closed_trancl)
  
  
      
  subsubsection \<open>Correctness lemma\<close>
  
  (* We try the same structure as for the soundness lemma *)
  lemma search_correct: "wp (search E v\<^sub>0) (\<lambda>V. V = E\<^sup>*``{v\<^sub>0})"
    unfolding search_def ws_add_def ws_get_def
    apply (intro wp_intro wp_while[where I="\<lambda>(wl,V). search_invar (set_mset wl, V)" and V="search_var"])
    apply (simp_all add: search_var_wf search_var_new search_var_sound_vis search_invar_init search_invar_new search_invar_vis search_invar_final)
    done

  lemmas [wp_intro] = wp_cons[OF search_correct]
end
  
(* Now we have an algorithm where we abstracted from implementation details.
  We used nondeterminism, where implementation was not clear, e.g., 
  which element to pick from workset.
  
  Next step: implement this algorithm using actual data structures!
*)

definition "R_wl wl ws \<equiv> ws = mset wl"
definition "wl_get wl \<equiv> return (hd wl, tl wl)"
definition "wl_add s wl \<equiv> do { sl \<leftarrow> spec (\<lambda>sl. set sl = s); return (sl@wl) }"

lemma wl_sng_refine: "x=x' \<Longrightarrow> R_wl [x] {#x'#}"
  unfolding R_wl_def
  by auto

lemma wl_is_empty_refine[simp]: "R_wl wl ws \<Longrightarrow> wl=[] \<longleftrightarrow> ws={#}"  
  unfolding R_wl_def
  by auto

  
lemma wl_get_refines[wp_intro]: 
  "R_wl wl ws \<Longrightarrow> refines (rel_prod (=) R_wl) (wl_get wl) (ws_get ws)"
  unfolding wl_get_def ws_get_def
  apply (intro wp_intro)
  unfolding R_wl_def
  by (cases wl) auto

lemma wl_add_refine[wp_intro]:
  "s=s' \<Longrightarrow> R_wl wl ws \<Longrightarrow> refines R_wl (wl_add s wl) (ws_add s' ws)"
  unfolding wl_add_def ws_add_def
  apply (intro wp_intro refines_spec_wpD)
  unfolding R_wl_def
  by auto
  
  
  

definition search1 :: "'v rel \<Rightarrow> 'v \<Rightarrow> 'v set nres" where
  "search1 E v \<equiv> do {
    (_,V) \<leftarrow> while (\<lambda>(wl,V). return (wl\<noteq>[])) (\<lambda>(wl,V). do {
      (v,wl) \<leftarrow> wl_get wl;
      if v\<notin>V then do {
        let V = insert v V;
        
        wl \<leftarrow> wl_add (E``{v}) wl;
        return (wl, V)
      } else
        return (wl,V)
    }) ([v],{});
    return V
  }"
  
  
lemma search1_refines: "\<lbrakk> E=E'; v=v' \<rbrakk> \<Longrightarrow> refines (=) (search1 E v) (search E' v')"  
  unfolding search1_def search_def
  (* Automation is a bit trickier. We do a semi-manual proof here. 
    Full automation requires advanced Isabelle tactics, e.g., using the Eisbach tactic language.
    
    Strategy: 
      1. expand over structure of program
      2. introduce refinement relations
      3. discharge VCs (and non-monadic refinements)
  
  *)  
  apply (intro wp_intro)
  apply (rule rel_rule relI[of R_wl] wl_sng_refine refl | assumption)+
  apply clarsimp
  apply (rule wp_intro)
  apply clarsimp
  apply (rule rel_rule)  
  apply simp
  apply (rule wp_intro)
  apply simp  
  apply simp  
  apply ((rule rel_rule relI[of R_wl] | clarsimp)+) []
  apply ((rule rel_rule relI[of R_wl] | clarsimp)+) []
  apply (rule rel_rule; simp)
  done
  
  
lemma search1_correct: "finite (E\<^sup>* `` {v\<^sub>0}) \<Longrightarrow> wp (search1 E v\<^sub>0) (\<lambda>V. V = E\<^sup>*``{v\<^sub>0})"
  apply (rule refines_spec_wpI)
  apply (rule refines_wp_trans[OF search1_refines search_correct])
  apply (rule refl)
  apply (rule refl)
  apply assumption
  apply simp
  done
 
 
  
subsection \<open>Graph by successor Function\<close>

type_synonym 'v sg = "('v \<Rightarrow> 'v list)"

definition sg_\<alpha> :: "'v sg \<Rightarrow> 'v rel"
  where "sg_\<alpha> sg \<equiv> {(u,v). v \<in> set (sg u)}"

definition sg_succs :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v list"
  where "sg_succs sg v \<equiv> sg v"

lemma sg_succs_correct[simp]: "set (sg_succs sg v) = sg_\<alpha> sg `` {v}"
  unfolding sg_succs_def sg_\<alpha>_def
  by auto
  
  
definition "wl_addl xs wl \<equiv> return (xs@wl)"  
  
lemma wl_addl_refines[wp_intro]: 
  "\<lbrakk>wl=wl'; xs' = set xs\<rbrakk> \<Longrightarrow> refines (=) (wl_addl xs wl) (wl_add xs' wl')"
  unfolding wl_addl_def wl_add_def
  (* Structure mismatch. We just use pointwise reasoning *)
  unfolding refines_def
  by auto
  

definition search2 :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v set nres" where
  "search2 g v \<equiv> do {
    (_,V) \<leftarrow> while (\<lambda>(wl,V). return (wl\<noteq>[])) (\<lambda>(wl,V). do {
      (v,wl) \<leftarrow> wl_get wl;
      if v\<notin>V then do {
        let V = insert v V;
        
        wl \<leftarrow> wl_addl (sg_succs g v) wl;
        return (wl, V)
      } else
        return (wl,V)
    }) ([v],{});
    return V
  }"
  
lemma search2_refines: "\<lbrakk>E = sg_\<alpha> g; v = v'\<rbrakk> \<Longrightarrow> refines (=) (search2 g v) (search1 E v')"  
  unfolding search2_def search1_def
  apply (intro wp_intro)
  apply (rule rel_rule | simp)+
  apply (rule refines_refl) (* Could be wp_intro rule! *)
  apply (rule rel_rule | simp)+
  apply (rule wp_intro)
  apply simp
  apply simp
  apply (rule rel_rule | simp)+
  done  
  
  
lemma search2_correct: "finite ((sg_\<alpha> g)\<^sup>* `` {v\<^sub>0}) 
  \<Longrightarrow> wp (search2 g v\<^sub>0) (\<lambda>V. V = (sg_\<alpha> g)\<^sup>*``{v\<^sub>0})"
  apply (rule refines_spec_wpI)
  apply (rule refines_wp_trans[OF search2_refines search1_correct])
  apply (rule refl)
  apply (rule refl)
  by auto
  
(* ctd here: use set implementation for V! Make deterministic nres-code exportable *)
  
     
end
