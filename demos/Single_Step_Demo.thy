theory Single_Step_Demo
imports Main
begin


section \<open>Backward proof\<close>


text\<open>A simple backward proof:\<close>
lemma "\<lbrakk> A; B \<rbrakk> \<Longrightarrow> A \<and> B"
apply(rule conjI)
apply assumption
apply assumption
done


text \<open>A slightly more complicated backwards proof\<close>
lemma "\<exists>x. \<forall>y. P x y \<Longrightarrow> \<forall>y. \<exists>x. P x y"
  apply (rule allI)
  apply (erule exE)  (* What happens if we do exI first ? *)
  apply (rule exI)
  apply (drule spec)
  by assumption
  
lemma "\<exists>x. \<forall>y. P x y \<Longrightarrow> \<forall>y. \<exists>x. P x y"
  apply (rule allI)
  apply (rule exI)
  apply (erule exE)
  apply (drule spec)
  by assumption

  
lemma "\<forall>y. \<exists>x. P x y \<Longrightarrow> \<exists>x. \<forall>y. P x y"
  apply (rule exI)
  apply (rule allI)
  apply (drule spec)
  apply (erule exE)
  apply assumption (* Why doesn't this work? And what would it mean if it worked? *)
  oops  

  
  
  
  

(******************** 
  BACK TO LECTURE

*)  
  
  
  


































section \<open>Forward Proofs\<close>

text\<open>Instantiation of theorems: "of"\<close>

text\<open>Positional:\<close>
thm conjI[of "A" "B"]
thm conjI[of "A"]
thm conjI[of _ "B"]

text\<open>By name:\<close>
thm conjI[where ?Q = "B"]
thm conjI[where Q = "B"]   (* May omit ? *)
  
(* If variable name ends with number, ? and ".0" must be written! (Idiosyncrasy of Isabelle) *)
lemma my_rev_append: "rev (a1@a2) = rev a2 @ rev a1" by (rule rev_append)
thm my_rev_append    
thm my_rev_append[where ?a1.0 = "[]"]
    
  

text\<open>Composition of theorems: OF\<close>

thm refl[of "a"]
thm conjI[OF refl[of "a"] refl[of "b"]]
thm conjI[OF refl[of "a"]]
thm conjI[OF _ refl[of "b"]]

notepad
begin
  fix A B C X Y Z :: bool

  assume thm1: "\<lbrakk>A ; B\<rbrakk> \<Longrightarrow> C"
  assume thm2: "\<lbrakk>X\<and>Y; Z\<rbrakk> \<Longrightarrow> B"

  thm thm1[OF _ thm2]
  

end



  
end
