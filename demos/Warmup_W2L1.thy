theory Warmup_W2L1
imports Main
begin


  (************************
    Notes on Homework 1:
    
    generally very nice solutions.
    
    One common problem:
      * wrong definition of rlen, and quickcheck didn't find the error
    
    Efficient vs. simple function definitions:
   
  **************************)


  (** Wrong definition of rlen *)
  
  fun decode :: "(nat\<times>'a) list \<Rightarrow> 'a list" where
    "decode [] = []"
  | "decode ((n,x)#xs) = replicate n x @ decode xs"

  fun rlen :: "'a \<Rightarrow> 'a list \<Rightarrow> nat" where
    "rlen x [] = 0"
  | "rlen x (y#ys) = (if x=y then Suc (rlen x ys) else 0)"

  fun encode :: "'a list \<Rightarrow> (nat \<times> 'a) list" where
    "encode [] = []"
  | "encode (x#xs) = (Suc (rlen x xs),x) # encode (drop (rlen x xs) xs)"
  
  value "decode (encode ''aaba'')"
  
  lemma decode_correct: "decode (encode xs) = xs" for xs::string
    sorry   
  
  
  (** Efficient vs. simple 
  
    replicate n x @ decode xs
  
    creates intermediate list, and then appends.
    In some execution models, that might be expensive.
    
    So why not?
  *)
  fun decode' :: "(nat\<times>'a) list \<Rightarrow> 'a list" where
    "decode' [] = []"
  | "decode' ((0,x)#xs) = decode' xs"
  | "decode' ((Suc n,x)#xs) = x # decode' ((n,x)#xs)"

  (*
    The recursion pattern for decode' is complicated!
    And so is the induction if you want to prove anything!

    What can be done:
      prove that decode' equals decode. Then you get the best of both worlds:
      use decode for simple correctness proofs, and decode' for efficient execution
  *)
  
  lemma decode'_eq[simp]: "decode' xs = decode xs"
    apply (induction xs rule: decode'.induct)
    by auto
  
  lemma decode'_correct: "decode' (encode xs) = xs" for xs::string
    apply (auto simp: decode_correct)
    done



  (*
    This week:
    
      induction, induction, induction \<dots>
      simplifier
      
      and maybe the binary search tree case study if time is left
  *)


  (************************
    WARMUP: Repetition (defining recursive functions, simple induction proofs)
  **************************)

  (* Higher Order Logic = Functional Programming + Logic *)

  (* Let's define a few functions *)

  (* Sum of first \<open>n\<close> odd numbers *)  
  
  fun sodds :: "nat \<Rightarrow> nat" where
    "sodds 0 = 0"
  | "sodds (Suc n) = 2*n+1 + sodds n"  (* Prefer Suc n = ... n over n = ... n-1 ! *)

  (*
    Induction proof that \<^term>\<open>sodds n = n^2\<close>
    
    Do on board first!
  *)  
  
  lemma "sodds n = n*n"
    apply (induction n)
    apply auto
    done

  (*
    Go stepwise through proof
  *)    
  lemma "sodds n = n*n"
    apply (induction n)
    subgoal
      by simp
    
    subgoal premises IH for n
      thm IH
      apply (simp only: sodds.simps)
      apply (simp only: IH)
      by simp
    done
  
  (************************
    WARMUP: New material (auxiliary lemmas)
  **************************)

  (* We could have written that differently! *)  
    
  lemma "sum_list (map (\<lambda>x. 2*x+1) [0..<n]) = n*n"
    (* Note the output syntax: \<open>(\<Sum>x\<leftarrow>[0..<n]. 2 * x + 1) = n * n\<close>*)
    apply (induction n)
    by auto
    (*
      Note: that proof depends on a sophisticated setup of simp-lemmas.
      You're not always that lucky, in particular when you are proving goals about your own functions.
      
      Stating/discovering the right lemmas about your functions is key to nice and concise proofs!
    *)

        
  (* Here's an example: Let's define sum_list from scratch, and see what's needed to get the same proof through! *)  

  fun my_sum_list :: "nat list \<Rightarrow> nat" where
    "my_sum_list [] = 0"
  | "my_sum_list (x#xs) = x+my_sum_list xs"  
    
  lemma "my_sum_list (map (\<lambda>x. 2*x+1) [0..<n]) = n*n"
    apply (induction n)
    apply auto
    
    (* Analyze subgoal where it gets stuck: 
    
      we see goal of shape "my_sum_list (xs @ [x])", but induction hypothesis only tells us what \<open>my_sum_list xs\<close> is!

      Intuitively, we can split the sum! Let's convince Isabelle of that.
    *)
    
    oops
  
  lemma my_sum_list_append: "my_sum_list (xs\<^sub>1@xs\<^sub>2) = my_sum_list xs\<^sub>1 + my_sum_list xs\<^sub>2"
    by (induction xs\<^sub>1) auto
    
  (* And try again! *)  
  lemma "my_sum_list (map (\<lambda>x. 2*x+1) [0..<n]) = n*n"
    apply (induction n)
    apply (auto simp: my_sum_list_append)
    done

  (* We can also put my_sum_list_append into the simp-set, 
    then auto will always rewrite with this rule. (And not terminate if the rule is not a suitable simp rule)
  *)  

  lemmas [simp] = my_sum_list_append
  lemma "my_sum_list (map (\<lambda>x. 2*x+1) [0..<n]) = n*n"
    by (induction n) auto
      

  (* Lemmas to split a function over (@) are a standard idiom, which you will see over and over again! *)
    
  thm map_append
  thm filter_append
  thm sum_list_append
  thm length_append
  thm set_append
  find_theorems "?f (?a@?b) = ?g (?f ?a) (?f ?b)"
  

  (* Now let's continue with lecture! (Slide 70, datatype general case) *)
  
  
  
  
  fun sep where
    "sep x (y#z#xs) = y#x#z#sep x (z#xs)"
  | "sep _ xs = xs"  
  
  thm sep.simps
  
  thm sep.induct
  
  lemma "length (sep x xs) \<ge> length xs"
    apply (induction x xs rule: sep.induct)
    apply auto
    done
  
  
  
  
              
end

