theory Warmup_W3L1
imports Main "HOL-Library.Multiset"
begin


  (************************
    Notes on Homework 2:

  **************************)

  (***************************************************
    What are good specifications?
    
    * simple
    * as basic concepts as possible
  *)
  fun mymax :: "nat list \<Rightarrow> nat" where
    "mymax [] = 0"
  | "mymax (x#xs) = max x (mymax xs)"

  (*
    For non-empty lists xs, \<open>mymax xs\<close> is contained in teh list \<open>xs\<close>.
  
    Which specification do you like most? Why?
  *)
  
  prop "xs \<noteq> [] \<Longrightarrow> List.find (\<lambda> a. a= mymax xs) xs \<noteq> None"
  prop "xs \<noteq> [] \<Longrightarrow> ListMem (mymax xs) xs"
  prop "xs \<noteq> [] \<Longrightarrow> mymax xs \<in> set xs"
  prop "mymax (x#xs) \<in> set (x#xs)"

    
  (***************************************************
    Specifying an alternative definition/equation
  *)
  lemma mymax_Cons_alt: "mymax (x#xs) = (if x\<ge>mymax xs then x else mymax xs)"
    by auto

  (* That's often simpler than copy-pasting the whole function. *)

  (* To use the equation, don't forget to remover overlapping ones from the simplifier! *)
    
  lemma "xs \<noteq> [] \<Longrightarrow> mymax xs \<in> set xs"
    (* e.g. for the whole proof *)
    supply [simp del] = mymax.simps(2)
    supply [simp add] = mymax_Cons_alt
    oops
  

  (***************************************************
    Good and bad simp rules
  *)  
  
  lemma fold_max_aux: "fold max (a # xs) 0 = mymax (a # xs)"
    apply (induction xs arbitrary: a)
    apply (auto simp add: Let_def max_def)
    done

  (** 'trick' to check what simplifier rewrites a term to *)    
  lemma "undefined (fold max (a # xs) 0)"  
    apply simp
    (* Check subgoal here*)
    oops
    
  lemma "mymax xs = fold max xs 0"
    apply (induction xs)
    apply simp
    
(*    apply (subst fold_max_aux)
    by simp
    
    apply simp
     
    thm fold.simps
*)    
    
    
    apply (simp add: fold_max_aux) (* Why doesn't this work? *)
    
    oops
      
    
    
  (***************************************************
    'Useless' induction proofs.
  
    I've seen a lot of them. Basically, applying induction where it isn't required.
  
    Think before you prove! While small proofs may go through the complicated way,
    further complicating already complicated proofs won't go well.
  *)  
    
  lemma [simp]: "fold max xs a = max a (mymax xs)"
    apply (induction xs arbitrary: a)
    apply (simp)
    apply (auto)
    done
  
  lemma "mymax xs = fold max xs 0"
    apply (induction xs arbitrary: a) (** What is a? Why an induction, if the lemma goes by simp? *)
    apply (simp)
    apply (auto)
    done
    

  (***************************************************
    Hiding trivialities behind definitions (and set them up to be automatically unfolded). ... Why? 
  *)  
  datatype t = Leaf | Node t t
  
  fun count_leaves :: "t \<Rightarrow> nat" where
    "count_leaves Leaf = 1" |
    "count_leaves (Node l r) = count_leaves l + count_leaves r"
  
  fun complete :: "t \<Rightarrow> nat \<Rightarrow> bool" where
    "complete Leaf               0      \<longleftrightarrow> True"
  | "complete (Node left right) (Suc n) \<longleftrightarrow> (complete left n \<and> complete right n)"
  | "complete _ _ \<longleftrightarrow> False"
  
    
  (** Why hide 2^n behind a fun? *)
  fun leave_count :: "nat \<Rightarrow> nat" where
    "leave_count n = 2^n"
  
  lemma "complete t n \<Longrightarrow> count_leaves t = 2^n" oops
  
  
  lemma "complete t n \<Longrightarrow> count_leaves t = leave_count n"
    apply (induction t n rule: complete.induct)
    apply auto
    done
        
      
  
  
  (***************************************************
    Last week:
    
      induction, induction, induction \<dots>
      simplifier
      binary search tree case study
  *)


  (************************
    WARMUP: Repetition induction proofs
  **************************)

  (* Rule of thumb: induction along recursion of a function.
    if function is primitive recursive \<rightarrow> use datatype induction rule
  *)

  (* Simple, along datatype *)
    
  fun count :: "'a \<Rightarrow> 'a list \<Rightarrow> nat" where
    "count x [] = 0"
  | "count x (y#ys) = (if x=y then 1 else 0) + count x ys"
  
  
  lemma "count x xs = length (filter ((=)x) xs)" for x :: nat
    by (induction xs) auto
  

  (* Computation induction *)      
    
  fun my_sorted :: "int list \<Rightarrow> bool" where
    "my_sorted (x#y#ys) \<longleftrightarrow> x\<le>y \<and> my_sorted (y#ys)"
  | "my_sorted _ = True" 
  
  thm my_sorted.simps 
  thm my_sorted.induct
    
  thm sorted_simps  
    
  lemma "my_sorted xs = sorted xs"
    apply (induction xs)
    apply auto
    oops
  
  lemma "my_sorted xs = sorted xs"
    apply (induction xs rule: my_sorted.induct)
    subgoal by force
    subgoal by auto
    subgoal by auto
    done
    

  (* Generalization *)
    
  thm fold_simps
  
  lemma "sum_list xs = fold (+) xs 0" for xs::"nat list"
    oops
      
  lemma sum_list_eq_fold: "sum_list xs + a = fold (+) xs a" for xs::"nat list"
    apply (induction xs arbitrary: a)
    apply (auto simp: algebra_simps)
    oops
      
  lemma sum_list_eq_fold_aux: "fold (+) xs a = sum_list xs + a" for xs::"nat list"
    apply (induction xs arbitrary: a)
    by (auto)
  
  lemma sum_list_eq_fold: "sum_list xs = fold (+) xs 0" for xs::"nat list"
    by (simp add: sum_list_eq_fold_aux)


    
  (* Computation induction, auxiliary lemmas *)  
        
  fun merge :: "'a::linorder list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
    "merge xs [] = xs"  
  | "merge [] ys = ys"  
  | "merge (x#xs) (y#ys) = (if x\<le>y then x # merge xs (y#ys) else y # merge (x#xs) ys)"
    
  lemma merge_mset[simp]: "mset (merge xs ys) = mset xs + mset ys"  
    apply (induction xs ys rule: merge.induct)
    by auto

  lemma merge_set[simp]: "set (merge xs ys) = set xs \<union> set ys"  
    apply (induction xs ys rule: merge.induct)
    by auto
    
    
  lemma merge_sorted: "sorted xs \<Longrightarrow> sorted ys \<Longrightarrow> sorted (merge xs ys)"  
    apply (induction xs ys rule: merge.induct)
    apply auto
    done
    
    
    
    
    
    
    
    
    
(*        
  lemma merge_set[simp]: "set (merge xs ys) = set xs \<union> set ys"  
    apply (induction xs ys rule: merge.induct)
    by auto
    
  lemma merge_sorted[simp]: "sorted xs \<Longrightarrow> sorted ys \<Longrightarrow> sorted (merge xs ys)"  
    apply (induction xs ys rule: merge.induct)
    by auto*)
    
    

  (********* + NEW MATERIAL: forward composition of lemmas  *)  
        
  fun msort :: "'a::linorder list \<Rightarrow> 'a list" where
    "msort xs = (
      let m = length xs div 2 in 
      if m=0 then xs 
      else 
        merge (msort (take m xs)) (msort (drop m xs))
    )"  

  declare msort.simps[simp del]  
  
  lemma msort_Nil[simp]: "msort [] = []" 
    apply (subst msort.simps)
    by auto
  
    
  lemma "mset (take n xs) + mset (drop n xs) = mset xs"
    thm append_take_drop_id[of n xs, symmetric]
    apply(subst arg_cong[where f=mset, OF append_take_drop_id[of n xs, symmetric]])
    apply (simp del: append_take_drop_id)
    done
    
  lemma aux: "mset (take n xs) + mset (drop n xs) = mset xs"  
    apply (subst arg_cong[OF append_take_drop_id[of n xs, symmetric], of mset])
    apply (subst mset_append)
    by simp
    
  lemma mset_msort[simp]: "mset (msort xs) = mset xs"
    apply (induction xs rule: msort.induct)
    apply (subst msort.simps)
    apply (auto simp: Let_def aux)
    done  

    
        
  lemmas set_msort[simp] = mset_eq_setD[OF mset_msort]  
  
  lemma aux2: "length xs div 2 = 0 \<Longrightarrow> sorted xs"
    apply (cases xs)
    apply simp
    subgoal for x xs'
      apply (cases xs')
      by auto
    done
  
  lemma sorted_msort[simp]: "sorted (msort xs)"
    apply (induction xs rule: msort.induct)
    apply (subst msort.simps)
    apply (auto simp: Let_def aux2 merge_sorted)
    (**                            ^ this lemma is required, too. *)
    done
  
  
  
  
  (**
    This week:
    
    beyond "by induct auto":
    
    \<^item> logical formulas
    \<^item> other proof methods, sledgehammer
    \<^item> single-step proofs
    \<^item> inductive definitions (if enough time)
  *)
  

  (* Examples for set comprehension *)  
  term "{ x :: nat. x < 10   }"
  
  term "{x. \<exists>y z. x=y+z \<and> y<10 \<and> z<10 }"
  
  lemma "{y + z |y z. y < 10 \<and> z < 10} = {0..18::nat}"
    apply auto
    by presburger
    
                  
end

