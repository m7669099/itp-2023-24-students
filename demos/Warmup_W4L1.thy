theory Warmup_W4L1
imports Main "HOL-Library.Multiset"
begin


  (************************
    Notes on Homework 3:
    
  **************************)

  (** auto as initial proof method: DON'T! *)
  
inductive cntT :: "nat \<Rightarrow> bool list \<Rightarrow> bool" where
  "cntT 0 []"
| "cntT n xs \<Longrightarrow> cntT n (False#xs)"  
| "cntT n xs \<Longrightarrow> cntT (Suc n) (True#xs)"  


lemma "cntT n xs \<longleftrightarrow> n=length (filter id xs)" 
  apply(auto) (**#SHOW# Note: auto as initial proof method, followed by more proof is brittle and considered bad style. *)
  subgoal
    apply(induction n xs rule: cntT.induct)
      apply(auto)
    done
  subgoal
    apply(induction xs arbitrary: n)
     apply(auto simp: cntT.intros)
    done
  done
  
(* Better: aux-lemmas, or something more controlled, e.g.: *)
lemma "cntT n xs \<longleftrightarrow> n=length (filter id xs)" 
  apply (rule iffI)
  subgoal
    apply(induction n xs rule: cntT.induct)
      apply(auto)
    done
  subgoal
    apply(induction xs arbitrary: n)
     apply(auto simp: cntT.intros)
    done
  done

  
(* Correctness lemmas: concrete and abstract operation related wrt abstraction function/invariant. 
  Not just some random properties of concrete function, e.g.: *)  
definition "bl_\<alpha> xs \<equiv> { i . i<length xs \<and> xs!i }"
  
definition "bl_lookup i xs \<equiv> if i < length xs then xs!i else False"

lemma bl_lookup_\<alpha> : "bl_lookup i xs \<longleftrightarrow> i\<in>bl_\<alpha> xs" (* OK! *)
  unfolding bl_\<alpha>_def bl_lookup_def
  apply(auto)
  done


definition "bl_insert i xs \<equiv> 
  if i < length xs then xs[i:=True] else (xs@(replicate (i-(length xs)) False))@[True]"

(**#SHOW# That's not strong enough! Makes no statement about other elements than i! *)
lemma bl_insert_\<alpha> : "bl_lookup i (bl_insert i xs)" 
  unfolding bl_lookup_def bl_insert_def
  apply(auto simp: nth_append)
  done
  
(* Correct: *)  
  
term insert
term "{x}\<union>s"

lemma "bl_\<alpha> (bl_insert i xs) = insert i (bl_\<alpha> xs)" 
  by (auto simp: nth_list_update nth_append bl_insert_def bl_\<alpha>_def)
    


lemma bl_\<alpha>_simps1: "bl_\<alpha> [] = {}"
  using bl_\<alpha>_def by simp

lemma bl_\<alpha>_simps2: "bl_\<alpha> (x # xs) = (if x then insert 0 (Suc ` bl_\<alpha> xs ) else (Suc ` bl_\<alpha> xs ))"
  unfolding bl_\<alpha>_def 
  using less_Suc_eq_0_disj by force

  
    
(* Fun disambiguates patterns! What ypu see is not always what you get! *)  
  
fun bl_union :: "(bool list) \<Rightarrow> (bool list) \<Rightarrow> (bool list)" where
  "bl_union xs [] = xs"
| "bl_union [] ys = ys"
| "bl_union (x#xs) (y#ys) = (if x \<or> y then True#bl_union xs ys else False#bl_union xs ys)"

thm bl_union.simps

(* I do not understand why this cannot be proven by the simplifier, since in my opinion these rules
   can exactly match with the second simp rule from bl_union *)
lemma bl_union_\<alpha>: "bl_\<alpha> (bl_union xs ys) = bl_\<alpha> xs \<union> bl_\<alpha> ys"
  apply(induction xs rule: bl_union.induct)
    apply(auto simp add: bl_\<alpha>_simps1 bl_\<alpha>_simps2)
  oops  

(** #SHOW# You ran into the pattern disambiguation done by fun! Have a look at the actual simp-rules: *)
thm bl_union.simps (** The second one looks not as expected, isn't it? 
  The part overlapping with the first rule has been "removed". However, as the RHS in the overlap are also
  the same, you can prove the desired rule later (actually quite common case in Isabelle) *)  
  
lemma bl_union_add_simp[simp]: "bl_union [] ys = ys"
  by (cases ys) auto  

lemma bl_union_\<alpha>: "bl_\<alpha> (bl_union xs ys) = bl_\<alpha> xs \<union> bl_\<alpha> ys"
  apply(induction xs rule: bl_union.induct)
    apply(auto simp add: bl_\<alpha>_simps1 bl_\<alpha>_simps2)
    (** And yes, you were right, the simplifier can now resolve everything. *)
  done    
  
  

  
(*** 
  Go for the easy proof! That's \<^bold>n\<^bold>o\<^bold>t necessarily: "unfold everything and see what auto leaves for us" 
*)  

  
fun bl_union2 :: "(bool list) \<Rightarrow> (bool list) \<Rightarrow> (bool list)" where
"bl_union2 [] [] = []"
|"bl_union2 (x#xs) (y#ys) = (if x\<or>y then (True#(bl_union2 xs ys))else (False#(bl_union2 xs ys)))"
|"bl_union2 (x#xs) [] = (x#xs)"
|"bl_union2 [] (x#xs) = (x#xs)"

lemma "bl_\<alpha> (bl_union2 xs ys) = bl_\<alpha> xs \<union> bl_\<alpha> ys"
  unfolding bl_\<alpha>_def
  apply(induction xs ys rule: bl_union2.induct)
  (* Student complained they are getting stuck here, and the goal "looks messy" *)
    
  (**#SHOW# First step: see on what subgoal auto gets stuck *)
  apply(auto simp add: bl_\<alpha>_simps1 bl_\<alpha>_simps2)[]
  
  (**2nd one. Apply something weaker, say auto without the simp-lemmas: also struck. Just simp *)
  apply simp
  (** Scan the messy subgoal for patterns \<dots> (requires some experience, but you quickly see (True#_)!i.
    That might require case-splitting, let's try *)
  find_theorems "(_#_)!_" thm nth_Cons'
  apply (simp add: nth_Cons')
  
  (** At this point I thought maybe something about length (bl_union _ _). 
    But then I asked myself: Why do you unfold bl_\<alpha> that early \<dots> making everything bigger (and messier) from the start.
    
    In particular, we have given as hints auxiliary lemmas that require bl_\<alpha> not to be unfolded \<dots>
  *)
    
  oops  
  
(** Trying again, with the aux-lemmas,  but bl_\<alpha> not unfolded, such that the aux-lemmas would actually apply somewhere: *)  
lemma "bl_\<alpha> (bl_union xs ys) = bl_\<alpha> xs \<union> bl_\<alpha> ys"
  apply(induction xs ys rule: bl_union.induct)
  apply(auto simp add: bl_\<alpha>_simps1 bl_\<alpha>_simps2)
  done
  
  
    
(******** Sometimes, playing around a bit with auto helps, though! *)    

definition "bl_inv xs \<equiv> (xs \<noteq> [] \<longrightarrow> last xs)"


lemma "bl_inv xs \<Longrightarrow> bl_inv ys \<Longrightarrow> bl_inv (bl_union xs ys)"
  unfolding bl_inv_def
  apply(induction xs ys rule: bl_union.induct)
     apply(auto split: if_splits)
 (*sledgehammer*) (** Finds a proof, but cannot be reconstructed *)
  (* Student got stuck here. Goal looks "nonsensical". *)
  oops


lemma "bl_inv xs \<Longrightarrow> bl_inv ys \<Longrightarrow> bl_inv (bl_union xs ys)"
  unfolding bl_inv_def
  apply(induction xs ys rule: bl_union.induct)
     apply(auto)
 
 (**#SHOW# Look at subgoal and apply standard heuristics: there's an unsplit if, try splitting *)
  apply (auto split: if_splits)
  
  (** ok, makes the goal not much more readable, but the obvious cases got eliminated.
    Now, when is bl_union xs ys = []? Make aux-lemma and done!
  
  *)
  oops  
  
  
lemma aux: "bl_union xs ys = [] \<longleftrightarrow> xs=[] \<and> ys=[]"
  apply (cases xs; cases ys)
  by auto

(*  apply(induction xs ys rule: bl_union.induct)
     apply(auto)
   done
*)   

lemma "bl_inv xs \<Longrightarrow> bl_inv ys \<Longrightarrow> bl_inv (bl_union xs ys)"
  unfolding bl_inv_def
  apply(induction xs ys rule: bl_union.induct)
  by (auto simp: aux)
 

  
      
  
  
  
      
  
  (*********************
    Last week:
    
      proofs beyond "induct auto"
  
  ***)

  (* Logic and proof automation *)
  
  lemma "\<forall>x::int. \<exists>y. x+y = 0"
    by algebra
  
  lemma "\<exists>y. \<forall>x::nat. y\<le>x"
    by blast  
    
  
  (* Sledgehammer *)  
    

  (* Single-step proofs *)    
    
  (* Forwards *)
  
  thm monoD mono_Suc
  thm monoD[OF mono_Suc]
  
  lemmas foo = conjI[OF refl monoD[OF mono_Suc]]

  (* Backwards *)  

  lemma "x\<le>y \<Longrightarrow> t=t \<and> Suc x \<le> Suc y"
    apply (rule conjI)
    apply (rule refl)
    thm monoD   (* Careful with higher-order unification! *)
    apply (rule monoD) back
    oops        

  lemma "x\<le>y \<Longrightarrow> t=t \<and> Suc x \<le> Suc y"
    apply (rule conjI)
    apply (rule refl)
    apply (rule monoD[where f=Suc])
    apply (rule mono_Suc)
    .
    
        
  lemma "x\<le>y \<Longrightarrow> t=t \<and> Suc x \<le> Suc y"
    apply (rule conjI)
    apply (rule refl)
    apply (rule monoD[OF mono_Suc])
    .
    

  (* Inductive Predicates *)  
    
  
  (*
    \<^item> 1 is an odd number
    \<^item> if n is odd, then n+2 is odd
  *)
  
  inductive odd :: "nat \<Rightarrow> bool" where
    odd1: "odd 1"
  | oddN: "odd n \<Longrightarrow> odd (n+2)"  
    
    
  (* True if and only if we can derive it from the above rules! *)  
        
  thm odd.intros (* Use this rule to prove that values are odd *)
  
  lemma "odd (1+2+2)"
    apply (rule odd.intros)
    apply (rule odd.intros)
    apply (rule odd.intros)
    done
  
  lemma "odd 5"
    using oddN[OF oddN[OF odd1]] 
    by (simp add: eval_nat_numeral)  
    
  thm odd.cases (* Odd must have been constructed by it's rules *)
  
  lemma "odd n \<Longrightarrow> n<5 \<Longrightarrow> n=1 \<or> n=3"
    apply (erule odd.cases)
    apply simp
    apply (erule odd.cases)
    apply simp
    apply (erule odd.cases)
    apply simp
    apply simp
    done
  
  (* Rule induction: odd is constructed by it's rules. We can do induction on these rules: *)
  lemma "odd n \<Longrightarrow> (\<exists>k. n=2*k+1)" 
    apply (induction rule: odd.induct)
    subgoal by auto
    subgoal by presburger
    done  
  
  
  (*
    This week:
    
    \<^item> more on inductive predicates
    \<^item> the Isar proof language
  
  *)
  
    
                  
end

