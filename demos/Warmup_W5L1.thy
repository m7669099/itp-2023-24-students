theory Warmup_W5L1
imports Main "HOL-Library.Multiset"
begin


  (************************
    Notes on Homework 4:
  **************************)

  (*** arbitrary fixed variables  *)
definition p_zero where "p_zero _ \<equiv> 0"
  
fun psum :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
  "psum x [] = x"
| "psum [] y = y"
| "psum (x#xs) (y#ys) = (x+y) # psum xs ys"

type_synonym poly = "nat \<Rightarrow> int"
definition poly\<alpha> :: "int list \<Rightarrow> poly" where 
  "poly\<alpha> xs i \<equiv> if i<length xs then xs!i else 0"

lemma psum_negative: "psum p (map uminus p) = replicate (length p) 0"
  apply(induction p)
   apply(auto)
  done
  
lemma psum_opposite : "\<exists> q. poly\<alpha> (psum p q) = p_zero"
proof -
  obtain XXX where "XXX = (map (\<lambda>x. -x) p)" by auto

  define XXX where "XXX = (map (\<lambda>x. -x) p)"
  thm XXX_def  
  
  
  then have negate_list: "psum p XXX = replicate (length p) 0" 
    using psum_negative by auto
  moreover 
  have "poly\<alpha> (replicate n 0) = p_zero"
  (**#SHOW#              ^ what's n? It's an undeclared fixed variable, basically rendering the fact useless! *)
    using poly\<alpha>_def p_zero_def by fastforce
  moreover
  have "poly\<alpha> (psum p XXX) = p_zero" 
    using poly\<alpha>_def p_zero_def negate_list by fastforce
  ultimately show ?thesis by blast
qed
  
  
(*** Preconditions of lemmas  *)
definition msum :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "msum n m \<equiv> (n + m) mod 7"
  
(**#SHOW# Preconditions not required \<dots> in this case, it's better to omit them, to make 
  the lemmas easier to use. *)
lemma "a mod 7 = a \<and> b mod 7 = b \<and> c mod 7 = c 
  \<Longrightarrow> msum a (msum b c) = msum (msum a b) c"
  using msum_def
  apply(presburger)
  done

(* Easier to use lemma: *)  
lemma "msum a (msum b c) = msum (msum a b) c"
  using msum_def
  apply(presburger)
  done



  
  (*** path-append lemma *)
  
  (* Proving a path-append lemma was key to easy solutions of some of the exercises.
    I saw some more special lemmas here, too, but they all were special cases of path-append:
  *)
  
context
  fixes E :: "'v rel" (*equal to 'v \<times> 'v set*)
begin
  fun path :: "'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
    "path u [] v \<longleftrightarrow> u=v"
  | "path u (x#xs) v \<longleftrightarrow> (u,x)\<in>E \<and> path x xs v"  
    

  lemma path_append[simp]: "path u (vs\<^sub>1@vs\<^sub>2) w \<longleftrightarrow> (\<exists>v. path u vs\<^sub>1 v \<and> path v vs\<^sub>2 w)"
    by (induction vs\<^sub>1 arbitrary: u) auto
  
  (* Here's a selection of 'special' lemmas, that all are readily 
    proved with path-append: *)  
  lemma subpath: "\<And>u. path u (ys@y#xs) w \<Longrightarrow> path u (ys@[y]) y" by auto
  lemma short_path: "path x (xs@y'#ys) u \<Longrightarrow> path x (xs@[y']) y'" by auto
  lemma "path u (xs@[x]) v \<Longrightarrow> x=v" by auto
  
  lemma end_of_path: "path u xs d \<and> (d,v)\<in>E \<Longrightarrow> path u (xs@[v]) v" by auto
  
  lemma path_implies_edge3: "path a (vvs@v#w#vs) b \<Longrightarrow> (v,w)\<in>E" by auto

  
  
  
  (* Also, path-append makes induction over paths 'direction-independent' *)  
  

  lemma reachable_imp_path: "(u,v)\<in>E\<^sup>* \<Longrightarrow> \<exists>xs. path u xs v"
    apply (induction rule: converse_rtrancl_induct)
    apply (auto intro: exI[where x="[]"] exI[where x="_#_"]) 
    done

  lemma "(u,v)\<in>E\<^sup>* \<Longrightarrow> \<exists>xs. path u xs v"
    apply (induction rule: rtrancl_induct)
    apply (auto intro: exI[where x="[]"]) []
    apply (fastforce intro: exI[where x="_@[_]"]) 
    done
      
  lemma "(u,v)\<in>E\<^sup>* \<Longrightarrow> \<exists>xs. path u xs v"
  proof (induction rule: rtrancl_induct)
    case base
    then show ?case by (auto intro: exI[where x="[]"])
  next
    case (step y z)
    then obtain xs where "path u xs y" by blast
    with \<open>(y,z)\<in>E\<close> have "path u (xs@[z]) z" 
      by auto (* \<leftarrow> Only works so nicely b/c path_append! *)
    then show ?case ..
  qed 
      

  
  
(** Usage of arbitrary: *)
lemma "path u (xs@[x]) v \<Longrightarrow> x=v"
  supply [simp del] = path_append
  apply (induction xs arbitrary: u) 
    (**#SHOW# Note: setting everything to arbitrary works in theory. 
      In practice, it increases the chances that auto&co won't find a proof! *)
  by auto

  
  

    
    
(** How do good inductive rules look? *)  
  
(* Compare: *)
inductive accept :: "'v \<Rightarrow> 'v set \<Rightarrow> bool" where
  "accept u ({u} \<union> F)" 
| "accept u F \<Longrightarrow> (v,u)\<in>E \<Longrightarrow>accept v F"

inductive accept' :: "'v \<Rightarrow> 'v set \<Rightarrow> bool" where
  "u\<in>F \<Longrightarrow> accept' u F"
| "accept' u F \<Longrightarrow> (v,u)\<in>E \<Longrightarrow>accept' v F"
  
(* These define the same, but, let's try to prove something *)

lemma assumes "(u,v)\<in>E\<^sup>*" "v\<in>F" shows "accept' u F"
  using assms
  apply (induction rule: converse_rtrancl_induct)
  apply (auto intro: accept'.intros)
  done
  
lemma assumes "(u,v)\<in>E\<^sup>*" "v\<in>F" shows "accept u F"
  using assms
  apply (induction rule: converse_rtrancl_induct)
  apply (auto intro: accept.intros)
  using accept.intros(1)
  
  (** Harder to apply accept.intros(1), as goal must have syntactic shape "{u}\<union>F".
  
    sledgehammer comes to the rescue in this simple case, but that won't scale.
   *)
  by (metis Set.set_insert accept.intros(1) insert_is_Un)
  
  
(** Unnecessary \<exists> *)  
  
inductive accept'' :: "'v \<Rightarrow> 'v set \<Rightarrow> bool" where
  accept_final: "u \<in> F \<Longrightarrow> accept'' u F" |
  accept_step: "\<exists>v. (u, v) \<in> E \<and> accept'' v F \<Longrightarrow> accept'' u F"
    (**#SHOW# No need for the \<exists> here! *)

    
    
    
    
    

  (** How to handle proof steps that you cannot solve, but think they are true?  
  
    use "sorry" for that step. Ideally with a comment why you think the step is true.
  
    Trye to keep 'visibility' of sorrie'd statement as local as possible.
    NEVER: sorry lemmas you know are not true. 
  *)
  
  context
    assumes acyclic: "E\<^sup>+ \<inter> Id = {}"
  begin
  
    (* I got the following: *)
      
    (* Bad lemma but allows me to continue with the proof *)
    lemma acyclic_aux: "path v vs w \<Longrightarrow> v \<in> set vs \<Longrightarrow> path v ys vp"
      sorry

    (* What do you think? Does this lemma hold? 
      And what would be the consequences if it didn't? 
      
      What would have been a better approach to "continue with the proof"?
    *)  
    
  end    
    
    
    

  
  
  
(*
  This week:
  
  \<^item> induction in Isar
  \<^item> abstract data types

*)



  (* 
  
    Let's start with an example:
    
    Theorem: 
      if there is a path between u and v, 
      then there is also a path that does not visit any node twice.
    
  
    (sketch proof on board)
  
    then in Isar!
  *)
  
  lemma 
    assumes "path u xs v"
    obtains xs' where "path u xs' v" "distinct xs'"
  proof -
  
    from assms have "\<exists>xs'. path u xs' v \<and> distinct xs'"
    proof (induction xs rule: length_induct)
      case (1 xs)
      
      hence IH: 
        "\<exists>xs'. path u xs' v \<and> distinct xs'" 
        if "length ys < length xs" "path u ys v" for ys
        using that by blast
      
      note PREMS = \<open>path u xs v\<close>  
        
        
        
      show ?case proof (cases)
        assume "distinct xs" with PREMS show ?thesis by blast
      next
        assume "\<not>distinct xs"
        
        thm not_distinct_decomp
        
        from not_distinct_decomp[OF this] obtain xs\<^sub>1 xs\<^sub>2 xs\<^sub>3 x where
          xs_split: "xs = xs\<^sub>1@[x]@xs\<^sub>2@[x]@xs\<^sub>3" by blast
        with PREMS obtain x\<^sub>1 x\<^sub>2 where 
          "path u xs\<^sub>1 x\<^sub>1" "(x\<^sub>1,x)\<in>E" "path x xs\<^sub>2 x\<^sub>2" "(x\<^sub>2,x)\<in>E" "path x xs\<^sub>3 v" 
          by clarsimp   
        hence shorter_path: "path u (xs\<^sub>1@[x]@xs\<^sub>3) v" (is "path _ ?p _")
          by auto   
      
        have "length ?p < length xs"  
          by (simp add: xs_split)
        from IH[OF this] shorter_path show ?thesis . 
      qed    
    qed
    with that show ?thesis by blast
  qed    




  
  
  
  
  
  
  
        
    
end











    
                  
end

