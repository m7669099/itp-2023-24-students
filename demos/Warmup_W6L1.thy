theory Warmup_W6L1
imports Main
begin

(* Notes on Homework 5 *)

  (** Automatic instantiations of existential *)


  fun path where
    "path E u [] v \<longleftrightarrow> u=v"
  | "path E u (v#vs) w \<longleftrightarrow> (u,v)\<in>E \<and> path E v vs w"  

  lemma path_append[simp]: "path E u (vs\<^sub>1@vs\<^sub>2) w \<longleftrightarrow> (\<exists>v. path E u vs\<^sub>1 v \<and> path E v vs\<^sub>2 w)"
    apply (induction vs\<^sub>1 arbitrary: u) by auto

  lemma relpow_Suc_prepend_simp: "E^^Suc n = E O E^^n"
    by (simp add: relpow_commute)
  
  lemmas [simp del] = relpow.simps(2)  
  lemmas [simp] = relpow_Suc_prepend_simp
    

  lemma relpow_path_conv: "(u,v)\<in>E^^n \<longleftrightarrow> (\<exists>vs. n = length vs \<and> path E u vs v)"
  proof (induction n arbitrary: u)
    case 0
    then show ?case by simp
  next
    case (Suc n)
    note IH = Suc.IH
    show ?case
    proof(rule iffI) \<comment> \<open>We split up an equivalence proof in two directions using this rule\<close>
      assume "(u, v) \<in> E ^^ Suc n"
      text \<open>
        From our assumption, it follows that there is an edge \<open>(u,w)\<close> and a sequence of edges in between
        \<open>(w,v)\<close> of length n for some node w. From the induction hypothesis we then follow that there is
        a path of length n between those same states. Since \<open>(u,w)\<close> is an edge, we can extend this to a
        path of length \<open>Suc n\<close> between u and v.
      \<close>

      then obtain w where "(u, w) \<in> E \<and> (w, v) \<in> E ^^ n" 
        by auto

      moreover
      with IH have "\<exists> vs. n = length vs \<and> path E w vs v"
        by auto

      ultimately 
      show "\<exists>vs. Suc n = length vs \<and> path E u vs v"
        (************************)
      
        (* This seems pretty straightforward, not sure why i had to get sledgehammer involved? *)
        apply auto
        (**#SHOW# There's an existential here, and that has a non-trivial instantiation *)
        subgoal for vs
          apply (rule exI[where x="w#vs"]) (** There's no easy way to 'guess' this instantiation. 
            In general, also sledgehammer is not very good at finding instantiations for \<exists>.
           *)
          by simp
        done
      (**
        by (metis length_Cons path.simps(2)) 
      *)  
        
        
    next
      assume "\<exists>vs. Suc n = length vs \<and> path E u vs v"
      text \<open>
        From this assumption, we know that there also is some path of length n between w and v for some
        state w. Applying the induction hypothesis then yields a sequence of length n. Since we also know 
        that (u,w) is an edge, we can extend the sequence to length Suc n
      \<close>

      moreover
      then obtain w where "(\<exists> vs. n = length vs \<and> path E w vs v) \<and> (u, w) \<in> E"
        by (metis length_Suc_conv path.simps(2))

      moreover
      with IH have "(w, v) \<in> E ^^ n" by simp

      ultimately
      show "(u, v) \<in> E ^^ Suc n" 
        by auto
    qed
  qed




  (** Generalization *)
  lemma fold_max_Max:"fold max xs a = Max (set (a#xs))"
    by (metis Max.set_eq_fold list.simps(15))
  (*I dont get why this is necessary and the other way around is not enough*)
  
  lemma "fold max xs 0 \<le> sum_list xs" for xs :: "nat list"
  proof (induction xs)
    case Nil
    then show ?case by simp
  next
    case (Cons a xs)
    then show ?case
    
      (**#SHOW# You forgot to generalize over the accumulator here. So your induction proof
        would not go through \<dots> but you proved an alternative characterization of 
        "fold max xs a" that does not use an accumulator, and 'rescued' the proof.
      *)
     by (auto simp add: fold_max_Max)
  qed


  
  (**
    And now back to Monads!

    This week
      \<^item> option monad
      \<^item> partial function
      \<^item> weakest precondition
      \<^item> while loops
      \<^item> well-founded relations
      \<^item> Worklist DFS
  
  *)

end
