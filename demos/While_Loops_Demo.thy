theory While_Loops_Demo
imports Partial_Function_Demo    
begin
      
  (* Constructing a WHILE loop *)
  
  context
    fixes b :: "'s \<Rightarrow> bool"
    fixes c :: "'s \<Rightarrow> 's option"
  begin
  
    partial_function (option) while :: "'s \<Rightarrow> 's option" where
      "while s = (
        if b s then do {
          s \<leftarrow> c s;
          while s
        } else Some s)" 
  
  end  
  
  lemmas [code] = while.simps (* Set-up for use with value*)
  value "while (\<lambda>l. length l < 7) (\<lambda>l. Some (False#l)) []"
  

  (* Euclid's Algorithm *)
  
  definition euclid :: "nat \<Rightarrow> nat \<Rightarrow> nat option" where "euclid a b \<equiv> do {
    (r,_) \<leftarrow> while (\<lambda>(a,b). a\<noteq>b) (\<lambda>(a,b). 
      if a<b then Some (b-a,a)
      else Some (a-b,b)
    ) (a,b);
    Some r
  }"
  
  value "euclid 16 10"  
  (* What does it compute? *)
  
  
  
  (** Back to lecture *)
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

  (* Proving while loops correct: Invariant! and termination *)
  
  lemma wp_while_aux:
    assumes WF: "wf V"
    assumes I0: "I s"
    assumes STEP: "\<And>s. \<lbrakk> I s; b s \<rbrakk> \<Longrightarrow> wp (c s) (\<lambda>s'. I s' \<and> (s',s)\<in>V)"
    shows "wp (while b c s) (\<lambda>s. I s \<and> \<not>b s)"
    using WF I0
  proof (induction s rule: wf_induct_rule)
    case (less s)
    
    note \<open>I s\<close> \<comment> \<open>Invariant holds for s\<close>
    
    show ?case proof (cases "b s")
      case True
      then show ?thesis
        apply (subst while.simps) 
        apply simp
        apply (rule wp_cons[OF STEP])
        apply (rule \<open>I s\<close>)
        apply assumption
        apply (rule less.IH)
        by auto
    next
      case False  
      then show ?thesis
        apply (subst while.simps) 
        by (simp add: \<open>I s\<close>)
    qed
  qed
      

  (* For better applicability, we combine the while-rule with a consequence rule: *)

  lemma wp_while:
    assumes WF: "wf V"
    assumes I0: "I s"
    assumes STEP: "\<And>s. \<lbrakk> I s; b s \<rbrakk> \<Longrightarrow> wp (c s) (\<lambda>s'. I s' \<and> (s',s)\<in>V)"
    assumes CONS: "\<And>s. \<lbrakk> I s; \<not>b s \<rbrakk> \<Longrightarrow> Q s"
    shows "wp (while b c s) Q"
    apply (rule wp_cons)
    apply (rule wp_while_aux[where I=I, OF WF I0 STEP])
    apply assumption
    apply assumption
    using CONS by blast
    

    
    
  (** Back to lecture *)
    
    
    
    
    
    
    
  (* Assembling wf relations *)  
  section \<open>Assembling Well-Founded Relations\<close>
  term measure \<comment> \<open>wf-relation based on mapping to nat\<close>
  lemma "(s',s)\<in>measure f \<longleftrightarrow> f s' < ((f s) :: nat)" by (rule in_measure)
  
  lemma "wf (measure f)" by (rule wf_measure) \<comment> \<open>Obviously well-founded: nat can only decrease finitely often!\<close>
  
  term "measure length"  (* length of list decreases in iterations *)
  
    
  term finite_psubset
  lemma "(s',s)\<in>finite_psubset \<longleftrightarrow> s' \<subset> s \<and> finite s" by (rule in_finite_psubset)
  thm wf_finite_psubset
  
  term "inv_image r f"  
  lemma "(s',s) \<in> inv_image r f \<longleftrightarrow> (f s', f s) \<in> r" by (rule in_inv_image)
  lemma "wf r \<Longrightarrow> wf (inv_image r f)" by (rule wf_inv_image)
  
  lemma "measure = inv_image less_than" by auto
      
  term "r\<^sub>1 <*lex*> r\<^sub>2"
  lemma "((a',b'), (a,b)) \<in> r\<^sub>1 <*lex*> r\<^sub>2 \<longleftrightarrow> (a',a)\<in>r\<^sub>1 \<or> (a'=a \<and> (b',b)\<in>r\<^sub>2)" by (rule in_lex_prod)
  lemma "wf r\<^sub>1 \<Longrightarrow> wf r\<^sub>2 \<Longrightarrow> wf (r\<^sub>1 <*lex*> r\<^sub>2)" by (rule wf_lex_prod)
  
  term "measure (\<lambda>(a,b,c). length b)"
  (* TODO: tutorial exercise: convert English text into wf-relations *)
    
  

  
    
  (** Back to lecture *)
  

  
  
  
  
  
  
  
  
  
  
  
  (* Proving Euclid Correct *)
  
  
  lemma euclid_aux1: "a < b \<Longrightarrow> gcd (b - a) a = gcd a b" for a b :: nat
    by (metis gcd.commute gcd_diff1_nat less_or_eq_imp_le)
    
  lemma euclid_aux2: "\<not> a < b \<Longrightarrow> gcd (a - b) b = gcd a b" for a b :: nat
    by (meson gcd_diff1_nat not_le)
  
  
  lemma wp_euclid: "\<lbrakk>a\<noteq>0; b\<noteq>0\<rbrakk> \<Longrightarrow> wp (euclid a b) (\<lambda>r. r=gcd a b)"  
    unfolding euclid_def
    apply (intro wp_intro 
      wp_while[where 
            V="measure (\<lambda>(a,b). a+b)"
        and I="\<lambda>(a',b'). gcd a b = gcd a' b' \<and> a'\<noteq>0 \<and> b'\<noteq>0"
      ])
    apply (simp_all)
    apply (simp add: euclid_aux1)
    apply (simp add: euclid_aux2)
    done
    
  lemmas [wp_intro] = wp_cons[OF wp_euclid]  
    
    
  (* While example: multiplication by addition. 
    (A classical introductory example for Hoare-Logic) 
  *)
    
  definition mul_by_add :: "nat \<Rightarrow> nat \<Rightarrow> nat option" where "mul_by_add a b = do {
    (_,s) \<leftarrow> while 
      (\<lambda>(a,s). a>0) 
      (\<lambda>(a,s). do {
        let a = a - 1;
        let s = s + b;
        Some (a,s)
      })
      (a,0);
      
    Some s
    }
    "
  
  lemma "wp (mul_by_add a b) (\<lambda>r. r = a*b)"
    unfolding mul_by_add_def
    apply (intro wp_intro 
      wp_while[where 
          V="measure (\<lambda>(a,s). a)"
      and I="\<lambda>(a',s). undefined"  (* Can you think of an invariant? *)
      ])
    oops
    
    
  
end
