theory Bellman_Ford
imports 
  "../Nres_Monad_Tools"
  Extended_Ext 
  Integer_Add_Insts
  "HOL-Library.RBT" 
  "HOL-Library.Multiset" 
  "HOL-Library.Product_Lexorder" 
begin

section \<open>Miscellaneous\<close>
(* Missing list lemma. 
  (occurs multiple tyimes in AFP, this one taken from Wimmer's FloydWarshall) 
*)
lemma distinct_length_le:"finite s \<Longrightarrow> set xs \<subseteq> s \<Longrightarrow> distinct xs \<Longrightarrow> length xs \<le> card s"
  by (metis card_mono distinct_card)
  
lemma finite_distinct: "finite s \<Longrightarrow> finite {xs . set xs \<subseteq> s \<and> distinct xs}"
proof -
  assume "finite s"
  hence "{xs . set xs \<subseteq> s \<and> distinct xs} \<subseteq> {xs. set xs \<subseteq> s \<and> length xs \<le> card s}"
  using distinct_length_le by auto
  moreover have "finite {xs. set xs \<subseteq> s \<and> length xs \<le> card s}"
  using finite_lists_length_le[OF \<open>finite s\<close>] by auto
  ultimately show ?thesis by (rule finite_subset)
qed






lemma finite_Inf_in_linorder:
  fixes A :: "'a::{complete_lattice,linorder} set"
  shows "finite A \<Longrightarrow> A\<noteq>{} \<Longrightarrow> Inf A \<in> A"  
  apply (rule finite_Inf_in; simp?)
  by (metis inf.absorb2 inf.orderE nle_le)

  
lemma finite_Inf_in:  
  "finite A \<Longrightarrow> Inf A \<noteq> \<infinity> \<Longrightarrow> Inf A \<in> A" for A :: "_::{conditionally_complete_lattice,linorder} extended set"
  by (metis Inf_greatest Pinf_le emptyE finite_Inf_in_linorder)
 


  
section \<open>Graphs\<close>

    
locale graph =
  fixes E :: "'v rel"  
begin

  definition "V \<equiv> fst`E \<union> snd`E"

  lemma E_in_V: "E \<subseteq> V\<times>V"
    unfolding V_def by force
    
  fun path where
    "path u [] v \<longleftrightarrow> u=v"  
  | "path u ((u\<^sub>1,u\<^sub>2)#p) v \<longleftrightarrow> u=u\<^sub>1 \<and> (u\<^sub>1,u\<^sub>2)\<in>E \<and> path u\<^sub>2 p v"
    
  lemma path_Cons_simp[simp]: "path u (e#p) v \<longleftrightarrow> (\<exists>u'. e=(u,u') \<and> (u,u')\<in>E \<and> path u' p v)"
    by (cases e) auto
  
  lemma path_append[simp]: "path u (p\<^sub>1@p\<^sub>2) v \<longleftrightarrow> (\<exists>v'. path u p\<^sub>1 v' \<and> path v' p\<^sub>2 v)"
    by (induction p\<^sub>1 arbitrary: u) auto
  
  lemma rtrancl_path_conv: "(u,v)\<in>E\<^sup>* \<longleftrightarrow> (\<exists>p. path u p v)"  
  proof (rule; clarify?)
    assume "(u,v)\<in>E\<^sup>*"  
    thus "(\<exists>p. path u p v)"
      apply (induction rule: converse_rtrancl_induct)
      subgoal by (rule exI[where x="[]"]; auto)
      subgoal for y z by (auto intro: exI[where x="(y,z)#_"])
      done
  next
    fix p
    assume "path u p v" thus "(u, v) \<in> E\<^sup>*"
      by (induction p arbitrary: u) (force+)
  qed    
  
  lemma path_rtranclD: "path u p v \<Longrightarrow> (u,v)\<in>E\<^sup>*"
    by (auto simp: rtrancl_path_conv)

  lemma rtrancl_pathE: assumes "(u,v)\<in>E\<^sup>*" obtains p where "path u p v"
    using assms by (auto simp: rtrancl_path_conv)
        
  lemma path_edges: "path u p v \<Longrightarrow> set p \<subseteq> E"
    by (induction p arbitrary: u) auto
    

    
  definition "simple_path p \<equiv> distinct (fst (hd p) # map snd p)"  
    
  lemma simple_path_length_bound:
    assumes "simple_path p"
    shows "length p < card (insert (fst (hd p)) (snd`set p))"
    using assms unfolding simple_path_def
    by (auto dest: distinct_card)

    
  lemma not_simple_path_decomp:
    assumes "path u p v"
    assumes "\<not>simple_path p"
    obtains p\<^sub>1 p\<^sub>2 p\<^sub>3 u' where "p=p\<^sub>1@p\<^sub>2@p\<^sub>3" "p\<^sub>2\<noteq>[]" "path u p\<^sub>1 u'" "path u' p\<^sub>2 u'" "path u' p\<^sub>3 v"
  proof -
  
    from assms show ?thesis
      unfolding simple_path_def
      apply (cases p; simp)
      apply (elim disjE; clarsimp dest!: not_distinct_decomp simp: in_set_conv_decomp map_eq_append_conv append_eq_map_conv)
      subgoal for p\<^sub>3
        apply (rule that[of "[]" "[(u,u)]" p\<^sub>3]) 
        by auto
      subgoal for v1 u' ys zs  
        apply (rule that[of "[]" "(u, u') # ys @ [(v1, u)]" zs])
        by auto
      subgoal for aa ba ys zs
        apply (rule that[of "[(u,ba)]" "ys @ [(aa, ba)]" zs])
        by auto
      subgoal for u' us a usa aa ba zsb
        apply (rule that[of "(u, u') # us @ [(a, ba)]" "usa @ [(aa, ba)]" zsb])
        by auto
      done
  qed    
    

  
  lemma simple_path:  
    assumes "path u p v"
    obtains p' where "path u p' v" "simple_path p'" "mset p' \<subseteq># mset p"
  proof -
    have "\<exists>p'. path u p' v \<and> simple_path p' \<and> mset p' \<subseteq># mset p" using assms
    proof (induction p rule: length_induct)
      case (1 p)
      
      note IH = "1.IH"[rule_format]
      
      show ?case proof (cases "simple_path p")
        case True
        then show ?thesis using "1.prems" by blast
      next
        case False
        from not_simple_path_decomp[OF "1.prems" False]
        obtain p\<^sub>1 p\<^sub>2 p\<^sub>3 u' where [simp]: "p = p\<^sub>1 @ p\<^sub>2 @ p\<^sub>3" "p\<^sub>2 \<noteq> []" and "path u p\<^sub>1 u'" "path u' p\<^sub>2 u'" "path u' p\<^sub>3 v" .
        hence "length (p\<^sub>1@p\<^sub>3) < length p" "path u (p\<^sub>1@p\<^sub>3) v" by auto
        from IH[OF this] obtain p' where "path u p' v" "simple_path p'" "mset p' \<subseteq># mset (p\<^sub>1 @ p\<^sub>3)" by blast
        then show ?thesis
          apply (rule_tac exI[where x=p'])
          apply simp
          using mset_subset_eq_add_right subset_mset.add_left_mono subset_mset.trans by blast
      qed
    qed        
    thus ?thesis using that by blast 
  qed              
  
end      

locale finite_graph = graph E for E :: "'v rel" 
  + assumes finite[simp,intro!]: "finite E"
begin

  lemma finite_V[simp,intro!]: "finite V"
    unfolding V_def
    by blast

end  



section \<open>Weighted Graphs\<close>  

subsection \<open>Weights\<close>
class weight = linordered_idom + conditionally_complete_lattice

lemma ew_le_self_plus_iff: "a \<le> a + b \<longleftrightarrow> a=-\<infinity> \<or> a=\<infinity> \<or> 0\<le>b" for a b :: "'w::weight extended"
  unfolding zero_extended_def
  by (cases a; cases b; simp)

lemma ew_less_minusI: "a+b<c \<Longrightarrow> a<c-b" for a b c :: "'w::weight extended" 
  by (cases a; cases b; cases c; simp)
  
lemma ew_less_minus_iff: "a<c-b \<longleftrightarrow> a+b<c \<or> (a\<noteq>\<infinity> \<and> b=\<infinity> \<and> c=\<infinity>) \<or> (a\<noteq>\<infinity> \<and> b=-\<infinity> \<and> c=-\<infinity>)" 
  for a b c :: "'w::weight extended"
  apply (cases a; cases b; cases c; simp) 
  apply linarith
  done
  
  
  
subsection \<open>Weighted Graphs\<close>
      
locale wgraph =
  fixes w :: "'v \<times> 'v \<Rightarrow> 'w::weight extended"
    and v\<^sub>0 :: 'v
begin    
  definition "E \<equiv> {e. w e \<noteq> \<infinity>}"  

  lemma inE_conv: "e\<in>E \<longleftrightarrow> w e \<noteq> \<infinity>"
    unfolding E_def by auto
    
  sublocale graph E .
    

  subsubsection "Weight of a path"
  definition pw :: "('v \<times> 'v) list \<Rightarrow> 'w extended" where
    "pw p \<equiv> sum_list (map w p)"

  lemma pw_empty[simp]: "pw [] = 0" unfolding pw_def by auto
  lemma pw_Cons[simp]: "pw (e#es) = w e + pw es" unfolding pw_def by auto
  lemma pw_append[simp]: "pw (p\<^sub>1@p\<^sub>2) = pw p\<^sub>1 + pw p\<^sub>2" unfolding pw_def by auto
    
  lemma path_weight: "path u p v \<Longrightarrow> pw p \<noteq> \<infinity>"
    apply (induction p arbitrary: u)
    by (auto simp: inE_conv)
    
  subsubsection "Distance"
  definition "\<delta> u v \<equiv> Inf { pw p | p. path u p v }"

  (* Exactly the reachable nodes have a distance \<noteq>\<infinity> *)
  lemma reachable_iff_dist: "\<delta> u v \<noteq> \<infinity> \<longleftrightarrow> (u,v)\<in>E\<^sup>*"  
  proof
    assume "\<delta> u v \<noteq> \<infinity>"  
    hence "\<delta> u v < \<infinity>" by (simp add: order_ext_less_inf_conv)
    then obtain p where "path u p v"
      unfolding \<delta>_def 
      using Inf_less_iff
      by fastforce
      
    thus "(u,v)\<in>E\<^sup>*" by (auto simp: rtrancl_path_conv)
  next
    assume "(u,v)\<in>E\<^sup>*"
    then obtain p where P: "path u p v" by (auto simp: rtrancl_path_conv)
    with path_weight have "pw p \<noteq> \<infinity>" by simp
    with P show "\<delta> u v \<noteq> \<infinity>" unfolding \<delta>_def 
      by (auto simp: Inf_eq_PInfty)
  qed  

  lemma reachable_iff_dist': "\<delta> u v = \<infinity> \<longleftrightarrow> (u,v)\<notin>E\<^sup>*"  
    using reachable_iff_dist by blast
  
  (*
    Note that the following two lemmas holds for the 
    general case where the graph may be infinite.
    
    Simpler lemmas for the finite case are proved further down.
  *)
    
  (* Distance is <x, iff there is a path with weight <x *)  
  lemma \<delta>_less_iff: "\<delta> u v < x \<longleftrightarrow> (\<exists>p. path u p v \<and> pw p < x)"
    unfolding \<delta>_def Inf_less_iff
    by blast

  (* Distance is \<le>x, iff for all y>x there's a path with weight <y *)    
  lemma \<delta>_le_iff: "\<delta> u v \<le> x \<longleftrightarrow> (\<forall>y>x. \<exists>p. path u p v \<and> pw p < y)"
    unfolding \<delta>_def
    by (simp add: Inf_le_iff) blast

  (* Distance to the source node is \<le> 0 *)
  lemma \<delta>_source: "\<delta> v\<^sub>0 v\<^sub>0 \<le> 0"
    by (metis \<delta>_le_iff path.simps(1) pw_empty)

    
  subsubsection "Triangle Property"
          
  theorem triangle: "\<delta> s v \<le> \<delta> s u + w (u,v)"
    apply (cases "w (u,v) = \<infinity>"; simp?)
  proof (subst \<delta>_le_iff; clarify)
    fix y
    assume A: "\<delta> s u + w (u, v) < y" and [simp]: "w (u,v)\<noteq>\<infinity>"
    hence "\<delta> s u < y - w (u, v)" 
      by (auto simp: ew_less_minusI) 
    then obtain p where "path s p u" and PWP: "pw p < y - w (u,v)"
      by (auto simp add: \<delta>_less_iff) 
    hence PATH: "path s (p@[(u,v)]) v" 
      by (simp_all add: inE_conv)
    hence "pw p + w (u,v) \<noteq> \<infinity>" 
      using path_weight by auto 
    with PWP A have "pw ((p@[(u,v)])) < y"
      by (auto simp: ew_less_minus_iff) 
    with PATH show "\<exists>p. path s p v \<and> pw p < y" by blast
  qed
  
  
  corollary triangle_path: 
    "path u p v \<Longrightarrow> \<delta> s v \<le> \<delta> s u + pw p"  
    apply (induction p arbitrary: u)
    apply simp
    apply clarsimp
    by (smt (verit, del_insts) add.assoc add_right_mono order_trans triangle)
  
  
  
  
  
  subsection \<open>Relaxation\<close>

  (* Predicate to state valid distance estimate *)
  definition is_est :: "('v \<Rightarrow> 'w extended) \<Rightarrow> bool" where "is_est d \<equiv> \<delta> v\<^sub>0 \<le> d \<and> d v\<^sub>0 \<le> 0"

  (* Initial distance estimate *)
  definition initial_est :: "'v \<Rightarrow> 'w extended" where "initial_est \<equiv> (\<lambda>_. \<infinity>)(v\<^sub>0:=0)"

  (* The initial estimate is valid *)  
  lemma initial_est_correct[simp]: "is_est initial_est"
    unfolding is_est_def initial_est_def
    by (auto simp: le_fun_def \<delta>_source)

  (* Relaxing an edge *)        
  definition "relax u v d \<equiv> 
    if d v > d u + w (u,v) then
      d(v := d u + w (u,v))
    else d
  "
  
  lemma relax_alt: "relax u v d = d ( v := min (d v) (d u + w (u,v)))"
    unfolding relax_def by auto

  lemma relax_id_iff: "relax u v d = d \<longleftrightarrow> d v \<le> d u + w (u,v)"  
    unfolding relax_def
    by (auto simp: fun_eq_iff)
    
  lemma relax_noE: "(u,v)\<notin>E \<Longrightarrow> relax u v d = d"    
    unfolding relax_def E_def 
    by (auto simp: fun_eq_iff)

    
  (* Relaxing preserves validity of estimate *)    
  theorem relax_correct[simp]: "is_est d \<Longrightarrow> is_est (relax u v d)"
    unfolding is_est_def relax_def
    apply (clarsimp simp: le_fun_def)
    by (meson add_right_mono linorder_not_le nle_le order_trans triangle)

  (* If we have reached \<delta>, nothing can be relaxed *)
  lemma no_relax_\<delta>:
    shows "relax u v (\<delta> v\<^sub>0) = \<delta> v\<^sub>0"
    by (metis linorder_not_le relax_def triangle)

  lemma relax_decr: "relax u v d \<le> d"  
    unfolding relax_def
    by (auto simp: le_fun_def)
    
  (* If we cannot relax anything, the estimate satisfies the triangle inequation *)
  context 
    fixes d
    assumes no_relax: "\<forall>u v. d \<le> relax u v d"
  begin  
  
    lemma no_relax_triangle: "d v \<le> d u + w (u,v)"
      using no_relax unfolding relax_def 
      by (force split: if_splits simp: le_fun_def) 
      
    lemma no_relax_triangle_path: 
      "path u p v \<Longrightarrow> d v \<le> d u + pw p"  
    proof (induction u p v rule: path.induct)
      case (1 u v)
      then show ?case by simp
    next
      case (2 u u\<^sub>1 u\<^sub>2 p v)
      
      from "2.prems" have [simp]: "u\<^sub>1 = u" and "(u, u\<^sub>2) \<in> E" "path u\<^sub>2 p v"
        by auto
      
      from "2.IH"[OF \<open>path u\<^sub>2 p v\<close>] have "d v \<le> d u\<^sub>2 + pw p" .
      also from no_relax_triangle[of u\<^sub>2 u] have "d u\<^sub>2 \<le> d u + w (u, u\<^sub>2)" .
      finally show ?case
        by (simp add: add.assoc add_right_mono)
    qed

    
    (* If we cannot relax anything, a valid estimate is precise *)
    theorem no_relax_est_precise:
      assumes "is_est d"
      shows "d = \<delta> v\<^sub>0"
    proof (rule ext, rule ccontr)
      fix v
      assume "d v \<noteq> \<delta> v\<^sub>0 v"
      with \<open>is_est d\<close> have "\<delta> v\<^sub>0 v < d v" unfolding is_est_def 
        by (metis le_funE order_le_less)
      with \<delta>_less_iff[of v\<^sub>0 v "d v"] obtain p where "path v\<^sub>0 p v" "pw p < d v" by auto
      note \<open>pw p < d v\<close> 
      also have "\<dots> \<le> d v\<^sub>0 + pw p" using no_relax_triangle_path[OF \<open>path v\<^sub>0 p v\<close>] .
      also have "d v\<^sub>0 \<le> 0" using \<open>is_est d\<close> unfolding is_est_def by auto
      finally have "pw p < pw p" by (simp add: add_mono)
      thus False ..
    qed
        
  end  
  
  
  
  subsection "Negative Cycles"
  (* Predicate to state that graph has a (reachable) negative cycle *)
  definition "has_negative_cycle \<equiv> \<exists>u p. (v\<^sub>0,u)\<in>E\<^sup>* \<and> path u p u \<and> pw p < 0"

  (* The next four lemmas are utility lemmas: boilerplate to simplify working with
    has_negative_cycle. *)
  lemma has_negative_cycleE: 
    assumes "has_negative_cycle"
    obtains u p where "(v\<^sub>0,u)\<in>E\<^sup>*" "path u p u" "pw p < 0"
    using assms unfolding has_negative_cycle_def by auto

  lemma has_negative_cycleI: 
    assumes "(v\<^sub>0,u)\<in>E\<^sup>*" "path u p u" "pw p < 0"
    shows has_negative_cycle
    using assms unfolding has_negative_cycle_def by auto
    
      
  lemma nnegD: "\<lbrakk>\<not>has_negative_cycle; (v\<^sub>0,u)\<in>E\<^sup>*; path u p u\<rbrakk> \<Longrightarrow> pw p \<ge> 0"
    using has_negative_cycleI leI by blast
  
  lemma nnegI: "\<lbrakk> \<And>u p. \<lbrakk>(v\<^sub>0,u)\<in>E\<^sup>*; path u p u\<rbrakk> \<Longrightarrow> pw p \<ge> 0 \<rbrakk> \<Longrightarrow> \<not>has_negative_cycle"  
    using has_negative_cycle_def leD by blast
  
  
  
  (* If there are no negative cycles, then there is a simple shortest path *)
  theorem shorter_simple_path:
    assumes nneg: "\<not>has_negative_cycle"
    assumes reach: "(v\<^sub>0,u)\<in>E\<^sup>*"
    assumes path: "path u p v"
    obtains p' where "simple_path p'" "path u p' v" "pw p' \<le> pw p"
  proof -
  
    have "\<exists>p'. simple_path p' \<and> path u p' v \<and> pw p' \<le> pw p" using path
    proof (induction p rule: length_induct)
      case (1 p)
      
      note IH = "1.IH"[rule_format]
      
      show ?case proof (cases "simple_path p")
        case True
        then show ?thesis using "1.prems" by blast
      next
        case False
        
        from not_simple_path_decomp[OF "1.prems" False] obtain p\<^sub>1 p\<^sub>2 p\<^sub>3 u' where 
          [simp]: "p = p\<^sub>1 @ p\<^sub>2 @ p\<^sub>3" "p\<^sub>2 \<noteq> []" and PATHS: "path u p\<^sub>1 u'" "path u' p\<^sub>2 u'" "path u' p\<^sub>3 v" .
        hence "length (p\<^sub>1@p\<^sub>3) < length p" "path u (p\<^sub>1@p\<^sub>3) v" by auto
        from IH[OF this] obtain p' where "simple_path p'" "path u p' v" "pw p' \<le> pw (p\<^sub>1 @ p\<^sub>3)" by blast
        moreover 
        from reach PATHS have "(v\<^sub>0,u')\<in>E\<^sup>*" 
          by (auto dest: path_rtranclD)
        from nnegD[OF nneg this \<open>path u' p\<^sub>2 u'\<close>] have "0 \<le> pw p\<^sub>2" .
        ultimately show ?thesis 
          apply (rule_tac exI[where x=p'])
          apply simp
          by (metis (no_types, lifting) add.commute add_0 add_right_mono leD leI order_le_less_trans)
      qed
    qed
    thus ?thesis using that by blast 
  qed              
  
  (* We only need to consider the simple paths for \<delta> *)    
  lemma \<delta>_dist: 
    assumes nneg: "\<not>has_negative_cycle"
    shows "\<delta> v\<^sub>0 v = \<Sqinter>{pw p |p. path v\<^sub>0 p v \<and> simple_path p}"
    apply (rule antisym)
    unfolding \<delta>_def
    subgoal by (blast intro: Inf_mono) 
    subgoal
      apply (rule Inf_mono)
      apply (force elim: shorter_simple_path[OF nneg rtrancl_refl])
      done
    done
    

  (* If there's a negative cycle, and no -\<infinity> estimates, 
    then we can always relax another edge.
  *)
  theorem neg_imp_relax:
    assumes "has_negative_cycle"  
    assumes "is_est d"
    assumes NOT_MINF: "-\<infinity> \<notin> range d"
    obtains u v where "relax u v d < d"
  proof -
    {
      assume A: "\<not> (relax u v d < d)" for u v
      have [simp]: "relax u v d = d" for u v 
      proof -
        from A[of u v] have "d \<le> relax u v d"
          by (simp add: less_fun_def relax_decr)
        also note relax_decr  
        finally show ?thesis by simp
      qed  
      have NO_RELAX: "\<forall>u v. d \<le> relax u v d" by auto

      from \<open>has_negative_cycle\<close> obtain v p where "(v\<^sub>0,v)\<in>E\<^sup>*" "path v p v" "pw p < 0"
        by (rule has_negative_cycleE)

      from NOT_MINF have "d v \<noteq> -\<infinity>" by (metis rangeI)
      have "d v \<noteq> \<infinity>" proof -
        from \<open>(v\<^sub>0,v)\<in>E\<^sup>*\<close> obtain pp where "path v\<^sub>0 pp v" by (auto simp: rtrancl_path_conv)
        hence "pw pp \<noteq> \<infinity>" by (simp add: path_weight)
        moreover from no_relax_triangle_path[OF NO_RELAX \<open>path v\<^sub>0 pp v\<close>] have "d v \<le> d v\<^sub>0 + pw pp" .
        moreover have "d v\<^sub>0 \<le> 0" using \<open>is_est d\<close> unfolding is_est_def by auto
        ultimately show ?thesis by auto
      qed
        
      from no_relax_triangle_path[OF NO_RELAX \<open>path v p v\<close>] have "d v \<le> d v + pw p" by auto
      with \<open>pw p < 0\<close> have False 
        using \<open>d v \<noteq> -\<infinity>\<close> \<open>d v \<noteq> \<infinity>\<close>
        by (auto simp: ew_le_self_plus_iff)
    } thus ?thesis using that by blast
  qed  
  
  
  
end

section \<open>Weighted Graphs with no \<open>-\<infinity>\<close> Edges\<close>

(*
  Our version of the algorithm assumes that there are no -\<infinity> edges.
  
  TODO: extension to support -\<infinity> edges and compute -\<infinity> distances, 
    also for nodes reachable from negative cycles.
    
*)

locale wgraph_nmi = wgraph w v\<^sub>0 
  for w :: "'v \<times> 'v \<Rightarrow> 'w::weight extended"
  and v\<^sub>0 :: 'v +
  assumes no_minf: "-\<infinity> \<notin> range w"  
begin    

  lemma no_minf_simp[simp]: "w e \<noteq> -\<infinity>" using no_minf 
    by (metis rangeI)

  (* We cannot relax to -\<infinity> *)  
  lemma not_minf_initial_est[simp]: "-\<infinity> \<notin> range initial_est"
    unfolding initial_est_def by auto
    
  lemma not_minf_relax[simp]: "-\<infinity> \<notin> range (relax u v d) \<longleftrightarrow> -\<infinity> \<notin> range d"  
    unfolding relax_def
    by auto
         


end


section \<open>Finite Weighted Graphs\<close>

locale finite_wgraph = wgraph w v\<^sub>0 + finite_graph E for w and v\<^sub>0 :: 'v
  
locale finite_wgraph_nmi = finite_wgraph w v\<^sub>0 + wgraph_nmi w v\<^sub>0 for w and v\<^sub>0 :: 'v


context finite_wgraph 
begin

  lemma finite_simple_paths: "finite {p. set p \<subseteq> E \<and> simple_path p}"
    unfolding simple_path_def
    apply (rule finite_subset[where B="{ p. set p \<subseteq> E \<and> distinct p}"])
    subgoal by (auto simp: distinct_map)
    by (blast intro: finite_distinct)

  (*
    In a finite graph, we can always obtain a simple shortest path
  *)
  theorem obtain_shortest_path: 
    assumes "\<not>has_negative_cycle"
    assumes "\<delta> v\<^sub>0 v \<noteq> \<infinity>"
    obtains p where "path v\<^sub>0 p v" "simple_path p" "pw p = \<delta> v\<^sub>0 v"
  proof -
    have "finite {pw p |p. path v\<^sub>0 p v \<and> simple_path p}"  
      apply (rule finite_image_set)
      apply (rule finite_subset[where B="{ p. set p \<subseteq> E \<and> simple_path p}"])
      subgoal by (auto dest: path_edges)
      by (blast intro: finite_simple_paths)
    with assms show ?thesis
      using that finite_Inf_in
      unfolding \<delta>_dist[OF \<open>\<not>has_negative_cycle\<close>] by force
      
  qed

  (*
    Predicate that states that an estimate is precise up to paths of length n.
    
    The main idea of Bellman-Ford is to incrementally compute estimates precise up
    to |V|. In a graph without negative cycles, these capture all shortest paths.
  *)
  definition "is_est_upto n d \<equiv> is_est d \<and> (\<forall>v p. path v\<^sub>0 p v \<and> length p \<le> n \<longrightarrow> d v \<le> pw p)"

  lemma is_est_upto_initial[simp]: "is_est_upto 0 initial_est"
    unfolding is_est_upto_def
    apply simp
    unfolding initial_est_def by auto
    
  lemma relax_correct_upto[simp]: "is_est_upto n d \<Longrightarrow> is_est_upto n (relax u v d)"  
    unfolding is_est_upto_def
    apply (clarsimp)
    by (metis dual_order.trans le_funE relax_decr)
    
    
  (*
    When the Bellmann-Ford algorithm reaches a fixed point,
    then the estimate is precise, and there are no negative cycles.
  *)  
  theorem bellman_ford_final_fixp:
    assumes NOT_MINF: "-\<infinity> \<notin> range d"
    assumes EST_UT: "is_est_upto n d"
    assumes RELAX_FIXP: "\<forall>(u,v)\<in>E. relax u v d = d"
    shows "d = \<delta> v\<^sub>0" "\<not>has_negative_cycle"
  proof -
    from RELAX_FIXP relax_noE have [simp]: "relax u v d = d" for u v
      by blast

    from EST_UT have EST: "is_est d" unfolding is_est_upto_def by auto
    
    from no_relax_est_precise[OF _ EST] show "d = \<delta> v\<^sub>0" by auto
  
    show "\<not>has_negative_cycle" proof (rule notI)
      assume "has_negative_cycle"
      from neg_imp_relax[OF this EST NOT_MINF] obtain u v where "relax u v d < d" .
      thus False by simp
    qed
        
  qed  
  
  (*
    If there are no negative cycles, 
    an estimate up to |V| is already precise.
  *)
  theorem nneg_upto_cardV_implies_precise:
    assumes "\<not>has_negative_cycle"
    assumes EST: "is_est_upto n d"
    assumes CARDV: "card V \<le> n+1"
    shows "d v = \<delta> v\<^sub>0 v"
    
    (*
      Proof idea: the shortest path has length <card V, and is thus covered by the precise estimate
    *)
  proof (cases)
    assume C: "\<delta> v\<^sub>0 v = \<infinity>"
    with EST show ?thesis
      unfolding is_est_upto_def is_est_def
      by (metis Pinf_le le_funD)
    
  next
    assume C: "\<delta> v\<^sub>0 v \<noteq> \<infinity>"
    from obtain_shortest_path[OF \<open>\<not>has_negative_cycle\<close> C] obtain p where 
      "path v\<^sub>0 p v" 
      "simple_path p" 
      "pw p = \<delta> v\<^sub>0 v" 
      .
      
    have "length p \<le> n" proof (cases "p=[]")
      assume "p\<noteq>[]"

      from \<open>path v\<^sub>0 p v\<close> have "insert (fst (hd p)) (snd ` set p) \<subseteq> V"
        using E_in_V \<open>p\<noteq>[]\<close>
        by (cases p) (auto dest!: path_edges)
      with simple_path_length_bound[OF \<open>simple_path p\<close>] have "length p < card V"
        by (meson card_mono finite_V order_less_le_trans)
      thus ?thesis using CARDV by (simp add: less_le)  
    qed simp    
        
    with EST \<open>path v\<^sub>0 p v\<close> have "d v \<le> \<delta> v\<^sub>0 v"
      unfolding \<open>pw p = \<delta> v\<^sub>0 v\<close>[symmetric]
      unfolding is_est_upto_def by auto
    also from EST have "\<delta> v\<^sub>0 v \<le> d v"
      unfolding is_est_upto_def is_est_def by (auto dest: le_funD)
    finally show ?thesis .  
  qed    

  (*
    When Bellman-Ford has not yet reached a fixed point after card V - 1 iterations,
    there must be a negative cycle.
  *)
  corollary bellman_ford_final_no_fixp:
    assumes NOT_MINF: "-\<infinity> \<notin> range d"
    assumes EST_UT: "is_est_upto n d"
    assumes CARDV: "card V \<le> n+1"
    assumes RELAX_NO_FIXP: "relax u v d \<noteq> d"
    shows "has_negative_cycle"
  proof (rule ccontr)
    assume "\<not>has_negative_cycle"
    from nneg_upto_cardV_implies_precise[OF this EST_UT CARDV] have "d = \<delta> v\<^sub>0"
      by (rule ext)
    with RELAX_NO_FIXP no_relax_\<delta> show False by simp
  qed  
  
end

section \<open>Abstract Bellman-Ford Algorithm\<close>
(*
  Pseudocode:
  
  d = initial_estimate
  
  for i=0; i<n; ++i
    for each (u,v)\<in>E: d = relax u v d
    
  if \<forall>(u,v)\<in>E. relax u v d = d
    return Some d
  else 
    return None

*)



(*
  Here, we assume that there are no -\<infinity> edges.

  TODO: probably not needed! 
    Should be done on refinement to graph data structure that does not support -\<infinity> edges!
    At the same time, or later, we can then refine d to something that does not support -\<infinity>!
    
*)
context finite_wgraph_nmi begin

definition "incr_estimate d \<equiv> do {
  es \<leftarrow> spec (\<lambda>es. set es = E);

  mfold (\<lambda>(u,v) d. return (relax u v d)) es d
}"

definition "can_relax d \<equiv> \<exists>(u,v)\<in>E. relax u v d \<noteq> d"
  
definition "bellman_ford n \<equiv> do {
  let d = initial_est;

  (_,d) \<leftarrow> while (\<lambda>(i,d). return (i<n)) (\<lambda>(i,d). do {
    d \<leftarrow> incr_estimate d;
    return (i+1,d)
  }) (0,d);

  if can_relax d then
    return None
  else 
    return (Some d)
}"



definition "incr_estimate_invar i es d \<equiv> 
    is_est_upto i d 
  \<and> - \<infinity> \<notin> range d
  \<and> ((\<forall>v p e. e\<in>es \<and> path v\<^sub>0 (p@[e]) v \<and> length p \<le> i \<longrightarrow> d v \<le> pw p + w e))
  "

lemma relax_one_more:
  assumes "(u, v) \<in> E" 
    "is_est_upto i d"
    "path v\<^sub>0 p u" "length p \<le> i"
  shows "relax u v d v \<le> pw p + w (u, v)"
proof -
  from assms(2-) have "d u \<le> pw p"
    unfolding is_est_upto_def by auto
  thus ?thesis
    unfolding relax_def 
    apply clarsimp
    by (meson add_right_mono dual_order.trans leI)
    
qed  
  
lemma incr_estimate_invar_pres:
  assumes "(u,v)\<in>E"
  assumes "incr_estimate_invar i es\<^sub>1 d"
  shows "incr_estimate_invar i (insert (u, v) es\<^sub>1) (relax u v d)"
  using assms
  unfolding incr_estimate_invar_def
  apply (clarsimp)
  apply (erule disjE)
  subgoal by (simp add: relax_one_more)
  subgoal for v' p u'
    using relax_decr[THEN le_funD]
    by (metis order_trans)
  done
  
lemma incr_estimate_invar_final:
  assumes "incr_estimate_invar i E d" 
  shows "is_est_upto (Suc i) d"
proof -
  from assms have "is_est_upto i d"
    unfolding incr_estimate_invar_def by auto
  hence EST: "is_est d"  
    unfolding is_est_upto_def by simp

  from assms have "(\<forall>v p e. e\<in>E \<and> path v\<^sub>0 (p@[e]) v \<and> length p \<le> i \<longrightarrow> d v \<le> pw p + w e)"  
    unfolding incr_estimate_invar_def by auto
  hence A: "\<And>v p e. path v\<^sub>0 (p@[e]) v \<Longrightarrow> length p \<le> i \<Longrightarrow> d v \<le> pw p + w e"
    by auto
  
  {
    fix p v
    assume PATH: "path v\<^sub>0 p v" and LEN: "length p \<le> Suc i"  
    
    have "d v \<le> pw p"
    proof (cases p rule: rev_cases)
      case Nil
      then show ?thesis
        using PATH EST unfolding is_est_def by simp
      
    next
      case [simp]: (snoc p' e)
      
      show ?thesis 
        apply simp
        apply (rule A)
        using PATH LEN by auto
      
    qed
  }  
  then show "is_est_upto (Suc i) d"
    unfolding is_est_upto_def
    by (clarsimp simp: EST)
qed    
  
lemma wp_incr_estimate[THEN wp_cons, wp_intro]: 
  assumes "is_est_upto i d" "-\<infinity>\<notin>range d"
  shows "wp (incr_estimate d) (\<lambda>d'. is_est_upto (Suc i) d' \<and> -\<infinity>\<notin>range d')"
  unfolding incr_estimate_def case_prod_app
  apply (intro wp_intro wp_mfold[where I="\<lambda>es _ d. incr_estimate_invar i (set es) d"])
  subgoal
    using assms
    unfolding incr_estimate_invar_def by auto
  apply clarsimp  
  subgoal by (auto intro: incr_estimate_invar_pres)
  apply clarsimp  
  subgoal by (auto intro: incr_estimate_invar_final)
  subgoal
    unfolding incr_estimate_invar_def by auto
  done


lemma bellman_ford_correct:
  assumes "card V \<le> n+1"
  shows "wp (bellman_ford n) (
      \<lambda>None \<Rightarrow> has_negative_cycle 
    | Some d \<Rightarrow> \<not>has_negative_cycle \<and> d=\<delta> v\<^sub>0)"
  unfolding bellman_ford_def can_relax_def
  apply (intro wp_intro wp_while[where 
        V="measure (\<lambda>(i,_). n-i)" 
    and I="\<lambda>(i,d). i\<le>n \<and> is_est_upto i d \<and> -\<infinity>\<notin>range d"]
  )
  apply simp_all
  subgoal using assms bellman_ford_final_no_fixp by blast
  subgoal using assms bellman_ford_final_fixp by blast
  done

end  


section \<open>Weighted Graph as Edge List\<close>

type_synonym ('v,'w) lwg = "('v \<times> 'w \<times> 'v) list"

fun lwg_the_edge :: "('v \<times> 'w \<times> 'v) \<Rightarrow> 'v\<times>'v" where
  "lwg_the_edge (u,_,v) = (u,v)"

definition lwg_invar :: "('v,'w) lwg \<Rightarrow> bool" where
  "lwg_invar edges = distinct (map lwg_the_edge edges)"

definition lwg_\<alpha> :: "('v,'w) lwg \<Rightarrow> 'v\<times>'v \<Rightarrow> 'w extended" where
  "lwg_\<alpha> edges \<equiv> \<lambda>(u,v). if \<exists>w. (u,w,v)\<in>set edges then Fin (THE w. (u,w,v)\<in>set edges) else \<infinity>"

  
lemma lwg_empty[simp]:
  "lwg_invar []"  
  "lwg_\<alpha> [] = (\<lambda>_. \<infinity>)"
  unfolding lwg_invar_def lwg_\<alpha>_def
  by auto
  
lemma lwg_append_edge[simp]:
  assumes "lwg_invar edges"
  assumes "lwg_\<alpha> edges (u,v) = \<infinity>"
  shows "lwg_invar (edges@[(u,w,v)])" "lwg_\<alpha> (edges@[(u,w,v)]) = (lwg_\<alpha> edges)((u,v) := Fin w)"
  using assms
  unfolding lwg_invar_def lwg_\<alpha>_def
  by (auto simp: fun_eq_iff split: if_splits)
  
lemma lwg_not_minf: "-\<infinity> \<notin> range (lwg_\<alpha> edges)"  
  unfolding lwg_\<alpha>_def
  by (auto split: if_splits)
  
lemma lwg_not_minf'[simp]: "lwg_\<alpha> edges e \<noteq> -\<infinity>"  
  by (metis lwg_not_minf rangeI)
  
lemma lwg_\<alpha>_ninf_conv:
  "lwg_\<alpha> edges e \<noteq> \<infinity> \<longleftrightarrow> (\<exists>x. lwg_\<alpha> edges e = Fin x)"  
  apply (cases "lwg_\<alpha> edges e")
  by auto
  
lemma lwg_invar_imp_inj: "\<lbrakk>lwg_invar edges; (u,w,v) \<in> set edges; (u,w',v) \<in> set edges\<rbrakk> \<Longrightarrow> w=w'"  
  unfolding lwg_invar_def
  by (metis Pair_inject distinct_map inj_onD lwg_the_edge.simps)
  
lemma lwg_edge_iff: "lwg_invar edges \<Longrightarrow> (u,w,v) \<in> set edges \<longleftrightarrow> lwg_\<alpha> edges (u,v) = Fin w"  
  unfolding lwg_\<alpha>_def
  apply clarsimp
  apply (rule theI2, assumption)
  by (auto dest: lwg_invar_imp_inj)

lemma lwg_edges_iff: "lwg_invar edges \<Longrightarrow> set edges = {(u,w,v). lwg_\<alpha> edges (u,v) = Fin w}"  
  by (auto simp: lwg_edge_iff)
  
lemma lwg_\<alpha>_ninf_finite[simp,intro!]: "finite {e. lwg_\<alpha> edges e \<noteq> \<infinity>}"  
  apply (rule finite_subset[where B="lwg_the_edge`set edges"])
  apply (force simp: lwg_\<alpha>_def split: if_splits) 
  by simp
    
  
lemma edges_refines_specE[wp_intro]: "\<lbrakk>lwg_invar edges\<rbrakk> \<Longrightarrow> refines (list_all2 (br lwg_the_edge)) (return edges) (spec (\<lambda>es. set es = wgraph.E (lwg_\<alpha> edges)))" 
  apply (rule return_refines_spec)
  apply (rule exI[where x="map lwg_the_edge edges"])
  apply (force
    simp: list_all2_map2 list_all2_same  br_def wgraph.E_def lwg_edges_iff
    simp: lwg_\<alpha>_ninf_conv 
    )
  done
  
 

section \<open>Implementation of weight map (and estimates) by RBT\<close>
  
type_synonym ('k,'w) wmi = "('k,'w) rbt"  
      
definition "wmi_\<alpha> t v \<equiv> case RBT.lookup t v of None \<Rightarrow> \<infinity> | Some w \<Rightarrow> Fin w"

definition "wmi_lookup \<equiv> wmi_\<alpha>"  
definition "wmi_empty \<equiv> RBT.empty"

definition "wmi_update t v w \<equiv> do {
  assert (w\<noteq>-\<infinity>);
  if (w=\<infinity>) then return (RBT.delete v t)
  else return (RBT.insert v (the_fin w) t)
}"

definition "wmi_entries \<equiv> RBT.entries"

lemma wmi_entries_correct[simp]:
  "(k,v)\<in>set (wmi_entries t) \<longleftrightarrow> wmi_\<alpha> t k = Fin v"
  "sorted (map fst (wmi_entries t))"
  "distinct (map fst (wmi_entries t))"
  unfolding wmi_entries_def wmi_\<alpha>_def
  by (auto split: option.splits simp flip: lookup_in_tree simp: sorted_entries distinct_entries)
  

lemma wmi_lookup_correct[simp]: "wmi_lookup t v = wmi_\<alpha> t v"
  unfolding wmi_lookup_def wmi_\<alpha>_def by simp

lemma wmi_empty_correct[simp]: "wmi_\<alpha> (wmi_empty) = (\<lambda>_. \<infinity>)"
  unfolding wmi_empty_def wmi_\<alpha>_def by simp

lemma wmi_update_wp: "w\<noteq>-\<infinity> \<Longrightarrow> wp (wmi_update t v w) (\<lambda>r. wmi_\<alpha> r = (wmi_\<alpha> t)(v:=w))"
  unfolding wmi_update_def wmi_\<alpha>_def 
  apply (intro wp_intro)
  by (auto)
  
lemmas [wp_intro] = wp_cons[OF wmi_update_wp]
  
lemma finite_ran_wmi_\<alpha>[simp,intro!]: "finite {e. wmi_\<alpha> wi e \<noteq> \<infinity>}"  
  apply (rule finite_subset[where B="dom (RBT.lookup wi)"])
  unfolding wmi_\<alpha>_def
  by (auto split: option.splits)
  

lemma minf_notin_wmi_\<alpha>: "-\<infinity> \<notin> range (wmi_\<alpha> wi)"
  unfolding wmi_\<alpha>_def
  by (auto split: option.splits)

lemma minf_notin_wmi_\<alpha>'[simp]: "wmi_\<alpha> wi x \<noteq> -\<infinity>"
  using minf_notin_wmi_\<alpha>[of wi]
  by (metis rangeI)

lemma wmi_\<alpha>_neq_inf_conv: "wmi_\<alpha> w x \<noteq> \<infinity> \<longleftrightarrow> (\<exists>y. wmi_\<alpha> w x = Fin y)"
  apply (cases "wmi_\<alpha> w x")
  by auto
    
section \<open>Concrete Graph\<close>  

(*
  The following locale sets the stage for the graph implementation:
  We have a graph given as both, an edge list and a weight map RBT.
*)    
locale rbt_wgraph = 
  fixes wi :: "('v::linorder \<times> 'v, 'w::weight) wmi"
  and v\<^sub>0 :: 'v
  and edges :: "('v \<times> 'w \<times> 'v) list"
  assumes edges_invar[simp]: "lwg_invar edges"
  assumes wi_eq[simp]: "wmi_\<alpha> wi = lwg_\<alpha> edges"
begin  

  sublocale wgraph_nmi "lwg_\<alpha> edges" v\<^sub>0
    by standard auto
    
  sublocale finite_wgraph_nmi "lwg_\<alpha> edges" v\<^sub>0
    apply standard
    unfolding E_def
    by (auto simp: )
    
end    
  

subsection \<open>Compute weight map from edge list\<close>  
definition "compute_wmi edges \<equiv> do {
  assert (lwg_invar edges);
  mfold (\<lambda>(u,w,v) wi. wmi_update wi (u,v) (Fin w)) edges wmi_empty
}"

lemma lwg_in_the_edge_conv: "(u,v)\<in>lwg_the_edge`set edges \<longleftrightarrow> lwg_\<alpha> edges (u,v) \<noteq> \<infinity>"
  unfolding lwg_\<alpha>_def
  by force 

lemma compute_wmi_correct1:
  assumes "lwg_invar edges"
  shows "wp (compute_wmi edges) (\<lambda>wi. wmi_\<alpha> wi = lwg_\<alpha> edges)"
  using assms
  unfolding compute_wmi_def case_prod_app lwg_invar_def
  apply (intro wp_intro wp_mfold[where I="\<lambda>es _ wi. wmi_\<alpha> wi = lwg_\<alpha> es \<and> lwg_invar es"])
  by (auto simp: lwg_in_the_edge_conv)

(*
  Computing the edge map establishes this rbt_wgraph
*)
lemma compute_wmi_correct[THEN wp_cons, wp_intro]:
  assumes "lwg_invar edges"
  shows "wp (compute_wmi edges) (\<lambda>wi. rbt_wgraph wi edges)"
  apply (rule compute_wmi_correct1[THEN wp_cons])
  apply (rule assms)
  apply unfold_locales
  apply (rule assms)
  .


subsection \<open>Compute size of V\<close>  
definition "compute_cardV edges \<equiv> length (remdups (map fst edges @ map (snd o snd) edges))"

lemma (in rbt_wgraph) compute_cardV_correct[simp]: "compute_cardV edges = card V"
  unfolding compute_cardV_def
  find_theorems length remdups
  apply (auto simp: length_remdups_card_conv)
  apply (rule arg_cong[where f=card])
  unfolding V_def
  unfolding E_def
  by (force simp: lwg_\<alpha>_ninf_conv lwg_edges_iff)
  
  
section \<open>Bellman-Ford with RBT and edge list\<close>  
    

definition "initial_esti v\<^sub>0 \<equiv> wmi_update wmi_empty v\<^sub>0 (0::('w::weight) extended)"

lemma (in rbt_wgraph) wp_initial_esti[THEN wp_cons, wp_intro]: 
  "wp (initial_esti v\<^sub>0) (\<lambda>r. wmi_\<alpha> r = initial_est)"
  unfolding initial_esti_def initial_est_def
  apply (intro wp_intro)
  by auto

  
definition "relaxi wi u v d \<equiv> 
  if wmi_lookup d v > wmi_lookup d u + wmi_lookup wi (u,v) then
    wmi_update d v (wmi_lookup d u + wmi_lookup wi (u,v))
  else return d
"  
  
lemma (in rbt_wgraph) wp_relaxi[THEN wp_cons, wp_intro]: 
  "wp (relaxi wi u v di) (\<lambda>r. wmi_\<alpha> r = relax u v (wmi_\<alpha> di))"
  unfolding relaxi_def relax_def
  apply (intro wp_intro)
  by (auto)
    
definition "incr_estimatei wi edges d \<equiv> do {
  let es = edges;

  mfold (\<lambda>(u,w,v) d. relaxi wi u v d) es d
}"
    
lemmas [rel_rule] = relI[of "br wmi_\<alpha>"]
  
lemma (in rbt_wgraph) incr_estimatei_refines[wp_intro]: 
  "d = wmi_\<alpha> di \<Longrightarrow> refines (br wmi_\<alpha>) (incr_estimatei wi edges di) (incr_estimate d)"
  unfolding incr_estimatei_def incr_estimate_def case_prod_app
  apply (rule wp_intro rel_rule brI | assumption)+
  apply simp
  apply (rule wp_intro rel_rule brI | assumption)+
  apply (erule relI)
  apply (rule wp_intro)+
  by (auto simp: br_def rel_def)
  
    
definition "can_relaxi edges di \<equiv> \<exists>(u,w,v)\<in>set edges. wmi_\<alpha> di u + Fin w < wmi_\<alpha> di v"
  
lemma (in rbt_wgraph) can_relaxi_refines[simp]: "can_relaxi edges di \<longleftrightarrow> can_relax (wmi_\<alpha> di)"
  unfolding can_relaxi_def can_relax_def relax_id_iff E_def
  by (force simp: lwg_\<alpha>_ninf_conv lwg_edges_iff)
  
    
(* 
  We start with an algorithm that assumes the 
  weight map and cardinality have already been computed.
  This will be easy to prove correct as refinement.
*)  
definition "bellman_ford_prei wi v\<^sub>0 edges n \<equiv> do {
  d \<leftarrow> initial_esti v\<^sub>0;

  (_,d) \<leftarrow> while (\<lambda>(i,d). return (i<n)) (\<lambda>(i,d). do {
    d \<leftarrow> incr_estimatei wi edges d;
    return (i+1,d)
  }) (0,d);

  if can_relaxi edges d then
    return None
  else 
    return (Some d)
}"

lemma (in rbt_wgraph) bellman_ford_prei_refines: 
  "n=n' \<Longrightarrow> refines (rel_option (br wmi_\<alpha>)) (bellman_ford_prei wi v\<^sub>0 edges n) (bellman_ford n')"
  unfolding bellman_ford_prei_def bellman_ford_def
  apply (intro wp_intro | rule rel_rule)+
  apply (rule brI; simp)
  apply (rule wp_intro rel_rule | simp)+
  apply (simp add: br_def)
  apply ((rule rel_rule | simp)+) []
  apply (rule rel_rule)
  apply (clarsimp simp: br_def)
  apply (rule relI; simp)
  apply (rule relI; simp)
  done

(* Assembling the correctness theorem *)

lemma (in rbt_wgraph) wp_bellman_ford_prei[THEN wp_cons, wp_intro]:
  assumes CV: "card V \<le> n+1"  
  shows "wp (bellman_ford_prei wi v\<^sub>0 edges n) (\<lambda>None \<Rightarrow> has_negative_cycle | Some d \<Rightarrow> \<not>has_negative_cycle \<and> wmi_\<alpha> d = \<delta> v\<^sub>0)"
proof -
  note bellman_ford_prei_refines[OF refl]
  also note bellman_ford_correct[OF CV]
  finally show ?thesis
    apply (rule refines_spec_wpI)
    by (auto split: option.splits simp: br_def option_rel_Some2)

qed  
  
(*
  Then we add computation of the weight map and the cardinality.
*)
definition "bellman_fordi v\<^sub>0 edges \<equiv> do {
  wi \<leftarrow> compute_wmi edges;
  let n = compute_cardV edges - 1;
  bellman_ford_prei wi v\<^sub>0 edges n
}"

context
  fixes edges :: "('v::linorder,'w::weight) lwg" and v\<^sub>0 :: 'v
begin

  interpretation wgraph "lwg_\<alpha> edges" v\<^sub>0 .

  lemma wp_bellman_fordi[THEN wp_cons, wp_intro]:
    shows "lwg_invar edges \<Longrightarrow> wp (bellman_fordi v\<^sub>0 edges) (\<lambda>None \<Rightarrow> has_negative_cycle | Some d \<Rightarrow> \<not>has_negative_cycle \<and> wmi_\<alpha> d = \<delta> v\<^sub>0)"
    unfolding bellman_fordi_def
    apply (intro wp_intro)
    apply simp
  proof goal_cases
    case (1 wi)
    then interpret rbt_wgraph wi v\<^sub>0 edges by blast
    
    show ?case
      apply (intro wp_intro)
      by auto
  qed
end
  
subsection \<open>Checking input invariant, and returning lists\<close>
  
definition "bellmann_ford_list edges v\<^sub>0 \<equiv> do {
  if lwg_invar edges then do {
    r \<leftarrow> bellman_fordi v\<^sub>0 edges;
    return (map_option wmi_entries r)
  } else
    return None

}"

context
  fixes edges :: "('v::linorder,'w::weight) lwg" and v\<^sub>0 :: 'v
begin

  interpretation wgraph "lwg_\<alpha> edges" v\<^sub>0 .

  lemma wp_bellmann_ford_list: "wp (bellmann_ford_list edges v\<^sub>0) 
    (\<lambda>None \<Rightarrow> \<not>lwg_invar edges \<or> has_negative_cycle 
      | Some ds \<Rightarrow> lwg_invar edges \<and> \<not>has_negative_cycle \<and> sorted (map fst ds) \<and> distinct (map fst ds) \<and> (\<forall>v w. (v,w)\<in>set ds \<longleftrightarrow> \<delta> v\<^sub>0 v = Fin w))"
    unfolding bellmann_ford_list_def
    apply (intro wp_intro)
    apply (simp_all split: option.splits)
    done
     
  lemmas [wp_intro] = wp_bellmann_ford_list[THEN wp_cons]  
    
end


section \<open>Exporting Haskell Code\<close>

lemma is_det_bellmann_ford_list[wp_intro]: "is_det (bellmann_ford_list edges v\<^sub>0)"
  unfolding bellmann_ford_list_def compute_wmi_def wmi_update_def 
    bellman_ford_prei_def 
    bellman_fordi_def initial_esti_def incr_estimatei_def relaxi_def case_prod_app
  by (intro wp_intro)

instance integer :: weight ..    

definition bellman_ford_list_int :: "(integer \<times> integer \<times> integer) list \<Rightarrow> integer \<Rightarrow> (integer \<times> integer) list option" 
  where "bellman_ford_list_int edges v\<^sub>0 \<equiv> dres (bellmann_ford_list edges v\<^sub>0)"
  
lemma bellmann_ford_list'_correct:
  "bellman_ford_list_int edges v\<^sub>0 = None \<Longrightarrow> \<not>lwg_invar edges \<or> wgraph.has_negative_cycle (lwg_\<alpha> edges) v\<^sub>0"
  "bellman_ford_list_int edges v\<^sub>0 = Some ds \<Longrightarrow> 
      lwg_invar edges 
    \<and> \<not>wgraph.has_negative_cycle (lwg_\<alpha> edges) v\<^sub>0 
    \<and> sorted (map fst ds) 
    \<and> distinct (map fst ds) 
    \<and> (\<forall>v w. (v,w)\<in>set ds \<longleftrightarrow> wgraph.\<delta> (lwg_\<alpha> edges) v\<^sub>0 v = Fin w)"
  unfolding bellman_ford_list_int_def
  using wp_det_dres[OF wp_bellmann_ford_list is_det_bellmann_ford_list, of edges v\<^sub>0]
  by auto
  
  
export_code bellman_ford_list_int in Haskell module_name Bellman_Ford file code
    

end
