theory Extended_Ext
imports "HOL-Library.Extended"
begin


fun the_fin where
  "the_fin (Fin x) = x"  
| "the_fin _ = undefined"  

lemma the_fin_inv[simp]: "\<lbrakk>w \<noteq> -\<infinity>; w \<noteq> \<infinity>\<rbrakk> \<Longrightarrow> Fin (the_fin w) = w"
  by (cases w) auto
  




instantiation extended :: (conditionally_complete_lattice) complete_lattice begin

  definition "Inf A \<equiv> (
    if A\<subseteq>{\<infinity>} then \<infinity>
    else if -\<infinity>\<in>A then -\<infinity>
    else if bdd_below (Fin -` A) then Fin (Inf (Fin -` A))
    else -\<infinity>
    )"
  
  definition "Sup A \<equiv> (
    if A\<subseteq>{-\<infinity>} then -\<infinity>
    else if \<infinity>\<in>A then \<infinity>
    else if bdd_above (Fin -` A) then Fin (Sup (Fin -` A))
    else \<infinity>
    )"

  instance
    apply standard
    unfolding Inf_extended_def Sup_extended_def top_extended_def bot_extended_def
    subgoal for x A by (cases x) (auto intro: cInf_lower)
    subgoal for A z  
      apply (cases z; clarsimp; intro conjI impI)
      subgoal by fastforce
      subgoal
        apply (rule cInf_greatest)
        apply (metis empty_iff insert_iff less_eq_extended.elims(2) subsetI vimageI)
        by fastforce
      subgoal by fastforce
      done
    subgoal for x A by (cases x) (auto intro: cSup_upper)
    subgoal for A z  
      apply (cases z; clarsimp; intro conjI impI)
      subgoal by fastforce
      subgoal
        apply (rule cSup_least)
        apply (metis emptyE extended.exhaust insertI1 subsetI vimageI2) 
        by fastforce
      subgoal by fastforce
      done
    subgoal by simp  
    subgoal by simp  
    done 

end    


instance extended :: ("{linorder,conditionally_complete_lattice}") complete_linorder
  by standard




lemma order_ext_less_inf_conv: "a<\<infinity> \<longleftrightarrow> a\<noteq>\<infinity>" for a :: "_::order extended"
  by (meson antisym_conv2 le_Pinf)
  

lemma Inf_eq_PInfty: "Inf A = \<infinity> \<longleftrightarrow> A={} \<or> A={\<infinity>}"  
  by (simp add: Inf_extended_def subset_singleton_iff)
    
lemma Inf_eq_MInfty: "-\<infinity>\<in>A \<Longrightarrow> Inf A = -\<infinity>"  
  by (auto simp: Inf_extended_def)
  
lemma Sup_eq_MInfty: "Sup A = -\<infinity> \<longleftrightarrow> A={} \<or> A={-\<infinity>}"  
  by (simp add: Sup_extended_def subset_singleton_iff)
  
lemma Sup_eq_PInfty: "\<infinity>\<in>A \<Longrightarrow> Sup A = \<infinity>"  
  by (auto simp: Sup_extended_def)
  

lemma inf_ne_zero[simp]: "\<infinity> \<noteq> 0" "0 \<noteq> \<infinity>" "-\<infinity> \<noteq> 0" "0 \<noteq> -\<infinity>"
  by (auto simp: zero_extended_def)


lemma plus_eq_inf_conv[simp]: "a + b = \<infinity> \<longleftrightarrow> a=\<infinity> \<or> b=\<infinity>"  
  by (cases a; cases b) auto

lemma plus_eq_minf_conv[simp]: "a + b = -\<infinity> \<longleftrightarrow> (a=-\<infinity> \<and> b\<noteq>\<infinity>) \<or> (a\<noteq>\<infinity> \<and> b=-\<infinity>)"
  by (cases a; cases b) auto

lemma infty_sum_eq[simp]:
  "a+\<infinity> = \<infinity>"
  "\<infinity>+a = \<infinity>"
  by auto  

  
    
lemma infty_eq_redirect[simp]: "NO_MATCH a \<infinity> \<Longrightarrow> NO_MATCH a (-\<infinity>) \<Longrightarrow> \<infinity>=a \<longleftrightarrow> a=\<infinity>" by blast 
lemma ninfty_eq_redirect[simp]: "NO_MATCH a \<infinity> \<Longrightarrow> NO_MATCH a (-\<infinity>) \<Longrightarrow> -\<infinity>=a \<longleftrightarrow> a=-\<infinity>" by blast 






end
