theory Integer_Add_Insts
imports Main
begin


instantiation integer :: lattice begin

  unbundle integer.lifting

  lift_definition sup_integer :: "integer \<Rightarrow> integer \<Rightarrow> integer" is "sup :: int \<Rightarrow> _" .
  lift_definition inf_integer :: "integer \<Rightarrow> integer \<Rightarrow> integer" is "inf :: int \<Rightarrow> _" .
  
  instance
    apply standard
    by (transfer, simp)+
  
  lemmas [code] = sup_integer_def inf_integer_def
    
end

lemma bdd_below_integer_param[transfer_rule]: "(rel_fun (rel_set pcr_integer) (=)) bdd_below bdd_below"
  unfolding bdd_below_def
  by transfer_prover

lemma bdd_above_integer_param[transfer_rule]: "(rel_fun (rel_set pcr_integer) (=)) bdd_above bdd_above"
  unfolding bdd_above_def
  by transfer_prover
            
instantiation integer :: conditionally_complete_lattice begin

  lift_definition Sup_integer :: "integer set \<Rightarrow> integer" is "Sup :: int set \<Rightarrow> _" .
  lift_definition Inf_integer :: "integer set \<Rightarrow> integer" is "Inf :: int set \<Rightarrow> _" .

  instance
    apply standard
    apply transfer
    apply (blast intro: cInf_lower)
    apply transfer
    apply (blast intro: cInf_greatest)
    apply transfer
    apply (blast intro: cSup_upper)
    apply transfer
    apply (blast intro: cSup_least)
    done

  declare [[code abort: "Sup::integer set \<Rightarrow> _" "Inf::integer set \<Rightarrow> _"]]  
    
end








end
