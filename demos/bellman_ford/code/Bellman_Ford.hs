{-# LANGUAGE EmptyDataDecls, RankNTypes, ScopedTypeVariables #-}

module Bellman_Ford(Nres, bellman_ford_list_int) where {

import Prelude ((==), (/=), (<), (<=), (>=), (>), (+), (-), (*), (/), (**),
  (>>=), (>>), (=<<), (&&), (||), (^), (^^), (.), ($), ($!), (++), (!!), Eq,
  error, id, return, not, fst, snd, map, filter, concat, concatMap, reverse,
  zip, null, takeWhile, dropWhile, all, any, Integer, negate, abs, divMod,
  String, Bool(True, False), Maybe(Nothing, Just));
import qualified Prelude;

data Num = One | Bit0 Num | Bit1 Num;

data Int = Zero_int | Pos Num | Neg Num;

one_int :: Int;
one_int = Pos One;

class One a where {
  one :: a;
};

instance One Int where {
  one = one_int;
};

class Zero a where {
  zero :: a;
};

instance Zero Int where {
  zero = Zero_int;
};

less_eq_num :: Num -> Num -> Bool;
less_eq_num (Bit1 m) (Bit0 n) = less_num m n;
less_eq_num (Bit1 m) (Bit1 n) = less_eq_num m n;
less_eq_num (Bit0 m) (Bit1 n) = less_eq_num m n;
less_eq_num (Bit0 m) (Bit0 n) = less_eq_num m n;
less_eq_num (Bit1 m) One = False;
less_eq_num (Bit0 m) One = False;
less_eq_num One n = True;

less_num :: Num -> Num -> Bool;
less_num (Bit1 m) (Bit0 n) = less_num m n;
less_num (Bit1 m) (Bit1 n) = less_num m n;
less_num (Bit0 m) (Bit1 n) = less_eq_num m n;
less_num (Bit0 m) (Bit0 n) = less_num m n;
less_num One (Bit1 n) = True;
less_num One (Bit0 n) = True;
less_num m One = False;

less_eq_int :: Int -> Int -> Bool;
less_eq_int (Neg k) (Neg l) = less_eq_num l k;
less_eq_int (Neg k) (Pos l) = True;
less_eq_int (Neg k) Zero_int = True;
less_eq_int (Pos k) (Neg l) = False;
less_eq_int (Pos k) (Pos l) = less_eq_num k l;
less_eq_int (Pos k) Zero_int = False;
less_eq_int Zero_int (Neg l) = False;
less_eq_int Zero_int (Pos l) = True;
less_eq_int Zero_int Zero_int = True;

class Ord a where {
  less_eq :: a -> a -> Bool;
  less :: a -> a -> Bool;
};

less_int :: Int -> Int -> Bool;
less_int (Neg k) (Neg l) = less_num l k;
less_int (Neg k) (Pos l) = True;
less_int (Neg k) Zero_int = True;
less_int (Pos k) (Neg l) = False;
less_int (Pos k) (Pos l) = less_num k l;
less_int (Pos k) Zero_int = False;
less_int Zero_int (Neg l) = False;
less_int Zero_int (Pos l) = True;
less_int Zero_int Zero_int = False;

instance Ord Int where {
  less_eq = less_eq_int;
  less = less_int;
};

class (One a, Zero a) => Zero_neq_one a where {
};

instance Zero_neq_one Int where {
};

data Nat = Zero_nat | Suc Nat;

one_nat :: Nat;
one_nat = Suc Zero_nat;

instance One Nat where {
  one = one_nat;
};

plus_nat :: Nat -> Nat -> Nat;
plus_nat (Suc m) n = plus_nat m (Suc n);
plus_nat Zero_nat n = n;

class Plus a where {
  plus :: a -> a -> a;
};

instance Plus Nat where {
  plus = plus_nat;
};

instance Zero Nat where {
  zero = Zero_nat;
};

less_eq_nat :: Nat -> Nat -> Bool;
less_eq_nat (Suc m) n = less_nat m n;
less_eq_nat Zero_nat n = True;

less_nat :: Nat -> Nat -> Bool;
less_nat m (Suc n) = less_eq_nat m n;
less_nat n Zero_nat = False;

instance Ord Nat where {
  less_eq = less_eq_nat;
  less = less_nat;
};

less_eq_prod :: forall a b. (Ord a, Ord b) => (a, b) -> (a, b) -> Bool;
less_eq_prod (x1, y1) (x2, y2) = less x1 x2 || less_eq x1 x2 && less_eq y1 y2;

less_prod :: forall a b. (Ord a, Ord b) => (a, b) -> (a, b) -> Bool;
less_prod (x1, y1) (x2, y2) = less x1 x2 || less_eq x1 x2 && less y1 y2;

instance (Ord a, Ord b) => Ord (a, b) where {
  less_eq = less_eq_prod;
  less = less_prod;
};

class (Ord a) => Preorder a where {
};

class (Preorder a) => Order a where {
};

instance (Preorder a, Preorder b) => Preorder (a, b) where {
};

instance (Order a, Order b) => Order (a, b) where {
};

class (Order a) => Linorder a where {
};

instance (Linorder a, Linorder b) => Linorder (a, b) where {
};

class Times a where {
  times :: a -> a -> a;
};

class (Times a) => Dvd a where {
};

instance Times Integer where {
  times = (\ a b -> a * b);
};

instance Dvd Integer where {
};

class Abs a where {
  absa :: a -> a;
};

instance Abs Integer where {
  absa = Prelude.abs;
};

one_integer :: Integer;
one_integer = (1 :: Integer);

instance One Integer where {
  one = one_integer;
};

sgn_integer :: Integer -> Integer;
sgn_integer k =
  (if k == (0 :: Integer) then (0 :: Integer)
    else (if k < (0 :: Integer) then (-1 :: Integer) else (1 :: Integer)));

class Sgn a where {
  sgn :: a -> a;
};

instance Sgn Integer where {
  sgn = sgn_integer;
};

class Uminus a where {
  uminus :: a -> a;
};

class Minus a where {
  minus :: a -> a -> a;
};

class (Plus a) => Semigroup_add a where {
};

class (Semigroup_add a) => Cancel_semigroup_add a where {
};

class (Semigroup_add a) => Ab_semigroup_add a where {
};

class (Ab_semigroup_add a, Cancel_semigroup_add a,
        Minus a) => Cancel_ab_semigroup_add a where {
};

class (Semigroup_add a, Zero a) => Monoid_add a where {
};

class (Ab_semigroup_add a, Monoid_add a) => Comm_monoid_add a where {
};

class (Cancel_ab_semigroup_add a,
        Comm_monoid_add a) => Cancel_comm_monoid_add a where {
};

class (Times a, Zero a) => Mult_zero a where {
};

class (Times a) => Semigroup_mult a where {
};

class (Ab_semigroup_add a, Semigroup_mult a) => Semiring a where {
};

class (Comm_monoid_add a, Mult_zero a, Semiring a) => Semiring_0 a where {
};

class (Cancel_comm_monoid_add a, Semiring_0 a) => Semiring_0_cancel a where {
};

class (Semigroup_mult a) => Ab_semigroup_mult a where {
};

class (Ab_semigroup_mult a, Semiring a) => Comm_semiring a where {
};

class (Comm_semiring a, Semiring_0 a) => Comm_semiring_0 a where {
};

class (Comm_semiring_0 a,
        Semiring_0_cancel a) => Comm_semiring_0_cancel a where {
};

class (One a, Times a) => Power a where {
};

class (Semigroup_mult a, Power a) => Monoid_mult a where {
};

class (One a, Semigroup_add a) => Numeral a where {
};

class (Monoid_mult a, Numeral a, Semiring a) => Semiring_numeral a where {
};

class (Semiring_numeral a, Semiring_0 a, Zero_neq_one a) => Semiring_1 a where {
};

class (Semiring_0_cancel a, Semiring_1 a) => Semiring_1_cancel a where {
};

class (Ab_semigroup_mult a, Monoid_mult a, Dvd a) => Comm_monoid_mult a where {
};

class (Comm_monoid_mult a, Comm_semiring_0 a,
        Semiring_1 a) => Comm_semiring_1 a where {
};

class (Comm_semiring_0_cancel a, Comm_semiring_1 a,
        Semiring_1_cancel a) => Comm_semiring_1_cancel a where {
};

class (Comm_semiring_1_cancel a) => Comm_semiring_1_cancel_crossproduct a where {
};

class (Semiring_0 a) => Semiring_no_zero_divisors a where {
};

class (Semiring_1 a,
        Semiring_no_zero_divisors a) => Semiring_1_no_zero_divisors a where {
};

class (Semiring_no_zero_divisors a) => Semiring_no_zero_divisors_cancel a where {
};

class (Cancel_semigroup_add a, Minus a, Monoid_add a,
        Uminus a) => Group_add a where {
};

class (Cancel_comm_monoid_add a, Group_add a) => Ab_group_add a where {
};

class (Ab_group_add a, Semiring_0_cancel a) => Ring a where {
};

class (Ring a,
        Semiring_no_zero_divisors_cancel a) => Ring_no_zero_divisors a where {
};

class (Group_add a, Numeral a) => Neg_numeral a where {
};

class (Neg_numeral a, Ring a, Semiring_1_cancel a) => Ring_1 a where {
};

class (Ring_1 a, Ring_no_zero_divisors a,
        Semiring_1_no_zero_divisors a) => Ring_1_no_zero_divisors a where {
};

class (Comm_semiring_0_cancel a, Ring a) => Comm_ring a where {
};

class (Comm_ring a, Comm_semiring_1_cancel a, Ring_1 a) => Comm_ring_1 a where {
};

class (Comm_semiring_1_cancel a,
        Semiring_1_no_zero_divisors a) => Semidom a where {
};

class (Comm_ring_1 a, Ring_1_no_zero_divisors a, Semidom a,
        Comm_semiring_1_cancel_crossproduct a) => Idom a where {
};

instance Plus Integer where {
  plus = (\ a b -> a + b);
};

instance Semigroup_add Integer where {
};

instance Cancel_semigroup_add Integer where {
};

instance Ab_semigroup_add Integer where {
};

instance Minus Integer where {
  minus = (\ a b -> a - b);
};

instance Cancel_ab_semigroup_add Integer where {
};

instance Zero Integer where {
  zero = (0 :: Integer);
};

instance Monoid_add Integer where {
};

instance Comm_monoid_add Integer where {
};

instance Cancel_comm_monoid_add Integer where {
};

instance Mult_zero Integer where {
};

instance Semigroup_mult Integer where {
};

instance Semiring Integer where {
};

instance Semiring_0 Integer where {
};

instance Semiring_0_cancel Integer where {
};

instance Ab_semigroup_mult Integer where {
};

instance Comm_semiring Integer where {
};

instance Comm_semiring_0 Integer where {
};

instance Comm_semiring_0_cancel Integer where {
};

instance Power Integer where {
};

instance Monoid_mult Integer where {
};

instance Numeral Integer where {
};

instance Semiring_numeral Integer where {
};

instance Zero_neq_one Integer where {
};

instance Semiring_1 Integer where {
};

instance Semiring_1_cancel Integer where {
};

instance Comm_monoid_mult Integer where {
};

instance Comm_semiring_1 Integer where {
};

instance Comm_semiring_1_cancel Integer where {
};

instance Comm_semiring_1_cancel_crossproduct Integer where {
};

instance Semiring_no_zero_divisors Integer where {
};

instance Semiring_1_no_zero_divisors Integer where {
};

instance Semiring_no_zero_divisors_cancel Integer where {
};

instance Uminus Integer where {
  uminus = negate;
};

instance Group_add Integer where {
};

instance Ab_group_add Integer where {
};

instance Ring Integer where {
};

instance Ring_no_zero_divisors Integer where {
};

instance Neg_numeral Integer where {
};

instance Ring_1 Integer where {
};

instance Ring_1_no_zero_divisors Integer where {
};

instance Comm_ring Integer where {
};

instance Comm_ring_1 Integer where {
};

instance Semidom Integer where {
};

instance Idom Integer where {
};

plus_num :: Num -> Num -> Num;
plus_num (Bit1 m) (Bit1 n) = Bit0 (plus_num (plus_num m n) One);
plus_num (Bit1 m) (Bit0 n) = Bit1 (plus_num m n);
plus_num (Bit1 m) One = Bit0 (plus_num m One);
plus_num (Bit0 m) (Bit1 n) = Bit1 (plus_num m n);
plus_num (Bit0 m) (Bit0 n) = Bit0 (plus_num m n);
plus_num (Bit0 m) One = Bit1 m;
plus_num One (Bit1 n) = Bit0 (plus_num n One);
plus_num One (Bit0 n) = Bit1 n;
plus_num One One = Bit0 One;

times_num :: Num -> Num -> Num;
times_num (Bit1 m) (Bit1 n) =
  Bit1 (plus_num (plus_num m n) (Bit0 (times_num m n)));
times_num (Bit1 m) (Bit0 n) = Bit0 (times_num (Bit1 m) n);
times_num (Bit0 m) (Bit1 n) = Bit0 (times_num m (Bit1 n));
times_num (Bit0 m) (Bit0 n) = Bit0 (Bit0 (times_num m n));
times_num One n = n;
times_num m One = m;

times_int :: Int -> Int -> Int;
times_int (Neg m) (Neg n) = Pos (times_num m n);
times_int (Neg m) (Pos n) = Neg (times_num m n);
times_int (Pos m) (Neg n) = Neg (times_num m n);
times_int (Pos m) (Pos n) = Pos (times_num m n);
times_int Zero_int l = Zero_int;
times_int k Zero_int = Zero_int;

uminus_int :: Int -> Int;
uminus_int (Neg m) = Pos m;
uminus_int (Pos m) = Neg m;
uminus_int Zero_int = Zero_int;

bitM :: Num -> Num;
bitM One = One;
bitM (Bit0 n) = Bit1 (bitM n);
bitM (Bit1 n) = Bit1 (Bit0 n);

dup :: Int -> Int;
dup (Neg n) = Neg (Bit0 n);
dup (Pos n) = Pos (Bit0 n);
dup Zero_int = Zero_int;

minus_int :: Int -> Int -> Int;
minus_int (Neg m) (Neg n) = sub n m;
minus_int (Neg m) (Pos n) = Neg (plus_num m n);
minus_int (Pos m) (Neg n) = Pos (plus_num m n);
minus_int (Pos m) (Pos n) = sub m n;
minus_int Zero_int l = uminus_int l;
minus_int k Zero_int = k;

sub :: Num -> Num -> Int;
sub (Bit0 m) (Bit1 n) = minus_int (dup (sub m n)) one_int;
sub (Bit1 m) (Bit0 n) = plus_int (dup (sub m n)) one_int;
sub (Bit1 m) (Bit1 n) = dup (sub m n);
sub (Bit0 m) (Bit0 n) = dup (sub m n);
sub One (Bit1 n) = Neg (Bit0 n);
sub One (Bit0 n) = Neg (bitM n);
sub (Bit1 m) One = Pos (Bit0 m);
sub (Bit0 m) One = Pos (bitM m);
sub One One = Zero_int;

plus_int :: Int -> Int -> Int;
plus_int (Neg m) (Neg n) = Neg (plus_num m n);
plus_int (Neg m) (Pos n) = sub n m;
plus_int (Pos m) (Neg n) = sub m n;
plus_int (Pos m) (Pos n) = Pos (plus_num m n);
plus_int Zero_int l = l;
plus_int k Zero_int = k;

abs_int :: Int -> Int;
abs_int i = (if less_int i Zero_int then uminus_int i else i);

divmod_step_int :: Int -> (Int, Int) -> (Int, Int);
divmod_step_int l qr =
  (case qr of {
    (q, r) ->
      (if less_eq_int (abs_int l) (abs_int r)
        then (plus_int (times_int (Pos (Bit0 One)) q) one_int, minus_int r l)
        else (times_int (Pos (Bit0 One)) q, r));
  });

divmod_int :: Num -> Num -> (Int, Int);
divmod_int (Bit1 m) (Bit1 n) =
  (if less_num m n then (Zero_int, Pos (Bit1 m))
    else divmod_step_int (Pos (Bit1 n)) (divmod_int (Bit1 m) (Bit0 (Bit1 n))));
divmod_int (Bit0 m) (Bit1 n) =
  (if less_eq_num m n then (Zero_int, Pos (Bit0 m))
    else divmod_step_int (Pos (Bit1 n)) (divmod_int (Bit0 m) (Bit0 (Bit1 n))));
divmod_int (Bit1 m) (Bit0 n) =
  (case divmod_int m n of {
    (q, r) -> (q, plus_int (times_int (Pos (Bit0 One)) r) one_int);
  });
divmod_int (Bit0 m) (Bit0 n) = (case divmod_int m n of {
                                 (q, r) -> (q, times_int (Pos (Bit0 One)) r);
                               });
divmod_int One (Bit1 n) = (Zero_int, Pos One);
divmod_int One (Bit0 n) = (Zero_int, Pos One);
divmod_int m One = (Pos m, Zero_int);

equal_num :: Num -> Num -> Bool;
equal_num (Bit0 x2) (Bit1 x3) = False;
equal_num (Bit1 x3) (Bit0 x2) = False;
equal_num One (Bit1 x3) = False;
equal_num (Bit1 x3) One = False;
equal_num One (Bit0 x2) = False;
equal_num (Bit0 x2) One = False;
equal_num (Bit1 x3) (Bit1 y3) = equal_num x3 y3;
equal_num (Bit0 x2) (Bit0 y2) = equal_num x2 y2;
equal_num One One = True;

equal_int :: Int -> Int -> Bool;
equal_int (Neg k) (Neg l) = equal_num k l;
equal_int (Neg k) (Pos l) = False;
equal_int (Neg k) Zero_int = False;
equal_int (Pos k) (Neg l) = False;
equal_int (Pos k) (Pos l) = equal_num k l;
equal_int (Pos k) Zero_int = False;
equal_int Zero_int (Neg l) = False;
equal_int Zero_int (Pos l) = False;
equal_int Zero_int Zero_int = True;

adjust_mod :: Num -> Int -> Int;
adjust_mod l r =
  (if equal_int r Zero_int then Zero_int else minus_int (Pos l) r);

modulo_int :: Int -> Int -> Int;
modulo_int (Neg m) (Neg n) = uminus_int (snd (divmod_int m n));
modulo_int (Pos m) (Neg n) = uminus_int (adjust_mod n (snd (divmod_int m n)));
modulo_int (Neg m) (Pos n) = adjust_mod n (snd (divmod_int m n));
modulo_int (Pos m) (Pos n) = snd (divmod_int m n);
modulo_int k (Neg One) = Zero_int;
modulo_int k (Pos One) = Zero_int;
modulo_int Zero_int k = Zero_int;
modulo_int k Zero_int = k;

of_bool :: forall a. (Zero_neq_one a) => Bool -> a;
of_bool True = one;
of_bool False = zero;

adjust_div :: (Int, Int) -> Int;
adjust_div (q, r) = plus_int q (of_bool (not (equal_int r Zero_int)));

divide_int :: Int -> Int -> Int;
divide_int (Neg m) (Neg n) = fst (divmod_int m n);
divide_int (Pos m) (Neg n) = uminus_int (adjust_div (divmod_int m n));
divide_int (Neg m) (Pos n) = uminus_int (adjust_div (divmod_int m n));
divide_int (Pos m) (Pos n) = fst (divmod_int m n);
divide_int k (Neg One) = uminus_int k;
divide_int k (Pos One) = k;
divide_int Zero_int k = Zero_int;
divide_int k Zero_int = Zero_int;

integer_of_int :: Int -> Integer;
integer_of_int k =
  (if less_int k Zero_int then negate (integer_of_int (uminus_int k))
    else (if equal_int k Zero_int then (0 :: Integer)
           else let {
                  l = (2 :: Integer) *
                        integer_of_int (divide_int k (Pos (Bit0 One)));
                  j = modulo_int k (Pos (Bit0 One));
                } in (if equal_int j Zero_int then l else l + (1 :: Integer))));

apsnd :: forall a b c. (a -> b) -> (c, a) -> (c, b);
apsnd f (x, y) = (x, f y);

divmod_integer :: Integer -> Integer -> (Integer, Integer);
divmod_integer k l =
  (if k == (0 :: Integer) then ((0 :: Integer), (0 :: Integer))
    else (if (0 :: Integer) < l
           then (if (0 :: Integer) < k then divMod (abs k) (abs l)
                  else (case divMod (abs k) (abs l) of {
                         (r, s) ->
                           (if s == (0 :: Integer)
                             then (negate r, (0 :: Integer))
                             else (negate r - (1 :: Integer), l - s));
                       }))
           else (if l == (0 :: Integer) then ((0 :: Integer), k)
                  else apsnd negate
                         (if k < (0 :: Integer) then divMod (abs k) (abs l)
                           else (case divMod (abs k) (abs l) of {
                                  (r, s) ->
                                    (if s == (0 :: Integer)
                                      then (negate r, (0 :: Integer))
                                      else (negate r - (1 :: Integer),
     negate l - s));
                                })))));

int_of_integer :: Integer -> Int;
int_of_integer k =
  (if k < (0 :: Integer) then uminus_int (int_of_integer (negate k))
    else (if k == (0 :: Integer) then Zero_int
           else (case divmod_integer k (2 :: Integer) of {
                  (l, j) ->
                    let {
                      la = times_int (Pos (Bit0 One)) (int_of_integer l);
                    } in (if j == (0 :: Integer) then la
                           else plus_int la one_int);
                })));

min :: forall a. (Ord a) => a -> a -> a;
min a b = (if less_eq a b then a else b);

inf_int :: Int -> Int -> Int;
inf_int = min;

map_fun :: forall a b c d. (a -> b) -> (c -> d) -> (b -> c) -> a -> d;
map_fun f g h = (g . h) . f;

inf_integera :: Integer -> Integer -> Integer;
inf_integera =
  map_fun int_of_integer (map_fun int_of_integer integer_of_int) inf_int;

class Inf a where {
  inf :: a -> a -> a;
};

instance Inf Integer where {
  inf = inf_integera;
};

max :: forall a. (Ord a) => a -> a -> a;
max a b = (if less_eq a b then b else a);

sup_int :: Int -> Int -> Int;
sup_int = max;

sup_integera :: Integer -> Integer -> Integer;
sup_integera =
  map_fun int_of_integer (map_fun int_of_integer integer_of_int) sup_int;

class Sup a where {
  sup :: a -> a -> a;
};

instance Sup Integer where {
  sup = sup_integera;
};

class (Abs a, Minus a, Uminus a, Zero a, Ord a) => Abs_if a where {
};

instance Ord Integer where {
  less_eq = (\ a b -> a <= b);
  less = (\ a b -> a < b);
};

instance Abs_if Integer where {
};

class (Semiring_1 a) => Semiring_char_0 a where {
};

class (Semiring_char_0 a, Ring_1 a) => Ring_char_0 a where {
};

instance Semiring_char_0 Integer where {
};

instance Ring_char_0 Integer where {
};

instance Preorder Integer where {
};

instance Order Integer where {
};

class (Sup a, Order a) => Semilattice_sup a where {
};

class (Inf a, Order a) => Semilattice_inf a where {
};

class (Semilattice_inf a, Semilattice_sup a) => Lattice a where {
};

instance Semilattice_sup Integer where {
};

instance Semilattice_inf Integer where {
};

instance Lattice Integer where {
};

instance Linorder Integer where {
};

class (Abs a, Sgn a, Idom a) => Idom_abs_sgn a where {
};

instance Idom_abs_sgn Integer where {
};

class (Ab_semigroup_add a, Order a) => Ordered_ab_semigroup_add a where {
};

class (Comm_monoid_add a,
        Ordered_ab_semigroup_add a) => Ordered_comm_monoid_add a where {
};

class (Ordered_comm_monoid_add a, Semiring a) => Ordered_semiring a where {
};

class (Ordered_semiring a, Semiring_0 a) => Ordered_semiring_0 a where {
};

class (Ordered_semiring_0 a,
        Semiring_0_cancel a) => Ordered_cancel_semiring a where {
};

class (Ordered_ab_semigroup_add a) => Strict_ordered_ab_semigroup_add a where {
};

class (Cancel_ab_semigroup_add a,
        Strict_ordered_ab_semigroup_add a) => Ordered_cancel_ab_semigroup_add a where {
};

class (Ordered_cancel_ab_semigroup_add a) => Ordered_ab_semigroup_add_imp_le a where {
};

class (Comm_monoid_add a,
        Strict_ordered_ab_semigroup_add a) => Strict_ordered_comm_monoid_add a where {
};

class (Ordered_cancel_ab_semigroup_add a, Ordered_comm_monoid_add a,
        Strict_ordered_comm_monoid_add a) => Ordered_cancel_comm_monoid_add a where {
};

class (Cancel_comm_monoid_add a, Ordered_ab_semigroup_add_imp_le a,
        Ordered_cancel_comm_monoid_add a) => Ordered_ab_semigroup_monoid_add_imp_le a where {
};

class (Ab_group_add a,
        Ordered_ab_semigroup_monoid_add_imp_le a) => Ordered_ab_group_add a where {
};

class (Ordered_ab_group_add a, Ordered_cancel_semiring a,
        Ring a) => Ordered_ring a where {
};

instance Ordered_ab_semigroup_add Integer where {
};

instance Ordered_comm_monoid_add Integer where {
};

instance Ordered_semiring Integer where {
};

instance Ordered_semiring_0 Integer where {
};

instance Ordered_cancel_semiring Integer where {
};

instance Strict_ordered_ab_semigroup_add Integer where {
};

instance Ordered_cancel_ab_semigroup_add Integer where {
};

instance Ordered_ab_semigroup_add_imp_le Integer where {
};

instance Strict_ordered_comm_monoid_add Integer where {
};

instance Ordered_cancel_comm_monoid_add Integer where {
};

instance Ordered_ab_semigroup_monoid_add_imp_le Integer where {
};

instance Ordered_ab_group_add Integer where {
};

instance Ordered_ring Integer where {
};

data Set a = Set [a] | Coset [a];

sup_integer :: Set Integer -> Integer;
sup_integer _ = error "Integer_Add_Insts.Sup_integer_inst.Sup_integer";

inf_integer :: Set Integer -> Integer;
inf_integer _ = error "Integer_Add_Insts.Inf_integer_inst.Inf_integer";

class Supa a where {
  supa :: Set a -> a;
};

class Infa a where {
  infa :: Set a -> a;
};

class (Infa a, Supa a, Lattice a) => Conditionally_complete_lattice a where {
};

class (Ordered_ab_semigroup_add a,
        Linorder a) => Linordered_ab_semigroup_add a where {
};

class (Linordered_ab_semigroup_add a,
        Ordered_ab_semigroup_add_imp_le a) => Linordered_cancel_ab_semigroup_add a where {
};

class (Linordered_cancel_ab_semigroup_add a,
        Ordered_ab_semigroup_monoid_add_imp_le a,
        Ordered_cancel_semiring a) => Linordered_semiring a where {
};

class (Linordered_semiring a) => Linordered_semiring_strict a where {
};

class (Order a, Zero_neq_one a) => Zero_less_one a where {
};

class (Linordered_semiring a, Semiring_1 a,
        Zero_less_one a) => Linordered_semiring_1 a where {
};

class (Linordered_semiring_1 a,
        Linordered_semiring_strict a) => Linordered_semiring_1_strict a where {
};

class (Abs a, Ordered_ab_group_add a) => Ordered_ab_group_add_abs a where {
};

class (Linordered_cancel_ab_semigroup_add a,
        Ordered_ab_group_add a) => Linordered_ab_group_add a where {
};

class (Linordered_ab_group_add a, Ordered_ab_group_add_abs a, Abs_if a,
        Linordered_semiring a, Ordered_ring a) => Linordered_ring a where {
};

class (Linordered_ring a, Linordered_semiring_strict a,
        Ring_no_zero_divisors a) => Linordered_ring_strict a where {
};

class (Comm_semiring_0 a, Ordered_semiring a) => Ordered_comm_semiring a where {
};

class (Comm_semiring_0_cancel a, Ordered_cancel_semiring a,
        Ordered_comm_semiring a) => Ordered_cancel_comm_semiring a where {
};

class (Linordered_semiring_strict a,
        Ordered_cancel_comm_semiring a) => Linordered_comm_semiring_strict a where {
};

class (Semiring_char_0 a, Linorder a, Comm_semiring_1 a,
        Ordered_comm_semiring a,
        Zero_less_one a) => Linordered_nonzero_semiring a where {
};

class (Linordered_comm_semiring_strict a, Linordered_nonzero_semiring a,
        Semidom a) => Linordered_semidom a where {
};

class (Comm_ring a, Ordered_cancel_comm_semiring a,
        Ordered_ring a) => Ordered_comm_ring a where {
};

class (Ordered_ab_group_add_abs a, Ordered_ring a) => Ordered_ring_abs a where {
};

class (Ring_char_0 a, Idom_abs_sgn a, Linordered_ring_strict a,
        Linordered_semidom a, Linordered_semiring_1_strict a,
        Ordered_comm_ring a, Ordered_ring_abs a) => Linordered_idom a where {
};

class (Conditionally_complete_lattice a, Linordered_idom a) => Weight a where {
};

instance Supa Integer where {
  supa = sup_integer;
};

instance Infa Integer where {
  infa = inf_integer;
};

instance Conditionally_complete_lattice Integer where {
};

instance Linordered_ab_semigroup_add Integer where {
};

instance Linordered_cancel_ab_semigroup_add Integer where {
};

instance Linordered_semiring Integer where {
};

instance Linordered_semiring_strict Integer where {
};

instance Zero_less_one Integer where {
};

instance Linordered_semiring_1 Integer where {
};

instance Linordered_semiring_1_strict Integer where {
};

instance Ordered_ab_group_add_abs Integer where {
};

instance Linordered_ab_group_add Integer where {
};

instance Linordered_ring Integer where {
};

instance Linordered_ring_strict Integer where {
};

instance Ordered_comm_semiring Integer where {
};

instance Ordered_cancel_comm_semiring Integer where {
};

instance Linordered_comm_semiring_strict Integer where {
};

instance Linordered_nonzero_semiring Integer where {
};

instance Linordered_semidom Integer where {
};

instance Ordered_comm_ring Integer where {
};

instance Ordered_ring_abs Integer where {
};

instance Linordered_idom Integer where {
};

instance Weight Integer where {
};

data Color = R | B;

data Rbta a b = Empty | Branch Color (Rbta a b) a b (Rbta a b);

newtype Rbt b a = RBT (Rbta b a);

data Extended a = Fin a | Pinf | Minf;

newtype Nres a = Nres_of_option (Maybe a);

empty :: forall a b. (Linorder a) => Rbt a b;
empty = RBT Empty;

balance :: forall a b. Rbta a b -> a -> b -> Rbta a b -> Rbta a b;
balance (Branch R a w x b) s t (Branch R c y z d) =
  Branch R (Branch B a w x b) s t (Branch B c y z d);
balance (Branch R (Branch R a w x b) s t c) y z Empty =
  Branch R (Branch B a w x b) s t (Branch B c y z Empty);
balance (Branch R (Branch R a w x b) s t c) y z (Branch B va vb vc vd) =
  Branch R (Branch B a w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch R Empty w x (Branch R b s t c)) y z Empty =
  Branch R (Branch B Empty w x b) s t (Branch B c y z Empty);
balance (Branch R (Branch B va vb vc vd) w x (Branch R b s t c)) y z Empty =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z Empty);
balance (Branch R Empty w x (Branch R b s t c)) y z (Branch B va vb vc vd) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch R (Branch B ve vf vg vh) w x (Branch R b s t c)) y z
  (Branch B va vb vc vd) =
  Branch R (Branch B (Branch B ve vf vg vh) w x b) s t
    (Branch B c y z (Branch B va vb vc vd));
balance Empty w x (Branch R b s t (Branch R c y z d)) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z d);
balance (Branch B va vb vc vd) w x (Branch R b s t (Branch R c y z d)) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z d);
balance Empty w x (Branch R (Branch R b s t c) y z Empty) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z Empty);
balance Empty w x (Branch R (Branch R b s t c) y z (Branch B va vb vc vd)) =
  Branch R (Branch B Empty w x b) s t (Branch B c y z (Branch B va vb vc vd));
balance (Branch B va vb vc vd) w x (Branch R (Branch R b s t c) y z Empty) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t (Branch B c y z Empty);
balance (Branch B va vb vc vd) w x
  (Branch R (Branch R b s t c) y z (Branch B ve vf vg vh)) =
  Branch R (Branch B (Branch B va vb vc vd) w x b) s t
    (Branch B c y z (Branch B ve vf vg vh));
balance Empty s t Empty = Branch B Empty s t Empty;
balance Empty s t (Branch B va vb vc vd) =
  Branch B Empty s t (Branch B va vb vc vd);
balance Empty s t (Branch v Empty vb vc Empty) =
  Branch B Empty s t (Branch v Empty vb vc Empty);
balance Empty s t (Branch v (Branch B ve vf vg vh) vb vc Empty) =
  Branch B Empty s t (Branch v (Branch B ve vf vg vh) vb vc Empty);
balance Empty s t (Branch v Empty vb vc (Branch B vf vg vh vi)) =
  Branch B Empty s t (Branch v Empty vb vc (Branch B vf vg vh vi));
balance Empty s t (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi))
  = Branch B Empty s t
      (Branch v (Branch B ve vj vk vl) vb vc (Branch B vf vg vh vi));
balance (Branch B va vb vc vd) s t Empty =
  Branch B (Branch B va vb vc vd) s t Empty;
balance (Branch B va vb vc vd) s t (Branch B ve vf vg vh) =
  Branch B (Branch B va vb vc vd) s t (Branch B ve vf vg vh);
balance (Branch B va vb vc vd) s t (Branch v Empty vf vg Empty) =
  Branch B (Branch B va vb vc vd) s t (Branch v Empty vf vg Empty);
balance (Branch B va vb vc vd) s t (Branch v (Branch B vi vj vk vl) vf vg Empty)
  = Branch B (Branch B va vb vc vd) s t
      (Branch v (Branch B vi vj vk vl) vf vg Empty);
balance (Branch B va vb vc vd) s t (Branch v Empty vf vg (Branch B vj vk vl vm))
  = Branch B (Branch B va vb vc vd) s t
      (Branch v Empty vf vg (Branch B vj vk vl vm));
balance (Branch B va vb vc vd) s t
  (Branch v (Branch B vi vn vo vp) vf vg (Branch B vj vk vl vm)) =
  Branch B (Branch B va vb vc vd) s t
    (Branch v (Branch B vi vn vo vp) vf vg (Branch B vj vk vl vm));
balance (Branch v Empty vb vc Empty) s t Empty =
  Branch B (Branch v Empty vb vc Empty) s t Empty;
balance (Branch v Empty vb vc (Branch B ve vf vg vh)) s t Empty =
  Branch B (Branch v Empty vb vc (Branch B ve vf vg vh)) s t Empty;
balance (Branch v (Branch B vf vg vh vi) vb vc Empty) s t Empty =
  Branch B (Branch v (Branch B vf vg vh vi) vb vc Empty) s t Empty;
balance (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) s t Empty
  = Branch B (Branch v (Branch B vf vg vh vi) vb vc (Branch B ve vj vk vl)) s t
      Empty;
balance (Branch v Empty vf vg Empty) s t (Branch B va vb vc vd) =
  Branch B (Branch v Empty vf vg Empty) s t (Branch B va vb vc vd);
balance (Branch v Empty vf vg (Branch B vi vj vk vl)) s t (Branch B va vb vc vd)
  = Branch B (Branch v Empty vf vg (Branch B vi vj vk vl)) s t
      (Branch B va vb vc vd);
balance (Branch v (Branch B vj vk vl vm) vf vg Empty) s t (Branch B va vb vc vd)
  = Branch B (Branch v (Branch B vj vk vl vm) vf vg Empty) s t
      (Branch B va vb vc vd);
balance (Branch v (Branch B vj vk vl vm) vf vg (Branch B vi vn vo vp)) s t
  (Branch B va vb vc vd) =
  Branch B (Branch v (Branch B vj vk vl vm) vf vg (Branch B vi vn vo vp)) s t
    (Branch B va vb vc vd);

paint :: forall a b. Color -> Rbta a b -> Rbta a b;
paint c Empty = Empty;
paint c (Branch uu l k v r) = Branch c l k v r;

balance_right :: forall a b. Rbta a b -> a -> b -> Rbta a b -> Rbta a b;
balance_right a k x (Branch R b s y c) = Branch R a k x (Branch B b s y c);
balance_right (Branch B a k x b) s y Empty =
  balance (Branch R a k x b) s y Empty;
balance_right (Branch B a k x b) s y (Branch B va vb vc vd) =
  balance (Branch R a k x b) s y (Branch B va vb vc vd);
balance_right (Branch R a k x (Branch B b s y c)) t z Empty =
  Branch R (balance (paint R a) k x b) s y (Branch B c t z Empty);
balance_right (Branch R a k x (Branch B b s y c)) t z (Branch B va vb vc vd) =
  Branch R (balance (paint R a) k x b) s y
    (Branch B c t z (Branch B va vb vc vd));
balance_right Empty k x Empty = Empty;
balance_right (Branch R va vb vc Empty) k x Empty = Empty;
balance_right (Branch R va vb vc (Branch R ve vf vg vh)) k x Empty = Empty;
balance_right Empty k x (Branch B va vb vc vd) = Empty;
balance_right (Branch R ve vf vg Empty) k x (Branch B va vb vc vd) = Empty;
balance_right (Branch R ve vf vg (Branch R vi vj vk vl)) k x
  (Branch B va vb vc vd) = Empty;

balance_left :: forall a b. Rbta a b -> a -> b -> Rbta a b -> Rbta a b;
balance_left (Branch R a k x b) s y c = Branch R (Branch B a k x b) s y c;
balance_left Empty k x (Branch B a s y b) =
  balance Empty k x (Branch R a s y b);
balance_left (Branch B va vb vc vd) k x (Branch B a s y b) =
  balance (Branch B va vb vc vd) k x (Branch R a s y b);
balance_left Empty k x (Branch R (Branch B a s y b) t z c) =
  Branch R (Branch B Empty k x a) s y (balance b t z (paint R c));
balance_left (Branch B va vb vc vd) k x (Branch R (Branch B a s y b) t z c) =
  Branch R (Branch B (Branch B va vb vc vd) k x a) s y
    (balance b t z (paint R c));
balance_left Empty k x Empty = Empty;
balance_left Empty k x (Branch R Empty vb vc vd) = Empty;
balance_left Empty k x (Branch R (Branch R ve vf vg vh) vb vc vd) = Empty;
balance_left (Branch B va vb vc vd) k x Empty = Empty;
balance_left (Branch B va vb vc vd) k x (Branch R Empty vf vg vh) = Empty;
balance_left (Branch B va vb vc vd) k x
  (Branch R (Branch R vi vj vk vl) vf vg vh) = Empty;

combine :: forall a b. Rbta a b -> Rbta a b -> Rbta a b;
combine Empty x = x;
combine (Branch v va vb vc vd) Empty = Branch v va vb vc vd;
combine (Branch R a k x b) (Branch R c s y d) =
  (case combine b c of {
    Empty -> Branch R a k x (Branch R Empty s y d);
    Branch R b2 t z c2 -> Branch R (Branch R a k x b2) t z (Branch R c2 s y d);
    Branch B b2 t z c2 -> Branch R a k x (Branch R (Branch B b2 t z c2) s y d);
  });
combine (Branch B a k x b) (Branch B c s y d) =
  (case combine b c of {
    Empty -> balance_left a k x (Branch B Empty s y d);
    Branch R b2 t z c2 -> Branch R (Branch B a k x b2) t z (Branch B c2 s y d);
    Branch B b2 t z c2 ->
      balance_left a k x (Branch B (Branch B b2 t z c2) s y d);
  });
combine (Branch B va vb vc vd) (Branch R b k x c) =
  Branch R (combine (Branch B va vb vc vd) b) k x c;
combine (Branch R a k x b) (Branch B va vb vc vd) =
  Branch R a k x (combine b (Branch B va vb vc vd));

rbt_del :: forall a b. (Ord a) => a -> Rbta a b -> Rbta a b;
rbt_del x Empty = Empty;
rbt_del x (Branch c a y s b) =
  (if less x y then rbt_del_from_left x a y s b
    else (if less y x then rbt_del_from_right x a y s b else combine a b));

rbt_del_from_left ::
  forall a b. (Ord a) => a -> Rbta a b -> a -> b -> Rbta a b -> Rbta a b;
rbt_del_from_left x (Branch B lt z v rt) y s b =
  balance_left (rbt_del x (Branch B lt z v rt)) y s b;
rbt_del_from_left x Empty y s b = Branch R (rbt_del x Empty) y s b;
rbt_del_from_left x (Branch R va vb vc vd) y s b =
  Branch R (rbt_del x (Branch R va vb vc vd)) y s b;

rbt_del_from_right ::
  forall a b. (Ord a) => a -> Rbta a b -> a -> b -> Rbta a b -> Rbta a b;
rbt_del_from_right x a y s (Branch B lt z v rt) =
  balance_right a y s (rbt_del x (Branch B lt z v rt));
rbt_del_from_right x a y s Empty = Branch R a y s (rbt_del x Empty);
rbt_del_from_right x a y s (Branch R va vb vc vd) =
  Branch R a y s (rbt_del x (Branch R va vb vc vd));

rbt_delete :: forall a b. (Ord a) => a -> Rbta a b -> Rbta a b;
rbt_delete k t = paint B (rbt_del k t);

impl_of :: forall b a. (Linorder b) => Rbt b a -> Rbta b a;
impl_of (RBT x) = x;

delete :: forall a b. (Linorder a) => a -> Rbt a b -> Rbt a b;
delete xb xc = RBT (rbt_delete xb (impl_of xc));

rbt_ins ::
  forall a b. (Ord a) => (a -> b -> b -> b) -> a -> b -> Rbta a b -> Rbta a b;
rbt_ins f k v Empty = Branch R Empty k v Empty;
rbt_ins f k v (Branch B l x y r) =
  (if less k x then balance (rbt_ins f k v l) x y r
    else (if less x k then balance l x y (rbt_ins f k v r)
           else Branch B l x (f k y v) r));
rbt_ins f k v (Branch R l x y r) =
  (if less k x then Branch R (rbt_ins f k v l) x y r
    else (if less x k then Branch R l x y (rbt_ins f k v r)
           else Branch R l x (f k y v) r));

rbt_insert_with_key ::
  forall a b. (Ord a) => (a -> b -> b -> b) -> a -> b -> Rbta a b -> Rbta a b;
rbt_insert_with_key f k v t = paint B (rbt_ins f k v t);

rbt_insert :: forall a b. (Ord a) => a -> b -> Rbta a b -> Rbta a b;
rbt_insert = rbt_insert_with_key (\ _ _ nv -> nv);

insert :: forall a b. (Linorder a) => a -> b -> Rbt a b -> Rbt a b;
insert xc xd xe = RBT (rbt_insert xc xd (impl_of xe));

rbt_lookup :: forall a b. (Ord a) => Rbta a b -> a -> Maybe b;
rbt_lookup Empty k = Nothing;
rbt_lookup (Branch uu l x y r) k =
  (if less k x then rbt_lookup l k
    else (if less x k then rbt_lookup r k else Just y));

lookup :: forall a b. (Linorder a) => Rbt a b -> a -> Maybe b;
lookup x = rbt_lookup (impl_of x);

member :: forall a. (Eq a) => [a] -> a -> Bool;
member [] y = False;
member (x : xs) y = x == y || member xs y;

gen_entries :: forall a b. [((a, b), Rbta a b)] -> Rbta a b -> [(a, b)];
gen_entries kvts (Branch c l k v r) = gen_entries (((k, v), r) : kvts) l;
gen_entries ((kv, t) : kvts) Empty = kv : gen_entries kvts t;
gen_entries [] Empty = [];

entriesa :: forall a b. Rbta a b -> [(a, b)];
entriesa = gen_entries [];

entries :: forall a b. (Linorder a) => Rbt a b -> [(a, b)];
entries x = entriesa (impl_of x);

remdups :: forall a. (Eq a) => [a] -> [a];
remdups [] = [];
remdups (x : xs) = (if member xs x then remdups xs else x : remdups xs);

distinct :: forall a. (Eq a) => [a] -> Bool;
distinct [] = True;
distinct (x : xs) = not (member xs x) && distinct xs;

gen_length :: forall a. Nat -> [a] -> Nat;
gen_length n (x : xs) = gen_length (Suc n) xs;
gen_length n [] = n;

plus_extended :: forall a. (Plus a) => Extended a -> Extended a -> Extended a;
plus_extended (Fin x) (Fin y) = Fin (plus x y);
plus_extended (Fin x) Pinf = Pinf;
plus_extended Pinf (Fin x) = Pinf;
plus_extended Pinf Pinf = Pinf;
plus_extended Minf (Fin y) = Minf;
plus_extended (Fin x) Minf = Minf;
plus_extended Minf Minf = Minf;
plus_extended Minf Pinf = Pinf;
plus_extended Pinf Minf = Pinf;

less_eq_extended :: forall a. (Order a) => Extended a -> Extended a -> Bool;
less_eq_extended (Fin x) (Fin y) = less_eq x y;
less_eq_extended uu Pinf = True;
less_eq_extended Minf (Fin v) = True;
less_eq_extended Minf Minf = True;
less_eq_extended Pinf (Fin v) = False;
less_eq_extended Pinf Minf = False;
less_eq_extended (Fin v) Minf = False;

less_extended :: forall a. (Order a) => Extended a -> Extended a -> Bool;
less_extended x y = less_eq_extended x y && not (less_eq_extended y x);

returna :: forall a. a -> Nres a;
returna x = Nres_of_option (Just x);

equal_extended :: forall a. (Eq a) => Extended a -> Extended a -> Bool;
equal_extended Pinf Minf = False;
equal_extended Minf Pinf = False;
equal_extended (Fin x1) Minf = False;
equal_extended Minf (Fin x1) = False;
equal_extended (Fin x1) Pinf = False;
equal_extended Pinf (Fin x1) = False;
equal_extended (Fin x1) (Fin y1) = x1 == y1;
equal_extended Minf Minf = True;
equal_extended Pinf Pinf = True;

fail :: forall a. Nres a;
fail = Nres_of_option Nothing;

bind1 :: forall a b. Maybe a -> (a -> Nres b) -> Nres b;
bind1 m f = (case m of {
              Nothing -> fail;
              Just a -> f a;
            });

bind_nres :: forall a b. Nres a -> (a -> Nres b) -> Nres b;
bind_nres (Nres_of_option m) f = bind1 m f;

assert :: Bool -> Nres ();
assert p = (if p then returna () else fail);

the_fin :: forall a. Extended a -> a;
the_fin (Fin x) = x;
the_fin Pinf = error "undefined";
the_fin Minf = error "undefined";

wmi_update ::
  forall a b.
    (Linorder a, Eq b) => Rbt a b -> a -> Extended b -> Nres (Rbt a b);
wmi_update t v w =
  bind_nres (assert (not (equal_extended w Minf)))
    (\ _ ->
      (if equal_extended w Pinf then returna (delete v t)
        else returna (insert v (the_fin w) t)));

wmi_alpha :: forall a b. (Linorder a) => Rbt a b -> a -> Extended b;
wmi_alpha t v = (case lookup t v of {
                  Nothing -> Pinf;
                  Just a -> Fin a;
                });

wmi_lookup :: forall a b. (Linorder a) => Rbt a b -> a -> Extended b;
wmi_lookup = wmi_alpha;

relaxi ::
  forall a b.
    (Linorder a, Plus b, Eq b,
      Order b) => Rbt (a, a) b -> a -> a -> Rbt a b -> Nres (Rbt a b);
relaxi wi u v d =
  (if less_extended (plus_extended (wmi_lookup d u) (wmi_lookup wi (u, v)))
        (wmi_lookup d v)
    then wmi_update d v (plus_extended (wmi_lookup d u) (wmi_lookup wi (u, v)))
    else returna d);

ifM :: forall a. Nres Bool -> Nres a -> Nres a -> Nres a;
ifM b c d = bind_nres b (\ bb -> (if bb then c else d));

dres :: forall a. Nres a -> a;
dres (Nres_of_option (Just x)) = x;

lwg_the_edge :: forall a b. (a, (b, a)) -> (a, a);
lwg_the_edge (u, (uu, v)) = (u, v);

lwg_invar :: forall a b. (Eq a) => [(a, (b, a))] -> Bool;
lwg_invar edges = distinct (map lwg_the_edge edges);

wmi_empty :: forall a b. (Linorder a) => Rbt a b;
wmi_empty = empty;

mfold :: forall a b. (a -> b -> Nres b) -> [a] -> b -> Nres b;
mfold f [] s = returna s;
mfold f (x : xs) s = bind_nres (f x s) (mfold f xs);

while :: forall a. (a -> Nres Bool) -> (a -> Nres a) -> a -> Nres a;
while b c s = ifM (b s) (bind_nres (c s) (while b c)) (returna s);

can_relaxi ::
  forall a b. (Linorder a, Plus b, Order b) => [(a, (b, a))] -> Rbt a b -> Bool;
can_relaxi edges di =
  any (\ (u, (w, v)) ->
        less_extended (plus_extended (wmi_alpha di u) (Fin w)) (wmi_alpha di v))
    edges;

compute_wmi ::
  forall a b. (Eq a, Linorder a, Eq b) => [(a, (b, a))] -> Nres (Rbt (a, a) b);
compute_wmi edges =
  bind_nres (assert (lwg_invar edges))
    (\ _ ->
      mfold (\ (u, (w, v)) wi -> wmi_update wi (u, v) (Fin w)) edges wmi_empty);

wmi_entries :: forall a b. (Linorder a) => Rbt a b -> [(a, b)];
wmi_entries = entries;

map_option :: forall a b. (a -> b) -> Maybe a -> Maybe b;
map_option f Nothing = Nothing;
map_option f (Just x2) = Just (f x2);

zero_extended :: forall a. (Zero a) => Extended a;
zero_extended = Fin zero;

initial_esti :: forall a b. (Linorder a, Weight b, Eq b) => a -> Nres (Rbt a b);
initial_esti v_0 = wmi_update wmi_empty v_0 zero_extended;

incr_estimatei ::
  forall a b c.
    (Linorder a, Plus b, Eq b,
      Order b) => Rbt (a, a) b -> [(a, (c, a))] -> Rbt a b -> Nres (Rbt a b);
incr_estimatei wi edges d = let {
                              es = edges;
                            } in mfold (\ (u, (_, a)) -> relaxi wi u a) es d;

bellman_ford_prei ::
  forall a b c.
    (Linorder a, Weight b, Eq b, One c, Plus c, Zero c,
      Ord c) => Rbt (a, a) b ->
                  a -> [(a, (b, a))] -> c -> Nres (Maybe (Rbt a b));
bellman_ford_prei wi v_0 edges n =
  bind_nres (initial_esti v_0)
    (\ d ->
      bind_nres
        (while (\ (i, _) -> returna (less i n))
          (\ (i, da) ->
            bind_nres (incr_estimatei wi edges da)
              (\ db -> returna (plus i one, db)))
          (zero, d))
        (\ (_, da) ->
          (if can_relaxi edges da then returna Nothing
            else returna (Just da))));

minus_nat :: Nat -> Nat -> Nat;
minus_nat (Suc m) (Suc n) = minus_nat m n;
minus_nat Zero_nat n = Zero_nat;
minus_nat m Zero_nat = m;

size_list :: forall a. [a] -> Nat;
size_list = gen_length Zero_nat;

compute_cardV :: forall a b. (Eq a) => [(a, (b, a))] -> Nat;
compute_cardV edges =
  size_list (remdups (map fst edges ++ map (snd . snd) edges));

bellman_fordi ::
  forall a b.
    (Eq a, Linorder a, Weight b,
      Eq b) => a -> [(a, (b, a))] -> Nres (Maybe (Rbt a b));
bellman_fordi v_0 edges =
  bind_nres (compute_wmi edges)
    (\ wi -> let {
               a = minus_nat (compute_cardV edges) one_nat;
             } in bellman_ford_prei wi v_0 edges a);

bellmann_ford_list ::
  forall a b.
    (Eq a, Linorder a, Weight b,
      Eq b) => [(a, (b, a))] -> a -> Nres (Maybe [(a, b)]);
bellmann_ford_list edges v_0 =
  (if lwg_invar edges
    then bind_nres (bellman_fordi v_0 edges)
           (\ r -> returna (map_option wmi_entries r))
    else returna Nothing);

bellman_ford_list_int ::
  [(Integer, (Integer, Integer))] -> Integer -> Maybe [(Integer, Integer)];
bellman_ford_list_int edges v_0 = dres (bellmann_ford_list edges v_0);

}
