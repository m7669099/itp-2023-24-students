#!/bin/gawk -f
BEGIN {
  print "module Graphs where {"
  num=1
}

BEGINFILE {
  gname=FILENAME

  sub(/\.d$/,"",gname)

  gsub(/[^A-Za-z0-9]/,"_",gname)

  if (! gname) {
    gname="graph_" num
    num=num+1
  }

  printf("%s = [",gname)
  first=1
}

gname && $1 == "a" {
  if (!first) printf(", ")
  first=0
  printf("(%d,(%d,%d))",$2,$3,$5)
}

ENDFILE {
  print("];")
  gname=0
}

END {
  print("}")
}
