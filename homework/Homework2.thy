chapter \<open>Homework 2\<close>
theory Homework2 (* This file must be called Homework2.thy *)
imports 
  Main 
  Homework_Lib (* Homework_Lib.thy must be in the same folder as this file! *)
begin

text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #2
  RELEASED: Fri, Nov 17 2023
  DUE:      Fri, Nov 24, 2023, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
  
  This homework is worth 40 points.
\<close>

section \<open>General Hints\<close>

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...
\<close>


section \<open>1. Lists\<close>

subsection \<open>Count (10p)\<close> 
text \<open>
  It is another day at the cookie factory and you are the on-call cookie engineer.
  Something has gone wrong and you have to count the salted cookies out of the cookie
  stacks. Being a good cookie engineer you decide that you want to make a reusable
  function so that you may count any type of cookie.
\<close>

text \<open> 1a)
  Define a function of type \<^typ>\<open>('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> nat\<close> to count elements of a
  list that satisfy predicate P. Define it recursively over the argument list. (2p)
\<close>

text \<open>Hint: remember to put parenthesis around if expressions\<close>

fun count :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> nat" where
"count _ _ = \<llangle>replace with your solution!\<rrangle>"

text \<open> 1b)
  This is way more efficient than @{term "length (filter P xs)"}, but will the result
  be the same? (2p)
\<close>

lemma "count P xs = length (filter P xs)"
oops

text \<open> 1c)
  Obviously, salted caramel cookies are also salted cookies.
  This means that the count of salted caramel should be less than the salted count.

  Prove the following lemma about predicates \<open>P\<close> and \<open>P'\<close>. 
  (Think \<open>P\<close> describing a salted caramel cookie, and \<open>P'\<close> describing any salted cookie) (3p)
\<close>

lemma "(\<forall>x. P x \<longrightarrow> P' x) \<Longrightarrow> count P xs \<le> count P' xs"
oops

text \<open> 1d)
  Also make sure that all the cookies in the stacks are either salty or not. (2p)
\<close>
  
lemma "count P xs + count (\<lambda>x. \<not>P x) xs = length xs"
oops

subsection \<open>2. Maximum salt flavour (10p + 3 bonus)\<close>

text \<open>
  Your boss is running towards you. He has a phone in his hand and looks worried.

  "The mixers have been running salt through the sugar dispenser. How many salted
  cookies are in the stacks? Just tell me the maximum number through all the stacks."
  
  Define a function to find the maximum element of a list. Your input is a list
  of natural numbers, each number represents the number of salty cookies in a stack.
\<close>

text\<open> 2a)
  Define the function recursively over the argument list. (3p)
\<close>

fun mymax :: "nat list \<Rightarrow> nat" where
"mymax _ = \<llangle>replace with your solution!\<rrangle>"

text \<open> 2b)
  Your boss is looking very worried. Time to make sure that you will give a useful
  answer! As proving your @{text mymax} correct is hard you decide to give a weaker
  statement: @{text mymax} computes an over-approximation. (3p)
\<close>

lemma "x \<in> set xs \<Longrightarrow> x \<le> mymax xs"
oops


text \<open> 2c)
  You tell your boss the result and he storms off without saying thanks. You make a
  mental note to hide some salty cookies in his personal sugary stack.
  
  As you are a very rigorous and mathematically inclined person, you cannot stop thinking 
  of how to exactly specify and prove that \<^const>\<open>mymax\<close> is actually correct.
  Finally, you come up with something like: 
    for non-empty lists, the value returned by mymax should be contained in the list.

  As your boss is already satisfied, and the correctness proof is a bit more complicated than usual,
  this is a bonus question:
  
  For 3 BONUS POINTS, state and prove a lemma that 
  for non-empty lists, the value returned by mymax is contained in the list.
  You will need an auxiliary lemma! (3 bonus points)
\<close>



lemma mymax_precise: "\<llangle>replace with your solution!\<rrangle>"
oops  

  
text \<open> 
  After spending some time to prove your function correct, you remember
  about @{term fold}. A fold takes a function, a list, and a starting value to compute
  a result as follows.

  fold f [] s = s
  fold f (x # xs) s = fold f xs (f x s)
\<close>
 
thm fold_simps (* Use thm to inspect the above definition! *)
 
text \<open> 2d)
  If you use @{term max} as the function and a starting value of 0 you can compute
  the maximum value of a list. (4p)
\<close>

text \<open>Hint: you will need an additional lemma, generalizing the original one for induction.\<close>



lemma "mymax xs = fold max xs 0"  
oops

subsection \<open>3. Map (10p)\<close>

text \<open>
  After all the folding you could really do some mapping!

  You write down a simple map definition but soon realize that
  it reverses the resulting list as you go through it.
\<close>

fun tmap where
  "tmap f [] acc = acc"  
| "tmap f (x#xs) acc = tmap f xs (f x # acc)"

text \<open>
  This is unfortunate. But wait a minute! It only reverses the input
  list and not the accumulator.
\<close>

text \<open>Hint: you will need an auxiliary lemma.\<close>



text \<open>So it will work if we give it the empty list as accumulator, and accept that we'll get the reversed list as result.\<close>

lemma "tmap f xs [] = rev (map f xs)"
oops
  

subsection \<open>4. Complete binary tree (10p)\<close>

text \<open>
  Enough about lists! They are boring anyway with their one dimensional
  simplicity.

  You look around the production line and see that some employee has
  arranged the boxes in a tree fashion!

  You remember the binary tree data-type from some long ago lecture on theorem proving:
\<close>

datatype t = Leaf | Node t t


text \<open> 4a)
  The base of your tree will need boxes to sustain the one above.
  You need to know how many leaves there will be in a tree to know
  if you can build it. (2p)
\<close>

fun count_leaves :: "t \<Rightarrow> nat" where
  (* Add your equations here, to compute the number of \<^const>\<open>Leaf\<close>s in a tree *)
"count_leaves _ = \<llangle>replace with your solution!\<rrangle>"

text\<open>Make sure the output is correct!\<close>
value "count_leaves (Node (Node (Leaf) (Leaf)) (Leaf)) = 3"

text \<open> 4b)
  You don't want your tree to fall over so let's make it complete.
  A tree is complete when it's a leaf or both sub-trees are the same height
  and are complete. (2p)
\<close>

text \<open>Hint: make sure you use parenthesis to group the logical operators =, AND
   on the right hand side of an equation or you'll get "not an equation" errors.

  Alternatively you can use the symbol \<longleftrightarrow> (<-->) in place of = in the equation.
\<close>

fun complete :: "t \<Rightarrow> nat \<Rightarrow> bool" where
  "complete Leaf               0      \<longleftrightarrow> \<llangle>replace with your solution!\<rrangle>"
| "complete (Node left right) (Suc n) \<longleftrightarrow> \<llangle>replace with your solution!\<rrangle>"
(* NOTE the following is a catch-all pattern for every input that does not match
  the previous equations. *)
| "complete _ _ \<longleftrightarrow> False"


text \<open>
  The height of a tree is pretty easy to compute. And since you have paid excellent attention in 
  your lectures, you write it down without any effort!
\<close>

fun height :: "t \<Rightarrow> nat" where
  "height Leaf = 0"
| "height (Node left right) = (max (height left) (height right)) + 1"


text \<open> 4c)
  You realize that \<open>complete t n\<close> characterizes complete trees of height \<open>n\<close>. 
  Your boss is confused, and wants to know why you defined the height function in first place.
  After some discussion, you agree with your boss that you'll show that \<open>complete\<close> and \<open>height\<close> 
  are related as expected: (3p)
\<close>

text \<open>Hint: use computation induction on the function \<^const>\<open>complete\<close>.\<close>

lemma "complete t n \<Longrightarrow> height t = n"
  oops
 


text \<open> 4d)
  You are worried that you don't have enough boxes in the factory for a very
  tall complete tree.

  Make a formula for the number of leaves that you will need for a complete tree
  of height n and prove it correct. (3p)
\<close>

text \<open>Hint: use computation induction on the function \<^const>\<open>complete\<close>.\<close>


lemma "\<llangle>replace with your solution!\<rrangle>"
oops

end
