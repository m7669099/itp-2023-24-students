chapter \<open>Homework 3\<close>
theory Homework3 (* This file must be called Homework3.thy *)
imports 
  Main 
  Homework_Lib (* Homework_Lib.thy must be in the same folder as this file! *)
  "demos/BST_Demo" (* This is the demo file from the lecture. 
    We have also put it in the homework folders *)
  "HOL-Library.Code_Target_Nat"
begin

text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #3
  RELEASED: Fri, Nov 24 2023
  DUE:      Fri, Dec 1, 2023, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
  
  This homework is worth 40 points.
\<close>

section \<open>General Hints\<close>

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...
\<close>

text \<open>Additional note: this file will only work if BST_Demo is in the right location.\<close>


section \<open>1. Forwards/Backwards Reasoning (12p)\<close>


text \<open> So far, proofs have been straightforward:
  1. Apply induction
  2. Apply auto
    2a. If successful, congratulations!
    2b. If not, shout at your computer and give up.

  Luckily, there are different ways to "debug" your proofs in Isabelle. With a bit of reasoning,
  you can try to find out why auto did not find a proof. In this exercise, we practice forward/backward 
  reasoning.

  You are given 6 lemmas. Auto can solve these lemmas without any help, but you are not going to
  use auto. You will solve each lemma twice, once using purely forward reasoning (using OF/THEN 
  attributes and finishing proofs with .) and once with backward reasoning (using rule/drule/erule).
  You find examples in the tutorial.
  
  More specifically, the forward proofs must have the form:
  
  \<^theory_text>\<open>
    using <theorem constructed by OF, THEN of, etc>
    by assumption
  \<close>
  
  You can also use \<open>.\<close> which is short for \<open>by assumption\<close>

  In the backwards proofs, you can use as many \<open>apply\<close>s as you want,
  but you can only use the proof methods \<open>rule, erule, drule, assumption\<close>,
  and you are *NOT* allowed to use OF, THEN, etc.
\<close>


subsection \<open>1a. Injectivity from Bijectivity (2p)\<close>

text \<open>Take a look at the following lemmas.\<close>
thm inv_o_cancel
thm bij_is_inj

text \<open>Solve using forward reasoning (using the OF keyword)\<close>
lemma "bij f \<Longrightarrow> inv f \<circ> f = id"
oops

text \<open>Solve using backwards reasoning (rule/drule/erule)\<close>
lemma "bij f \<Longrightarrow> inv f \<circ> f = id" 
oops


subsection \<open>1b. Surjectivity from Bijectivity (2p)\<close>

text \<open>Take a look at the following lemmas.\<close>
thm surj_f_inv_f
thm bij_is_surj

text \<open>Solve using forward reasoning (using the OF keyword)\<close>
lemma "bij f \<Longrightarrow> f (inv f y) = y"
oops

text \<open>Solve using backwards reasoning (rule/drule/erule)\<close>
lemma "bij f \<Longrightarrow> f (inv f y) = y"
oops


subsection \<open>1c. Bijectivity from Injectivity and Surjectivity (2p)\<close>

text \<open>Take a look at the following lemmas.\<close>
thm bijI
thm inv_inv_eq

text \<open>Solve using forward reasoning (using the OF keyword)\<close>
lemma "inj f \<Longrightarrow> surj f \<Longrightarrow> inv (inv f) = f"
oops

text \<open>Solve using backwards reasoning (rule/drule/erule)\<close>
lemma "inj f \<Longrightarrow> surj f \<Longrightarrow> inv (inv f) = f"
oops


subsection \<open>1d. Addition of Natural numbers in Integers (2p)\<close>

text \<open>Take a look at the following lemmas.\<close>
thm Nats_subset_Ints
thm set_mp
thm Nats_add

text \<open>Solve using forward reasoning (using the OF keyword)\<close>
lemma "a \<in> \<nat> \<Longrightarrow> b \<in> \<nat> \<Longrightarrow> a + b \<in> \<int>"
oops

text \<open>Solve using backwards reasoning (rule/drule/erule)\<close>
lemma "a \<in> \<nat> \<Longrightarrow> b \<in> \<nat> \<Longrightarrow> a + b \<in> \<int>"
oops


subsection \<open>1e. Power sets over unions (2p)\<close>

text \<open>Take a look at the following lemmas.\<close>
thm Un_Pow_subset
thm Un_upper1
thm subset_trans

text \<open>Solve using forward reasoning (using the OF keyword)\<close>
lemma "Pow A \<subseteq> Pow (A \<union> B)"
oops

text \<open>Solve using backwards reasoning (rule/drule/erule)\<close>
lemma "Pow A \<subseteq> Pow (A \<union> B)"
oops


subsection \<open>1f. Nonnegative factorials (2p)\<close>

text \<open>Take a look at the following lemmas.\<close>
thm order_trans
thm fact_ge_self
thm le0

text \<open>Solve using forward reasoning (using the OF keyword)\<close>
lemma "(0::nat) \<le> fact y" 
oops

text \<open>Solve using backwards reasoning (rule/drule/erule)\<close>
lemma "(0::nat) \<le> fact y"
oops



section \<open>2. Deduplication with accumulator (9p)\<close>

text \<open>
  This is a deduplication function that uses a set \<open>s\<close> to keep track of the elements that have
  already been added to the output. If an element is in \<open>s\<close>, it will not be added to the output. 
  If it is not in \<open>s\<close>, we add it to the output and to \<open>s\<close>, so that on subsequent encounters, 
  this element will be excluded.\<close>

fun dedup_aux where
  "dedup_aux s [] = []"
| "dedup_aux s (x#xs) = (if x\<in>s then dedup_aux s xs else x#dedup_aux ({x}\<union>s) xs)"


subsection \<open>2a. Correctness of deduplication (3p)\<close>

text \<open>
  We implement the deduplication function by fixing \<open>s={}\<close> initially, as we have not encountered 
  any elements of the list yet. We do this in function \<open>dedup\<close>.
  We want to show the correctness of \<open>dedup\<close>. This means that the set of elements in the
  deduplicated list are equal to the elements of the input. Also the deduplicated list is 
  distinct.
  
  (Hint: generalization!)
\<close>

(*ROOM FOR AUXILIARY LEMMAS*)


definition "dedup = dedup_aux {}"  
  
lemma dedup_correct: 
  "set (dedup xs) = set xs"  
  "distinct (dedup xs)"  
oops

subsection \<open>2b. Implementation of a set as a BST (3p)\<close>

text \<open>
  In terms of runtime, sets are very inefficient. By implementing the set as a binary search 
  tree (BST), which you have seen in a demo in the lecture, we could speed up the membership 
  test. Define \<open>dedup_aux_impl\<close> similarly to \<open>dedup_aux\<close>, but using a BST to insert and test
  membership. Look at the following functions:

  NOTE: In the tutorials we have shown function definitions over multiple inductive types.
    For this particular exercise, we want you to forget about that and use the given template
    to implement the functions.
\<close>

term bst_lookup
term bst_insert
  
fun dedup_aux_impl :: "BST \<Rightarrow> int list \<Rightarrow> int list" where
  "dedup_aux_impl t [] = \<llangle>replace with your solution!\<rrangle>"
| "dedup_aux_impl t (x#xs) = \<llangle>replace with your solution!\<rrangle>"

(* Hint: for an easy proof, try to define your function as similar as possible to dedup_aux! *)  
  
subsection \<open>2c. Correctness of the implementation (3p)\<close>

text \<open>
  To show that we have correctly implemented this function \<open>dedup_impl\<close>, we don't have to 
  reprove that our function produces distinct lists that contains the same set of elements as 
  the input. We can implicitly show this by showing equality of \<open>dedup\<close> (for which we have already 
  proven this) and \<open>dedup_impl\<close>.
\<close>

text \<open>Start with a lemma about \<open>dedup_aux_impl\<close>, that looks like this 
  (we have omitted a necessary precondition here, that you should add)\<close>


lemma dedup_aux_impl_refine[simp]: 
  "\<llangle>replace with your solution!\<rrangle> \<Longrightarrow> dedup_aux_impl s xs = dedup_aux (bst_set s) xs"
  oops


definition "dedup_impl \<equiv> dedup_aux_impl bst_empty"  

text \<open>The main proof should be straightforward then: \<close>
lemma dedup_impl_refine[simp]: "dedup_impl xs = dedup xs"
oops

section \<open>3. Inductive Predicates (6p)\<close>
  

text \<open>
  The following defines an inductive predicate that determines for a natural number and 
  a list of bools how many elements are \<open>True\<close>. E.g. \<open>cntT 3 [False, True, False, True, True]\<close> holds but
  \<open>cntT 1 [False, False, True, True]\<close> does not.
\<close> 
  
inductive cntT :: "nat \<Rightarrow> bool list \<Rightarrow> bool" where
  "cntT 0 []"
| "cntT n xs \<Longrightarrow> cntT n (False#xs)"  
| "cntT n xs \<Longrightarrow> cntT (Suc n) (True#xs)"  

text \<open>
  The following lemma proves that the predicate behaves as expected. 
  Can you see why? Try to understand the statement and then prove it.

  HINT: You may want to split your proofs into different parts. E.g. to prove A \<longleftrightarrow> B
    it is sometimes easier to prove A \<Longrightarrow> B and B \<Longrightarrow> A. 
    Also, when proving x=y \<Longrightarrow> A, we may also substitute y for x in A to help the simplifier.
\<close>

(* ROOM FOR AUXILIARY LEMMAS *)


lemma "cntT n xs \<longleftrightarrow> n=length (filter id xs)" 
oops

  

section \<open>4. Abstract Datatypes (13p)\<close>

text \<open>
  You are given a bitset, the same that we have seen in the practical. We have implemented a 
  delete function there, but we are of course also interested in the other operations (insert,
  membership, union, intersection, empty set).
  You are given the abstraction function that you have seen in the tutorial:
\<close>

definition "bl_\<alpha> xs \<equiv> { i . i<length xs \<and> xs!i }"

text \<open>
  Define the following operations and prove them correct.
\<close>

subsection \<open>4a. Empty Bitset (3p)\<close>

definition "bl_empty = \<llangle>replace with your solution!\<rrangle>"

lemma bl_empty_\<alpha>: "bl_\<alpha> bl_empty  = {}"
oops

subsection \<open>4b. Membership (3p)\<close>

definition "bl_lookup i xs \<equiv> \<llangle>replace with your solution!\<rrangle>"

lemma bl_lookup_\<alpha> : "bl_lookup i xs \<longleftrightarrow> i\<in>bl_\<alpha> xs"
oops

subsection \<open>4c. Insert (3p)\<close>

definition "bl_insert i xs \<equiv> \<llangle>replace with your solution!\<rrangle>"

(* Come up with the correctness lemma yourself! *)
lemma bl_insert_\<alpha> : "\<llangle>replace with your solution!\<rrangle>" 
oops

subsection \<open>4d Union (4p)\<close>

text \<open>
  Since we did not define bl_\<alpha> inductively, one may encounter problems when applying induction.
  We introduce simp lemmas that mimic an inductive definition. See if you can understand what they
  do.

  Hint the \<open>`\<close> operation is called the image of a set. You can see it as a \<open>map\<close> function for sets.
\<close>

value "(\<lambda>x. x * 2::nat) ` {1,2,3}"
value "Suc ` {1,2,3}"

lemma bl_\<alpha>_simps1: "bl_\<alpha> [] = {}"
  using bl_\<alpha>_def by simp

lemma bl_\<alpha>_simps2: "bl_\<alpha> (x # xs) = (if x then insert 0 (Suc ` bl_\<alpha> xs ) else (Suc ` bl_\<alpha> xs ))"
  unfolding bl_\<alpha>_def 
  using less_Suc_eq_0_disj by force

text \<open>Now implement the union (consider using a recursive function) and prove the correctness.\<close>

fun bl_union :: "(bool list) \<Rightarrow> (bool list) \<Rightarrow> (bool list)" where
"bl_union _ _ = []"


lemma bl_union_\<alpha>: "bl_\<alpha> (bl_union xs ys) = bl_\<alpha> xs \<union> bl_\<alpha> ys"
oops


section \<open>BONUS. Intersection with invariants (5p)\<close>

text \<open>THIS IS A BONUS EXERCISE, MEANING THAT YOU CAN POTENTIALLY SCORE 45 OUT OF 40 POINTS BY 
  CORRECTLY ANSWERING THESE EXERCISES!

  You have seen this invariant in the tutorial. When deleting an element, you want to potentially
  shrink the list such that the last element remains True. This way, you don't unnecessarily store
  values that don't hold any information.
  
\<close>

definition "bl_inv xs \<equiv> (xs \<noteq> [] \<longrightarrow> last xs)"


subsection \<open>B1. Invariant preservation of union (1p)\<close>

text \<open> 
  Show us that you can correctly prove that the union function preserves the invariant.
  HINT: You may use sledgehammer!  
\<close>

lemma "bl_inv xs \<Longrightarrow> bl_inv ys \<Longrightarrow> bl_inv (bl_union xs ys)"
oops

subsection \<open>B2. Intersection with invariant preservation (4p)\<close>

text \<open>
  Invariant preservation for intersection is harder than it is for union, as the last element after
  intersecting may be False for the naive implementation. It is up to you to come up with a 
  solution and prove it correct.
  Full points if you can define intersection, and show that it 
  computes the intersection and preserves the invariant.
\<close>

text \<open>
  WARNING: This is an open exercise with a lot of missing proofs and definitions! It will be
  very easy to get stuck on details. We award partial points for those who define an intersection
  function that does not preserve the invariant. Try to implement this first. If you feel confident
  you can prove invariant preservation later.
\<close>



definition "bl_intersection xs ys = \<llangle>replace with your solution!\<rrangle>"



lemma "bl_\<alpha> (bl_intersection xs ys) = bl_\<alpha> xs \<inter> bl_\<alpha> ys"
oops

lemma "bl_inv (bl_intersection xs ys)"
oops

end
