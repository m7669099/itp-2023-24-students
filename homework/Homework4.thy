chapter \<open>Homework 4\<close>
theory Homework4 (* This file must be called Homework4.thy *)
imports
  Main
  Homework_Lib (* Homework_Lib.thy must be in the same folder as this file! *)
begin


text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #4
  RELEASED: Fri, Dec 1, 2023
  DUE:      Fri, Dec 8, 2023, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
  
  This homework is worth 40 points + 3 bonus points.
\<close>

section \<open>General Hints\<close>

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...
\<close>

section \<open>1. Polynomials 18p + 3bp\<close>

text \<open>
  On planet Olosac polynomials roam free as far as you can see. Unfortunately you don't
  get to see them much as you are on earth trying to fix the broken telescope at the
  cookie factory.

  The lenses are clearly unusable covered in choccolate fudge but the computer monitor
  at the workstation shows some data that maybe you can use.

  Some of this polynomials are very big and have lots of coefficients. Instead of spelling
  them out as a list a common way to represent them is to have a function that for each
  position @{term i} will give back coefficient @{term a\<^sub>i}.

  Therefore the polynomial \<open>a\<^sub>0 + a\<^sub>1 x + a\<^sub>2 x\<^sup>2 + a\<^sub>3 x\<^sup>3 + ...\<close> is represented by the
  function \<open>f(0) = a\<^sub>0, f(1) = a\<^sub>1, f(2) = a\<^sub>2, f(3) = a\<^sub>3 , ...\<close>
\<close>

type_synonym poly = "nat \<Rightarrow> int"

subsection \<open>1a. Define Sum and Zero (2p)\<close>

text \<open>
  This representation is convenient when you want to represent the sum of two polynomials
  because it is just another function.

  Define a function of type @{typ "poly \<Rightarrow> poly \<Rightarrow> poly"} implementing addition for polynomials. 
\<close>

definition psum' :: "poly \<Rightarrow> poly \<Rightarrow> poly" where
"psum' _ _ \<equiv> \<llangle>replace with your solution!\<rrangle>"

text \<open>
  Sometimes though your telescope does not pick up anything at all but you know
  better! You are looking at the constant polynomial zero!

  Define @{term p_zero} the polynomial constantly zero everywhere.
\<close>

definition p_zero where
"p_zero _ \<equiv> \<llangle>replace with your solution!\<rrangle>"

subsection \<open>1b. Prove Neutral (2p)\<close>
text \<open>
  And obviously @{term p_zero} is the left and right neutral element for your
  sum operation.

  State and prove that @{term p_zero} is the left and right neutral element for @{term psum'}.
\<close>



subsection \<open>1c: Prove Assoc + Comm (2p)\<close>
text \<open>
  You scratch your head trying to remember the first math courses you took
  when studying as a cookie engineer. You know the order in which you sum
  more polynomials does not matter.

  Prove that for your definition @{term psum'} you can "omit the parenthesis"
  because it is associative.
\<close>

lemma psum'_associative : "psum' p (psum' q r) = psum' (psum' p q) r"
sorry

text \<open>
  Moreover the order in the sum also should not matter!

  State and prove that @{term psum'} is commutative.
\<close>



subsection \<open>1d: Prove inverse (2p)\<close>

text \<open>
  But more importantly for each polynomial there is always an opposite
  that will annihilate. Two opposite polynomials will sum up to @{term p_zero}
  and pass undetected.

  Prove the following lemma in Isar. 
\<close>

lemma psum'_opposite : "\<exists> q. psum' p q = p_zero"
proof (* Feel free to use proof - if you don't like the default exI rule to be applied here *)
oops


text \<open>
  This still does not explain why there's so many @{term p_zero}s in the data
  on your screen. Maybe it has to do with multiplication instead of addition?

  You start scribbling down the formula for polynomial multiplication and
  eventually get it down to something that Isabelle can understand.
\<close>

definition pmul' :: "poly \<Rightarrow> poly \<Rightarrow> poly" where
  "pmul' f g \<equiv> \<lambda> k . \<Sum> {f i * g j | i j . i + j = k }"


text \<open>
  Now that you can multiply things are much clearer.

  @{term p_zero} is the apex predator on this planet as it will
  annihilate any other polynomial it is multiplied with!

  You find a napkin on which somebody scribbled a proof of that!
  Unfortunately it is not completely readable. Finish the proof that
   @{term p_zero} is the right neutral element for @{term pmul'}.

  First do the proof on paper and try bringing it over to Isabelle.
  Do not use sledgehammer for this proof.
  
  Hint: The formula \<^term>\<open>\<Sum> {f i * g j | i j . i + j = k }\<close> looks scary.
    However, once you realize what the set you sum over actually is for 
    your "g = p_zero" case, it quickly looses it's horror. 
  
\<close>

subsection \<open>1e: Multiplicative Annihilator (3p)\<close>

lemma pmul'_absorb : "pmul' f p_zero = p_zero"
proof
  fix k

  show "pmul' f p_zero k = p_zero k" sorry
qed

text \<open>
  You grow tired of waiting for the computer to pick up the pace. Representing polynomials
  by functions is clever but you are paying for it dearly with telescope downtime.

  There is an alternative representation where you only keep the coefficients @{term a\<^sub>i} that
  are not null but that is difficult to implement. Instead we will store all coefficients @{term a\<^sub>i}
  between @{term a\<^sub>0}, the minimum coefficient, and @{term a\<^sub>n}, the maximum coefficient.
\<close>

text \<open>
  Once again you find yourself defining how to do addition. This time on lists of coefficients.

  Define the function @{term psum} of type @{typ "int list \<Rightarrow> int list \<Rightarrow> int list"} that
  implements addition over polynomials.
\<close>
subsection \<open>1f: List Polynomials (2p)\<close>

fun psum :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
"psum _ _ = \<llangle>replace with your solution!\<rrangle>"

text \<open>
  It is unfortunate that while more efficient this implementation has a big problem.

  The zero polynomial @{term p_zero} has infinitely many representations: [], [0], [0. 0], [0, 0, 0], ...
  You settle on @{term "[]"} as your champion to start your proofs but make a mental note
  to figure out how to represent all of them as one.
\<close>

subsection \<open>1g: List Neutral (1p)\<close>

text \<open>
  State and prove that @{term "[]"} is a left and right neutral element for @{term psum}.
  Make sure to add both rules to the simpset.
\<close>


subsection \<open>1h: List Assoc (2p)\<close>

text \<open>
  And again addition over polynomials as lists is associative.
  Prove that @{term psum} is associative.
  
  Hint: even if you choose an appropriate induction rule, you may need an additional case distinction.
\<close>

lemma psum_associative: "psum as (psum bs cs) = psum (psum as bs) cs"
sorry


subsection \<open>1i: List Inverse (2p)\<close>

text \<open>
  You start working on the proof that every polynomial has an opposite but you
  quickly discover a fatal flaw in your lemma. What is it?

  Write your answer here:

  
\<close>
lemma psum_opposite : "\<exists> q. psum p q = []"
  oops

  
text \<open>Fortunately, you remember some odd concept called abstract data types, that might help here.

  You define an abstraction function that maps list-represented polynomials to the function representation.
\<close>  

definition poly\<alpha> :: "int list \<Rightarrow> poly" where "poly\<alpha> xs i \<equiv> if i<length xs then xs!i else 0"


text \<open>Now you can fix your lemma and prove it: \<close>
lemma psum_opposite : "\<exists> q. poly\<alpha> (psum p q) = p_zero"
text \<open>
  While there are many ways to prove this lemma, we suggest to first come up with some list
  XXX such that: \<^prop>\<open>psum p XXX = replicate (length p) 0\<close>

  Next, show that \<^prop>\<open>poly\<alpha> (replicate n 0) = p_zero\<close> for all \<open>n\<close>.   

  Piecing everything together should give you the lemma easily.
\<close>
oops


subsection \<open>1j: Correctness of zero and addition (3 BONUS POINTS)\<close>


text \<open>Show that the empty list represents the zero polynomial. (This is a good simp-lemma)\<close>

lemma poly\<alpha>_zero[simp]: "poly\<alpha> [] = p_zero"
oops  





text \<open>
  Next, show that polynomial addition is correct.
  
  Note: while this lemma looks pretty innocent, its proof isn't! 
  
  We recommend a computation induction over psum. 
    In the two base cases, remember your lemmas about neutral elements for polynomial addition.
    (Maybe you have added them to the simpset already?)
  
    For the step case, continue with point-wise reasoning:
      two polynomials are equal if they are equal on all indices (@{thm ext}). 
      Make a case distinction if the index is 0 or "Suc i", and use auxiliary lemmas for each case.
    
\<close> 

  
      
lemma poly\<alpha>_psum: "poly\<alpha> (psum xs ys) = psum' (poly\<alpha> xs) (poly\<alpha> ys)"
sorry
  


  
section \<open>2. Modular arithmetic (8p)\<close>

text \<open>
  Time for a nap! Unfortunately algebra is everywhere, even in your dreams!
  
  You cannot stop thinking about addition and how it's everywhere, even when
  working with modular arithmetic.

  You can define addition over the set of remainders modulo p {0, 1, 2, ..., p-1}
  as follows. For n, m \<in> {0, 1, 2, ..., p-1} you sum m+n and then take the remainder
  modulo p.

  As this is still not a nightmare, you quickly find Isabelle's built-in \<^term>\<open>(mod)\<close> function
  to implement addition modulo 7:
\<close>

definition msum :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "msum n m \<equiv> (n + m) mod 7"

  
subsection \<open>2a: Closedness (2p)\<close>  
text \<open>
  Unfortunately the type signature @{typ "nat \<Rightarrow> nat \<Rightarrow> nat"} is not enough
  for our purposes. State and prove that @{term msum} returns a value in the
  set {0..6}
\<close>



subsection \<open>2b: Neutral (2p)\<close>  

text \<open>
  When looking for neutral elements, you identify 0 as the obvious candidate,
  and quickly type the lemma:
\<close>
 
lemma msum_neutral_l: "msum a 0 = a"
  text \<open>But wait, something is fishy here! While quickcheck thinks the lemma is fine, 
    the nitpick-tool almost instantly comes up with a counterexample \<open>a=7\<close>
  \<close>
  quickcheck
  nitpick
  oops

text \<open>Clearly, there's a precondition missing here! Add the missing precondition, and prove the lemma.
  Also prove that 0 is a right neutral element! 
  Don't forget to add these lemmas to the @{text simp} set.
\<close>
  

lemma msum_neutral_l [simp]: "\<llangle>replace with your solution!\<rrangle>"
  oops

lemma msum_neutral_r [simp]: "\<llangle>replace with your solution!\<rrangle>"
  oops

  
subsection \<open>2c: Assoc and Comm (2p)\<close>  
  
text \<open>
  It is time again to show that the order or parenthesis does not matter
  when using @{term msum}. State and prove that @{term msum} is associative.
\<close>



text \<open>
  State and prove that @{term msum} is commutative.
\<close>



subsection \<open>2d: Inverse (2p)\<close>  

text \<open>
  Define an inverse function \<open>minv\<close> and show that for each element \<open>x \<in> {0..6}\<close>,
  we have that \<open>minv x \<in> {0..6}\<close> and \<open>x\<close> and \<open>inv x\<close> add up to \<open>0\<close>.
  
  Hint: use sledgehammer and find-theorems to find the necessary lemmas about (mod) in Isabelle.
\<close>

definition minv :: "nat \<Rightarrow> nat" where "minv x = \<llangle>replace with your solution!\<rrangle>"

   
  

section \<open>3. Graphs (14p)\<close>



context
  fixes E :: "'v rel" (*equal to 'v \<times> 'v set*)
  
begin

text \<open>
  This is a context. One of the use cases of a context is to fix a variable to a certain type. In
  this context, we notice the line \<open>fixes E :: "'v rel"\<close>. 
  This line makes \<open>E\<close> available in every lemma and definition. In particular, in definitions, 
  we can use it without passing it as parameter.
  
  Only when we leave the context, the extra parameter will be added to our definitions.
  
  As example, take the function \<open>path\<close> below, that uses \<open>E\<close> as implicit parameter.
  It defines that there is a path between two nodes, the path being the list of visited nodes.
  \<close>

  fun path :: "'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
    "path u [] v \<longleftrightarrow> u=v"
  | "path u (x#xs) v \<longleftrightarrow> (u,x)\<in>E \<and> path x xs v"  
    
  
  
subsection \<open>3a. Understanding Paths (2p)\<close>  
  text \<open>Let's understand how a path is constructed. 
    How is the first/last node on a path related to the start/end node?
    
    Prove the following lemmas or explain why they are not true!
  \<close>
  lemma "path u (x#xs) v \<Longrightarrow> x=u" 
    oops
  
  lemma "path u (xs@[x]) v \<Longrightarrow> x=v" 
    oops
  
  
  
subsection \<open>3b. Path to Transitive Closure (2p)\<close>

text \<open>
  Isabelle has a built-in definition that does something similar: the reflexive transitive closure. 
  For our graph, the reflexive transitive closure \<open>E\<^sup>*\<close> (ctrl + arrow up + *) calculates the set of 
  edges that you get if there is a series of edges of length 0 or more between two states. I.e. if 
  (u,v), (v,w), (w,x) are in E, then \<open>(u,x) \<in> E\<^sup>*\<close> (as well as (u,u), (u,v), (u,w), (v,x) etc.).

  We suspect that the definition of path is closely related to \<open>E\<^sup>*\<close>. If we have
  a path \<open>xs\<close> between \<open>u\<close> and \<open>v\<close>, then the edge \<open>(u,v)\<close> must be in \<open>E\<^sup>*\<close>.
  Prove that this is indeed correct.
\<close>

term "E\<^sup>*"
term rtrancl

  lemma path_imp_reachable: "path u xs v \<Longrightarrow> (u,v)\<in>E\<^sup>*"  
oops    


subsection \<open>3c. Transitive Closure to Path (2p)\<close>

text \<open>
  We suspect that the converse must also hold. Try to prove the following lemma

  HINT: You may want to use an induction rule for the reflexive transitive closure. We have already 
    made a selection for you. Try to apply all of them and reason about which of those works best 
    for this proof.
  WARNING: All of them work, but only one of them allows for a nice proof.
\<close>

thm rtrancl_induct
thm rtrancl.induct
thm converse_rtrancl_induct

  lemma reachable_imp_path: "(u,v)\<in>E\<^sup>* \<Longrightarrow> \<exists>xs. path u xs v" 
oops



subsection \<open>3d. Acyclic graphs (3p)\<close>

text \<open>
  We can also use contexts to make assumptions about about the fixed variables. In our case we
  assume E to be acyclic. I.e. if there is a path between any states u and v, then there is no path
  between v and u.

  We use the transitive closure \<open>E\<^sup>+\<close> (without reflexivity, i.e. \<open>(u,u) \<in> E\<^sup>+ \<longleftrightarrow> (u,u) \<in> E\<close>, whereas 
  \<open>(u,u) \<in> E\<^sup>*\<close> always holds) for this assumption. Try to understand why this assumption makes our 
  graphs acyclic.
  
\<close>
  context
    assumes acyclic: "E\<^sup>+ \<inter> Id = {}"
  begin

text \<open>
  Now try to prove using Isar that a path on an acyclic graph is always distinct. 
  I.e. every state is visited at most once.
  Fill in the missing steps in the Isar proof.
\<close>

    lemma "path u vs w \<Longrightarrow> distinct vs"
    proof (induction vs arbitrary: u)
      case Nil
      then show ?case sorry
    next
      case (Cons v vs)
      
(*
  Via induction, we have to prove that \<open>v#vs\<close> is distinct. With the information that we have, we 
  can at least prove that part of the path is distinct.
*)
      have P: "path v vs w" and IH: "distinct vs" sorry
        
      have "v\<notin>set vs" 
      proof (rule notI) (* Proof by contradiction *)
        assume "v\<in>set vs"
        thm in_set_conv_decomp
text \<open>
  We can show that \<open>v#vs\<close> is distinct by showing that \<open>v\<close> is not in \<open>vs\<close>. 
  We can do so by contradiction. We assume that \<open>v\<close> is in \<open>vs\<close>. We can then obtain a decomposition
  of our path \<open>vs\<^sub>1@v#vs\<^sub>2\<close> (see @{thm in_set_conv_decomp}). This means that \<open>vs\<^sub>1\<close> is a path between \<open>v\<close>
  and some other state \<open>v'\<close> and that there exists an edge \<open>(v',v)\<close> (you may need an auxiliary lemma).
  Given that such a path exists, we know from previous exercises that there is a node \<open>(v,v') \<in> E\<^sup>*\<close>
  But now this means that (given \<open>(v,v') \<in> E\<^sup>*\<close> and \<open>(v',v) \<in> E\<close>) \<open>(v,v) \<in> E\<^sup>+\<close>. 
  But we have made an assumption about E, can you see the contradiction? 
  Now try to put this proof into Isar.
\<close>

        show False sorry
      qed
      
      with IH show ?case sorry
    qed
      
  
  end

text \<open>We are now leaving the context that assumes that our graph is acyclic.\<close>

subsection \<open>3e. Reachability (2p)\<close>

text \<open>
  A common concept in applications graph theory (for example checking whether an automaton accepts 
  a given word) is to check for reachability. In other words. Given a starting position \<open>u\<close> and a 
  set of final states \<open>F\<close>, can we reach any state in \<open>F\<close> by following edges starting at \<open>u\<close>.
  We want to characterize this by an inductive definition \<open>accept\<close>. 
    \<^item> if \<open>u \<in> F\<close> then \<open>accept u F\<close> should hold. 
    \<^item> if there is an edge \<open>(u,v)\<close> such that \<open>accept v F\<close> holds, then \<open>accept u F\<close> should hold. 
    
  Encode this as an inductive definition.
\<close>

  inductive accept :: "'v \<Rightarrow> 'v set \<Rightarrow> bool" where
    (* Add your rules here *)



subsection \<open>3f. Accept and path (2p)\<close>

text \<open>
  You may have already noticed that \<open>accept\<close> corresponds to the existence of a path to any state
  in F. Try to prove the equivalence of both.
  HINT: For equivalence \<open>A \<longleftrightarrow> B\<close> you may want to prove \<open>A \<Longrightarrow> B\<close> and \<open>B \<Longrightarrow> A\<close> separately. Try to
  remember why it is useful to do this.
\<close>



  lemma accept_eq_path: "accept u F \<longleftrightarrow> (\<exists>v\<in>F. \<exists>xs. path u xs v)"
oops

subsection \<open>3g. Accept and rtrancl (1p)\<close>

text \<open>
  Look at your proofs so far, it should now be easy to prove the following equivalence using lemmas 
  that you already have.
\<close>



  lemma "(\<exists>v\<in>F. (u,v)\<in>E\<^sup>*) \<longleftrightarrow> accept u F"
oops

end



end
