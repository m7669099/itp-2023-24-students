chapter \<open>Homework 4 --- Hints\<close>
theory Homework4_Hints 
imports
  Main
  Homework_Lib (* Homework_Lib.thy must be in the same folder as this file! *)
begin

(*
  A hint on induction with Isar, as I didn't cover the material in the lecture.
  
  Also have a look at demos/Isar_Induction_Demo.thy!
*)

type_synonym poly = "nat \<Rightarrow> int"


fun psum :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
  "psum [] xs = \<llangle>replace with your solution!\<rrangle>"
| "psum xs [] = \<llangle>replace with your solution!\<rrangle>"
| "psum (x # xs) (y # ys) = \<llangle>replace with your solution!\<rrangle>( psum xs ys)"


(*
  Induction in Isar is not different from induction on apply-style level.
  
  \<^item> write 
      proof (induction \<dots>) 
    instead of 
      apply (induction \<dots>)

  \<^item> use the cases-autocompletion, and prove the cases
  
  Here's an example:
*)


lemma psum_associative: "psum as (psum bs cs) = psum (psum as bs) cs"
(*
  Let's assume you want to do computation induction on psum bs cs here \<dots> in Isar
*)
proof(induction bs cs arbitrary: as rule: psum.induct) (* Take the autocompletion *)
  case (1 xs)
  then show ?case sorry
next
  case (2 v va)
  then show ?case sorry
next
  case (3 x xs y ys)
  
  thm 3  (* That's the name of the induction hypothesis (and all other premises, should be there any) *)
  
  show ?case sorry
qed 



end
