chapter \<open>Homework 5\<close>
theory Homework5 (* This file must be called Homework5.thy *)
imports
  Main
  Homework_Lib (* Homework_Lib.thy must be in the same folder as this file! *)
  "HOL-Library.Multiset"
begin


text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #4
  RELEASED: Fri, Dec  8, 2023
  DUE:      Fri, Dec 15, 2023, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
  
  This homework is worth 40 points.
\<close>

section \<open>General Hints\<close>

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...
\<close>


section \<open>0. Induction Bonus (5 bonus points)\<close>
text \<open>This exercise is for you to catch up with induction proofs. 
  There's no new material here, but a sequence of induction proofs of various difficulty.
  
  Try them to test your understanding of induction, and earn a few bonus points! 
  
  Sledgehammer is allowed to find proofs, but not to brute-force proofs. 
  No points for long (>3 lemmas), unstructured, sledgehammer generated proofs! 
  Small sledgehammer proofs, just adding one or two missing lemmas are OK.
  
  Notes: 
    \<^item> when done with an adequate approach, none of the proofs requires more than 10 lines!
    \<^item> for some of the later proofs, you need what you proved earlier. 
      Use sorry if you cannot prove those earlier lemmas, such that you can continue on the later lemmas.
    \<^item> read the \<open>Induction_Examples.thy\<close> first!
\<close>

subsection \<open>fold max accumulator (.5bp)\<close>
(* Straightforward induction, but discharging step-case requires a tiny bit of work or sledgehammer.
  We recommend doing an Isar-proof!
*)
lemma fold_max_acc: "fold max xs a \<ge> a" for a :: nat
sorry

subsection \<open>fold max vs sum (.5bp)\<close>

(*
  Will need generalization. Step-case may require a bit of work!
  We recommend doing an Isar-proof!
*)
lemma "fold max xs 0 \<le> sum_list xs" for xs :: "nat list"
sorry

subsection \<open>fold max append (.5bp)\<close>

  
  
(* Hint: before generalizing this one, try to simplify the lemma! 
  You'll profit from already set-up lemmas in the standard library! 
  After that, you still have to prove a generalized version!
  
  Also, have a look at the ac_simps lemmas. They contain some useful simp-lemmas for max!
*)  
find_theorems "fold _ (_@_)"

thm ac_simps

lemma max_fold_append: "fold max (xs@ys) 0 = max (fold max xs 0) (fold max ys 0)" for xs :: "nat list"
sorry  
  

subsection \<open>fold max in set (1bp)\<close>
  
(*
  A direct proof of this lemma gets complex. 
  Try to break it down!

  Hint: in_set_conv_decomp \<and> what you have already proved (or sorry'd)
*)
thm in_set_conv_decomp

lemma "x\<in>set xs \<Longrightarrow> fold max xs 0 \<ge> x" for x :: nat
sorry  
  

subsection \<open>Weird function and sorting (.5bp)\<close>  
(* That's enough ordinary folding. Let's do something weird! *)  
  
(* Try to understand what the following function does! *)
fun weird :: "nat list \<Rightarrow> nat list" where
  "weird [] = []"
| "weird (x#xs) = x # weird (filter ((\<le>)x) xs)"  
  

(* Straightforward with correct induction *)  
lemma "sorted xs \<Longrightarrow> weird xs = xs"
sorry  

subsection \<open>Weird Filtering (.5bp)\<close>  

(* Almost straightforward with correct induction. Step-case requires transitivity reasoning beyond auto, but you can try0! *)
lemma filter_weird: "filter (\<lambda>x. a\<le>x) (weird xs) = weird (filter (\<lambda>x. a\<le>x) xs)"
sorry  

subsection \<open>Weird squared (.5bp)\<close>  
  
(* We can even go weirder! Also try to understand what this function does! *)  
fun weird2 :: "nat \<Rightarrow> nat list \<Rightarrow> nat list" where
  "weird2 v [] = []"
| "weird2 v (x#xs) = (if v\<le>x then x#weird2 x xs else weird2 v xs)"  
  

(* Almost straightforward with correct induction. Step-case requires transitivity reasoning beyond auto, but you can try0! *)
lemma "\<forall>x\<in>set (weird2 v xs). v\<le>x"
sorry  



subsection \<open>Extra weird (1bp)\<close>
(*
  As you might already have figured out, our weird functions do almost the same. You're going to prove that now.


  \<^bold>W\<^bold>a\<^bold>r\<^bold>n\<^bold>i\<^bold>n\<^bold>g: 
    there are probably many ways to prove that, but it's not straightforward, and easy to 'go down the wrong rabbit hole'.
    Key is to not only find the right generalization, but also to set up the induction correctly.
    The easiest induction might look different from what you would expect!
    
    For some inspiration how a generalization might look like, look again at thm filter_weird!
    
    Hint: once you find a good generalization, it's almost "by induction auto"
    
*)
lemma weird2_eq: "weird2 0 xs = weird xs"
sorry





section \<open>1. 2x2 Matrices (10p)\<close>

text \<open>
  As a warmup it is time for you to implement 2 by 2 matrices over the integers.

  A matrix M is a grid of elements \<open>m\<^sub>i\<^sub>j\<close> where i is the row index and
  j is the column index. A 2 by 2 matrix will have 2 rows and 2 columns.

  We can represent visually the matrix M as a grid.

  \<open>     | m\<^sub>1\<^sub>,\<^sub>1   m\<^sub>1\<^sub>,\<^sub>2 |
    M = | m\<^sub>2,\<^sub>1   m\<^sub>2\<^sub>,\<^sub>2 |
  \<close>

  On a computer though it is nicer to represent them as a data type:
\<close>  

datatype matrix = M int int int int

 
text \<open> 
  i.e., the above matrix would be represented as \<open>M m\<^sub>1\<^sub>,\<^sub>1 m\<^sub>1\<^sub>,\<^sub>2 m\<^sub>2,\<^sub>1 m\<^sub>2\<^sub>,\<^sub>2\<close>.

  Two matrix can be summed element wise but multiplication is a little more
  difficult. There is also another interesting operation called determinant
  of a matrix.

  Define the following functions: \<open>matsum\<close>, \<open>matmul\<close> \<open>matd\<close> for matrix sum,
  matrix multiplication and matrix determinant (look it up if you don't remember your basic maths ;) ).

  Prove that \<open>matsum\<close> is associative and commutative.
  Prove that \<open>matmul\<close> is associative.
  Prove that \<open>matd\<close> is a multiplicative function, i.e. \<open>matd (matmul A B) = matd A * matd B\<close>.
\<close>

text \<open>
  Hints: 
    \<^item> no induction required, just case distinction!
    \<^item> the set of simplifications \<open>algebra_simps\<close> will come in handy for the proofs!
\<close>



section \<open>2. Encoding and decoding (10p)\<close>

subsection \<open>2.1 Even and odds are what? (5p)\<close>

text \<open>
  A coding from domain A to the natural numbers is an injective function \<open>f\<close>
  that for each element \<open>a \<in> A\<close> computes a natural number \<open>f(a)\<close>.

  Codings are used to prove many wonderful properties but what we focus on
  is the cardinality of sets.

  As an example we can prove that the sets \<int> and \<nat> have the same cardinality
  by showing a coding from \<int> to \<nat>.

  A simple coding \<open>enc\<close> from from \<int> to \<nat> is the following:

  enc m =    2 * m         when m \<ge> 0
  enc m = - (2 * m + 1)    when m < 0
  
  Define the function \<open>enc\<close> of type  @{typ "int \<Rightarrow> nat"}
  that computes the previous coding.

  Use @{term "(nat z)"} to convert an integer number z \<ge> 0 to
  its natural representation.
\<close>

text \<open>
  Hint: Take care in not using @{term nat} when the argument is negative.
\<close>

value "(nat (-1))"

definition enc :: "int \<Rightarrow> nat" where
"enc _ \<equiv> \<llangle>replace with your solution!\<rrangle>"

text \<open>
  Prove the general idea behind this encoding.
  The result @{term "(enc m)"} will be even if and only if m is positive
  and it will be odd if and only if the argument is negative. 
\<close>
lemma enc_pos_even: "even (enc m) \<longleftrightarrow> m \<ge> 0"
oops

lemma enc_not_pos_odd: "odd (enc m) \<longleftrightarrow> m < 0"
oops


text \<open>
  We could prove that @{term enc} is injective but it actually easier to prove
  it has an inverse \<open>dec\<close>.

  Define the function \<open>dec\<close> of type  @{typ "nat \<Rightarrow> int"}
  that is the left inverse of @{term enc}.

  Use @{term "(int n)"} to convert a natural number n to
  its integer representation.
\<close>

definition dec :: "nat \<Rightarrow> int" where
"dec _ \<equiv> \<llangle>replace with your solution!\<rrangle>"

text \<open>
  Prove the general idea behind the decoding.
  The result @{term "(dec m)"} will be positive if and only if m is even
  and it will be negative if and only if m is odd.
\<close>

lemma dec_even_pos : "even m \<longleftrightarrow> dec m \<ge> 0"
oops

lemma dec_odd_not_pos: "odd m \<longleftrightarrow> dec m < 0"
oops

text \<open>
  Prove that @{term dec} is the left inverse of @{term enc}.
\<close>

lemma dec_enc_inv : "dec (enc m) = m"
oops

text \<open>
  Finally use lemma \<open>dec_env_inv\<close> to prove that @{term enc} in injective.
\<close>

lemma enc_inj : "inj enc"
oops

subsection \<open>2.2 Encoding of lists (5p)\<close>

text \<open>
  The idea of coding is actually more general than this. You can
  encode as a natural number many different things, e.g. lists.

  In the following we assume the existence of two functions @{term enc}
  and @{term dec} of types @{typ "nat \<times> nat => nat"} and @{typ "nat \<Rightarrow> nat \<times> nat"}},
  to encode and decode pairs of natural numbers.

  Here @{typ "nat \<times> nat"} is the type of pairs of natural numbers.

  We also assume that @{term dec} is the left inverse of @{term enc}.
\<close>


context
  fixes enc :: "nat\<times>nat \<Rightarrow> nat"
    and dec :: "nat \<Rightarrow> nat\<times>nat"
  
  assumes inv_dec_enc: "dec (enc p) = p"
begin


text \<open>

  One way of encoding a list into a natural number is to encode the list
  
  \<open>[ x\<^sub>1, x\<^sub>2, ..., x\<^sub>n ] as pairs enc (x\<^sub>1, enc (x\<^sub>2, enc ( ..., x\<^sub>n )...) )\<close>. 
  
  In order to decode, we also need to know when we shall stop decoding. 
  An easy way is to simply encode the length of the list first, i.e.
  
  \<open>enc (n, enc (x\<^sub>1, enc (x\<^sub>2, enc ( ..., x\<^sub>n )...)))\<close>
  
  First, define the function \<open>enc_list_aux\<close> of type @{typ "nat list \<Rightarrow> nat"}} that produces
  the encoding without the length, for non-empty lists.
\<close>

fun enc_list_aux :: "nat list \<Rightarrow> nat" where
"enc_list_aux _ = \<llangle>replace with your solution!\<rrangle>"

text \<open>
  Next, define the corresponding decode function, that takes as additional argument the
  non-zero length of the list:
\<close>

fun dec_list_aux :: "nat \<Rightarrow> nat \<Rightarrow> nat list" where
"dec_list_aux (Suc 0) _ = \<llangle>replace with your solution!\<rrangle>"
  | "dec_list_aux (Suc n) _ = \<llangle>replace with your solution!\<rrangle>"


text \<open>
  Prove that a non-empty list can be encoded as a number and decoded back
  if we know its length.
\<close>

lemma inv_list_aux: "xs \<noteq> [] \<Longrightarrow> dec_list_aux (length xs) (enc_list_aux xs) = xs"
oops

text \<open>Hint: you will need the fact @{thm inv_dec_enc} from the context, stating that dec is the left inverse of enc.\<close>




text \<open>
  The next step is to encode *any* list, i.e., encode a pair of the list 
  length and the auxiliary encoding \<open>enc (n, enc (x\<^sub>1, enc (x\<^sub>2, enc ( ..., x\<^sub>n )...)))\<close>
  
  Define the function \<open>enc_list\<close> of type @{typ "nat list \<Rightarrow> nat"}.
\<close>

definition enc_list :: "nat list \<Rightarrow> nat" where
"enc_list _ \<equiv> \<llangle>replace with your solution!\<rrangle>"

text \<open>
  Define the inverse function @{term dec_list}.
  This time we decode the natural number as a pair of length and encoded list value
  and we delegate the hard work to @{term dec_list_aux}.
\<close>

definition dec_list :: "nat \<Rightarrow> nat list" where
"dec_list _ \<equiv> \<llangle>replace with your solution!\<rrangle> "

text \<open>
  Finally prove that @{term dec_list} is the left inverse of @{term enc_list}.
\<close>

lemma "dec_list (enc_list xs) = xs"
oops
end


section \<open>3. Partitions (10p)\<close>

text \<open>
  For this exercise, we introduce multisets. A multiset is a finite set which allows elements to 
  occur multiple times. I.e. the multiset \<open>{#1,2,2,3#}\<close> is not the same as \<open>{#1,2,3#}\<close> (in contrast,
  \<open>{1,2,2,3} = {1,2,3}\<close>). Reordering does not affect equality (\<open>{#1,2,2,3#} = {#1,2,3,2#}\<close>). 
  We use them now in the context of 'partitioning'.
  
  Partitioning a natural number \<open>n\<close> means finding all multisets of natural numbers such that the 
  sum over all elements in the multisets add up to \<open>n\<close>. 
  I.e. \<open>partitions 3 = {{#3#},{#2,1#},{#1,1,1#}\<close>. The following definition computes the set of
  partitions. Try to understand why it works.
\<close>

fun partitions :: "nat \<Rightarrow> (nat multiset) set" where
  "partitions 0 = {{#}}" |
  "partitions (Suc n) = {add_mset (Suc x) X | x X. x < Suc n \<and> X \<in> partitions (n - x)}"

text \<open>
  To show the correctness of this function, we need to prove that a partition is in the output
  (given input n) if and only if a partition summed up equals n.
\<close>


subsection \<open>3a. Partitions sum to n (5p)\<close>

text \<open>
  Let's start with the easy case. If X is a partition of n, then summing up all entries of X
  equals n. We have given the outline of the Isar proof. Try to understand why we chose these
  parameters and finish the proof.
\<close>

lemma "X \<in> partitions n \<Longrightarrow> n = \<Sum>\<^sub># X" 
proof(induction n arbitrary: X rule: partitions.induct)
  case 1
  then show ?case 
    by auto
next
  case (2 n)

  note IH = 2(1)
  note XP = 2(2)

  text \<open>
    You are given the Induction Hypothesis (IH) and the fact that X is a partition of n (XP)
    With the information you have, you know that X is not the empty multiset. In other words, there
    exists a decomposition of X into another multiset Y and an element \<open>y \<in> X\<close> which we easily
    derive from the definition of \<open>partitions\<close>. We also know that Y must be another partition.
    This allows us to apply the induction hypothesis to obtain the sum of all values in Y.
    We also know that adding \<open>Suc y\<close> to partition Y increases its sum by \<open>Suc y\<close>. Try to find out
    why we add \<open>Suc y\<close> here (look at the definitions).
    This should suffice to prove the theorem.
  \<close>

 show ?case 
sorry
qed



subsection \<open>3b. Sum of n is a partition. (5p)\<close>

text \<open>
  The less intuitive direction is most likely showing that any multiset that sums up to n is
  also a partition. We have to be careful here though. Our definition does not include any
  partitions that contain a 0. This is why we state that 0 must not be in X (I.e. \<open>0 \<notin># X\<close>).
  Now fill in the missing proof. Try to understand why we chose these parameters and finish 
  the proof.
\<close>

lemma "0 \<notin># X \<Longrightarrow> n = \<Sum>\<^sub># X \<Longrightarrow> X \<in> partitions n"
proof(induction arbitrary: n rule: multiset_induct)
  case empty
  then show ?case
    by auto
next
  case (add y Y)

  note IH = add.IH
  note N0 = add.prems(1)
  note NSUM = add.prems(2)

text \<open>
  You are given the induction hypothesis (IH). You are also given a decomposition of a multiset 
  into a multiset Y and an element y. A guarantee that 0 is not in the multiset \<open>add_mset y Y\<close> 
  (N0) and that the multiset sums up to n (NSUM). We can derive that y is not 0 and Y does not 
  contain 0 from N0. We also know from NSUM what the sum of multiset Y is and that \<open>y \<le> n\<close>.
  We now have enough information to apply the induction hypothesis. Which tells us that Y is a
  partition. Since y is not 0 (N0) we know that n is not 0 (\<open>y \<le> n\<close>). So there exists a natural
  number m and x such that \<open>n = Suc m\<close> and \<open>y = Suc x\<close>. The latter allows us to prove \<open>x < n\<close>.
  We now have enough information to unfold the function definition and prove our theorem.
\<close>


  show ?case sorry
qed


section \<open>4. Refinement of Depth-first search (10p)\<close>

text \<open>
  Don't be afraid, the next proof may seem a bit overwhelming at first, but large portions have
  already been completed. You just have to understand what is going on and fill in the missing 
  parts.

  We are going to apply abstract datatypes to do a refinement-based proof. This means that we prove
  an abstract algorithm correct using "well-behaved" datatypes like a set. We then replace the set 
  with a more implementation-heavy data structure that is more efficient to implement (in memory, 
  performance or any other metric). This is the implementation. We then show a correspondence 
  between the abstract algorithm and the implementation. If we have proven the abstract algorithm
  correct, we know via this correspondence that the implementation is also correct.
\<close>


text \<open>
  In this exercise we take a look at a Depth-first search algorithm with fuel. Fuel is a natural 
  number n that is decreases with the depth of our search. This means that we explore a graph up 
  to depth n. Deeper nodes are not explored. This allows us to define DFS as a function without 
  having to deal with termination.

  We define a graph as a set of edges. An edge is a tuple of nodes. The type synonym for this is
  \<open>rel\<close> (see below).
\<close>

typ "'v rel"

text \<open>
  Before we apply this refinement technique, we show that DFS with fuel (n) correctly finds any 
  node v if there is a path of length at most n from the initial node to v. As we already said,
  most proofs for the correctness are already done and merely serve as setup for the main exercise.
  Try to understand them but don't get stuck on the details.

  Quick refresher: DFS is an algorithm that explores all nodes that are reachable from an initial 
  node in a graph recursively. After visiting a node v, one of its unvisited successors is visited.
  If there are no more unvisited successors, we return to the parent node (from which v was 
  visited) and repeat until we have visited all successors of the initial node. Our adaptation uses
  fuel which means we abort at depth n.

  For any node found by DFS, there is a sequence of connected edges whose length is \<le>n. A sequence 
  of length i is obtained via \<open>E^^i\<close> where E is the set of edges. The set of all sequences of 
  length \<le> n is obtained using \<open>En\<close>.
\<close>

  definition En :: "'v rel \<Rightarrow> nat \<Rightarrow> 'v rel" where "En E n \<equiv> \<Union>i\<le>n. E^^i" 
(* 
  NOTE: since this includes E^^0, all self loops are sequences of length 0, even for nodes that 
  don't have self-loops. This is comparable to the reflexive transitive closure where this also 
  occurs. This is a design choice in our definition and depends on your use-case. We chose this 
  approach because it is easier to deal with.
*)

  lemma En_0[simp]: "En E 0 = Id" unfolding En_def by auto  \<comment> \<open>Sequences of length 0 are "self loops"\<close>

  lemma En_Suc: "En E (Suc n) = En E n \<union> E^^(Suc n)"  \<comment> \<open>Sequences up to length Suc n are sequences up to length n and sequences of length Suc n\<close>
    unfolding En_def
    by (simp add: atMost_Suc sup.commute)
  
  lemma En_refl[simp, intro!]: "(v,v)\<in>En E n"  \<comment> \<open>Self loops are always in En\<close>
    unfolding En_def by force

  lemma in_En_conv: "(u,v)\<in>En E n \<longleftrightarrow> (\<exists>i\<le>n. (u,v)\<in>E^^i)"  \<comment> \<open>Sequences in En have a length that is \<le> n\<close>
    unfolding En_def by auto

  lemma relpow_mono: "E\<subseteq>E' \<Longrightarrow> E^^n \<subseteq> E'^^n" for E E' :: "'a rel"  \<comment> \<open>Subgraphs also have a subset of sequences of length n\<close>
    by (induction n) auto 
    
  lemmas relpow_mono' = relpow_mono[THEN subsetD]  
    
  lemma En_mono: "E\<subseteq>E' \<Longrightarrow> En E n \<subseteq> En E' n"  \<comment> \<open>Subgraphs also have a subset of sequences in En\<close>
    unfolding En_def
    by (auto simp: relpow_mono')
    
  lemmas En_mono' = En_mono[THEN subsetD]


text \<open>
  You have already seen the concept of paths in Isabelle. Paths are not directly occurring in our
  proof goals. But this definition is very straightforward to work with. We just need to connect 
  it to the concepts that we want to prove things about like \<open>En\<close>.

  First of all we set up some lemmas about path. You can safely ignore them for the exercises.
  However, for practice, we recommend that you try to understand what they do and how they are used
  (later on in the correctness proof).
\<close>

  fun path where
    "path E u [] v \<longleftrightarrow> u=v"
  | "path E u (v#vs) w \<longleftrightarrow> (u,v)\<in>E \<and> path E v vs w"  

  lemma path_append[simp]: "path E u (vs\<^sub>1@vs\<^sub>2) w \<longleftrightarrow> (\<exists>v. path E u vs\<^sub>1 v \<and> path E v vs\<^sub>2 w)"
    apply (induction vs\<^sub>1 arbitrary: u) by auto
  
  lemma path_refl[simp,intro!]: "path E u [] u" by simp

  lemma path_no_returnI:
    assumes "path E u vs v"
    obtains vs' where "path E u vs' v" "u\<notin>set vs'" "length vs' \<le> length vs"
    using assms
    apply (cases "u\<in>set vs")
    subgoal
      thm in_set_conv_decomp_last[of u vs] (* Instantiate, otherwise would loop on \<open>_\<notin>set _\<close> *)
      by (auto simp: in_set_conv_decomp_last[of u vs])
    subgoal by auto
    done
  
  lemma path_mono: "E\<subseteq>E' \<Longrightarrow> path E u vs v \<Longrightarrow> path E' u vs v"
    apply (induction vs arbitrary: u) by auto

  lemma path_restrict: "path E u vs v \<Longrightarrow> path (E \<inter> ((insert u (set (butlast vs)))\<times>(set vs))) u vs v"
  proof (induction vs arbitrary: u)
    case Nil
    then show ?case by auto
  next
    case (Cons u' vs)
    hence "path (E \<inter> (insert u' (set (butlast vs))) \<times> set vs) u' vs v" by auto
    hence "path (E \<inter> (insert u (insert u' (set (butlast vs)))) \<times> (insert u' (set vs))) u' vs v"
      apply (rule path_mono[rotated])
      by auto
    thus ?case using Cons.prems by auto
  qed    
    
    
  lemma relpow_Suc_prepend_simp: "E^^Suc n = E O E^^n"
    by (simp add: relpow_commute)
  
  lemmas [simp del] = relpow.simps(2)  
  lemmas [simp] = relpow_Suc_prepend_simp


subsection \<open>4a. Connecting En to path (2p)\<close>

text \<open>
  Now it is time to actually connect \<open>En\<close> to path. If \<open>(u,v) \<in> En E n\<close>, then there exists a path of
  length less than or equal to n. To prove this, we first connect the concepts of a sequence of 
  length n \<open>E^^n\<close> to a path as follows:
\<close>
  
  lemma relpow_path_conv: "(u,v)\<in>E^^n \<longleftrightarrow> (\<exists>vs. n = length vs \<and> path E u vs v)"
  proof (induction n arbitrary: u)
    case 0
    then show ?case by simp
  next
    case (Suc n)
    show ?case
    proof(rule iffI) \<comment> \<open>We split up an equivalence proof in two directions using this rule\<close>
      assume "(u, v) \<in> E ^^ Suc n"
text \<open>
  From our assumption, it follows that there is an edge \<open>(u,w)\<close> and a sequence of edges in between
  \<open>(w,v)\<close> of length n for some node w. From the induction hypothesis we then follow that there is
  a path of length n between those same states. Since \<open>(u,w)\<close> is an edge, we can extend this to a
  path of length \<open>Suc n\<close> between u and v.
\<close>

      thus "\<exists>vs. Suc n = length vs \<and> path E u vs v" sorry
    next
      assume "\<exists>vs. Suc n = length vs \<and> path E u vs v"
text \<open>
  From this assumption, we know that there also is some path of length n between w and v for some
  state w. Applying the induction hypothesis then yields a sequence of length n. Since we also know 
  that (u,w) is an edge, we can extend the sequence to length Suc n
\<close>

      with UW show "(u, v) \<in> E ^^ Suc n" sorry
    qed
  qed
  
text \<open>This allows a nice and easy proof that shows the equivalence of \<close>
  lemma En_path_conv: "(u,v)\<in>En E n \<longleftrightarrow> (\<exists>vs. length vs \<le> n \<and> path E u vs v)"
    by (auto simp: in_En_conv relpow_path_conv) 
  
  
text \<open>Now it is time to show the correctness of the algorithm against our definitions.\<close>
  lemma itddfs_aux1: "(u,v)\<in>E \<Longrightarrow> (v,w)\<in>En E n \<Longrightarrow> (u,w)\<in>En E (Suc n)"  
    by (force simp: in_En_conv)
  
  lemma itddfs_aux2: "(u, v) \<in> En E (Suc n) \<Longrightarrow> u\<noteq>v \<Longrightarrow> (u,v)\<in>E O En E n"  
    apply (clarsimp simp: in_En_conv)
    subgoal for i
      apply (cases i)
      subgoal by simp
      subgoal by (fastforce simp: in_En_conv)
      done
    done
    

  locale nat_graph = 
    fixes succs :: "nat \<Rightarrow> nat list" \<comment> \<open>This function returns for any node the list of its successors\<close>
    fixes P :: "nat \<Rightarrow> bool" \<comment> \<open>P returns the final nodes. Similarly to automata theory.\<close>
  begin     


    definition "E \<equiv> {(u,v). v\<in>set (succs u)}" \<comment> \<open>The set of edges is the set of all tuples induced by function \<open>succs\<close>\<close>

    lemma abs_succs[simp]: "v \<in> set (succs u) \<longleftrightarrow> (u,v)\<in>E"
      unfolding E_def by auto

text \<open>
  We now define the DFS algorithm with fuel. This is often used in the context of iterative 
  deepening. A hybrid form of DFS and BFS. It allows to quickly explore deep paths while not 
  getting stuck at one branch. Our DFS tries to find a final node (one that satisfies P) and 
  returns True if it finds one.

  We also use a set V to keep track of the visited nodes. If a node has already been visited then
  we don't have to explore it again. Any time we discover a new node we add it to V.
  Note that we use \<open>list_ex\<close> here which is an existence quantifier for lists.
\<close>
          
    fun itddfs2_abs where
      "itddfs2_abs V 0 v = (if v \<in> V then False else P v)"
    | "itddfs2_abs V (Suc n) v = (if v \<in> V then False else if P v then True else list_ex (itddfs2_abs (insert v V) n) (succs v))"  
      
text \<open>Some auxiliary lemmas\<close>
    lemma itddfs2_aux1: 
      assumes NE: "u \<notin> V"
      assumes 1: "(u, v) \<in> E"
      assumes 2: "(v, w) \<in> En (E \<inter> (- insert u V) \<times> UNIV) n"
      shows "(u,w) \<in> En (E \<inter> (-V) \<times> UNIV) (Suc n)"
    proof -
      from 2 have "(v, w) \<in> En (E \<inter> (-V) \<times> UNIV) n" 
        apply (rule En_mono'[rotated])
        by blast
      from itddfs_aux1[OF _ this, where u=u] show ?thesis 
        using 1 NE by blast
    qed
    
    lemma itddfs2_aux2: 
      assumes A: "(u, w) \<in> En (E \<inter> (- V) \<times> UNIV) (Suc n)" 
      assumes NE: "u\<noteq>w"
      shows "\<exists>v\<in>set (succs u). (v, w) \<in> En (E \<inter> (- insert u V) \<times> UNIV) n"
    proof -
      from A obtain vs where "path (E \<inter> (- V) \<times> UNIV) u vs w" "length vs \<le> Suc n"
        by (auto simp: En_path_conv)
      from path_no_returnI[OF this(1)] this(2) obtain vs' where 
        "path (E \<inter> (- V) \<times> UNIV) u vs' w" "u \<notin> set vs'" and LEN': "length vs' \<le> Suc n" 
        by (metis order_trans)
      with NE obtain v vs'' where 
        [simp]: "vs'=v#vs''" 
        and S1: "(u,v)\<in>E" 
        and P'': "path (E \<inter> (- V) \<times> UNIV) v vs'' w" 
        and NN: "u\<noteq>v" "u\<notin>set vs''"
        by (cases vs') auto
        
      from path_restrict[OF P''] have "path (E \<inter> (- insert u V) \<times> UNIV) v vs'' w" 
        apply (rule path_mono[rotated])
        using NN by (auto dest: in_set_butlastD)
      with S1 show ?thesis using LEN'
        by (auto simp: En_path_conv) 
    qed    
      
text \<open>We generalize set V and \<close>
    lemma itddfs2_correct_aux: "(\<forall>v\<in>V. \<not>P v) \<Longrightarrow> itddfs2_abs V n u \<longleftrightarrow> (\<exists>v. (u,v)\<in>En (E \<inter> ((-V) \<times> UNIV)) n \<and> P v)"
      apply (induction V n u rule: itddfs2_abs.induct)
      apply auto []
      apply (rule iffI)
      subgoal for V n v by (auto split: if_splits simp: list_ex_iff dest: itddfs2_aux1)
      subgoal
        apply (clarsimp simp: list_ex_iff)
        apply (intro conjI impI)
        subgoal by blast
        subgoal by (auto dest: itddfs_aux2)
        subgoal by (blast dest: itddfs2_aux2)
        done
      done

text \<open>
  And with some effort, we have proven the correctness of the algorithm. But we used sets for this
  algorithm. For an implementation, these are not suitable. We want something that is more 
  efficient. That is why you will now prove the correctness of the same algorithm, but the set of
  visited edges is now implemented as a sorted list.
\<close>
    theorem itddfs2_abs_correct: "itddfs2_abs {} n u \<longleftrightarrow> (\<exists>v. (u,v)\<in>En E n \<and> P v)"
      by (simp add: itddfs2_correct_aux)
      

  end

text \<open>The abstraction function and the invariant. The invariant is actually necessary for correct 
  efficient implementation of this data structure.\<close>

  definition "slist_\<alpha> xs = set xs" \<comment> \<open>Abstraction: list to set\<close>
  definition "slist_invar xs = (sorted xs \<and> distinct xs)" \<comment> \<open>Invariant: our list is sorted and no element occurs more than once.\<close>

text \<open>
  We implement membership via a binary search. When looking for an element i in a sorted list, we 
  query the element halfway through the list. If it equals i, we have found what we are looking for.
  If \<open>i\<close> is smaller, then the index must be in the part of the list following it (lists are sorted)
  and repreat this procedure for that part of the list. Conversely, if \<open>i\<close> is greater, then if must 
  be in the part preceding it, so we continue the binary search there.
\<close>
  fun slist_member :: "nat \<Rightarrow> nat list \<Rightarrow> bool" where 
    "slist_member y xs = (if xs=[] then False else (let i = length xs div 2 in (if xs!i > y then slist_member y (take i xs) else if xs!i < y then slist_member y (drop (Suc i) xs) else True)))"
  lemmas [simp del] = slist_member.simps

text \<open>We insert elements just via a linear implementation.\<close>
  fun slist_insert :: "nat \<Rightarrow> nat list \<Rightarrow> nat list" where
    "slist_insert x [] = [x]" |
    "slist_insert x (y#ys) = (if x = y then y#ys else if x < y then (x#y#ys) else y#slist_insert x ys)"


subsection \<open>4b. slist_member implies set membership (3p)\<close>

text \<open>
  Now that we have all of the desired operations, it is time to prove that they correctly 
  represent set operations. We start with membership. \<open>slist_member\<close> should correspond to the
  set membership \<open>\<in>\<close>. Note that the invariant needs to be satisfied for this, since we are doing a
  binary search. We split our equality proof in two implications again.
\<close>

  lemma slist_member_correct_aux1: "slist_invar xs \<Longrightarrow> slist_member x xs \<Longrightarrow> x \<in> slist_\<alpha> xs"
  proof(induction xs rule: slist_member.induct)
  next
    case (1 x vs)
    then show ?case proof (cases "vs=[]")
      case True thus ?thesis
        using "1.prems"(2) slist_member.simps by auto
    next
      case False

      define i where "i = length vs div 2"
      hence IL: "i < length vs" using False
        by auto
  
text \<open>
  We can make our own case distinction using \<open>consider\<close>. We now that there is three cases in 
  binary search. The case where x is less than, equal or greater than the element halfway of the 
  list.
\<close>
      then consider (lt) "vs ! i < x" | (eq) "vs ! i = x" | (gt) "vs ! i > x"
        by linarith

text \<open>
  You get the drill by now. Gather enough information for the induction hypothesis in the lt and 
  gt cases. The eq case is easy enough.
\<close>

      then show ?thesis
      proof cases
        case lt

 
        show ?thesis sorry
      next
        case eq
        then show ?thesis
          unfolding slist_\<alpha>_def 
          using IL nth_mem by blast
      next
        case gt

        show ?thesis sorry
          
      qed
    qed
  qed


subsection \<open>4c. Set membership to slist_member (3p)\<close>

text \<open>
  We will not bother you with the other direction too much. But there are two nice auxiliary lemmas
  that are nice to prove:
  \<open>sorted_lt_nth_mem_drop\<close> states that in a nonempty sorted list, if we have an index i and a value
  y that is in that list, then it is also in the part of the list after index i.

  sorted list [.., .., .... .., .., .... .., ..] that contains y
                     index i ^  |--------------|
  y greater than this value ^          ^ so y must be in here

\<close>

  lemma sorted_lt_nth_mem_drop:
    assumes "sorted xs"
    assumes "xs\<noteq>[]"
    assumes "i<length xs"
    assumes "xs!i < y"
    assumes "y\<in>set xs"
    shows "y\<in>set (drop (Suc i) xs)"
  proof -
    have XSD: "xs = take i xs @ xs!i # drop (Suc i) xs"
      by (simp add: Cons_nth_drop_Suc assms(3))
    from \<open>y\<in>set xs\<close> obtain j where "y = xs!j" and "j < length xs"
      by(auto simp: in_set_conv_nth)
    with \<open>sorted xs\<close> have "i < j"
      by (metis assms(3) assms(4) leD leI sorted_nth_mono)
    hence "y = drop (Suc i) xs ! (j - Suc i)" 
      by (simp add: Suc_leI \<open>y = xs ! j\<close> assms(3))
    with XSD show ?thesis 
      by (metis Suc_leI \<open>i < j\<close> \<open>j < length xs\<close> assms(3) length_drop less_diff_iff nth_mem)
  qed
  

text\<open>
  Next up we have the symmetric case. Now it is up to you to finish the proof.
\<close>

  lemma sorted_gt_nth_mem_take:
    assumes "sorted xs"
    assumes "xs\<noteq>[]"
    assumes "i<length xs"
    assumes "y < xs!i"
    assumes "y\<in>set xs"
    shows "y\<in>set (take i xs)"
  proof -
 
    show ?thesis sorry
  qed    

text \<open>
  The other direction then still requires some effort, but it works. We did it for you. 
\<close>

  lemma slist_member_correct_aux2: "slist_invar xs \<Longrightarrow> x \<in> slist_\<alpha> xs \<Longrightarrow> slist_member x xs"
    unfolding slist_invar_def slist_\<alpha>_def
    apply(induction xs rule: slist_member.induct)
    subgoal for y xs
      apply (cases "xs=[]")
      subgoal by simp
      subgoal
        apply (subst slist_member.simps; simp)
        apply (auto simp: Let_def sorted_wrt_take sorted_wrt_drop sorted_lt_nth_mem_drop sorted_gt_nth_mem_take)
        done
      done
    done
        
text \<open>
  And with that the correctness proof of the membership function goes through.
\<close>
  lemma slist_member_correct: "slist_invar xs \<Longrightarrow> slist_member x xs \<longleftrightarrow> x \<in> slist_\<alpha> xs"
    using slist_member_correct_aux1 slist_member_correct_aux2 
    by blast

  lemma slist_insert_correct: "slist_\<alpha> (slist_insert x xs) = insert x (slist_\<alpha> xs)"
    apply (induction xs)
    unfolding slist_\<alpha>_def 
    by auto

  lemma slist_insert_invar: "slist_invar xs \<Longrightarrow> slist_invar (slist_insert x xs)"
    using slist_insert_correct unfolding slist_invar_def slist_\<alpha>_def
    apply (induction xs)
    by auto 

  lemma slist_empty_correct: "slist_\<alpha> [] = {}"
    unfolding slist_\<alpha>_def by blast

  lemma slist_empty_invar: "slist_invar []"
    unfolding slist_invar_def by simp



subsection \<open>4d. Correctness of our implementation (2p)\<close>

  context nat_graph
  begin

text \<open>
  This is the exact same algorithm. But all of the set operations have been replaced with slist 
  operations that we have proven to be equivalent (modulo abstraction). 
\<close>

    fun itddfs2_slist_impl where
        "itddfs2_slist_impl vds 0 v = (if slist_member v vds then False else P v)"
      | "itddfs2_slist_impl vds (Suc n) v = (if slist_member v vds then False else if P v then True else list_ex (itddfs2_slist_impl (slist_insert v vds) n) (succs v))"  

                                                                                                                           
    text \<open>You will probably use
      induction so we prove the generalized version. Can you find a reason why we assume the invariant
      to hold on our generalized list?\<close>
    lemma itddfs2_slist_impl_refine: "slist_invar vds \<Longrightarrow> itddfs2_slist_impl vds n v \<longleftrightarrow> itddfs2_abs (slist_\<alpha> vds) n v"
      thm list_ex_iff  \<comment> \<open>To unfold list_ex, use this theorem\<close>
oops

    theorem itddfs2_slist_impl_correct: "itddfs2_slist_impl [] n u \<longleftrightarrow> (\<exists>v. (u,v)\<in>En E n \<and> P v)"
oops

text \<open>
  Congratulations, you have now proven the correctness of the implementation through refinement.
  If you want to see why this is important for program verification, try to prove the correctness
  for the implementation directly, without having an abstract version. You will see that the
  encoding of the set as a sorted list will either get mixed with your correctness proofs, or you
  need to do extensive management to keep these two concepts separate.
  With refinement, this separation of implementation and correctness is separated automatically.
  Allowing you to focus on the proofs only.
\<close>
  end


end
