chapter \<open>Homework 7\<close>
theory Homework7
imports Main
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #7
    RELEASED: Wed, Dec 15 2022
    DUE:      Wed, Jan 12, 2022, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)

section \<open>1: Be creative (80 points + bonus points for creative/elegant/ingenious solutions)\<close>

(*  
  Develop your own piece of interesting theory. 
  This can be correctness of an algorithm, a mathematical theorem, etc.
  
  Requirement: something must be proved, i.e., 
  just a functional program in Isabelle without correctness proof is not enough!
                
  Ideally, come up with your own idea. Do not copy from existing repositories without proper referencing, 
    we fill find out about this!
  
  This project works in two stages:
  
  Until Friday, 22.12.2023, 23:59 you must submit your initial idea for a feasibility check!
    For that, use a separate email with subject [ITP PROJECT]! 
    \<^bold>D\<^bold>o \<^bold>n\<^bold>o\<^bold>t \<^bold>c\<^bold>o\<^bold>m\<^bold>b\<^bold>i\<^bold>n\<^bold>e \<^bold>w\<^bold>i\<^bold>t\<^bold>h \<^bold>y\<^bold>o\<^bold>u\<^bold>r \<^bold>h\<^bold>o\<^bold>m\<^bold>e\<^bold>w\<^bold>o\<^bold>r\<^bold>k \<^bold>s\<^bold>u\<^bold>b\<^bold>m\<^bold>i\<^bold>s\<^bold>s\<^bold>i\<^bold>o\<^bold>n\<^bold>, \<^bold>o\<^bold>r \<^bold>w\<^bold>e \<^bold>m\<^bold>i\<^bold>g\<^bold>h\<^bold>t \<^bold>o\<^bold>v\<^bold>e\<^bold>r\<^bold>l\<^bold>o\<^bold>o\<^bold>k \<^bold>y\<^bold>o\<^bold>u\<^bold>r \<^bold>s\<^bold>u\<^bold>b\<^bold>m\<^bold>i\<^bold>s\<^bold>s\<^bold>i\<^bold>o\<^bold>n\<^bold>!
    
    Ideally, your feasibility check submission contains a .thy file with:
      \<^item> A description of what you want to formalize
        \<^item> including reference to source of your idea, if applicable (e.g. advent of code #12)
      \<^item> For a mathematical theorem: the theorem statement, formally in Isabelle
      \<^item> For an algorithm: pseudocode and (semi)formal correctness specification
      \<^item> A plan: how to tackle your project? Where do you expect difficulties? 
        Is there a minimal viable product?

  Submission deadline: 12.01.2023, 23:59
    you must submit your final project by then.

    
  Should you plan to formalize an algorithm, make sure to 
  solve Homework6, and read/understand DFS_Demo before you start working on your proofs.
    
      
  If you need inspiration what to do, here are a few suggestions:
        
  E.g. port some existing proofs to Isabelle/HOL \<^url>\<open>https://toccata.gitlabpages.inria.fr/toccata/gallery/\<close>
    \<^item> Pancake Sorting \<^url>\<open>https://toccata.gitlabpages.inria.fr/toccata/gallery/pancake_sorting.en.html\<close>
    \<^item> Ring Buffer (possibly combined with refinement) \<^url>\<open>https://toccata.gitlabpages.inria.fr/toccata/gallery/ring_buffer.en.html\<close>
    \<^item> Same Fring \<^url>\<open>https://toccata.gitlabpages.inria.fr/toccata/gallery/same_fringe.en.html\<close>
    \<^item> Minimal Distance of two words \<^url>\<open>https://toccata.gitlabpages.inria.fr/toccata/gallery/edit_distance.en.html\<close>
    \<^item> Maximal subarray problem \<^url>\<open>https://toccata.gitlabpages.inria.fr/toccata/gallery/maximum_subarray.en.html\<close>
    Be aware, porting proofs may be harder than it sounds!    

  Or some other ideas:
    \<^item> natural merge sort (e.g. implemented in Haskell \<^url>\<open>https://hackage.haskell.org/package/base-4.17.0.0/docs/src/Data.OldList.html#sortBy\<close>)
    \<^item> a (simple) algorithm to solve linear equations (e.g. Gauss)
    \<^item> prime number sieves
    \<^item> grammar (inductive) and parser (fun) for arithmetic expressions/Simple XML/\<dots>, including lexer!
    \<^item> splitting the chain (in german, but google translate does a reasonable job)) \<^url>\<open>https://www.spektrum.de/raetsel/wie-oft-muss-die-kette-mindestens-zerschnitten-werden/2179968\<close>
    
  Additionally, try to think about some interesting mathematical/logic problem that you encountered
  in a hackathon (or other contest like Advent of Code), video or during a lecture
    
    
  Please document your proofs well. Add as many comments as possible so we can
    understand what you are proving. Good documentation will affect the grading
    in a positive way.

  Hint: keep it simple! If you have an ambitious idea, first try to prove the minimal viable 
    product


  Grading:
    Your grades will be determined by a number of things:
    - Use of sledgehammer:
        We don't appreciate 'wild' proof attempts that are somehow rescued with sledgehammer, 
        and show that you didn't really understand how the proof works. 
        Still, we prefer a somehow completed proof over a "sorry".
        
        Using sledgehammer to bring forward a clear, targeted proof structure is fine, though.
    - Use of sorry: We think that "sorry" is a good way to test whether an auxiliary lemma can be
        used to prove your statement. If you run out of time, we prefer a finished proof that has
        a few sorries in it over a proof that never came to its most important statement. If you 
        sorry a proof, please give us a comment as to why you think that your lemma should hold. 
        If you do so, we will show more mercy in the grading. However, if you sorry a lemma that 
        is clearly False, we will be strict. So be careful!
        tldr: IF YOU USE SORRY, ADD COMMENTS SAYING WHY YOU THINK YOUR SORRIED LEMMA HOLDS
    - Use of comments: use plenty of comments to explain us your thought process and the structure 
        of your proof. Also add comments what your lemmas intuitively mean, where appropriate. 
        Ideally, your comments read like a story that will guide us through your proof.
    - Ingenuity/elegance: Clever and elegant proofs of non-trivial theorems will get you bonus points.
        If you have proven a lemma and want to make it more elegant because you had a clever insight, 
        please keep the old lemma (as well as the new lemma of course) and explain the differences 
        and the insight (you don't have to, but it increases the chances that we know how to 
        appreciate your proof).
    - Creativity: We really like it if you come up with your own idea or give your own twist to 
        one of our suggestions. Let your mind run free!

    We understand that the last three points are a bit fluffy. If you are not sure whether your
    proof meets our standards you can always ask us for feedback on your idea/proof We may also 
    give you some additional ideas that you can work on. Please be on time with this!

    Disclaimer: If you did not ask for our feedback in time, please don't complain afterwards.

    You can contact Peter (p.lammich@utwente.nl) 
    and/or Bram (b.kohlen@utwente.nl) 
    and/or Edoardo (e.putti@utwente.nl)
    for any questions.


    For now, merry Christmas, a happy new year and good luck proving your theorems!

*)




end

