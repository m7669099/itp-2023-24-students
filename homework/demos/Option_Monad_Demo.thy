chapter \<open>Option monad and tools, from option-monad-demo and while-demo\<close> 
theory Option_Monad_Demo
imports 
  Complex_Main
  "HOL-Library.Monad_Syntax" (* To get the do-notation in Isabelle *)
  
begin


section \<open>Assertions\<close> 
  
(* Assert. Return () if condition is true, None otherwise. *)
  
definition "assert P \<equiv> if P then Some () else None"

lemma assert_simps[simp]:
  "assert False = None"
  "assert True = Some ()"
  by (auto simp: assert_def)

  
section \<open>Weakest Precondition\<close>
  
  fun wp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "wp None Q = False"
  | "wp (Some x) Q = Q x"  
  
  lemma wp_bind[simp]: "wp (do { x\<leftarrow>m; f x }) Q = wp m (\<lambda>x. wp (f x) Q)"
    by (cases m) auto

  subsection \<open>Intro-Rules\<close>  
    
  named_theorems wp_intro
  
  lemma wp_returnI[wp_intro]: "Q a \<Longrightarrow> wp (Some a) Q" by simp
  lemma wp_bindI[wp_intro]: "wp m (\<lambda>x. wp (f x) Q) \<Longrightarrow> wp (do { x\<leftarrow>m; f x }) Q" by simp

  (* Consequence rule: Weakening post-condition 
  
    If c ensures Q, and Q \<Longrightarrow> Q', then c ensures Q'
  *)
  lemma wp_cons: 
    assumes "wp c Q"
    assumes "\<And>s. Q s \<Longrightarrow> Q' s"
    shows "wp c Q'"
    using assms by (cases c) auto
  
    
  (* Plus case-splitting rules for better usability  *)
  
  lemma wp_case_prodI[wp_intro]:
    assumes "\<And>a b. \<lbrakk> p=(a,b) \<rbrakk> \<Longrightarrow> wp (f a b) Q"
    shows "wp (case p of (a,b) \<Rightarrow> f a b) Q"
    using assms by (cases p) auto
  

  lemma wp_if[wp_intro]:
    assumes "b \<Longrightarrow> wp c Q"
    assumes "\<not>b \<Longrightarrow> wp d Q"
    shows "wp (if b then c else d) Q"
    using assms by auto


section \<open>While Loops\<close>  

  context
    fixes b :: "'s \<Rightarrow> bool"
    fixes c :: "'s \<Rightarrow> 's option"
  begin
  
    partial_function (option) while :: "'s \<Rightarrow> 's option" where
      "while s = (
        if b s then do {
          s \<leftarrow> c s;
          while s
        } else Some s)" 

        
    lemma wp_while_aux:
      assumes WF: "wf V"
      assumes I0: "I s"
      assumes STEP: "\<And>s. \<lbrakk> I s; b s \<rbrakk> \<Longrightarrow> wp (c s) (\<lambda>s'. I s' \<and> (s',s)\<in>V)"
      shows "wp (while s) (\<lambda>s. I s \<and> \<not>b s)"
      using WF I0
    proof (induction s rule: wf_induct_rule)
      case (less s)
      
      note \<open>I s\<close> \<comment> \<open>Invariant holds for s\<close>
      
      show ?case proof (cases "b s")
        case True
        then show ?thesis
          apply (subst while.simps) 
          apply simp
          apply (rule wp_cons[OF STEP])
          apply (rule \<open>I s\<close>)
          apply assumption
          apply (rule less.IH)
          by auto
      next
        case False  
        then show ?thesis
          apply (subst while.simps) 
          by (simp add: \<open>I s\<close>)
      qed
    qed
        
  
    (* For better applicability, we combine the while-rule with a consequence rule: *)
    lemma wp_while:
      assumes WF: "wf V"
      assumes I0: "I s"
      assumes STEP: "\<And>s. \<lbrakk> I s; b s \<rbrakk> \<Longrightarrow> wp (c s) (\<lambda>s'. I s' \<and> (s',s)\<in>V)"
      assumes CONS: "\<And>s. \<lbrakk> I s; \<not>b s \<rbrakk> \<Longrightarrow> Q s"
      shows "wp (while s) Q"
      apply (rule wp_cons)
      apply (rule wp_while_aux[where I=I, OF WF I0 STEP])
      apply assumption
      apply assumption
      using CONS by blast
          
  end

end