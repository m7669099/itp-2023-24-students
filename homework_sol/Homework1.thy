chapter \<open>Homework 1\<close>
theory Homework1 (* This file must be called Homework1.thy *)
imports 
  Main 
  Homework_Lib (* Homework_Lib.thy must be in the same folder as this file! *)
begin
  
text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #1
  RELEASED: Mon, Nov 13 2023
  DUE:      Fri, Nov 17, 2023, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
  
  This homework is worth 20 points.
  
  This is a small homework, as you only have a few days to work on it. 
  The next homeworks will be bigger, typically 40 points.
\<close>
  

section \<open>General Hints\<close>  

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...
\<close>


section \<open>Run Length Decoding\<close>

text \<open>
  Run length encoding works by summarizing runs of equal elements to a number and one copy of the element.
  
  For example, the string (list of characters) \<^term>\<open>''abbbbbccd''\<close> is encoded as
  \<^term>\<open>[(1,CHR ''a''), (5, CHR ''b''), (2, CHR ''c''), (1,CHR ''d'') ]\<close>

  For data with long runs of equal elements (think of slides with a lot of white pixels), 
  this will compress the data.
\<close>


subsection \<open>Decoder (8p)\<close>

text \<open>
  Check out the function \<^const>\<open>replicate\<close>. 
  What does it do?
  
  You can check it's type, use value to play around with it, 
  or have a look at it's definition (use CTRL-click to jump to definition)
\<close>

(* Check the output of the following commands *)

term replicate

value "replicate 10 True"

value "replicate 10 ''Hello''"


text \<open>Now define a function of type \<^typ>\<open>(nat\<times>'a) list \<Rightarrow> 'a list\<close> that decodes a run-length encoded list.
  Define it recursively over the argument list.
\<close>

fun decode :: "(nat\<times>'a) list \<Rightarrow> 'a list" where
  "decode [] = []"
| "decode ((n,x)#xs) = replicate n x @ decode xs"
  
(* Test your function on a few examples! *)
value "decode [(1,CHR ''a''), (5, CHR ''b''), (2, CHR ''c''), (1,CHR ''d'') ]"
(*
  Note: the error message about "No code equations for replace_with_your_solution" 
  should go away once you have added your actual solution.
*)


subsection \<open>Encoder Step 1  (4p)\<close>

text \<open>Define a function \<open>rlen\<close> of type \<^typ>\<open>'a \<Rightarrow> 'a list \<Rightarrow> nat\<close> 
  that counts how many elements equal to the first argument there are at the beginning of the list.
  E.g., \<open>rlen 2 [2,2,2,2,4,2,4,5,5] = 4\<close>.
\<close> 

text \<open>Hint: You can use
  \<^term>\<open>(if cond then exp1 else exp2)\<close>
  
  Always put it in parentheses, as you will get syntax errors otherwise, e.g.:
  \<^term>\<open>f x = (if cond then exp1 else exp2)\<close>
\<close>

fun rlen :: "'a \<Rightarrow> 'a list \<Rightarrow> nat" where
(* add your equations here *)

  "rlen x [] = 0"
| "rlen x (y#ys) = (if x=y then Suc (rlen x ys) else 0)"


text \<open>Test your implementation on a few values!\<close>

value "rlen CHR ''a'' ''aaabc''"

subsection \<open>Encoder Step 2  (4p)\<close>

text \<open>Define the encoder, using your \<open>rlen\<close>, and \<^const>\<open>drop\<close> to remove the run from the start of a list.

  \<^term>\<open>drop n xs\<close> drops the first \<open>n\<close> elements from the list \<open>xs\<close>, e.g. \<^term>\<open>drop 2 ''aaabc'' = ''abc''\<close>
\<close>

term drop
value "drop 2 ''aaabc''"

fun encode :: "'a list \<Rightarrow> (nat \<times> 'a) list" where
(* add your equations here *)

  "encode [] = []"
| "encode (x#xs) = (Suc (rlen x xs),x) # encode (drop (rlen x xs) xs)"


text \<open>Test your function, using quickcheck! Do NOT prove anything yet!\<close>

lemma "decode (encode xs) = xs" for xs::string
  quickcheck
  (* This should output "Quickcheck found no counterexample."
  
    If it outputs something like:
      Quickcheck found a counterexample:
        xs = [CHR 0xFF]
      Evaluated terms:
        decode (encode xs) = []  
        
    use value to debug your program, and fix it!
  *)
  
  oops (* We are not going to *prove* this (yet), so we abandon the proof attempt *)  


subsection \<open>Simple proof  (4p)\<close>  
  
text \<open>Prove by induction on the list, that the length of a run cannot be longer than the list\<close>
lemma "rlen x xs \<le> length xs"  
  (* Replace the oops with your proof! *)
  
  apply (induction xs)
  apply auto
  done
  
  
  
  
  
  
text \<open>Obviously, we *can* also prove the correctness of the decoder and encoder: \<close>

(* Auxiliary lemma is required: *)
lemma [simp]: "replicate (rlen x xs) x @ drop (rlen x xs) xs = xs"
  apply (induction xs)
  by auto
  
lemma "decode (encode xs) = xs" for xs::string
  apply (induction xs rule: encode.induct) (* Computation induction over encoder. *)
  by auto
  
  
  
  
  
  
end

