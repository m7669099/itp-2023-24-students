theory Homework6
  imports Main
          Homework_Lib
          "demos/Option_Monad_Tools"
begin


text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #6
  RELEASED: Fri, Dec 15, 2023
  DUE:      Fri, Dec 22, 2023, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
  
  This homework is worth 20 points.
\<close>

section \<open>General Hints\<close>

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...
\<close>

section \<open>1. Reduced fractions (10p)\<close>

text \<open>
  During the lecture we saw one of the possible implementation of Euclid's algorithm
  and proved it correct. The correctness of the algorithm is expressed as a lemma
  about the weakest-precondition for the program.

  At each step of the algorithm you compute the pair at step n+1 \<open>a\<^sub>n\<^sub>+\<^sub>1, b\<^sub>n\<^sub>+\<^sub>1\<close> from the pair at
  step n \<open>a\<^sub>n, b\<^sub>n\<close>. The GCD of the pairs does not change.
  
  Here is a version of Euclid's algorithm that uses integer numbers.
\<close>

definition euclid :: "int \<Rightarrow> int \<Rightarrow> int option" where "euclid a b \<equiv> do {
  (r,_) \<leftarrow> while (\<lambda>(a\<^sub>n,b\<^sub>n). a\<^sub>n\<noteq>b\<^sub>n) (\<lambda>(a\<^sub>n,b\<^sub>n). if a\<^sub>n < b\<^sub>n then Some (b\<^sub>n-a\<^sub>n,a\<^sub>n) else Some (a\<^sub>n-b\<^sub>n,b\<^sub>n)) (a,b);
  Some r
}"

lemma wp_euclid: "a > 0 \<Longrightarrow> b > 0 \<Longrightarrow> wp (euclid a b) (\<lambda>r. r = gcd a b)"  
  unfolding euclid_def
  apply (intro wp_intro 
    wp_while[where 
          V="measure (\<lambda>(a,b). nat(a+b))"
      and I="\<lambda>(a',b'). gcd a b = gcd a' b' \<and> a'>0 \<and> b'>0"
    ])
  apply (simp_all add: gcd_diff1)
  by (metis gcd.commute)


text \<open>
  Given a fraction a/b you can always reduce it to a normalized form where a and b are
  coprime: in Isabelle's terms you would write @{term "coprime a b"}.

  The normalize a fraction you have to divide both numerator and denominator by the same
  common divisor until the only common divisor is 1.

  Write a program \<open>redfrac\<close> that computes the normalized form of any fraction a/b.
  
  Hint:
    - use @{const euclid} to find the greatest common divisor
    - does @{const euclid} work on all integer numbers? Take care of
      using @{const abs} to make your program correct. It will appear using the notation
      \<open>abs a = \<bar>a\<bar> \<close>
    - do not complicate the program with checking for \<open>b = 0\<close>.
      We assume \<open>b \<noteq> 0\<close> as the precondition.
\<close>

definition redfrac :: "int \<Rightarrow> int \<Rightarrow> (int \<times> int) option" where 
"redfrac a b \<equiv> do {
  if a=0 then Some (0,1)
  else do {
    gcd \<leftarrow> euclid \<bar>a\<bar> \<bar>b\<bar>;
    Some (a div gcd, b div gcd)
  }
}"

text \<open>
  We want to prove our program correct. To do so we want to prove that if \<open>b \<noteq> 0\<close> the program
  computes the normalized form of the fraction a/b.

  Prove the following lemma about the program \<open>redfrac\<close>.

  In the postcondition \<open>(Q = \<lambda>(a',b'). Fract a b = Fract a' b' \<and> coprime a' b')\<close>
  the term @{const Fract} is the data constructor for a fraction. The result fraction
  \<open>Fract a' b'\<close> must be the same as the original \<open>Fract a b\<close> but the two numbers
  must be coprime.

  Hint:
    - use the set of lemmas \<open>wp_intro\<close> where possible to make the simplifier go
      through \<open>wp (case ...), wp (if ...), ...\<close>
    - use sledgehammer when convenient
    - do not unfold euclid! Use the proof of correctness from the lecture along
      with \<open>wp_cons\<close>
\<close>


lemmas [wp_intro] = wp_cons[OF wp_euclid]


lemma wp_redfrac: "b \<noteq> 0 \<Longrightarrow> wp (redfrac a b) (\<lambda>(a',b'). Fract a b = Fract a' b' \<and> coprime a' b')"

  unfolding redfrac_def
  apply (intro wp_intro)
  by (simp_all add: rat_number_collapse Fract_coprime div_gcd_coprime)





section \<open>2. Finding a leaf (10p)\<close>

text \<open>
  Our next task is to find a leaf in a directed acyclic graph (DAG). In other words, find a path 
  to an arbitrary node. In other words, a node for which there is no outgoing edges. Since we are 
  working with DAGs this is an easy task. Given a starting node we just pick an arbitrary successor
  and repeat the process until we reach a leaf.

  This is the algorithm that we want to prove correct. For this we reintroduce the \<open>path\<close> which is
  the output of our program. We give you some auxiliary lemmas of paths. You have seen these before
  in the homework.
\<close>

fun path where
  "path E u [] v \<longleftrightarrow> u=v"
| "path E u (x#xs) v \<longleftrightarrow> (u,x)\<in>E \<and> path E x xs v"    

lemma path_append[simp]: "path E u (xs@ys) w \<longleftrightarrow> (\<exists>v. path E u xs v \<and> path E v ys w)"
  by (induction xs arbitrary: u) auto

lemma reachable_imp_path: "(u,v)\<in>E\<^sup>* \<Longrightarrow> \<exists>xs. path E u xs v" 
proof(induction rule: converse_rtrancl_induct)
  case base
  have "path E v [] v"
    by simp
  then show ?case by blast
next
  case (step y z)
  then obtain xs where "path E z xs v"
    by presburger
  with step.hyps(1) have "path E y (z#xs) v"
    by simp
  then show ?case
    by blast
qed

lemma path_imp_reachable: "path E u xs v \<Longrightarrow> (u,v)\<in>E\<^sup>*"  
  apply (induction xs arbitrary: u)
  apply simp
  by force

lemma reachable_eq_path:
  "(u,v)\<in>E\<^sup>* \<longleftrightarrow> (\<exists>xs. path E u xs v)" 
  using path_imp_reachable reachable_imp_path by meson


subsection \<open>2a. Finite reachability of a path (2p)\<close>

text\<open>
  If we can only reach a finite set of states from some state \<open>u\<close> (I.e. \<open>finite (E\<^sup>* `` {u})\<close>) and
  have any path starting in \<open>u\<close> and ending in \<open>v\<close>, then we can also only reach a finite set of
  states from v, (since v is reachable from u).
\<close>

lemma finite_reachable_path: 
  assumes PATH: "path E u vs v" and FIN: "finite (E\<^sup>* `` {u})" 
  shows "finite (E\<^sup>* `` {v})"

proof -
  from PATH have "(u,v) \<in> E\<^sup>*" by(rule path_imp_reachable)
  hence "E\<^sup>* `` {v} \<subseteq> E\<^sup>* `` {u}" by force
  with FIN show ?thesis 
    using finite_subset by blast
qed

  


text\<open>
  We define an abstraction function \<open>sg_\<alpha>\<close> and two operations \<open>has_succs\<close> which checks if a state 
  has a successor and \<open>pick_succs\<close> which picks one (in this case the first in the list) successor. 
\<close>

type_synonym 'v sg = "'v \<Rightarrow> 'v list"

definition sg_\<alpha> :: "'v sg \<Rightarrow> 'v rel" where "sg_\<alpha> succ \<equiv> {(u,v). v\<in>set (succ u)}"

lemma succ_correct: "set (succ u) = sg_\<alpha> succ `` {u}"
  unfolding sg_\<alpha>_def
  by simp

definition has_succs :: "'v sg \<Rightarrow> 'v \<Rightarrow> bool" where "has_succs succ u \<equiv> succ u\<noteq>[]"  
  
lemma has_succs_correct[simp]: "has_succs succ u \<longleftrightarrow> u\<in>Domain (sg_\<alpha> succ)"
  unfolding sg_\<alpha>_def has_succs_def
  by (fastforce simp: in_set_conv_decomp neq_Nil_conv) 
  
definition pick_succ :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v" where "pick_succ succ u = hd (succ u)"

lemma first_succ_correct[simp]: "(u,v) \<in> (sg_\<alpha> succ) \<Longrightarrow> (u,pick_succ succ u) \<in> sg_\<alpha> succ"  
  unfolding sg_\<alpha>_def pick_succ_def
  apply (cases "succ u")
  by auto
  
text \<open>
  Our \<open>find_leaf\<close> function is then simply an iteration of checking whether there is a successor and 
  picking one from which we repeat these steps.
  
  vs=[]
  
  while u has successor
    u = some successor of u
    vs = vs@[u]
  
  
  Formally:
\<close>

definition "find_leaf succ u \<equiv> 
  while (\<lambda>(u,vs). has_succs succ u) (\<lambda>(u,vs). do {
    let u = pick_succ succ u;
    let vs = vs@[u];
    Some (u,vs)
  }) (u,[])"



text \<open>First, we use a context to fix our assumptions\<close>
context
  fixes g :: "'v sg"
  fixes v\<^sub>0 :: 'v \<comment> \<open>We also need a start node\<close>
  assumes acyclic: "acyclic (sg_\<alpha> g)" 
  assumes finite: "finite ((sg_\<alpha> g)\<^sup>*``{v\<^sub>0})" \<comment> \<open>We assume our graph to be acyclic and only have a finite reach.\<close>
begin
  abbreviation "E \<equiv> sg_\<alpha> g" \<comment> \<open>Abbreviation for edges\<close>

  
  subsection \<open>2b. Towards termination (3p)\<close>
  
  text\<open>
    Next up, we have an auxiliary lemma that states that the set of reachable states strictly 
    decreases. You are going to prove it, but also think of why this is useful in our case.
  
    REMEMBER: Since we are using a while-loop we have to prove termination somehow.
  \<close>
  
  
  lemma acyclic_image_psubset:
    assumes UV: "(u,v) \<in> E" 
    shows "E\<^sup>* `` {v} \<subset> E\<^sup>* `` {u}"
  proof -
  
  text \<open>
    To show \<open>A \<subset> B\<close> we can show \<open>A \<subseteq> B\<close> and find an element \<open>x\<close> such that \<open>x \<in> B\<close> but \<open>x \<notin> A\<close>.
    Is \<open>u\<close> reachable from \<open>v\<close>? And how about \<open>v\<close> from itself?
  \<close>
  
  
    have "E\<^sup>* `` {v} \<subseteq> E\<^sup>* `` {u}"
      using UV by force
    moreover have "u \<notin> E\<^sup>* `` {v}" 
    proof (rule notI)
      assume "u \<in> E\<^sup>* `` {v}"
      hence "(v,u) \<in> E\<^sup>*"
        by blast
      with UV have "(u,u) \<in> E\<^sup>+"
        by simp
      with acyclic show False 
        by (simp add: acyclic_def)
    qed
    ultimately
  
    show ?thesis  by blast 
  qed

    
subsection \<open>2c. Proving correctness (5p)\<close>

  text \<open>
    We are now going to prove the correctness.
    
    Use auxiliary lemmas and try to keep separate the verification condition part, the ADT part, 
    and the actual reasoning about graphs and paths.

    In particular, all operations on the sg - datastructure are already there and proved.  
    This means that we will never have to look into \<open>sg_\<alpha>\<close>. So unfolding it will not
    be necessary.
  
    NOTE: I MEAN IT, DON'T UNFOLD \<open>sg_\<alpha>\<close> EVER. WHATEVER PROBLEM YOU FACE, UNFOLDING IT WILL MAKE IT WORSE.
  \<close>
  

  text \<open>We have provided the variant for you\<close>  
  definition find_leaf_var :: "('v \<times> 'v list) rel" where
    "find_leaf_var = inv_image finite_psubset (\<lambda>(v,vs). E\<^sup>*``{v})"

  text \<open>As invariant, it's enough to state that the 
    current path goes from \<open>v\<^sub>0\<close> to the current node\<close>
  definition find_leaf_invar :: "'v \<times> 'v list \<Rightarrow> bool" where
     "find_leaf_invar \<equiv> \<lambda>(v,vs). path E v\<^sub>0 vs v"
     
  (* Space for your auxiliary lemmas.
  
    Hints: 
      \<^item> do not include unnecessary preconditions, like s=(u,vs) when you never use s in the lemma!
      \<^item> for proving that the state in a loop iteration gets smaller, 
        remember the lemma acyclic_image_psubset that you just proved (or sorry'd)
        
  *)  
  
  
  
  lemma find_leaf_var_wf: "wf find_leaf_var"
    unfolding find_leaf_var_def by auto
  
  lemma find_leaf_invar_init: "find_leaf_invar (v\<^sub>0, [])"
    unfolding find_leaf_invar_def by auto
    
  lemma find_leaf_invar_pres: 
    assumes "find_leaf_invar (u, vs)" "(u, u') \<in> E"
    shows "find_leaf_invar (pick_succ g u, vs @ [pick_succ g u])"  
    using assms unfolding find_leaf_invar_def by auto
    
  lemma find_leaf_var_decr: 
    assumes I: "find_leaf_invar (u, vs)" and E: "(u, u') \<in> E"
    shows "((pick_succ g u, vs @ [pick_succ g u]), u, vs) \<in> local.find_leaf_var"
  proof -
    from I have [simp]: "finite (E\<^sup>*``{u})"
      unfolding find_leaf_invar_def
      by (auto intro: finite_reachable_path finite)
      
    show ?thesis
      using assms
      unfolding find_leaf_var_def find_leaf_invar_def
      apply (clarsimp)
      apply (rule acyclic_image_psubset)
      using first_succ_correct[where ?succ=g, OF \<open>(u,u')\<in>E\<close>] .
  qed
        
  lemma find_leaf_invar_final: "find_leaf_invar (u, vs) \<Longrightarrow> path E v\<^sub>0 vs u"
    unfolding find_leaf_invar_def by auto

    
      
  lemma wp_find_leaf: "wp (find_leaf g v\<^sub>0) (\<lambda>(v,vs). path E v\<^sub>0 vs v \<and> v\<notin>Domain E)"
    unfolding find_leaf_def
    apply (intro wp_intro wp_while[where I=find_leaf_invar and V=find_leaf_var])
    
    (* Use proof exploration to come up with some meaningful auxiliary lemmas. 
       Don't unfold find_leaf_var and find_leaf_invar here, but only in the auxiliary lemmas. 
      (But you should try to get rid of the let and cases here! )
    *)
    
    (* If you cannot prove a subgoal (or the resp. aux lemma), use sorry and try the others! *)
    
    (* At the end, clean up your proof! *)
    
    
    apply (auto simp: find_leaf_var_wf find_leaf_invar_init find_leaf_invar_pres Let_def find_leaf_var_decr find_leaf_invar_final)
    
    done

end (* context *)










end
