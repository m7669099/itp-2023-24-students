theory Homework_Lib
imports Main
begin
  (* Just some magic we use to prepare the homework assignments. 
    You can safely ignore that!
  *)
  consts replace_with_your_solution :: "'a::{}"
  notation replace_with_your_solution ("\<llangle>replace with your solution!\<rrangle>")


  abbreviation (input) hide_undefined :: "'a::{} \<Rightarrow> 'a::{}" ("\<llangle>undef _\<rrangle>") where "hide_undefined x \<equiv> x"
  abbreviation (input) hide_undefined_prop :: "prop \<Rightarrow> prop" ("\<llangle>undefp _\<rrangle>") where "hide_undefined_prop x \<equiv> x"

end
