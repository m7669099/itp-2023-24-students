theory BST_Demo
imports Main
begin

section \<open>Basic Definitions\<close>

datatype BST = Node BST int BST | Leaf

fun bst_set :: "BST \<Rightarrow> int set" where
  "bst_set Leaf = {}"
| "bst_set (Node l x r) = bst_set l \<union> {x} \<union> bst_set r"  

fun bst_invar :: "BST \<Rightarrow> bool" where
  "bst_invar Leaf \<longleftrightarrow> True"
| "bst_invar (Node l x r) \<longleftrightarrow> 
      bst_invar l 
    \<and> bst_invar r 
    \<and> (\<forall>y\<in>bst_set l. y<x) 
    \<and> (\<forall>y\<in>bst_set r. y>x)"  

    

section \<open>Operations\<close>

subsection \<open>Empty\<close>

definition "bst_empty = Leaf" 

lemma bst_empty_invar[simp]: "bst_invar bst_empty" by (simp add: bst_empty_def)
lemma bst_empty_set[simp]: "bst_set bst_empty = {}" by (simp add: bst_empty_def)

lemmas bst_empty_correct = bst_empty_invar bst_empty_set

subsection \<open>Lookup\<close>

fun bst_lookup :: "int \<Rightarrow> BST \<Rightarrow> bool" where
  "bst_lookup x Leaf \<longleftrightarrow> False"
| "bst_lookup x (Node l y r) 
    \<longleftrightarrow> (if x=y then True else if x<y then bst_lookup x l else bst_lookup x r)"  


lemma bst_lookup_correct[simp]: "bst_invar t \<Longrightarrow> bst_lookup x t \<longleftrightarrow> x \<in> bst_set t"
  by (induction t) auto

subsection \<open>Insert\<close>

fun bst_insert :: "int \<Rightarrow> BST \<Rightarrow> BST" where
  "bst_insert x Leaf = Node Leaf x Leaf"
| "bst_insert x (Node l y r) = (
    if x=y then Node l y r 
    else if x<y then Node (bst_insert x l) y r 
    else Node l y (bst_insert x r))"
  
lemma bst_insert_set[simp]: "bst_invar t \<Longrightarrow> bst_set (bst_insert x t) = {x}\<union>bst_set t"
  by (induction t) auto

lemma bst_insert_invar[simp]: "bst_invar t \<Longrightarrow> bst_invar (bst_insert x t)"  
  by (induction t) auto

lemmas bst_insert_correct = bst_insert_set bst_insert_invar
 

thm bst_empty_correct bst_lookup_correct bst_insert_correct

(*
  These lemmas define the behaviour of BSTs in terms of set operations!
  
  The concrete type (BST) comes with an invariant, and an abstraction function into the abstract type (set).
  
  Each operation on the concrete type is justified by the corresponding operation on the abstract type.
  Additionally, it preserves the invariant.
    

  This pattern is called abstract data types (ADT). 
  It is very useful for reasoning about algorithms that use data structures.
  
*)



  
subsection \<open>Delete\<close>

(*
  Idea: when deleting x of Node l x r, and both l and r are no leafs,
    we find and remove the greatest element from l, and insert it for x
*)


(*
  Removing/obtaining greatest element of non-leaf tree
*)

fun bst_del_max where
  "bst_del_max (Node l x Leaf) = l"
| "bst_del_max (Node l x r) = Node l x (bst_del_max r)"
  
fun bst_get_max where
  "bst_get_max (Node l x Leaf) = x"
| "bst_get_max (Node l x r) = bst_get_max r"


(* Quite often, we need auxiliary lemmas, like the ones below: *)
lemma bst_set_split: "t\<noteq>Leaf \<Longrightarrow> bst_set t = bst_set (bst_del_max t) \<union> {bst_get_max t}"
  by (induction t rule: bst_get_max.induct) auto

lemma bst_max_ge: "t\<noteq>Leaf \<Longrightarrow> bst_invar t \<Longrightarrow> \<forall>x\<in>bst_set t. x\<le>bst_get_max t"  
  by (induction t rule: bst_get_max.induct) auto
  
lemma bst_max_gt: "t\<noteq>Leaf \<Longrightarrow> bst_invar t \<Longrightarrow> \<forall>x\<in>bst_set (bst_del_max t). x<bst_get_max t"
  apply (induction t rule: bst_get_max.induct)
  subgoal by auto
  subgoal for l x rl xx rr
    apply auto
    using bst_max_ge[of "Node rl xx rr"]
    by auto
  subgoal by auto
  done
    
lemma bst_max_notin: "t\<noteq>Leaf \<Longrightarrow> bst_invar t \<Longrightarrow> bst_get_max t \<notin> bst_set (bst_del_max t)"
  using bst_max_gt by auto

(* Finally, the set and invar lemma: *)  
lemma bst_max_set: "bst_invar t \<Longrightarrow> t\<noteq>Leaf \<Longrightarrow> bst_set (bst_del_max t) = bst_set t - {bst_get_max t}"
  using bst_max_notin
  by (simp add: bst_set_split)
  
lemma bst_max_invar: "bst_invar t \<Longrightarrow> t\<noteq>Leaf \<Longrightarrow> bst_invar (bst_del_max t)"
  by (induction t rule: bst_get_max.induct) (auto simp: bst_max_set)

  
(* Combine two trees, assuming that elements in l < elements in r *)  
      
fun bst_combine :: "BST \<Rightarrow> BST \<Rightarrow> BST" where
  "bst_combine Leaf r = r"
| "bst_combine l Leaf = l"  
| "bst_combine l r = Node (bst_del_max l) (bst_get_max l) r"   (* Use max value from left as new root *)

thm bst_combine.simps

lemma bst_combine_add_simps[simp]: "bst_combine l Leaf = l" by (cases l; auto) (* Additional simp-rule, not added by default *)

(* Avoid too deep splitting of node:
  The lemmas about del_max/get_max work for \<open>t\<noteq>Leaf\<close>.
  
  However, if we use induction/case rules for bst_combine, the last case will have "Node _ _ _".
  And there are simp-rules that unfold, e.g., bst_set (Node _ _ _), rendering the lemmas about del_max unusable!

  Thus, we will not (syntactically) split t, but just check if t is Leaf or not. 
  The following lemma is used for the non-leaf case:
*)
lemma bst_combine_nl: "l\<noteq>Leaf \<Longrightarrow> r\<noteq>Leaf 
  \<Longrightarrow> bst_combine l r = Node (bst_del_max l) (bst_get_max l) r"
  by (cases l; cases r; simp)
  


lemma bst_combine_set[simp]: "bst_invar r \<Longrightarrow> bst_set (bst_combine l r) = bst_set l \<union> bst_set r"
  apply (cases "l=Leaf"; cases "r=Leaf")
  by (simp_all add: bst_combine_nl bst_set_split)

lemma bst_combine_invar[simp]: 
  "bst_invar l \<Longrightarrow> bst_invar r \<Longrightarrow> \<forall>x\<in>bst_set l. \<forall>y\<in>bst_set r. x<y 
  \<Longrightarrow> bst_invar (bst_combine l r)"
  apply (cases "l=Leaf"; cases "r=Leaf")
  using bst_max_gt
  apply (simp_all add: bst_combine_nl bst_max_invar bst_set_split)
  done

fun bst_delete :: "int \<Rightarrow> BST \<Rightarrow> BST" where
  "bst_delete x Leaf = Leaf"  
| "bst_delete x (Node l y r) = (
    if x<y then Node (bst_delete x l) y r 
    else if x>y then Node l y (bst_delete x r) 
    else bst_combine l r)"  
  
lemma bst_delete_set[simp]: "bst_invar t \<Longrightarrow> bst_set (bst_delete x t) = bst_set t - {x}"
  by (induction t) auto

lemma bst_delete_invar[simp]: "bst_invar t \<Longrightarrow> bst_invar (bst_delete x t)"
  apply (induction t) 
  apply (force intro: bst_combine_invar) (* Sometimes, proof procedures (and setup) beyond simp and auto is required *)
  by simp

lemmas bst_delete_correct = bst_delete_set bst_delete_invar



section \<open>Summary\<close>

(*
  We have proven the following theorems:
*)
thm bst_empty_correct bst_lookup_correct bst_insert_correct bst_delete_correct

(*
  
  We have broken down the complex delete operation into smaller operations,
  (find_max, delete_max, combine), and justified these smaller operations as ADT operations.
  \<^bold>\<Longrightarrow> \<^bold>M\<^bold>o\<^bold>d\<^bold>u\<^bold>l\<^bold>a\<^bold>r\<^bold>i\<^bold>t\<^bold>y \<^bold>t\<^bold>o \<^bold>f\<^bold>i\<^bold>g\<^bold>h\<^bold>t \<^bold>c\<^bold>o\<^bold>m\<^bold>p\<^bold>l\<^bold>e\<^bold>x\<^bold>i\<^bold>t\<^bold>y\<^bold>! \<^bold>E\<^bold>s\<^bold>s\<^bold>e\<^bold>n\<^bold>t\<^bold>i\<^bold>a\<^bold>l \<^bold>i\<^bold>n \<^bold>p\<^bold>r\<^bold>o\<^bold>g\<^bold>r\<^bold>a\<^bold>m\<^bold>m\<^bold>i\<^bold>n\<^bold>g \<^bold>a\<^bold>n\<^bold>d \<^bold>p\<^bold>r\<^bold>o\<^bold>v\<^bold>i\<^bold>n\<^bold>g
  
  
*)

end
