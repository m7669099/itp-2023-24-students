theory "Tutorial1-ex"
imports Main
begin

(*
  This is the first tutorial sheet for the ITP course. Working this sheet out is optional, 
  but recommended to follow the practical session. We will discuss the content in the practical 
  session on the 16th of November between 8:45 and 10:30.
*)


(*
  Specify a function which takes a natural number n as a parameter and calculates the sum of all
  natural numbers up to (including) n
*) 

fun sum_upto :: "nat \<Rightarrow> nat" where
  "sum_upto 0 = 0"
| "sum_upto (Suc n) = Suc n + sum_upto n"

value "sum_upto 3"


(*
  The sum that we have just defined is equal to n * (n + 1) / 2. A classical example of a lemma
  that we can solve using induction. With the added bonus that now Isabelle will take over most 
  of the work.
*)

lemma "sum_upto n = n * (n + 1) div 2"
  apply(induction n) 
  apply auto
  done
  


(*
  In the lecture we have taken a look at the tree datatype which stores data in the nodes.
  We now look at a tree that stores data in the leafs. Define the datatype here:
*)

datatype 'a tree = Node "'a tree" "'a tree" | Leaf 'a

value "Node (Node (Leaf (1::nat)) (Leaf 2)) (Leaf 3)"


(*
  We can convert a tree to a list by adding the data from left to right to a list.
*)

fun to_list :: "'a tree \<Rightarrow> 'a list" where
  "to_list (Leaf v) = [v]"
| "to_list (Node lt rt) = to_list lt @ to_list rt"

value "to_list (Node (Node (Leaf (1::nat)) (Leaf 2)) (Leaf 3))"
value "length (to_list (Node (Node (Leaf (1::nat)) (Leaf 2)) (Leaf 3)))"

(*
  This definition of trees is not very "memory"-efficient as we don't store data in the nodes. 
  Let's quantify how the amount of empty nodes compares to the actual data that is stored in the 
  tree.
*)

fun count_nodes :: "'a tree \<Rightarrow> nat" where
  "count_nodes (Leaf v) = 0"
| "count_nodes (Node lt rt) = count_nodes lt + count_nodes rt + 1"

value "count_nodes (Node (Node (Leaf (1::nat)) (Leaf 2)) (Leaf 3))"

(*
  Use these functions to come up with a relation between the amount of data stored and the number 
  of nodes.
*)

lemma "length (to_list t) = count_nodes t + 1"
  apply(induction t)
  apply auto
  done


(*
  A commonly used function in functional programming is the map function. \<open>map f xs\<close> applies a
  function f to each element in the list xs. Look at the evaluation below to see it in action.
*)

value "[0..<10]"
value "map (\<lambda> n. 3 * n) [0..<10]"


(*
  We can define a similar function for trees. Define a function \<open>map_on_tree f t\<close> which computes 
  a tree where each leaf was mapped using f. 
*)

fun map_on_tree :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a tree \<Rightarrow> 'b tree" where
  "map_on_tree f (Leaf v) = (Leaf (f v))"
| "map_on_tree f (Node lt rt) = Node (map_on_tree f lt) (map_on_tree f rt)"


(*
  If we did this correctly, the map function applied to \<open>to_list t\<close> is the same as the to_list 
  function applied to map_on_tree. Try to come up with a lemma and prove it correct.
*)

lemma "map f (to_list t) = to_list (map_on_tree f t)"
  apply(induction t)
  apply auto
  done


(*
  We define a function \<open>alternate a xs\<close> which returns a list in which each entry in \<open>xs\<close> is followed
  by \<open>a\<close>. For example \<open>alternate 8 [2,4,6] = [2,8,4,8,6,8]\<close>
*)

fun alternate :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "alternate _ [] = []"
| "alternate v (x#xs) = x#v#alternate v xs"

(*
  We now do something similar for trees. We replace each \<open>Leaf x\<close> with \<open>Node (Leaf x) (Leaf a)\<close>.
*) 

fun alternate_tree :: "'a \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "alternate_tree v (Leaf x) = Node (Leaf x) (Leaf v)"
| "alternate_tree v (Node lt rt) = Node (alternate_tree v lt) (alternate_tree v rt)"


(*
  If we convert an alternate_tree to a list, it's the same as converting the tree to a list 
  directly and then applying alternate on it.
  WARNING, this proof may need some additional steps here. Try to figure it out!
*)

lemma "alternate a (to_list t) = to_list (alternate_tree a t)" 
  apply(induction t)
  apply auto (*Simple induction auto does not work here, try to figure out what's wrong. SPOILER: Is the Induction Hypothesis applied?*)
  oops

lemma alternate_append: "alternate a (xs @ ys) = alternate a xs @ alternate a ys"
  apply(induction xs)
  apply auto
  done

lemma "alternate a (to_list t) = to_list (alternate_tree a t)" 
  apply(induction t)
  apply (auto simp: alternate_append)
  done



end




