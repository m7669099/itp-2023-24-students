theory "Tutorial1"
imports Main
begin

(*
  This is the first tutorial sheet for the ITP course. Working this sheet out is optional, 
  but recommended to follow the practical session. We will discuss the content in the practical 
  session on the 16th of November between 8:45 and 10:30.
*)


(*
  Specify a function which takes a natural number n as a parameter and calculates the sum of all
  natural numbers up to (including) n
*) 

fun sum_upto :: "nat \<Rightarrow> nat" where
  "sum_upto _ = undefined"


(*
  The sum that we have just defined is equal to n * (n + 1) / 2. A classical example of a lemma
  that we can solve using induction. With the added bonus that now Isabelle will take over most 
  of the work.
*)

lemma "sum_upto n = n * (n + 1) div 2"
  sorry


(*
  In the lecture we have taken a look at the tree datatype which stores data in the nodes.
  We now look at a tree that stores data in the leafs. Define the datatype here:
*)

datatype 'a tree = TODO


(*
  We can convert a tree to a list by adding the data from left to right to a list.
*)

fun to_list :: "'a tree \<Rightarrow> 'a list" where
  "to_list _ = undefined"


(*
  This definition of trees is not very "memory"-efficient as we don't store data in the nodes. 
  Let's quantify how the amount of empty nodes compares to the actual data that is stored in the 
  tree.
*)

fun count_nodes :: "'a tree \<Rightarrow> nat" where
  "count_nodes _ = undefined"


(*
  Use these functions to come up with a relation between the amount of data stored and the number 
  of nodes.
*)

lemma specify_your_lemma
  sorry


(*
  A commonly used function in functional programming is the map function. \<open>map f xs\<close> applies a
  function f to each element in the list xs. Look at the evaluation below to see it in action.
*)

value "[0..<10]"
value "map (\<lambda> n. 3 * n) [0..<10]"


(*
  We can define a similar function for trees. Define a function \<open>map_on_tree f t\<close> which computes 
  a tree where each leaf was mapped using f. 
*)

fun map_on_tree :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a tree \<Rightarrow> 'b tree" where
  "map_on_tree _ _ = undefined"


(*
  If we did this correctly, the map function applied to \<open>to_list t\<close> is the same as the to_list 
  function applied to map_on_tree. Try to come up with a lemma and prove it correct.
*)

lemma specify_your_lemma
  sorry


(*
  We define a function \<open>alternate a xs\<close> which returns a list in which each entry in \<open>xs\<close> is followed
  by \<open>a\<close>. For example \<open>alternate 8 [2,4,6] = [2,8,4,8,6,8]\<close>
*)

fun alternate :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "alternate _ _ = undefined"


(*
  We now do something similar for trees. We replace each \<open>Leaf x\<close> with \<open>Node (Leaf x) (Leaf a)\<close>.
*) 

fun alternate_tree :: "'a \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "alternate_tree _ _ = undefined"


(*
  If we convert an alternate_tree to a list, it's the same as converting the tree to a list 
  directly and then applying alternate on it.
  WARNING, this proof may need some additional steps here. Try to figure it out!
*)

lemma "alternate a (to_list t) = to_list (alternate_tree a t)" 
  sorry



end




