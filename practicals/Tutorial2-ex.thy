theory "Tutorial2-ex"
  imports Main
begin

(*
  Tutorial #2
  Working this sheet out is optional, but recommended to follow the practical session. 
  We will discuss the content in the practical session on the 23rd of November between 
  8:45 and 10:30.
*)

section \<open>List of lists\<close>


(* 
  In this exercise we consider lists that contain lists. First of all we want to know how
  many elements are in these lists in total. Define a function \<open>total_length\<close> that calculates this.
  E.g. \<open>total_length [[a,b,c],[d,e]] = 5\<close> 
*)

fun total_length :: "'a list list \<Rightarrow> nat" where
  "total_length [] = 0"
| "total_length (xs#xss) = length xs + total_length xss"

value "total_length [[a,b,c],[d,e]]"

(* 
  This number should not be affected by empty lists that are in our list. 
  E.g. \<open>total_length [[a,b],[],[c,d]] = total_length [[a,b],[c,d]]\<close>
  Prove the following lemma:
*)

lemma "total_length xss = total_length (filter (\<lambda>xs. xs \<noteq> []) xss)"
  apply(induction xss)
  apply auto
  done


(* 
  Next up, we flatten a list of lists into a single list, containing all elements in the same order
  E.g. \<open>flatten [[a,b,c],[d],[e,f]] = [a,b,c,d,e,f]\<close>
*)

fun flatten :: "'a list list \<Rightarrow> 'a list" where
  "flatten [] = []"
| "flatten (xs#xss) = xs @ flatten xss"

value "flatten [[a,b,c],[d],[e,f]]"


(* 
  Now show that the total length of a list of lists is equal to the length of a flattened list.
  Specify such lemma and prove it:
*)

lemma "total_length xss = length (flatten xss)"
  apply(induction xss)
  apply auto
  done

(* 
  You are given a reverse function, given a list as an input, it calculates the list in reverse 
  order
*)

fun reverse :: "'a list \<Rightarrow> 'a list" where
  "reverse [] = []"
| "reverse (x#xs) = reverse xs @ [x]"




(* The \<open>rev\<close> function reverses a list. However, reversing a list of lists does not reverse the 
  flattened list. To achieve this, each element of the list needs to be reversed as well.
  We want to prove this propery that the reverse of a list of reversed lists is the reverse of a 
  flattened list. Specify this lemma and prove it correct.

  You may need auxiliary lemmas.
*)

lemma [simp]: "flatten (xs @ ys) = flatten xs @ flatten ys"
  apply(induction xs)
  apply auto
  done

lemma reverse_append: "reverse (xs @ ys) = reverse ys @ reverse xs"
  apply(induction xs)
  apply auto
  done

lemma "flatten (reverse (map reverse xss)) = reverse (flatten xss)"
  apply(induction xss)
  apply (auto simp: reverse_append)
  done


(* 
  We can also calculate the total length of a list of lists via a fold operation.
  Come up with a suitable fold and show it correctly calculates the total length.

  You may need auxiliary lemmas.
*)

lemma aux1: "fold (\<lambda> xs acc. acc + length xs) xss a = a + total_length xss"
  apply(induction xss arbitrary: a)
  by auto

lemmas [simp] = aux1


lemma "fold (\<lambda> xs acc. acc + length xs) xss 0 = total_length xss"
  apply auto
  done

(*
  We can also represent flatten as a fold function, come up with a suitable fold and show it
  correctly calculates the flattened list.

  You may need auxiliary lemmas.
*)

lemma aux2: "fold (\<lambda> xs acc. acc @ xs) xss a = a @ flatten xss"
  apply(induction xss arbitrary: a)
  by auto


lemma "fold (\<lambda> xs acc. acc @ xs) xss [] = flatten xss"
  apply(induction xss)
  apply (auto simp: aux2)
  done


section \<open>Binary tree\<close>

(* 
  You are given a tree datatype that sores information in the Nodes. You have already seen this in 
  Tutorial 1.
*)

datatype 'a tree = Node "'a tree" 'a "'a tree" | Leaf

(*
  In this exercise, we want to write a function that updates a sub-tree inside a larger tree. 
  For that, we first have to define what a ``location'' inside a tree means. In Isabelle, we can 
  use the @{command type_synonym} to define shorthands for types.
  A location (loc) is a list of @{typ bool}s that are either @{const True} (go left) or 
  @{const False} (go right).
*)

type_synonym loc = "bool list"

(*
  Define a \<open>lookup\<close> function that takes a tree and a location and returns the sub-tree 
  at that position. If the location is too long, just return @{const Leaf}. A tree accepts a 
  location if we end up in a Node when looking up the location.
*)

fun accepts :: "'a tree \<Rightarrow> loc \<Rightarrow> bool" (*<*) where
  "accepts Leaf _ = False"
| "accepts _ [] = True"
| "accepts (Node lt v rt) (x#xs) = accepts (if x then lt else rt) xs"
(*>*)


(*
  We also want to retrieve data from our tree. However, only locations that lead to a node
  return data. Other locations should not return any data. We can use the \<open>option\<close> datatype
  for this. When a location leads to a node \<open>Node lt v rt\<close>, our lookup function returns \<open>Some v\<close>.
  Otherwise it returns \<open>None\<close>
  Define this lookup function
*)

fun lookup :: "'a tree \<Rightarrow> loc \<Rightarrow> 'a option" (*<*) where
  "lookup Leaf _ = None"
| "lookup (Node _ v _) [] = Some v"
| "lookup (Node lt v rt) (x#xs) = lookup (if x then lt else rt) xs"
(*>*)

value "lookup (Leaf::nat tree) []"
value "lookup (Node Leaf (3::nat) (Node Leaf 2 Leaf)) [False]"
value "lookup (Node Leaf (3::nat) (Node Leaf 2 Leaf)) [False, True]"
value "lookup (Node Leaf (3::nat) (Node Leaf 2 Leaf)) [False, True, False]"


(*
  If we implemented everything correctly, the lookup function should return data if and only if
  the location is accepted. Specify a lemma and prove it.
*)

lemma "accepts t xs \<longleftrightarrow> (\<exists>x. lookup t xs = Some x)"
  apply(induction rule: accepts.induct)
  apply auto
  done

thm lookup.induct

(*
  The height of the tree is already given. It determines the longest path between the root and 
  any Leaf.
*)

fun height :: "'a tree \<Rightarrow> nat" where
  "height Leaf = 0"
| "height (Node lt x rt) = max (height lt) (height rt) + 1"


(*
  If the length of a location is greater than the height of a tree, then the location does not 
  point to any data and therefore \<open>lookup\<close> should not return any data. 
  Specify a lemma using \<open>height\<close> and \<open>lookup\<close> and prove it.
*)

lemma aux3:"length l \<ge> height t \<Longrightarrow> lookup t l = None"
  apply(induction rule: lookup.induct)
  apply auto
  done

(*
  The converse should also hold. If data is returned, then the length of the location must be
  strictly less than the height of the tree. 
  Specify a lemma using \<open>height\<close> and \<open>lookup\<close> and prove it.
  There are multiple ways to prove this lemma.
*)

lemma "lookup t l = Some v \<Longrightarrow> length l < height t"
  using aux3 leI by fastforce  (*sledgehammer*)



end