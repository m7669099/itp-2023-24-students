theory "Tutorial3-ex"
  imports Main
begin

(*
  Tutorial #3
  Working this sheet out is optional, but recommended to follow the practical session. 
  We will discuss the content in the practical session on the 27th of November between 
  10:45 and 12:30.
  NOTE: This week's practical has been moved to Monday for this week only! The lectures will take 
  place on Tuesday and Wednesday and this tutorial does contain some material that will only be
  discussed in the lectures after. 
*)

section \<open>Forwards and Backwards Reasoning\<close>

(*
  So far in this course, auto has been able to prove most we have thrown at it, though usually
  relying on induction. This shows the strength of auto, but it will not always be able to prove
  anything you throw at it. In today's practical, we will have a look at different means of proving
  that do not involve auto. The following lemmas can all be solved with auto directly. However,
  we are going to prove these with forwards and backwards reasoning. Forwards reasoning means that
  we combine lemmas with the attributes OF or THEN to slot together new lemmas. We instantiate
  a lemma by filling in the assumptions using other lemmas. If these other lemmas also contain 
  assumptions, these will become assumptions over the new lemma. In other words, we have a 
  lemma X: A \<Longrightarrow> B and a lemma Y: B \<Longrightarrow> C. Then Y[OF X] is the lemma A \<Longrightarrow> C. The consequence of 
  X (which is B) is substituted into the assumptions of Y. This allows us to combine concepts 
  of different lemmas with each other. Since we are building up a lemma by filling in the 
  assumptions, we are doing the proof in a forward manner. Therefore, we speak of forwards reasoning.

  Below we have given an example of how these attributes work.
*)

value "[2..<8]"
value "last [2..<8]"

thm upt_conv_Nil last_ConsL
thm last_ConsL[OF upt_conv_Nil]
thm upt_conv_Nil[THEN last_ConsL]

find_theorems "_ = [] \<Longrightarrow> _"


(*
  The alternative is backwards reasoning. Instead of building a proof by filling in the assumptions,
  we start reasoning from the proof goal. Saw we want to proof A \<Longrightarrow> B but we only have a lemma 
  C \<Longrightarrow> D \<Longrightarrow> B. If we apply it as a rule, we get the new proof obligations A \<Longrightarrow> C and A \<Longrightarrow> D.
*)

lemma "j1 \<le> i1 \<Longrightarrow> last (x # [i1..<j1]) = x"
  apply(rule last_ConsL)
  apply(rule upt_conv_Nil)
  apply assumption
  done

thm last_ConsL
thm upt_conv_Nil

(*
  Now let us try this out for ourselves. Prove these lemmas twice. Once with forward reasoning 
  and once with backwards reasoning. Any other proof methods are not allowed.
*)

find_theorems "_ < _ \<Longrightarrow> _ < Suc _"
thm less_SucI
find_theorems "_ < Suc _"

thm order.strict_trans

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) \<Longrightarrow> x < Suc (Suc (Suc (Suc (Suc (Suc 0)))))"
  apply(rule less_SucI)
  apply assumption
  done

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) \<Longrightarrow> x < Suc (Suc (Suc (Suc (Suc (Suc (Suc 0))))))"
  apply(rule order.strict_trans)
  apply assumption
thm Suc_mono
  apply(rule Suc_mono)+
thm zero_less_Suc
  apply(rule zero_less_Suc)
  done

thm order.strict_trans

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) \<Longrightarrow> x < Suc (Suc (Suc (Suc (Suc (Suc (Suc 0))))))"
  apply(erule order.strict_trans)
  apply(rule Suc_mono)+
  apply(rule zero_less_Suc)
  done

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) \<Longrightarrow> x < Suc (Suc (Suc (Suc (Suc (Suc (Suc 0))))))"
  using order.strict_trans[OF _ order.strict_trans[OF lessI lessI, of "Suc (Suc (Suc (Suc (Suc 0))))"]] .



(*WE SKIPPED THESE EXERCISES IN THE PRACTICAL SESSIONS*)

lemma aux1: "n > Suc 0 \<Longrightarrow> n < n * n"
  apply(rule n_less_n_mult_m) 
  apply(rule Suc_lessD)
  apply assumption+
  done

thm n_less_n_mult_m[OF Suc_lessD, of n n for n]

lemma "n > Suc 0 \<Longrightarrow> last [n..<n*n] = n*n-1"
  apply(rule last_upt)
  apply(rule aux1)
  apply assumption
  done

lemma "n > Suc 0 \<Longrightarrow> last [n..<n*n] = n*n-1"
  using last_upt[OF aux1] .

(*
  These lemmas are a bit more involved, and if we apply forwards reasoning we will have a deep 
  nesting of lemmas if we even succeed at all. This nesting makes forwards reasonin efficient for
  short proofs, but for larger proofs you will quickly lose the overview. That's why we prove next
  lemmas with backwards reasoning.
*)


(*WE SKIPPED THESE NEXT 3 EXERCISES IN THE PRACTICAL SESSIONS*)

lemma "\<forall>x. P x \<longrightarrow> P' x \<Longrightarrow> P y \<Longrightarrow> P' y"
  apply(erule allE[where ?x=y])
  apply(erule impE)
  apply assumption+
  done

lemma "x = 0 \<or> y = 0 \<Longrightarrow> x * y = 0" for x y::nat
  apply(erule disjE)
  subgoal
    apply(drule arg_cong[where ?f="(\<lambda>x. x * y)"])
    unfolding mult_zero_left
    apply assumption
    done
  subgoal
    apply(drule arg_cong[where ?f="(\<lambda>y. x * y)"])
    unfolding mult_zero_right
    apply assumption
    done
  done


lemma "\<exists> x. D x \<longrightarrow> (\<forall> x. D x)"
  apply(cases "\<forall>x. D x")
  subgoal
    apply(rule exI; rule impI)
    apply assumption
    done
  subgoal
    apply(rule exI[where ?x ="SOME x. \<not> D x"])
    apply(rule impI)
    apply(unfold not_all)
    apply(fold some_eq_ex)
    apply(erule notE)
    apply assumption
    done
  done



section \<open>Abstract Datatypes\<close>

(* 
  Next up, we take a look at abstract datatypes. In software engineering, sets are common data 
  structures for storing and marking data for example. There are different implementations of sets
  dependent on what you want to optimize for. Hashsets are efficient if you want to look up a lot
  of data quickly. Bitsets are more efficient if you try to optimize memory. But all impelmentations
  behave roughly the way you would expect a set in mathematics to behave. This means that the same
  lemmas of a set apply to a bitset.
*)

find_theorems "?a::('a set)"

(*
  As you can see there are about 7000 lemmas on Isabelles sets in the standard library. Most of 
  them too specific for general purposes. Still, you don't want to prove every lemma again when
  when defining a bitset in isabelle. This is where abstract datatypes come in. We connect all
  operations of a bitset to an equivalent operation for sets using an abstraction (\<alpha>) function
  and an invariant to ignore bitsets that are not admissible.
  We then show that each operation before and after applying \<alpha> are equal and preserve the invariant.

  We define a bitset as a list of bools.
*)

type_synonym bitset = "bool list"

(*
  For our implementation of bitsets, we assume that we occupy minimal amount of space. So the last 
  element of the list must be True, or in other words, the list cannot end in False.
*)
definition "bl_inv xs \<equiv> (xs \<noteq> [] \<longrightarrow> last xs)"


(* 
  The abstraction function converts our bitset into a nat set. When xs!i is true, we have that
  "i \<in> bl_\<alpha> xs" (proving this is part of the homework).
*)
definition "bl_\<alpha> xs \<equiv> { i . i<length xs \<and> xs!i }"

(*
  In this tutorial, we take a look at the delete operation where we remove an element from the 
  bitset. We then prove that this is equal to the remove operation (X - {x}) for a set.
  When looking purely at the abstraction with bl_\<alpha>, this proof is straightforward. However,
  implementing a remove function that satisfies the invariant is more challenging.

  We first try to implement remove without invariant preservation.
  Once we have succeeded in that we adapt our remove function to preserve the invariant.

  WARNING: We are building everything from scratch for this exercise. This makes it very hard and 
  very likely to get stuck at the details. If you notice you are spending too much time on this, do
  consider leaving this exercise open!
*)


value "[2..<5] ! 2"

fun bitset_compact where 
  "bitset_compact xs = (if xs = [] \<or> last xs then xs else bitset_compact (butlast xs))"

definition bitset_remove_aux :: "bitset \<Rightarrow> nat \<Rightarrow> bitset" where 
  "bitset_remove_aux xs i = (if i < length xs then xs[i:=False] else xs)"

term "X - {x}"
value "[True, False,False]"

lemma "bl_\<alpha> (bitset_remove_aux xs i) = bl_\<alpha> xs - {i}"
  unfolding bitset_remove_aux_def bl_\<alpha>_def
  by (auto simp: nth_list_update)

definition "bitset_remove xs i = bitset_compact (bitset_remove_aux xs i)"


lemma (*"bl_inv xs \<Longrightarrow> *)"(bl_inv (bitset_remove xs i))"
  sorry










(*We did not finish the tutorial in time, the following part shows how to finish the proof!*)



fun bl_compact where 
  "bl_compact xs = (if xs=[] \<or> last xs 
  then xs else bl_compact (butlast xs))"  
  
(*We remove bl_compact.simps from the simp set. If we would leave it in, the simplifier would 
  continuously try to simplify the lemma, try to find out why,*)
lemmas [simp del] = bl_compact.simps

(*
  Instead, we create our own simplification lemmas.
*)
lemma bl_compact_simps[simp]:
  "bl_compact [] = []"
  "bl_compact (xs@[x]) = (if x then xs@[x] 
  else bl_compact xs)"
  subgoal by (simp add: bl_compact.simps)
  subgoal by (simp add: bl_compact.simps)
  done

(* Another way how to define the same function: (just as an example) *)  
function bl_compact' where 
    "bl_compact' [] = []"
  | "bl_compact' (xs@[x]) = (if x then xs@[x] 
  else bl_compact' xs)"
  (* Prove that patterns are exhaustive, i.e., we have not forgotten anything *)
  apply (rule rev_exhaust; assumption)
  
  (* Prove that equations are non-overlapping (or, more precisely, equal on any possible overlap) *)
  by auto
  
termination by (lexicographic_order) (* Prove termination by the same heuristics that 'fun' uses *) 

(* That's what we get. *)
thm bl_compact'.simps
thm bl_compact'.induct 

(* Note, the induction rule is similar enough to rev_induct, such that we can use that instead *)
thm rev_induct

   
(*We prove the abstraction and the invariant preservation of bl_compact independently of bl_remove
  so that we can keep functionality of both functions separate and therefore keep the proofs 
  clearer. First we prove bl_compact *)
lemma bl_compact_inv[simp]: "bl_inv (bl_compact xs)"  
  apply (induction xs rule: rev_induct)
  by (auto simp: bl_inv_def)
  

lemma bl_\<alpha>_False[simp]: "bl_\<alpha> (xs @ [False]) = bl_\<alpha> xs"  
  unfolding bl_\<alpha>_def
  by (auto simp: nth_append)
  
lemma bl_compact_\<alpha>[simp]: 
  "bl_\<alpha> (bl_compact xs) = bl_\<alpha> xs"  
  apply (induction xs rule: rev_induct)
  by auto
  

(*We then consider an auxiliary delete function, that correctly deletes the element, but does not
  preserve the invariant. *)

definition "bl_del_aux xs i \<equiv> if i<length xs 
  then xs[i:=False] else xs"
  
lemma "bl_del_aux xs i = xs[i:=False]"
  unfolding bl_del_aux_def by auto
  
lemma bl_del_aux_\<alpha>[simp]: 
  "bl_\<alpha> (bl_del_aux xs i) = bl_\<alpha> xs - {i}"  
  unfolding bl_\<alpha>_def bl_del_aux_def
  by (auto simp: nth_list_update)
  

(*We then combine them to show correctness and invariant preservation*)
definition "bl_del xs i \<equiv> bl_compact (bl_del_aux xs i)"  
  
lemma bl_del_correct[simp]:
  "bl_\<alpha> (bl_del xs i) = bl_\<alpha> xs - {i}"
  "bl_inv (bl_del xs i)"
  unfolding bl_del_def
  by auto



end