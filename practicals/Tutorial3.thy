theory "Tutorial3"
  imports Main
begin

(*
  Tutorial #3
  Working this sheet out is optional, but recommended to follow the practical session. 
  We will discuss the content in the practical session on the 27th of November between 
  10:45 and 12:30.
  NOTE: This week's practical has been moved to Monday for this week only! The lectures will take 
  place on Tuesday and Wednesday and this tutorial does contain some material that will only be
  discussed in the lectures after. 
*)

section \<open>Forwards and Backwards Reasoning\<close>

(*
  So far in this course, auto has been able to prove most we have thrown at it, though usually
  relying on induction. This shows the strength of auto, but it will not always be able to prove
  anything you throw at it. In today's practical, we will have a look at different means of proving
  that do not involve auto. The following lemmas can all be solved with auto directly. However,
  we are going to prove these with forwards and backwards reasoning. Forwards reasoning means that
  we combine lemmas with the attributes OF or THEN to slot together new lemmas. We instantiate
  a lemma by filling in the assumptions using other lemmas. If these other lemmas also contain 
  assumptions, these will become assumptions over the new lemma. In other words, we have a 
  lemma X: A \<Longrightarrow> B and a lemma Y: B \<Longrightarrow> C. Then Y[OF X] is the lemma A \<Longrightarrow> C. The consequence of 
  X (which is B) is substituted into the assumptions of Y. This allows us to combine concepts 
  of different lemmas with each other. Since we are building up a lemma by filling in the 
  assumptions, we are doing the proof in a forward manner. Therefore, we speak of forwards reasoning.

  Below we have given an example of how these attributes work.
*)

value "[2..<8]"
value "last [2..<8]"

thm upt_conv_Nil last_ConsL
thm last_ConsL[OF upt_conv_Nil]
thm upt_conv_Nil[THEN last_ConsL]

find_theorems "_ = [] \<Longrightarrow> _"


(*
  The alternative is backwards reasoning. Instead of building a proof by filling in the assumptions,
  we start reasoning from the proof goal. Saw we want to proof A \<Longrightarrow> B but we only have a lemma 
  C \<Longrightarrow> D \<Longrightarrow> B. If we apply it as a rule, we get the new proof obligations A \<Longrightarrow> C and A \<Longrightarrow> D.
*)

lemma "j1 \<le> i1 \<Longrightarrow> last (x # [i1..<j1]) = x"
  apply(rule last_ConsL)
  apply(rule upt_conv_Nil)
  apply assumption
  done

(*
  Now let us try this out for ourselves. Prove these lemmas twice. Once with forward reasoning 
  and once with backwards reasoning. Any other proof methods are not allowed.
*)

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) \<Longrightarrow> x < Suc (Suc (Suc (Suc (Suc (Suc 0)))))"
  oops

lemma "n > Suc 0 \<Longrightarrow> n < n * n"
  oops

lemma "n > Suc 0 \<Longrightarrow> last [n..<n*n] = n*n-1"
  oops

(*
  These lemmas are a bit more involved, and if we apply forwards reasoning we will have a deep 
  nesting of lemmas if we even succeed at all. This nesting makes forwards reasonin efficient for
  short proofs, but for larger proofs you will quickly lose the overview. That's why we prove next
  lemmas with backwards reasoning.
*)

lemma "\<forall>x. P x \<longrightarrow> P' x \<Longrightarrow> P y \<Longrightarrow> P' y"
  oops

lemma "x = 0 \<or> y = 0 \<Longrightarrow> x * y = 0" for x y::nat
  oops

lemma "\<exists> x. (D x \<longrightarrow> (\<forall> x. D x))"
  oops



section \<open>Abstract Datatypes\<close>

(* 
  Next up, we take a look at abstract datatypes. In software engineering, sets are common data 
  structures for storing and marking data for example. There are different implementations of sets
  dependent on what you want to optimize for. Hashsets are efficient if you want to look up a lot
  of data quickly. Bitsets are more efficient if you try to optimize memory. But all impelmentations
  behave roughly the way you would expect a set in mathematics to behave. This means that the same
  lemmas of a set apply to a bitset.
*)

find_theorems "?a::('a set)"

(*
  As you can see there are about 7000 lemmas on Isabelles sets in the standard library. Most of 
  them too specific for general purposes. Still, you don't want to prove every lemma again when
  when defining a bitset in isabelle. This is where abstract datatypes come in. We connect all
  operations of a bitset to an equivalent operation for sets using an abstraction (\<alpha>) function
  and an invariant to ignore bitsets that are not admissible.
  We then show that each operation before and after applying \<alpha> are equal and preserve the invariant.

  We define a bitset as a list of bools.
*)

type_synonym bitset = "bool list"

(*
  For our implementation of bitsets, we assume that we occupy minimal amount of space. So the last 
  element of the list must be True, or in other words, the list cannot end in False.
*)
definition "bl_inv xs \<equiv> (xs \<noteq> [] \<longrightarrow> last xs)"


(* 
  The abstraction function converts our bitset into a nat set. When xs!i is true, we have that
  "i \<in> bl_\<alpha> xs" (proving this is part of the homework).
*)
definition "bl_\<alpha> xs \<equiv> { i . i<length xs \<and> xs!i }"

(*
  In this tutorial, we take a look at the delete operation where we remove an element from the 
  bitset. We then prove that this is equal to the remove operation (X - {x}) for a set.
  When looking purely at the abstraction with bl_\<alpha>, this proof is straightforward. However,
  implementing a remove function that satisfies the invariant is more challenging.

  We first try to implement remove without invariant preservation.
  Once we have succeeded in that we adapt our remove function to preserve the invariant.

  WARNING: We are building everything from scratch for this exercise. This makes it very hard and 
  very likely to get stuck at the details. If you notice you are spending too much time on this, do
  consider leaving this exercise open!
*)


(*PROVIDE YOUR DEFINITIONS AND PROOFS*)



end