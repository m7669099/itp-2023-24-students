theory "Tutorial4-ex"
  imports Main "HOL.Transcendental" "HOL-Computational_Algebra.Primes"
begin


(*
  Tutorial #4
  Working this sheet out is optional, but recommended to follow the practical session. 
  We will discuss the content in the practical session on the 7th of December between 
  8:45 and 10:30.
*)

(*
  We are going to prove that the square root of 2 is irrational. The common proof is by 
  contradiction: We assume \<open>sqrt 2 \<in> \<rat>\<close>, which means that \<open>sqrt = m/n\<close> for some natural numbers 
  m and n such that m and n are the lowest possible value, in other words, m and n are coprime.
  Squaring both sides and reordering yields m\<^sup>2 = 2 * n\<^sup>2. This means 2 divides m\<^sup>2 \<open>2 dvd m\<^sup>2\<close> and 
  hence \<open>2 dvd m\<close> see \<open>thm power2_eq_square\<close>. This means that \<open>4 dvd m\<^sup>2\<close> which means \<open>4 dvd 2 * n\<^sup>2\<close> 
  which means \<open>2 dvd n\<^sup>2\<close> which means \<open>2 dvd n\<close>. Since both m and n are divisible by 2, they are not
  coprime, which contradicts our earlier statement that m and are coprime.

  Now prove it using Isar. The following lemmas can come in handy:
*)

thm Rats_abs_nat_div_natE
thm power2_eq_square
thm prime_dvd_power

lemma sqrt_2_irrational: "sqrt 2 \<notin> \<rat>"
proof
  assume "sqrt 2 \<in> \<rat>"
  then obtain m n where "n \<noteq> 0" and they_are_coprime : "coprime m n" and "\<bar>sqrt 2\<bar> = real m / real n" by (rule Rats_abs_nat_div_natE)
      (* m *)
  then have "sqrt 2 ^ 2 = m ^ 2 / n ^ 2"  by (simp add: power_divide)
  then have "(sqrt 2) ^ 2 * n ^ 2 = m^2"  by (simp add: \<open>n \<noteq> 0\<close> nonzero_eq_divide_eq)
  then have todo:  "2  * n ^ 2 = m ^ 2"  using of_nat_power_eq_of_nat_cancel_iff by fastforce
  then have "2 dvd m ^ 2" by presburger
  then have md2: "2 dvd m" 
    by simp
  then have "4 dvd m ^ 2" by auto
  then obtain m' where "m^2 = (m')^2 * 4" using \<open>even m\<close> by fastforce
    (* n *)
  with todo have "2*n^2 = 4 * m'^2" by auto
  then have "n^2 = 2 * m' ^2" by simp
  then have "2 dvd n^2" by simp
  then have nd2: "2 dvd n" by simp
  with md2 have "\<not>coprime m n" unfolding coprime_def 
    by force
  with they_are_coprime show "False" by auto
qed


(*
  Next up, we look at inductive datatypes. We have a language of parentheses where words are only 
  accepted if each left parenthesis (LPAR) is matched with a unique right parenthesis (RPAR) 
  afterwards. Define this language as an inductive datatype.
*)

datatype par = LPAR | RPAR

inductive S :: "par list \<Rightarrow> bool" where
  fin: "S []"
| par: "S xs \<Longrightarrow> S (LPAR # xs @ [RPAR])"
| dup: "S xs \<Longrightarrow> S ys \<Longrightarrow> S (xs @ ys)"


(*
  A single character cannot be a word in this language, try to reason about why this is and prove it.
*)


lemma S_even_length: "S xs \<Longrightarrow> even (length xs)"
  apply(induction xs rule: S.induct)
  by auto

lemma S_not_singularity: "\<not>S [x]"
  using S_even_length
  by fastforce



(*
  A word that only consists of LPAR can also not be accepted by this language.
*)


lemma S_not_replicate_LPAR: "S xs \<Longrightarrow> xs = (replicate n LPAR) \<Longrightarrow> n = 0"
  apply(induction xs arbitrary: n rule: S.induct)
  subgoal for n
    by simp
  subgoal for w n
    by (metis last.simps last_replicate par.distinct(1) snoc_eq_iff_butlast)
  subgoal for v w n
    by (simp add: append_eq_conv_conj)
  done


lemma "n > 0 \<Longrightarrow> \<not>S (replicate n LPAR)"
  using S_not_replicate_LPAR by blast



(*
  We can charactrize our definition by a function \<open>match_parentheses\<close> where a counter keeps track
  of the difference in the number of LPAR and RPAR encountered so far. This number must always be
  non-negative. Once it becomes negative, we know that we have found an RPAR that cannot be matched
  with an LPAR.
  We now show that this function exactly characterizes our inductive definition.
*)

fun match_parentheses :: "par list \<Rightarrow> nat \<Rightarrow> bool" where 
  "match_parentheses [] n \<longleftrightarrow> n = 0" |
  "match_parentheses (x # xs) n = (case x of 
    LPAR \<Rightarrow> match_parentheses xs (Suc n) | 
    RPAR \<Rightarrow> (case n of 
      0 \<Rightarrow> False | 
      Suc m \<Rightarrow> match_parentheses xs m
    )
  )"

thm S.induct
thm match_parentheses.induct

(*
We use this general lemma and instantiate the desired parameters when we apply it
We generalize this lemma because it is equally difficult to prove this versus "S (xs @ ys) \<Longrightarrow> S (xs @ [LPAR,RPAR] @ ys)"
which is the lemma that we actually need.
*)
lemma S_insert:  "S (xs @ ys) \<Longrightarrow> S zs \<Longrightarrow> S (xs @ zs @ ys)"
  proof (induction "(xs @ ys)" arbitrary: xs ys rule: S.induct)
    case (par w)
    then show ?case 
      proof (cases "xs = [] \<or> ys = []")
        case True
        then show ?thesis 
          apply(cases "xs = []")
          using par.hyps 
          by (auto intro!: dup par.prems S.par)
      next
        case False
        obtain xs' ys' where XSD: "xs = LPAR # xs'" and YSD: "ys = ys' @ [RPAR]" 
          using par.hyps(3) False 
          by (metis Cons_eq_append_conv append_butlast_last_id last_append last_snoc)
        hence WDEF: "w = xs' @ ys'" using par.hyps(3) by simp
        have "S (xs' @ zs @ ys')" 
          using par(2)[OF WDEF par.prems] .
        from this[THEN S.par] show ?thesis 
          by(auto simp: XSD YSD)
      qed
  next
    case (dup v w)

    from dup.hyps(5) consider (la)"\<exists>us. v = xs @ us \<and> ys = us @ w " 
      | (ra)"\<exists>us. w = us @ ys \<and> xs = v @ us"
      by(auto simp: append_eq_append_conv2)

    then show ?case
    proof cases
      case la
      then obtain us where "v = xs @ us" and YSD: "ys = us @ w"
        by blast
      hence "S (xs @ zs @ us)" 
        by(auto intro!: dup(2) simp: dup(6))
      then show ?thesis 
        by(auto simp: YSD dest: S.dup[OF _ dup(3)])
    next
      case ra
      then obtain us where "w = us @ ys" and XSD: "xs = v @ us"
        by blast
      hence "S (us @ zs @ ys)"
        by(auto intro!: dup(4) simp: dup(6))
      then show ?thesis
        by(auto simp: XSD dest: S.dup[OF dup(1)])
    qed
  next
    case fin
    then show ?case
      by simp
  qed


(*We generalized this lemma to be able to prove match_parentheses_to_S*)
lemma match_parentheses_to_S_aux: "match_parentheses xs n \<Longrightarrow> S (replicate n LPAR @ xs)" (*In the tutorial, this was wrong, it should have been LPAR which has to be replicated. Try to find out why.*)
  apply(induction xs n rule: match_parentheses.induct)
  apply(simp add: fin)
  apply (auto 
      split: par.splits nat.splits 
      simp flip: replicate_append_same 
      simp: fin
      intro: S_insert[OF _ par[OF fin], simplified]
      )
  done

lemma match_parentheses_to_S: "match_parentheses xs 0 \<Longrightarrow> S xs" 
  using match_parentheses_to_S_aux by force


(*
  The next two lemmas are auxiliary lemmas, we generalized them so that they may be useful in other
  contexts as well, without making them harder to proof. (In this case the computation will be the
  same independent of the value of m and n). 
*)
lemma match_parentheses_append: "match_parentheses xs n \<Longrightarrow> match_parentheses ys m \<Longrightarrow> match_parentheses (xs @ ys) (n + m)"
  apply(induction xs n rule: match_parentheses.induct)
  apply (auto split: par.split nat.splits)
  done

lemma match_parentheses_RPAR: "match_parentheses xs n \<Longrightarrow> match_parentheses (xs @ [RPAR]) (Suc n)"
  apply(induction xs n rule: match_parentheses.induct)
  apply (auto split: par.split nat.splits)
  done

lemma S_to_match_parentheses: "S xs \<Longrightarrow> match_parentheses xs 0" 
  apply(induction rule: S.induct)
  by (auto intro: match_parentheses_append[of _ 0 _ 0, simplified] match_parentheses_RPAR)


(*
  We have split the proof into two parts.
  This splits our proof into an "easy" part and a "hard" part.
  Matching a word in S to match_parentheses is relatively easy, we induct over the rules of S
  and then show that the parentheses stay balanced.
  Matching a word from match_parentheses to S is harder because match_parentheses computes an output
  by going through the word character by character, which does not correspond to the inductive
  definition of S.  
*)
lemma "match_parentheses xs 0 \<longleftrightarrow> S xs"
  using S_to_match_parentheses match_parentheses_to_S_aux by force

end