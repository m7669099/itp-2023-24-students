theory Tutorial4
  imports Main "HOL.Transcendental" "HOL-Computational_Algebra.Primes"
begin


(*
  Tutorial #4
  Working this sheet out is optional, but recommended to follow the practical session. 
  We will discuss the content in the practical session on the 7th of December between 
  8:45 and 10:30.
*)

(*
  We are going to prove that the square root of 2 is irrational. The common proof is by 
  contradiction: We assume \<open>sqrt 2 \<in> \<rat>\<close>, which means that \<open>sqrt = m/n\<close> for some natural numbers 
  m and n such that m and n are the lowest possible value, in other words, m and n are coprime.
  Squaring both sides and reordering yields m\<^sup>2 = 2 * n\<^sup>2. This means 2 divides m\<^sup>2 \<open>2 dvd m\<^sup>2\<close> and 
  hence \<open>2 dvd m\<close> see \<open>thm power2_eq_square\<close>. This means that \<open>4 dvd m\<^sup>2\<close> which means \<open>4 dvd 2 * n\<^sup>2\<close> 
  which means \<open>2 dvd n\<^sup>2\<close> which means \<open>2 dvd n\<close>. Since both m and n are divisible by 2, they are not
  coprime, which contradicts our earlier statement that m and are coprime.

  Now prove it using Isar. The following lemmas can come in handy:
*)

thm Rats_abs_nat_div_natE
thm power2_eq_square
thm prime_dvd_power

lemma sqrt_2_irrational: "sqrt 2 \<notin> \<rat>"
  oops


(*
  Next up, we look at inductive datatypes. We have a language of parentheses where words are only 
  accepted if each left parenthesis (LPAR) is matched with a unique right parenthesis (RPAR) 
  afterwards. Define this language as an inductive datatype.
*)

datatype par = LPAR | RPAR

inductive S :: "par list \<Rightarrow> bool" where
  "S _"



(*
  A single character cannot be a word in this language, try to reason about why this is and prove it.
*)

lemma S_not_singularity: "\<not>S [x]"
  oops



(*
  A word that only consists of LPAR can also not be accepted by this language.
*)

lemma "n > 0 \<Longrightarrow> \<not>S (replicate n LPAR)"
  oops



(*
  We can charactrize our definition by a function \<open>match_parentheses\<close> where a counter keeps track
  of the difference in the number of LPAR and RPAR encountered so far. This number must always be
  non-negative. Once it becomes negative, we know that we have found an RPAR that cannot be matched
  with an LPAR.
  We now show that this function exactly characterizes our inductive definition.
*)

fun match_parentheses :: "par list \<Rightarrow> nat \<Rightarrow> bool" where 
  "match_parentheses [] n \<longleftrightarrow> n = 0" |
  "match_parentheses (x # xs) n = (case x of 
    LPAR \<Rightarrow> match_parentheses xs (Suc n) | 
    RPAR \<Rightarrow> (case n of 
      0 \<Rightarrow> False | 
      Suc m \<Rightarrow> match_parentheses xs m
    )
  )"


lemma "match_parentheses xs 0 \<longleftrightarrow> S xs"
  oops

end