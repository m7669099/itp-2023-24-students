section "Arithmetic and Boolean Expressions"

theory "Tutorial5-ex" imports Main "HOL-Library.RBT"
begin


(* 
  ANNOUNCEMENT: Next week's homework (sheet 6) will be a 'Come up with your own proof' exercise 
  where you can apply   everything you've learned so far to something that interests you.
  Think of something from a hackathon, Youtube video or a course at the UT. We will also give you
  some hints on next week's sheet.
  It's good to be ambitious, but also be realistic, a proof will always take longer than expected.
*)


(*
  We are going to look at different ways of abstraction and refinement. Abstraction means
  representing an implementation as an "easy to deal with" formal representation. Sets for example
  are a very common concept in pseudocode. When implementing the algorithm from pseudocode, you
  will use something with a set interfact, that below the hood does some more work to make you feel
  like you are dealing with sets. We will also explore this, where we implement a set as a red-black
  tree.
  The usual approach when proving an implementation correct, involves showing the correctness of
  an abstract algorithm. I.e. the one using a set. Proving the correctness is then relatively easy.
  For the implementation, we only need to show that the abstract representation is an abstraction
  of the implementation. I.e. every operation in the correct abstract algorithm corresponds to
  an operation in the implementation. Showing correctness of the implementation is then easy.
  
  This allows us to keep concepts for the correctness proofs automatically separated from concepts
  on the correctness of the implementation. Lemmas will not interfere and the simplifier will
  never simplify too much, as the concepts are encapsulated behind definitions.

  But before going through all of this, we do some warmup exercises.
*)

(*
  We have seen this graph definition before, we define a graph as a set of edges.
*)

section \<open>Abstract graph\<close>

text \<open>A graph G is a pair of sets called vertices and edges (V, E).
  
  The set of edges E is a set of vertices pairs; E \<subseteq> V \<times> V.

  We can describe algorithms on this abstract graph and try and prove them
  correct using pen and paper.

  An an example we want a path \<pi> from a \<in> V to b \<in> V : a list of vertices that
  starts in a and ends in b.

  We can implement such an algorithm as follows.

  \<open>
  def find_path(graph, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        if not graph.has_key(start):
            return None
        for node in graph[start]:
            if node not in path:
                newpath = find_path(graph, node, end, path)
                if newpath: return newpath
        return None  
  \<close>

  What operations do we need on the graph to execute this algorithm?
  What is the graph API that we require?
\<close>

context
  fixes graph       (* a type 'v t for my graph *)
        member      (* a function 'v t \<Rightarrow> 'v \<Rightarrow> bool *)
        neighbours  (* a function 'v t \<Rightarrow> 'v \<Rightarrow> 'v list *)
begin
  (* python to Isabelle translation omitted *)
end

section \<open>Concrete graphs\<close>

text \<open>
  This is a concrete graph implementation for the previous algorithm.
  It implements the graph as a set of edges. Using a set of edges it is
  possible to implement both \<open>member\<close> and \<open>neighbours\<close>.
\<close>

type_synonym 'v graph = "('v\<times>'v) set"


definition neighbours :: "'v graph" where
  "neighbours \<equiv> undefined"


definition member :: "'v graph \<Rightarrow> 'v \<Rightarrow> bool" where
  "member \<equiv> undefined"


text \<open>We want to run our algorithm on concrete data. What are we missing?

  \<open>
  G = {
   'A': ['B', 'C'],
   'B': ['C', 'D'],
   'C': ['D'],
   'D': ['C'],
   'E': ['F'],
   'F': ['C']
  }
  \<close>
  
  It is possible to implement the same concrete data in Isabelle.
\<close>

definition "G \<equiv> {
  (''A'', ''B''),
  (''A'', ''C''),
  (''B'', ''C''),
  (''B'', ''D''),
  (''C'', ''D''),
  (''D'', ''C''),
  (''E'', ''F''),
  (''F'', ''C'')
}"

section \<open>More concrete graphs\<close>

text \<open>
  Python has a convenient syntax for expressing the previous concrete graph.
  What happens though is that "under the hood" there must be some code
  to execute!

  We rewrite the concrete graph from dictionary syntax to dictionary operations
  to see what we are missing! This is one of the possible many rewrites.

  \<open>
  G = {}                           # empty
  G['A'] = G.get('A', []) + ['B']  # add_edge
  G['A'] = G.get('A', []) + ['C']  ...
  ...

  ...
  G['F'] = G.get('F', []) + ['C']  # add_edge
  \<close>

  Which operations are we missing on our concrete implementation?
\<close>

context
  fixes graph       (* a type 'v t for my graph *)
        member      (* a function 'v t \<Rightarrow> 'v \<Rightarrow> bool *)
        neighbours  (* a function 'v t \<Rightarrow> 'v \<Rightarrow> 'v list *)

        empty       (* a function of type 'v t *)
        add_edge    (* a function of type 'v t \<Rightarrow> ('v\<times>'v) \<Rightarrow> 'v t *)
begin
  (* python to Isabelle translation omitted *)
end


definition empty_graph :: "'v graph" where 
  "empty_graph \<equiv> {}"

definition add_edge :: "('v\<times>'v) \<Rightarrow> 'v graph \<Rightarrow> 'v graph" where
  "add_edge e E \<equiv> E \<union> {e}"

section \<open>Different concrete graphs\<close>

text \<open>
  An alternative to using a dictionary is to use an association list.

  (u, v) \<in> E \<leftrightarrow> (u, v) is in the concrete list


  \<open>
  G = []                        # ?
  G.insert(0, ('A', 'B'))       # ?
  G.insert(0, ('A', 'C'))       # ?
  G.insert(0, ('B', 'C'))       # ?
  G.insert(0, ('B', 'D'))       # ?
  G.insert(0, ('C', 'D'))       # ?
  G.insert(0, ('D', 'C'))       # ?
  G.insert(0, ('E', 'F'))       # ?
  G.insert(0, ('F', 'C'))       # ?
  \<close>

  We have to define the rest of the functions \<open>member\<close> and \<open>neighbours\<close>
  to make it a usable graph concrete data type.
\<close>

type_synonym 'v list_graph = "('v\<times>'v) list"

definition lg_empty :: "'v list_graph" where 
  "lg_empty \<equiv> []"


definition lg_add_edge :: "('v\<times>'v) \<Rightarrow> 'v list_graph \<Rightarrow> 'v list_graph" where 
  "lg_add_edge x xs \<equiv> x # xs"


section \<open>Refinements\<close>

text \<open>We have two concrete implementations of graphs. Are they related? They do share the same API.

  The way to relate the two implementations is to see that we can go back and forth between them.

  Having one direction only, e.g. @{typ "'v list_graph \<Rightarrow> 'v graph"} is called a .... ?
\<close>

definition lg_\<alpha> :: "'v list_graph \<Rightarrow> 'v graph" where
  "lg_\<alpha> xs = set(xs)"


text \<open>
  Now we have to show that our refinement actually works as expected.

  For each concrete implementation \<phi>, \<psi> of a member of the graph API f
  we want to show that the following diagram commutes.

  \<open>
    f abstract operation
    \<phi>\<^sub>f concrete implementation of f on 'v list_graph
    \<psi>\<^sub>f concrete implementation of f on 'v graph

  \<open>
    'v list_graph --- lg_\<alpha> ---> 'v graph
          |                        |
          |                        |
          \<phi>\<^sub>f                       \<psi>\<^sub>f
          |                        |
          \<down>                        \<down>
    'v list_graph --- lg_\<alpha> ---> 'v graph
  \<close>

  Or equivalenty in an algebraic equation.

    \<open>\<forall> g . \<psi>\<^sub>f \<circ> lg_\<alpha> g = lg_\<alpha> \<circ> \<phi>\<^sub>f g\<close>

  All our lemmas will be of this form.
  \<close>
\<close>

context
  notes [simp] = lg_\<alpha>_def empty_graph_def lg_empty_def lg_add_edge_def add_edge_def
begin

lemma lg_empty_correct [simp]: "lg_\<alpha> lg_empty = empty_graph"
  by simp

lemma lg_add_edge_correct [simp]: "lg_\<alpha> (lg_add_edge e E) = add_edge e (lg_\<alpha> E)"
  by simp

end

subsection \<open>Efficient concrete graphs\<close>

text \<open>
  Different data structures have different runtime characteristics for the same API:

                 'v graph   |  'v list_graph
                 ---------------------------
  empty           constant  |     constant
  member           linear   |      linear
  neighbours       linear   |      linear
  add_edge        constant  |     constant

  Refining a @{typ "'v graph"} with a @{typ "'v list_graph"} does not help.
  
  We want a better runtime for \<open>member\<close> and \<open>neighbours\<close> if possible.
  Ideally we would have constant time for both but sublinear will do.

  We encode a concrete graph as a Red-Black tree where the keys are of
  type @{typ "'v"} and the values are of type @{typ "'v list"}. The keys
  will be vertices from the graph, the values will be the list of neighbours.

  For each of the functions in our graph API we prove the diagram commutes
  by showing that the equation holds.
\<close>

type_synonym 'v rbt_graph = "('v,'v list) rbt"

(*<*)
definition rg_neighbours :: "'v::linorder \<Rightarrow> 'v rbt_graph \<Rightarrow> 'v list" where
  "rg_neighbours u t \<equiv> (case (RBT.lookup t u) of Some (vs) \<Rightarrow> vs | None \<Rightarrow> [])"
(*>*)

(*<*)
lemma rg_neighbours_mem : "v \<in> set(rg_neighbours u t) \<longleftrightarrow> (\<exists> vs. RBT.lookup t u = Some vs \<and> v \<in> set(vs))"
  sorry
(*>*)

definition rg_\<alpha> :: "'v::linorder rbt_graph \<Rightarrow> 'v graph"  where
(*< "rg_\<alpha> t \<equiv> undefined" *)
  "rg_\<alpha> t \<equiv> {(u,v) | u v. v \<in> set(rg_neighbours u t)}"
(*>*)

definition rg_empty :: "'v::linorder rbt_graph" where 
(*< "rg_empty \<equiv> undefined" *)
  "rg_empty \<equiv> RBT.empty"
(*>*)

definition rg_add_edge :: "('v::linorder\<times>'v) \<Rightarrow> 'v rbt_graph \<Rightarrow> 'v rbt_graph" where
(*< "rg_add_edge \<equiv> undefined" *)
 "rg_add_edge \<equiv> (\<lambda> (u, v) t. (RBT.insert u (v # (rg_neighbours u t)) t))"
(*>*)

context
  notes [simp] =  rg_empty_def rg_add_edge_def add_edge_def
begin

lemma rg_empty_\<alpha> [simp]: "rg_\<alpha> rg_empty = {}"
(*< oops *)
  unfolding rg_\<alpha>_def rg_neighbours_def
  by auto
(*>*)

lemma rg_add_edge_\<alpha> [simp]: "rg_\<alpha> (rg_add_edge x t) = add_edge x (rg_\<alpha> t)"
(*< oops *)
  unfolding
    rg_\<alpha>_def
    rg_add_edge_def
    add_edge_def
  by (auto simp: rg_neighbours_mem split: prod.splits if_splits)
(*>*)
end

subsection \<open>Refinement + Refinement = ? \<close>

text \<open>
  Suppose you have two commuting diagrams.

  \<open>
    'v list_graph --- lg_\<alpha> ---> 'v graph    'v graph --- gl_\<alpha> --> 'v list_graph
          |                        |            |                       |
          |                        |            |                       |
          \<phi>\<^sub>f                       \<psi>\<^sub>f           \<psi>\<^sub>f                      \<phi>\<^sub>f
          |                        |            |                       |
          \<down>                        \<down>            \<down>                       \<down> 
    'v list_graph --- lg_\<alpha> ---> 'v graph    'v graph --- gl_\<alpha> --> 'v list_graph
  \<close>

  You can paste them together on the two vertical edges and get two equations

  \<open>gl_\<alpha> \<circ> lg_\<alpha> \<circ> \<phi>\<^sub>f = \<phi>\<^sub>f \<circ> gl_\<alpha> \<circ> lg_\<alpha>\<close>
  \<open>lg_\<alpha> \<circ> gl_\<alpha> \<circ> \<psi>\<^sub>f = \<psi>\<^sub>f \<circ> lg_\<alpha> \<circ> gl_\<alpha>\<close>
  
  These equations tell us that gl_\<alpha> and lg_\<alpha> are inverse.
\<close>

text \<open>
  We show a direct abstraction going from @{typ "'v list_graph"} to
  @{typ "'v rbt_graph"} because it is easier than pasting together
  more abstraction functions.

  The idea is that we have a new commuting diagram and a new equation.

  \<open>
    'v list_graph --- read_lst ---> 'v rbt_graph
          |                             |
          |                             |
         lg_\<alpha>                          rg_\<alpha>
          |                             |
          \<down>                             \<down>
        'v g      --- identity --->   'v g
  \<close>

  \<open>rg_\<alpha> \<circ> read_lst = identity \<circ> lg_\<alpha>\<close>
\<close>

definition read_lst :: \<open>'v::linorder list_graph \<Rightarrow> 'v rbt_graph\<close> where 
(*<*)
  "read_lst xs \<equiv> fold rg_add_edge xs rg_empty"
(*>*)

(*<*)
lemma rg_add_edge_singleton_\<alpha> : "rg_\<alpha> (rg_add_edge a rg_empty) = {a}"
  by (simp add: add_edge_def)

lemma lg_\<alpha>_alt: "lg_\<alpha> xs = fold add_edge xs empty_graph"
proof -
  have "fold add_edge xs E = lg_\<alpha> xs \<union> E" for E
    apply(induction xs arbitrary: E)
     apply(auto simp: add_edge_def lg_\<alpha>_def)
    done
  from this[where E="empty_graph"] show ?thesis unfolding empty_graph_def by auto
qed
  


lemma \<open>rg_\<alpha> (read_lst xs) = lg_\<alpha> xs\<close> 
(*<*)
proof -
  have "rg_\<alpha> (fold rg_add_edge xs t) = fold add_edge xs (rg_\<alpha> t)" for t
    apply(induction xs arbitrary: t)
    apply(auto)
    done
  from this[where t = "rg_empty"] show ?thesis 
    unfolding read_lst_def lg_\<alpha>_alt empty_graph_def by auto
qed
(*>*)


(* We move on to sets implemented as RBTs. You have already seen this in the 
  lecture. We define the set as a \<open>('a,unit) rbt\<close>, which means that the RBT contains no data, only
  keys. We test for membership by looking whether the key is available in the RBT. For insert/delete
  we add/remove the key from the RBT. *)


(* From Abs_Data_Type_Demo: *)
(* We can get a set implementation from a map implementation, by using unit values *)
type_synonym 'a rbts = "('a,unit) rbt"
definition "rbts_member x t \<equiv> RBT.lookup t x = Some ()"
definition "rbts_set t \<equiv> { x. rbts_member x t }"
definition "rbts_empty \<equiv> RBT.empty"
definition "rbts_insert x t \<equiv> RBT.insert x () t"
definition "rbts_delete x t \<equiv> RBT.delete x t"

lemma rbts_empty_correct: "rbts_set rbts_empty = {}"
  by (auto simp: rbts_member_def rbts_set_def rbts_empty_def)
lemma rbts_member_correct: 
  "rbts_member x t \<longleftrightarrow> x\<in>rbts_set t" by (auto simp: rbts_member_def rbts_set_def)
lemma rbts_insert_correct:
  "rbts_set (rbts_insert x t) =  insert x (rbts_set t)" by (auto simp: rbts_member_def rbts_insert_def rbts_set_def)
lemma rbts_delete_correct:
  "rbts_set (rbts_delete x t) = rbts_set t - {x}"  by (auto simp: rbts_member_def rbts_delete_def rbts_set_def)


(* We added a function to test whether an rbt set is empty *)
definition "rbts_is_empty t \<equiv> RBT.is_empty t"

lemma rbts_is_empty_correct: "rbts_is_empty t \<longleftrightarrow> rbts_set t = {}" for t :: "'a::linorder rbts"
proof -
  have [simp]: "(\<forall>x. m x \<noteq> Some ()) \<longleftrightarrow> m=Map.empty" for m :: "'a \<rightharpoonup> unit"
    using option.discI by fastforce
  show ?thesis
    by (auto simp: rbts_member_def rbts_is_empty_def rbts_set_def)
qed  
  
(* The correctness lemmas are obvious candidates for simp-lemmas: *)  
lemmas rbts_set_correct[simp] = 
  rbts_empty_correct rbts_member_correct rbts_insert_correct rbts_delete_correct rbts_is_empty_correct


(*
  We now move on to abstraction. We have seen a calculation to find out whether a graph has 
  deadlocks. This formula uses sets to represent the graphs and for the intermediate calculation
  (the substraction of nodes in the graph). We now implement our graph as a list graph and the set
  for the intermediate calculation as an rbts (from the demos). Remember that we want to do 
  abstraction. We only prove a correspondence between the abstract operations and the 
  implementations to show the correctness. Via a refinement proof we can then proof correctness.
  
*)

  
(* Abstract concept of nodes in a graph *)
definition V :: "'v graph \<Rightarrow> 'v set" where 
(*<*)
  "V E = fst ` E \<union> snd ` E"
(*>*)

(* Abstract concept of dead end *)
definition has_dead_end :: "'v graph \<Rightarrow> bool" where
(*<*)
  "has_dead_end g \<equiv> \<exists>u \<in> V g. \<nexists>v. v \<in> V g \<and> (u, v) \<in> g"
(*>*)

(* Alternative abstract representation, that suggests implementation by set difference *)
lemma has_dead_end1: "has_dead_end g \<longleftrightarrow> snd`g - fst`g \<noteq> {}"
(*<*)
  unfolding has_dead_end_def V_def
  apply auto
  by force+
(*>*)

(* Concrete implementation (naive) *)
definition lg_has_dead_end :: "'v list_graph \<Rightarrow> bool" where 
(*<*)
  "lg_has_dead_end xs \<equiv> set (map snd xs) - set (map fst xs) \<noteq> {}"
(*>*)

lemma lg_has_dead_end_correct: "lg_has_dead_end xs \<longleftrightarrow> has_dead_end (lg_\<alpha> xs)"
(*<*)
  unfolding lg_has_dead_end_def lg_\<alpha>_def has_dead_end1
  by simp
  
(*>*)

(*
  Operation list to rbt set
*)

(* Add operation *)  
  
(* Note: This is a generic operation. 
  It's not specific to RBTs, but the pattern works for all 
  set implementations that support insert and empty  
*)

definition "rbts_from_list xs = fold rbts_insert xs rbts_empty"

lemma [simp]: "rbts_set (rbts_from_list xs) = set xs"  
proof -
  have "rbts_set (fold rbts_insert xs s) = rbts_set s \<union> set xs" for s
    apply (induction xs arbitrary: s)
    by auto
  thus ?thesis unfolding rbts_from_list_def by auto
qed  

definition "rbts_diff_list s xs = fold rbts_delete xs s"

lemma [simp]: "rbts_set (rbts_diff_list s xs) = rbts_set s - set xs"
  unfolding rbts_diff_list_def
  apply (induction xs arbitrary: s)
  by auto

(* More efficient implementation with two passes, and red-black-tree *)
definition lg_has_dead_end_rbt :: "'v::linorder list_graph \<Rightarrow> bool" where 
  "lg_has_dead_end_rbt xs \<equiv> 
\<not> rbts_is_empty (rbts_diff_list (rbts_from_list (map snd xs)) (map fst xs))"

lemma "lg_has_dead_end_rbt xs = has_dead_end (lg_\<alpha> xs)"
proof -

  have "lg_has_dead_end_rbt xs = lg_has_dead_end xs"
    unfolding lg_has_dead_end_rbt_def lg_has_dead_end_def
    by auto
  also have "\<dots> = has_dead_end (lg_\<alpha> xs)" 
    by (rule lg_has_dead_end_correct)
  finally show ?thesis . 
qed  
(*>*)


end
