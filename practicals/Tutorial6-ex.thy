chapter \<open>Tutorial 6\<close>
theory Tutorial6
imports "demos/Option_Monad_Tools"
begin


text\<open> To warm up, we are going to prove a classic that you have seen before. \<close>


lemma sum_list_upt_alt: "sum_list [0..<Suc n] = n * (n + 1) div 2"
  apply(induction n)
  apply auto
  done

lemma "wp (mfold (\<lambda> x s. Some (x + s)) [0..<Suc n] 0) (\<lambda> r. r = n * (n + 1) div 2)"
  apply(rule wp_mfold[where ?I="\<lambda> xs ys s. s = sum_list xs"])
  subgoal by simp
  subgoal for xs\<^sub>1 x xs\<^sub>2 s
    apply(intro wp_intro) by auto
  subgoal for s
    using sum_list_upt_alt apply auto
    done
  done

text \<open>
  We will now look at a more serious application of the monadic fold. We can build a histogram 
  out of a list of integers. The histogram keeps track of how often a value occurs in a list.
\<close>

fun count where
  "count [] _ = 0"  
| "count (x#xs) y = (if x=y then Suc (count xs y) else count xs y)"  
    
lemma count_append[simp]: "count (xs@ys) x = count xs x + count ys x"
  by (induction xs) auto

  
fun max_list :: "nat list \<Rightarrow> nat" where
  "max_list [] = 0"  
| "max_list (x#xs) = max x (max_list xs)"  
  
lemma max_list_append[simp]: "max_list (xs@ys) = max (max_list xs) (max_list ys)"
  by (induction xs) auto

definition "hist xs \<equiv> mfold (\<lambda> x s. Some (\<lambda> y. if x=y then s y + 1 else s y)) xs (\<lambda> y. 0)" 

term "count xs"

lemma hist_correct: "wp (hist xs) (\<lambda>h. h=count xs)"
  unfolding hist_def
  apply(rule wp_mfold[where ?I="\<lambda> xs ys s. s = count xs"])
  subgoal by auto
  subgoal by auto
  subgoal .
  done
  


subsection \<open>Graph by successor Function\<close>

type_synonym 'v sg = "('v \<Rightarrow> 'v list)"

definition sg_\<alpha> :: "'v sg \<Rightarrow> 'v rel"
  where "sg_\<alpha> sg \<equiv> {(u,v). v \<in> set (sg u)}"

definition sg_succs :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v list"
  where "sg_succs sg v \<equiv> sg v"

lemma sg_succs_correct[simp]: "set (sg_succs sg v) = sg_\<alpha> sg `` {v}"
  unfolding sg_succs_def sg_\<alpha>_def
  by auto



context
  fixes g :: "'v sg"
begin
term "sg_succs v"

  partial_function (option) dfs :: "'v \<Rightarrow> 'v set \<Rightarrow> 'v set option" where
    "dfs v V = do {
      if v \<in> V then Some V 
      else mfold dfs (sg_succs g v) (insert v V)
    }"

  abbreviation "E \<equiv> sg_\<alpha> g"


  context 
    fixes v\<^sub>0 :: "'v"
    assumes finite_reachable[simp,intro!]: "finite (E\<^sup>*``{v\<^sub>0})"
  begin

    lemma dfs_sound_aux:
      assumes "V \<subseteq> E\<^sup>*``{v\<^sub>0}" "(v\<^sub>0,v) \<in> E\<^sup>*"
      shows "wp (dfs v V) (\<lambda> V'. V \<subseteq> V' \<and> V' \<subseteq> E\<^sup>*``{v\<^sub>0})"  
    proof -
      have "wf (inv_image finite_psubset (\<lambda> V'. E\<^sup>*``{v\<^sub>0} - V'))" by simp
      then show ?thesis using assms
      proof(induction V arbitrary: v rule: wf_induct_rule)
        case (less V)

        thm less.IH[THEN wp_cons]

        have aux: "sg_succs g v = xs\<^sub>1 @ x # xs\<^sub>2 \<Longrightarrow> (v,x) \<in> E" for xs\<^sub>1 x xs\<^sub>2
          by (metis Image_singleton_iff in_set_conv_decomp sg_succs_correct)


        show ?case 
          apply(subst dfs.simps)
          apply(intro wp_intro wp_mfold[where ?I="\<lambda> xs ys V'. insert v V \<subseteq> V' \<and> V' \<subseteq> E\<^sup>*``{v\<^sub>0} "] less.IH[THEN wp_cons])
          subgoal using less by blast
          subgoal using less by blast
          subgoal by auto
          subgoal for xs\<^sub>1 x xs\<^sub>2 V\<^sub>1
            by auto
          subgoal for xs\<^sub>1 x xs\<^sub>2 s 
            apply(frule aux) by auto
          subgoal for xs\<^sub>1 x xs\<^sub>2 V' V''  by auto
          subgoal
            by simp
          done
      qed
    qed
    


    lemma dfs_sound: "wp (dfs v\<^sub>0 {}) (\<lambda> V'. V' \<subseteq> E\<^sup>*``{v\<^sub>0})"
      using dfs_sound_aux
      by fastforce
  

  end

end





end
   



(* If time suffices, we will work on the DFS algorithm from the demo. To be released. *)
