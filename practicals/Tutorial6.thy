chapter \<open>Tutorial 6\<close>
theory Tutorial6
imports "demos/Option_Monad_Tools"
begin


text\<open> To warm up, we are going to prove a classic that you have seen before. \<close>


lemma "wp (mfold (\<lambda> x s. Some (x + s)) [0..<Suc n] 0) (\<lambda> r. r = n * (n + 1) div 2)"
  oops


text \<open>
  We will now look at a more serious application of the monadic fold. We can build a histogram 
  out of a list of integers. The histogram keeps track of how often a value occurs in a list.
\<close>

fun count where
  "count [] _ = 0"  
| "count (x#xs) y = (if x=y then Suc (count xs y) else count xs y)"  
    
lemma count_append[simp]: "count (xs@ys) x = count xs x + count ys x"
  by (induction xs) auto

  
fun max_list :: "nat list \<Rightarrow> nat" where
  "max_list [] = 0"  
| "max_list (x#xs) = max x (max_list xs)"  
  
lemma max_list_append[simp]: "max_list (xs@ys) = max (max_list xs) (max_list ys)"
  by (induction xs) auto

definition "hist xs \<equiv> undefined"  

lemma hist_correct: "wp (hist xs) (\<lambda>h. h=count xs)"
  oops
  


subsection \<open>Graph by successor Function\<close>

type_synonym 'v sg = "('v \<Rightarrow> 'v list)"

definition sg_\<alpha> :: "'v sg \<Rightarrow> 'v rel"
  where "sg_\<alpha> sg \<equiv> {(u,v). v \<in> set (sg u)}"

definition sg_succs :: "'v sg \<Rightarrow> 'v \<Rightarrow> 'v list"
  where "sg_succs sg v \<equiv> sg v"

lemma sg_succs_correct[simp]: "set (sg_succs sg v) = sg_\<alpha> sg `` {v}"
  unfolding sg_succs_def sg_\<alpha>_def
  by auto

