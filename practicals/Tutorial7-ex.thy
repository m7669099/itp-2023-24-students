theory Tutorial7
  imports Main "HOL-Library.Monad_Syntax"
begin

section \<open>State monad\<close>

text \<open>
  Just like Haskell the language of Isabelle is pure. This means that there is
  no implicit state of the program. We cannot make use of what is not a parameter
  of our function because that breaks purity.

  Haskell solves the "run a stateful computation" the same way that it solves everything:
  by wrapping it into a Monad and passing that explicitly as a parameter and returning
  it as a value.

  The simplest examples of stateful computation is that of a state machine.
  \<open>
    locked = True                                    # the implicit state

    def step(event):
        if event == 'coin':
            print('thank you')
            locked = False                           # changes to the implicit state
            return
        if event == 'push':
            if locked:
                print('please insert a coin')
                return
            else:
                print('have a nice day')
                locked = True                        # change to the implicit state
                return
  \<close>

  Haskell instead forces you to make the state explicit and have computations on
  state reflect that in the types. This is achieved using the State type.

  \<open>
    -- s type of the state, e.g. data TurnstileState = Locked | Unlocked
    -- a type of result of stateful computation, e.g. data TurnstileOutput = Thank | Open | Tut
    newtype State s a = State { runState :: s -> (a, s) }
  \<close>

  You don't use the State type alone because Haskell wants to enforce purity, e.g.
  you cannot pattern match on State to get a value out of it.

  Each stateful computation \<open>'s \<Rightarrow> ('a, 's)\<close> will be an instance of the
  state monad.

  \<open>
    instance Monad (State s) where
      
      return :: a -> State s a
      return x = State ( s -> (x, s) )
      
      (>>=) :: State s a -> (a -> State s b) -> State s b
      p >>= k = state $ s0 ->
        let (x, s1) = runState p s0  -- Running the first processor on s0.
        in runState (k x) s1         -- Running the second processor on s1.
  \<close>
\<close>

section \<open>State monad and Isabelle\<close>

text \<open>
  Again, just like Haskell the language of Isabelle is pure. Because some computation is
  not pure we use the same patterns as Haskell to express it.

  In our example we will use the game of tic-tac-toe as the motivation. We cannot express
  this game as an impure computation so we will go through the state monad.

  The syntax is different from Haskell's but the following is the same encoding.

  The "left hand side" of our monad's type constructor is the return type, the "right hand side"
  is the type of the state.
\<close>

datatype ('a, 's) s_monad = SCOMP (srun: "('s \<Rightarrow> 'a \<times> 's)")

definition sreturn :: "'a \<Rightarrow> ('a, 's) s_monad" where 
  "sreturn x = SCOMP (\<lambda> s. (x, s))"

definition sbind :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) s_monad) \<Rightarrow> ('b, 's) s_monad" where 
  "sbind m f = SCOMP (\<lambda> s. (let (v, s') = (srun m) s in srun (f v) s'))"

text \<open>Prove the monad's requirements.\<close>

lemma "sbind (sreturn x) f = f x"
  unfolding sbind_def sreturn_def by auto

lemma "sbind m sreturn = m"
  unfolding sbind_def sreturn_def by auto

lemma "sbind (sbind m f) g = sbind m (\<lambda> x. sbind (f x) g)"
  unfolding sbind_def
  by (auto split: prod.splits)

subsection \<open>Tic-tac-toe and the state monad\<close>

text \<open>
  Here is a possible representation of the game data. We don't really want to think
  about it right now and won't use it in the rest of the tutorial.
\<close>

datatype cell = X | O | Empty

datatype board = Board
                     cell cell cell
                     cell cell cell
                     cell cell cell

definition empty_board where
"empty_board \<equiv> Board
                     Empty Empty Empty
                     Empty Empty Empty
                     Empty Empty Empty"

text \<open>
  In our game of tic-tac-toe we start with the empty board and no winner
  so here is an initial implementation of start.

  Start is the program that gives you the empty board and makes no
  other computation.
\<close>

definition start:: "(unit, board) s_monad" where
  "start \<equiv> SCOMP (\<lambda> _. ((), empty_board))"

text \<open>
  We also would like to say who won this game. A program to evaluate
  who won will use the state monad to look at the board and decide.

  Example: In a rigged game we could always make X win.
  rigged_game ignores the board completely and just returns the X symbol.
\<close>

definition rigged_game :: "(cell, board) s_monad" where
  "rigged_game \<equiv> sreturn X"

section \<open>Using the state monad\<close>

text \<open>
  We want to think about how we will use the state monad in our program.

  A program to play tic-tac-toe wil require some actions, e.g. \<open>place_top_left X current_board\<close>,
  and some logic to decide where to play.

  To do so we need to get the current state of the game and update the current state of the game.
  Our low level operations on the state monad are \<open>sget\<close> to get the state and \<open>sput\<close> to update
  the state.

  The "left hand side" of our monad's type signatures is the return type, the "right hand side"
  is the type of the state. In  our programs \<open>sget\<close> and \<open>sput\<close> the type of the state does
  not change but the return type changes.

  \<open>sget\<close> is the program that evaluates to the current board and does not change the state.
  \<open>sput S\<close> is the program that evaluates to () and replaces the current state with S.
\<close>

definition sget :: "('s, 's) s_monad" where "sget = SCOMP (\<lambda> s. (s,s))"

definition sput :: "'s \<Rightarrow> (unit, 's) s_monad" where "sput s = SCOMP (\<lambda> _. ((), s))"

text \<open>
  Both programs \<open>sget\<close> and \<open>sput\<close> are programs manipulating the state, this is reflected
  in their signatures. All programs manipulating the state will have the return type
  @{typ "('s, 'a) s_monad"} for some type variables s, a.
\<close>

section \<open>Proving with the state monad\<close>

text \<open>
  We can now move on to prove stateful programs correct. The correctness of a program now depends
  on the input state and the weakest precondition definition has to be updated.

  Our program is now defined as a computation in the monad @{term s_monad}. This
  means that it will be a term of type @{typ "('a, 's) s_monad"} which means it is
  a program with no parameters that returns a type 'a and operates on a state of type 's.

  The assertion Q will take both the return value and the state as a parameter but instead
  of using the type signature @{typ "'a \<times> 's \<Rightarrow> bool"} we will use the equivalent @{typ "'a \<Rightarrow> 's \<Rightarrow> bool"}.

  The final type parameter to the weakest precondition is the initial state \<open>s\<close>.
  This will make \<open>swp m Q\<close> a predicate over states.
\<close>

definition swp :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "swp m Q s = (let (a,s') = (srun m s) in Q a s')"

text \<open>
  Our programs are still pretty simple so we can work out what the weakest precondition
  will be for them.

  We know very little programs of type @{typ "('a, 's) s_monad"} so we can do them all.

  1. \<open>return x\<close>
  2. \<open>bind m f\<close>
  3. \<open>sget\<close>
  4. \<open>sput S\<close>
\<close>

lemma "swp (sreturn x) Q s \<longleftrightarrow> Q x s"
  unfolding swp_def sreturn_def by auto

lemma "swp (sbind m f) Q s = swp m (\<lambda> x s. swp (f x) Q s) s"
  unfolding swp_def sbind_def by auto

lemma "swp (sget) Q s = Q s s"
  unfolding swp_def sget_def by auto

lemma "swp (sput s') Q s = Q () s'"
  unfolding swp_def sput_def by auto

text \<open>
  We also have another rule, the rule of consequence.
\<close>

lemma swp_cons: 
  assumes "swp c Q' s"
  assumes "\<And>r s. Q' r s \<Longrightarrow> Q r s"
  shows "swp c Q s"
  using assms
  unfolding swp_def by auto

section \<open>State-Error monad\<close>

text \<open>
  Not all programs that work on a tic-tac-toe board are total. What can we do
  when we want to update a X to O? We need to encode partial programs.
\<close>

text \<open>
  To encode partial stateful programs in Isabelle we need to use the State-Error monad.
  The change is obvious. We wrap the return type of the monad's computation in an option.

  We update the type signature of each of the previous programs and wrap the result
  in Some.
\<close>

datatype ('a, 's) se_monad = COMP (run: "('s \<Rightarrow> ('a \<times> 's) option)")

definition return :: "'a \<Rightarrow> ('a, 's) se_monad" where 
  "return x = COMP (\<lambda> s. Some (x,s))"
            
definition se_bind :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) se_monad) \<Rightarrow> ('b, 's) se_monad" where 
  "se_bind m f = COMP (\<lambda> s. do { (x,s') \<leftarrow> run m s; run (f x) s'})"

definition get :: "('s, 's) se_monad" where "get = COMP (\<lambda> s. Some (s,s))"

definition put :: "'s \<Rightarrow> (unit, 's) se_monad" where "put s = COMP (\<lambda> _. Some ((), s))"

text \<open>We will prove that this is a monad later after defining the weakest-precondition.\<close>

text \<open>
  With the change to the State-Error monad we can add another primitive program. Because all the
  previous programs cannot fail we miss one program to express partial function.

  We will use \<open>fail\<close> to represent errors, e.g. placing a X where O is already.
  Just like \<open>return a\<close> returns the value a uncoditionally \<open>fail\<close> returns the value None
  unconditionally.
\<close>

definition fail :: "('a, 's) se_monad" where "fail = COMP (\<lambda> _. None)"

text \<open>
  We have previously seen that we can prove programs in the option monad correct.
  Here we want to extend this proof machinery to the State-Error monad.
\<close>

fun owp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "owp None Q = False"
  | "owp (Some v) Q = Q v" 

lemma owp_bind[simp]: 
  "owp (do {x\<leftarrow>m; f x}) Q \<longleftrightarrow> owp m (\<lambda>x. owp (f x) Q)"
  by (cases m; auto)

lemma owp_cons: 
  assumes "owp c Q'"
  assumes "\<And>r. Q' r \<Longrightarrow> Q r"
  shows "owp c Q"
  using assms by (cases c) auto

subsection \<open>Weakest precondition for the State-Error monad\<close>

text \<open>
  Just as we defined the weakest precondition for the state monad we can now define
  the weakest precondition for the state-error monad.

  The parameters for the weakest precondition are the following.

  A stateful partial program \<open>m\<close> in the monad @{term se_monad}.

  The assertion Q will take both the return value and the state as a parameter but instead
  of using the type signature @{typ "'a \<times> 's \<Rightarrow> bool"} we will use the equivalent @{typ "'a \<Rightarrow> 's \<Rightarrow> bool"}.

  The final type parameter to the weakest precondition is the initial state \<open>s\<close>.
  This will make \<open>wp m Q\<close> a predicate over states.

  Because we have wrapped the pair \<open>('a, 's)\<close> in an option we can reuse owp.
\<close>

definition wp :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "wp m Q s = (owp (run m s) (\<lambda> (a,s). Q a s))"

subsection \<open>Monad properties and proof system\<close>

text \<open>We now prove the monad properties for our basic operations \<open>return\<close>, \<open>se_bind\<close>, \<open>fail\<close>\<close>

lemma se_return_bind: "se_bind (return x) f = f x"
  unfolding se_bind_def return_def by auto

lemma se_bind_return: "se_bind m return = m"
  unfolding se_bind_def return_def by auto

lemma se_bind_assoc: "se_bind (se_bind m f) g = se_bind m (\<lambda> x. se_bind (f x) g)"
  unfolding se_bind_def 
  by (auto split: Option.bind_splits)

lemma bind_fail: "se_bind m (\<lambda> _. fail) = fail" (*This lemma does not hold in general for monads *)
  unfolding se_bind_def fail_def by (auto split: Option.bind_splits)

lemma fail_bind: "se_bind fail f = fail"
  unfolding se_bind_def fail_def by (auto split: Option.bind_splits)

text \<open>
  And we can finally prove the lemmas for our proof system.

  We prove the weakest-precondition for each of our primitive operations
  in the state-error monad (\<open>return\<close>, \<open>se_bind\<close>, \<open>fail\<close>, \<open>get\<close>, \<open>put\<close>)
  and put them in the simp set \<open>wp_simps\<close>.
\<close>

lemma se_wp_return: "wp (return x) Q s \<longleftrightarrow> Q x s"
  unfolding wp_def return_def by auto

lemma se_wp_bind: "wp (se_bind m f) Q s = wp m (\<lambda> x s. wp (f x) Q s) s"
  unfolding wp_def se_bind_def 
  apply clarsimp 
  apply(rule arg_cong[where f="owp (run m s)"]) 
  by auto

lemma se_wp_fail: "wp fail Q s = False"
  unfolding wp_def fail_def
  by auto

lemma se_wp_get: "wp (get) Q s = Q s s"
  unfolding get_def wp_def by auto

lemma se_wp_put: "wp (put s') Q s = Q () s'"
  unfolding put_def wp_def by auto
  
lemmas wp_simps = se_wp_return se_wp_bind se_wp_fail se_wp_get se_wp_put

named_theorems wp_intro

text \<open>
  We also combine the rules in the simp set \<open>wp_simps\<close> with the rule iffD2 to be used as introduction rules.

  All the rules in the simp set \<open>wp_simp\<close> are of the shape \<open>wp m Q s = ?a\<close> which means that they
  will compose nicely with theore iffD2. Theorem iffD2 asserts that instead of proving a term on the left
  hand side of an equality we can just prove the equivalence and give the right hand side's proof.
\<close>

lemmas [wp_intro] = wp_simps[THEN iffD2]

lemma wp_cons: 
  assumes "wp c Q' s"
  assumes "\<And>r s. Q' r s \<Longrightarrow> Q r s"
  shows "wp c Q s"
  using assms unfolding wp_def
  by (auto intro: owp_cons)

text \<open>
  From our primitive operations on the state-error monad we can define more complicated programs.
  
  We also prove basic equivalences and the weakest-precondition.
\<close>

definition assert :: "bool \<Rightarrow> (unit, 's) se_monad" where
  "assert P \<equiv> if P then return () else fail"
  
lemma assert_simps[simp]:
  "assert False = fail"
  "assert True = return ()"
  by (auto simp: assert_def)

lemma wp_assert: "P \<Longrightarrow> Q () s \<Longrightarrow> wp (assert P) Q s"
  using se_wp_return[of "()" "Q" "s"]
  unfolding assert_def
  by auto

lemmas [wp_intro] = wp_assert

text \<open>
  If you haven't noticed yet we did not prove lemmas that are not generic.

  All the lemmas we proved are on the generic type @{typ "('a, 's) se_monad"}. We can reuse them
  whenever we see fit.

  We will use the lemmas we have proved until now to verify real programs in the next
  part of the tutorial.
\<close>


(* 
  Adhoc overloading allows us to use the bind notation for monads. In other words, we can define
  programs with the do-notation:
  do{
    x \<leftarrow> op1
    y \<leftarrow> op2 x
    ...
    return z
  }
*)
adhoc_overloading bind se_bind

section \<open>Programs manipulating the filesystem\<close>

text \<open>
  We can model programs manipulating the filesystem as terms of a state monad.

  To do so we encode the state of the filesystem as a map from a path to
  the pair (content of file, open for writing). Again, as there may be no file for
  a path, we wrap everything in an option.

  Then we instantiate the state error monad to represent programs manipulating
  the file system as terms of type \<open>('a, fs) se_monad\<close>.

  There are two layers of options this time but it will be no problem.
\<close>

type_synonym fs = "string \<Rightarrow> (string \<times> bool) option"

type_synonym 'a fsM = "('a, fs) se_monad"

text \<open>
  The primitive operations on the filesystem are the first programs that we
  will define.

  Which operations do you need to open a file and write "ITP is the best
  course at UTwente"?

  A filesystem usually supports operations like \<open>create\<close>, \<open>delete\<close>, \<open>read\<close>, \<open>write\<close>,
  \<open>open\<close>, \<open>close\<close>, \<open>is_open\<close>, \<open>file_exists\<close>.
\<close>

definition lookup :: "('k \<rightharpoonup> 'v) \<Rightarrow> 'k \<Rightarrow> ('v,'s) se_monad"
  where "lookup fs n \<equiv> (case fs n of None \<Rightarrow> fail | Some v \<Rightarrow> return v)"

lemma wp_lookup[wp_intro]:
  "n\<in>dom fs \<Longrightarrow> (\<forall>v. fs n = Some v \<longrightarrow> Q v s) \<Longrightarrow> wp (lookup fs n) Q s"
  unfolding lookup_def
  apply (cases "fs n")
  by (auto simp: wp_simps)


definition "open" :: "string \<Rightarrow> unit fsM" where 
"open n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert(\<not>op);
  put (fs(n\<mapsto>(d,True)))
}"

definition close :: "string \<Rightarrow> unit fsM" where 
"close n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert(op);
  put (fs(n\<mapsto>(d,False)))
}"

definition read :: "string \<Rightarrow> string fsM" where 
"read n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert(op);
  return d
}"


definition "is_open" :: "string \<Rightarrow> bool fsM" where 
"is_open n \<equiv> do{
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  return op
}"

subsection \<open>Correctness of primitive operation\<close>
(*
  Just defining these functions is not enough. If we want to make guarantees on the behaviour we 
  need to come up with wp lemmas for the operations. We also need to come up with constructs that
  allow us reason about how the file system has been manipulated. We do this using two auxiliary
  definitons: 
   - \<open>open_files\<close> is a set of paths that are open.
   - \<open>data\<close> is a function that returns only the data at a path
*)

definition "open_files fs = {n. \<exists>d. fs n = Some (d,True) }"
definition "data fs = map_option fst o fs"
definition "path_ex fs n \<equiv> \<exists>X. data fs n = Some X"

thm wp_intro

lemma wp_open[wp_intro]: "path_ex fs n \<Longrightarrow> n \<notin> open_files fs \<Longrightarrow> wp (open n) (\<lambda> _ fs'. 
  open_files fs' = insert n (open_files fs) \<and>
  data fs' = data fs \<and>
  path_ex fs' n = path_ex fs n
) fs"
  unfolding open_def open_files_def data_def path_ex_def
  apply(rule wp_intro | rule wp_cons | (auto)[])+
  done

lemma wp_close[wp_intro]: "path_ex fs n \<Longrightarrow> n \<in> open_files fs \<Longrightarrow> wp (close n) (\<lambda> _ fs'. 
  open_files fs' = open_files fs - {n} \<and>
  data fs' = data fs \<and>
  path_ex fs' n = path_ex fs n
) fs"
  unfolding close_def open_files_def data_def path_ex_def
  apply(rule wp_intro | rule wp_cons | (auto)[])+
  done

lemma wp_read[wp_intro]: "path_ex fs n \<Longrightarrow> n \<in> open_files fs \<Longrightarrow> wp (read n) (\<lambda> d fs'. 
  fs' = fs \<and>
  data fs n = Some d \<and>
  path_ex fs' n = path_ex fs n
) fs"
  unfolding read_def open_files_def data_def path_ex_def
  apply(rule wp_intro | rule wp_cons | (auto)[])+
  done

lemma wp_is_open[wp_intro]: "path_ex fs n \<Longrightarrow> wp (is_open n) (\<lambda> f fs'. 
  fs' = fs \<and>
  (f \<longleftrightarrow> n \<in> open_files fs) \<and>
  path_ex fs' n = path_ex fs n
) fs"
  unfolding is_open_def open_files_def data_def path_ex_def
  apply(rule wp_intro | rule wp_cons | (auto)[])+
  done


subsection \<open>Filesystem operations\<close>
                                                      
(*
  Based on these operations, we can define more advanced functions, like a \<open>open_and_read\<close> or a 
  \<open>copy_file\<close> function.
   - For \<open>open_and_read\<close> you need \<open>open\<close> \<open>close\<close> \<open>is_open\<close> and \<open>read\<close>. It checks whether a file
     is open en if not, opens and then reads it, otherwise it just reads it.
   - For \<open>copy_file\<close> you also need the other functions, it copies the contents of a source path
     to a destination path.

  Implement them and prove them correct:
*)
(* Derived operations *)


definition "open_and_read n = do{
  op \<leftarrow> is_open n;
  if op then do{
    d \<leftarrow> read n;
    return d
  }
  else do{
    open n;
    d \<leftarrow> read n;
    close n;
    return d
  }
}"

lemma wp_if[wp_intro]:
  assumes "b \<Longrightarrow> wp c1 Q s"
  assumes "\<not>b \<Longrightarrow> wp c2 Q s"
  shows "wp (if b then c1 else c2) Q s"
  using assms by simp

lemma wp_open_and_read[wp_intro]: "path_ex fs n \<Longrightarrow> wp (open_and_read n) (\<lambda> d fs'. 
  open_files fs' = open_files fs \<and>
  data fs' = data fs \<and>
  path_ex fs' n = path_ex fs n \<and>
  data fs n = Some d
) fs"
  unfolding open_and_read_def
  apply(rule wp_intro | rule wp_cons | (auto)[])+
  done
  

(*We did not cover these in the tutorial, but the copy operation can easily be 
implemented with some additional operations as well.*)

definition create :: "string \<Rightarrow> unit fsM" where  
"create n \<equiv> do {
  fs \<leftarrow> get;
  assert (\<not>path_ex fs n);
  put (fs(n\<mapsto>([],False)))
}"

definition delete :: "string \<Rightarrow> unit fsM" where 
"delete n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert (\<not>op);
  put (fs(n:=None))
}"

definition "write" :: "string \<Rightarrow> string \<Rightarrow> unit fsM" where 
"write n d \<equiv> do {
  fs \<leftarrow> get;
  (_,op) \<leftarrow> lookup fs n;
  assert (op);
  put (fs(n\<mapsto>(d,op)))
}"

definition "file_exists" :: "string \<Rightarrow> bool fsM" where 
"file_exists n \<equiv> do {
  fs \<leftarrow> get;
  return (n \<in> dom fs)
}"

definition copy_file :: "string \<Rightarrow> string \<Rightarrow> unit fsM" where 
"copy_file src tgt \<equiv> do {
  d \<leftarrow> open_and_read src;
  ex \<leftarrow> file_exists tgt;
  (if ex then delete tgt else return ());
  create tgt;
  open tgt;
  write tgt d;
  close tgt
}"
  
lemma wp_create[wp_intro]: "\<not>path_ex fs n \<Longrightarrow> wp 
  (create n) 
  (\<lambda> _ fs'. 
    data fs' = (data fs)(n\<mapsto>[])
  \<and> open_files fs' = open_files fs
  \<and> path_ex fs' n
  ) fs"
  unfolding create_def data_def open_files_def path_ex_def
  by (rule wp_intro | rule wp_cons | (auto)[])+
  
lemma wp_delete[wp_intro]: "\<lbrakk> path_ex fs n; n\<notin> open_files fs \<rbrakk>  
  \<Longrightarrow> wp (delete n) (\<lambda> _ fs'. data fs' = (data fs)(n:=None) 
    \<and> open_files fs' = open_files fs
    \<and> \<not>path_ex fs' n) fs"
  unfolding delete_def data_def open_files_def path_ex_def
  by (rule wp_intro | rule wp_cons | (auto)[])+

lemma wp_write[wp_intro]: "\<lbrakk> n \<in> open_files fs\<rbrakk> 
  \<Longrightarrow> wp (write n d) (\<lambda> _ fs'. 
    data fs' = (data fs)(n\<mapsto>d)
  \<and> open_files fs' = open_files fs 
  \<and> path_ex fs' n
) fs"
  unfolding write_def data_def open_files_def path_ex_def
  by (rule wp_intro | rule wp_cons | (auto)[])+
  
lemma wp_file_exists[wp_intro]: "wp (file_exists n) 
  (\<lambda> r fs'. r = path_ex fs' n 
    \<and> fs' = fs) fs"
  unfolding file_exists_def data_def path_ex_def
  by (rule wp_intro | rule wp_cons | (auto)[])+


lemma "\<lbrakk>data fs src = Some d;
  path_ex fs src;
  tgt \<notin> open_files fs\<rbrakk> 
  \<Longrightarrow> wp (copy_file src tgt) 
  (\<lambda> _ fs'. data fs' = (data fs)(tgt\<mapsto>d) 
    \<and> open_files fs' = open_files fs) fs"
  unfolding copy_file_def
  by (rule wp_intro | rule wp_cons | (auto)[])+