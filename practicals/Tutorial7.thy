theory Tutorial7
  imports Main "HOL-Library.Monad_Syntax"
begin


(*
  We now introduce the state monad. The state monad maps a state to an output of type 'a and a new
  state 's. The state monad essentially is a monad that performs operations on all possible states
  at the same time. Let's imagine a game of tic-tac-toe. We can perform an action like for example:
  Place an X in the top left corner and return who won the game if applicable (X if there are three
  crosses in a row, O if there are three rings in a row and - otherwise).
  The nice thing about the state monad is that we don't have to explicitly carry the state around.
  Rather, this operation returns a function f that, given a configuration, performs the actions as 
  described above:

    _|_|_        X|_|_
  f _|_|_ = (-,  _|_|_  )
     | |          | |

    _|_|_        X|_|_
  f _|O|_ = (-,  _|O|_  )
     | |          | |

    _|_|O        X|_|O
  f X|O|_ = (X,  X|O|_  )
    X| |         X| |

  In essence, you can see it as if you are playing tic-tac-toe on all possible configurations 
  (or states ;) ) at the same time. But as we see later, the state monad lets us define operations
  as if the configuration is already given.

  Naturally, this monad has a bind and a return function.

*)

datatype ('a, 's) s_monad = SCOMP (srun: "('s \<Rightarrow> 'a \<times> 's)")

definition sreturn :: "'a \<Rightarrow> ('a, 's) s_monad" where 
  "sreturn x = undefined"

definition sbind :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) s_monad) \<Rightarrow> ('b, 's) s_monad" where 
  "sbind m f = undefined"


(*
  The state monad is accompanied by a get and put function. \<open>sget\<close> "retrieves" the global state 
  that we are working on and \<open>sput\<close> updates the global state. 
  In the tic-tac-toe setting, we would use \<open>sget\<close> to obtain the configuration of the game so that
  we can place our X and evaluate whether we have won. We then use \<open>sput\<close> to update the global
  state which we can pass on to a new operation which then operates with the updated board as the
  global state.
*)

definition sget :: "('s, 's) s_monad" where "sget = SCOMP (\<lambda> s. (s,s))"

definition sput :: "'s \<Rightarrow> (unit, 's) s_monad" where "sput s = SCOMP (\<lambda> _. ((), s))"


(*
  Because the output of a program with the state monad is dependent on the input state, the 
  weakest precondition also requires a state. \<open>swp m Q s\<close> is satisfied if the output of \<open>m s\<close>
  (let's call it (a,s')) satisfies \<open>Q a s'\<close>
*)

definition swp :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "swp m Q s = (let (a,s') = (srun m s) in Q a s')"


(*
  Do you remember the monadic laws? Define and prove them here:
*)




(*
  To make the verification machinery work, we also define the wp lemmas for the operations that 
  we have so far defined. Define lemmas for return, bind, put and get. We may also need a 
  consequence rule.
*)






(*
  These are the basics to verify programs that use the state-error monad. We can now define programs
  that play tic-tac-toe for us (given the right setup) and more!. But wait, there is a problem. 
  What if we perform operation f that we mentioned in the beginning on a board that does not allow 
  it? E.g.

    O|_|_
  f _|_|_ = ?
     | |  

  This would be an invalid move. There is no structure in our monad that can tell us something went
  wrong. To make sure that we can identify erroneous behaviour, we amend the state monad to become
  a state-error monad. I.e.

    _|_|_             X|_|_
  f _|_|_ = Some (-,  _|_|_  )
     | |               | |

    O|_|_
  f _|_|_ = None
     | |  

  We won't put you through defining all of this again, s it is mostly repeating the state monad 
  setup in a more extensive way.

*)

datatype ('a, 's) se_monad = COMP (run: "('s \<Rightarrow> ('a \<times> 's) option)")

definition return :: "'a \<Rightarrow> ('a, 's) se_monad" where 
  "return x = COMP (\<lambda> s. Some (x,s))"
            
definition se_bind :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) se_monad) \<Rightarrow> ('b, 's) se_monad" where 
  "se_bind m f = COMP (\<lambda> s. do { (x,s') \<leftarrow> run m s; run (f x) s'})"


(* 
  Adhoc overloading allows us to use the bind notation for monads. In other words, we can define
  programs with the do-notation:
  do{
    x \<leftarrow> op1
    y \<leftarrow> op2 x
    ...
    return z
  }
*)
adhoc_overloading bind se_bind

(*
  Apart from the get and put functions, we now also have a fail. Fail represents an error on all
  possible input states. For example, if we define an operation that places an X in the middle and
  then a Y in the middle in tic-tac-toe.
*)
definition fail :: "('a, 's) se_monad" where "fail = COMP (\<lambda> _. None)"

definition get :: "('s, 's) se_monad" where "get = COMP (\<lambda> s. Some (s,s))"

definition put :: "'s \<Rightarrow> (unit, 's) se_monad" where "put s = COMP (\<lambda> _. Some ((), s))"




(*
  You have seen the weakest precondition for the option monad in the lecture a couple of times.
  We use it as a tool to define the wp for the state-error monad.
*)

fun owp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "owp None Q = False"
  | "owp (Some v) Q = Q v" 

lemma owp_bind[simp]: 
  "owp (do {x\<leftarrow>m; f x}) Q \<longleftrightarrow> owp m (\<lambda>x. owp (f x) Q)"
  by (cases m; auto)

lemma owp_cons: 
  assumes "owp c Q'"
  assumes "\<And>r. Q' r \<Longrightarrow> Q r"
  shows "owp c Q"
  using assms by (cases c) auto


(*
  The wp for the state-error monad now basically does the same as it did before, but it additionally
  guarantees that the program does not crash on state \<open>s\<close>.
*)

definition wp :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "wp m Q s = (owp (run m s) (\<lambda> (a,s). Q a s))"


(*
  These are the same rules (in an extended form) that we have already seen for the state monad.
  We have the standard monadic rules and the wp rules.
  NOTE: We have also added \<open>fail\<close> into the mix here.
*)
lemma se_return_bind: "se_bind (return x) f = f x"
  unfolding se_bind_def return_def by auto

lemma se_bind_return: "se_bind m return = m"
  unfolding se_bind_def return_def by auto

lemma se_bind_assoc: "se_bind (se_bind m f) g = se_bind m (\<lambda> x. se_bind (f x) g)"
  unfolding se_bind_def 
  by (auto split: Option.bind_splits)

lemma bind_fail: "se_bind m (\<lambda> _. fail) = fail" (*This lemma does not hold for all monads*)
  unfolding se_bind_def fail_def by (auto split: Option.bind_splits)

lemma fail_bind: "se_bind fail f = fail"
  unfolding se_bind_def fail_def by (auto split: Option.bind_splits)

lemma se_wp_return: "wp (return x) Q s \<longleftrightarrow> Q x s"
  unfolding wp_def return_def by auto

lemma se_wp_bind: "wp (se_bind m f) Q s = wp m (\<lambda> x s. wp (f x) Q s) s"
  unfolding wp_def se_bind_def 
  apply clarsimp 
  apply(rule arg_cong[where f="owp (run m s)"]) 
  by auto

lemma se_wp_fail: "wp fail Q s = False"
  unfolding wp_def fail_def
  by auto

lemma se_wp_get: "wp (get) Q s = Q s s"
  unfolding get_def wp_def by auto

lemma se_wp_put: "wp (put s') Q s = Q () s'"
  unfolding put_def wp_def by auto
  
lemmas wp_simps = se_wp_return se_wp_bind se_wp_fail se_wp_get se_wp_put

named_theorems wp_intro
lemmas [wp_intro] = wp_simps[THEN iffD2]

lemma wp_cons: 
  assumes "wp c Q' s"
  assumes "\<And>r s. Q' r s \<Longrightarrow> Q r s"
  shows "wp c Q s"
  using assms unfolding wp_def
  by (auto intro: owp_cons)

(*
  Now that our programs can crash, we can also introduce assert. \<open>assert P\<close> returns no information 
  (unit) if P is True and crashes otherwise. Come up with some useful simplifications.
  (\<open>assert True\<close>, \<open>assert False\<close>) and wp lemmas.
*)




(*
  Now it is time to put our setup to the test! No more messing around with simple examples like 
  tic-tac-toe. We now want to verify a file system. We take a simple definition of a file system
  that takes a path (string) and returns the contents of the file at that path and a boolean that
  marks whether the file is currently already open for writing. If there is no file at the path,
  it returns None.
*)

type_synonym fs = "string \<Rightarrow> (string \<times> bool) option"

type_synonym 'a fsM = "('a, fs) se_monad"

(*
  The file system monad (fsM) lets us define programs on the file system. The input is a filesystem
  \<open>fs\<close> and the output is a manipulated version of \<open>fs\<close> and an output of type \<open>'a\<close>. A program can
  now be something like "create a file at path xyz and write the string "ITP is the best course at 
  this university" to it".

  When creating a system like this, it is always important to ask yourself which operations build
  the core of your system. For a file system you want to create, delete, read, write, open, close, 
  existence of files and checking whether a file is already open.
  
*)

definition lookup :: "('k \<rightharpoonup> 'v) \<Rightarrow> 'k \<Rightarrow> ('v,'s) se_monad"
  where "lookup fs n \<equiv> (case fs n of None \<Rightarrow> fail | Some v \<Rightarrow> return v)"

lemma wp_lookup[wp_intro]:
  "n\<in>dom fs \<Longrightarrow> (\<forall>v. fs n = Some v \<longrightarrow> Q v s) \<Longrightarrow> wp (lookup fs n) Q s"
  unfolding lookup_def
  apply (cases "fs n")
  by (auto simp: wp_simps)


definition create :: "string \<Rightarrow> unit fsM" where  
"create n \<equiv> undefined"

definition delete :: "string \<Rightarrow> unit fsM" where 
"delete n \<equiv> undefined"
    
definition "open" :: "string \<Rightarrow> unit fsM" where 
"open n \<equiv> undefined"

definition close :: "string \<Rightarrow> unit fsM" where 
"close n \<equiv> undefined"

definition read :: "string \<Rightarrow> string fsM" where 
"read n \<equiv> undefined"

definition "write" :: "string \<Rightarrow> string \<Rightarrow> unit fsM" where 
"write n d \<equiv> undefined"

definition "file_exists" :: "string \<Rightarrow> bool fsM" where 
"file_exists n \<equiv> undefined"

definition "is_open" :: "string \<Rightarrow> bool fsM" where 
"is_open n \<equiv> undefined"


(*
  Just defining these functions is not enough. If we want to make guarantees on the behaviour we 
  need to come up with wp lemmas for the operations. We also need to come up with constructs that
  allow us reason about how the file system has been manipulated. We do this using two auxiliary
  definitons: 
   - \<open>open_files\<close> is a set of paths that are open.
   - \<open>data\<close> is a function that returns only the data at a path
*)

definition "open_files fs = {n. \<exists>d. fs n = Some (d,True) }"
definition "data fs = map_option fst o fs"

                                                            
(*
  Based on these operations, we can define more advanced functions, like a \<open>open_and_read\<close> or a 
  \<open>copy_file\<close> function.
   - For \<open>open_and_read\<close> you need \<open>open\<close> \<open>close\<close> \<open>is_open\<close> and \<open>read\<close>. It checks whether a file
     is open en if not, opens and then reads it, otherwise it just reads it.
   - For \<open>copy_file\<close> you also need the other functions, it copies the contents of a source path
     to a destination path.

  Implement them and prove them correct:
*)
end