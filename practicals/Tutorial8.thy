theory Tutorial8
  imports Main "HOL-Library.Extended_Nat" "./demos/Nres_Monad_Tools"
begin


(*

  For this tutorial, we are going to prove the correctness of Dijkstra's algorithm using the
  nres monad. We base our proof on an AFP entry that forms a solid basis for our proof:
  \<^url>\<open>https://www.isa-afp.org/sessions/prim_dijkstra_simple/#Dijkstra_Abstract.html\<close>
  However, this formalization does not use the refinement framework and therefore we cannot simply
  copy the correctness proofs. But the work that we have here can easily be adapted to our needs.

  The first section \<open>Graph\<close> defines the abstract graph. The second section \<open>Paths\<close> proves some
  important lemmas on paths. The core idea of Dijkstra is defined in the \<open>Distance\<close> section. This
  section has some core lemmas like the triangle lemma (the shortest path to node v is always at 
  most as long as the shorted path to one of the predecessors plus the weight from the predecessor 
  to v) and the decomposition of shortest paths (which is also a shortest path). Finally, we have
  some lemmas about the algorithm itself and its invariant. We will now try to shape our algorithm
  in such a way that we can reuse all of this work for our algorithm in the nres monad.

*)

subsection \<open>Graph\<close>

lemma least_antimono: "X\<noteq>{} \<Longrightarrow> X\<subseteq>Y \<Longrightarrow> (LEAST y::_::wellorder. y\<in>Y) \<le> (LEAST x. x\<in>X)"
  by (metis LeastI_ex Least_le equals0I rev_subsetD)

text \<open>
  A weighted graph is represented by a function from edges to weights.

  For simplicity, we use @{typ enat} as weights, @{term \<open>\<infinity>\<close>} meaning 
  that there is no edge.
\<close>

type_synonym ('v) wgraph = "('v \<times> 'v) \<Rightarrow> enat"

text \<open>We encapsulate weighted graphs into a locale that fixes a graph\<close>
locale WGraph = fixes w :: "'v wgraph"
begin
text \<open>Set of edges with finite weight\<close>
definition "edges \<equiv> {(u,v) . w (u,v) \<noteq> \<infinity>}"

subsection \<open>Paths\<close>
text \<open>A path between nodes \<open>u\<close> and \<open>v\<close> is a list of edge weights
  of a sequence of edges from \<open>u\<close> to \<open>v\<close>.
  
  Note that a path may also contain edges with weight \<open>\<infinity>\<close>.
\<close>

fun path :: "'v \<Rightarrow> enat list \<Rightarrow> 'v \<Rightarrow> bool" where
  "path u [] v \<longleftrightarrow> u=v"
| "path u (l#ls) v \<longleftrightarrow> (\<exists>uh. l = w (u,uh) \<and> path uh ls v)"

lemma path_append[simp]: 
  "path u (ls1@ls2) v \<longleftrightarrow> (\<exists>w. path u ls1 w \<and> path w ls2 v)"
  by (induction ls1 arbitrary: u) auto

text \<open>There is a singleton path between every two nodes 
  (it's weight might be \<open>\<infinity>\<close>).\<close>  
lemma triv_path: "path u [w (u,v)] v" by auto
  
text \<open>Shortcut for the set of all paths between two nodes\<close>    
definition "paths u v \<equiv> {p . path u p v}"

lemma paths_ne: "paths u v \<noteq> {}" using triv_path unfolding paths_def by blast

text \<open>If there is a path from a node inside a set \<open>S\<close>, to a node outside 
  a set \<open>S\<close>, this path must contain an edge from inside \<open>S\<close> to outside \<open>S\<close>.
\<close>
lemma find_leave_edgeE:
  assumes "path u p v"
  assumes "u\<in>S" "v\<notin>S"
  obtains p1 x y p2 
    where "p = p1@w (x,y)#p2" "x\<in>S" "y\<notin>S" "path u p1 x" "path y p2 v"
proof -
  have "\<exists>p1 x y p2. p = p1@w (x,y)#p2 \<and> x\<in>S \<and> y\<notin>S \<and> path u p1 x \<and> path y p2 v"
    using assms
  proof (induction p arbitrary: u)
    case Nil
    then show ?case by auto
  next
    case (Cons a p)
    from Cons.prems obtain x where [simp]: "a=w (u,x)" and PX: "path x p v" 
      by auto
    
    show ?case proof (cases "x\<in>S")
      case False with PX \<open>u\<in>S\<close> show ?thesis by fastforce
    next
      case True from Cons.IH[OF PX True \<open>v\<notin>S\<close>] show ?thesis
        by clarsimp (metis WGraph.path.simps(2) append_Cons)
    qed
  qed
  thus ?thesis by (fast intro: that) 
qed

subsection \<open>Distance\<close>

text \<open>The (minimum) distance between two nodes \<open>u\<close> and \<open>v\<close> is called \<open>\<delta> u v\<close>.\<close>

definition "\<delta> u v \<equiv> LEAST w::enat. w\<in>sum_list`paths u v"

lemma obtain_shortest_path: 
  obtains p where "path s p u" "\<delta> s u = sum_list p"
  unfolding \<delta>_def using paths_ne
  by (smt Collect_empty_eq LeastI_ex WGraph.paths_def imageI image_iff 
          mem_Collect_eq paths_def)

lemma shortest_path_least:  
  "path s p u \<Longrightarrow> \<delta> s u \<le> sum_list p"
  unfolding \<delta>_def paths_def
  by (simp add: Least_le)

lemma distance_refl[simp]: "\<delta> s s = 0"
  using shortest_path_least[of s "[]" s] by auto
  
lemma distance_direct: "\<delta> s u \<le> w (s, u)"  
  using shortest_path_least[of s "[w (s,u)]" u] by auto

text \<open>Triangle inequality: The distance from \<open>s\<close> to \<open>v\<close> is shorter than 
  the distance from \<open>s\<close> to \<open>u\<close> and the edge weight from \<open>u\<close> to \<open>v\<close>. \<close>  
lemma triangle: "\<delta> s v \<le> \<delta> s u + w (u,v)"
proof -
  have "path s (p@[w (u,v)]) v" if "path s p u" for p using that by auto
  then have "(+) (w (u,v)) ` sum_list ` paths s u \<subseteq> sum_list ` paths s v"
    by (fastforce simp: paths_def image_iff simp del: path.simps path_append) 
  from least_antimono[OF _ this] paths_ne have 
    "(LEAST y::enat. y \<in> sum_list ` paths s v) 
    \<le> (LEAST x::enat. x \<in> (+) (w (u,v)) ` sum_list ` paths s u)"
    by (auto simp: paths_def)
  also have "\<dots> = (LEAST x. x \<in> sum_list ` paths s u) + w (u,v)"
    apply (subst Least_mono[of "(+) (w (u,v))" "sum_list`paths s u"])
    subgoal by (auto simp: mono_def)
    subgoal by simp (metis paths_def mem_Collect_eq 
                          obtain_shortest_path shortest_path_least)
    subgoal by auto
    done
  finally show ?thesis unfolding \<delta>_def .
qed
  
text \<open>Any prefix of a shortest path is a shortest path itself.
  Note: The \<open>< \<infinity>\<close> conditions are required to avoid saturation in adding to \<open>\<infinity>\<close>!
\<close>
lemma shortest_path_prefix:
  assumes "path s p\<^sub>1 x" "path x p\<^sub>2 u" 
  and DSU: "\<delta> s u = sum_list p\<^sub>1 + sum_list p\<^sub>2" "\<delta> s u < \<infinity>"
  shows "\<delta> s x = sum_list p\<^sub>1" "\<delta> s x < \<infinity>"
proof -  
  have "\<delta> s x \<le> sum_list p\<^sub>1" using assms shortest_path_least by blast
  moreover have "\<not>\<delta> s x < sum_list p\<^sub>1" proof
    assume "\<delta> s x < sum_list p\<^sub>1"
    then obtain p\<^sub>1' where "path s p\<^sub>1' x" "sum_list p\<^sub>1' < sum_list p\<^sub>1"
      by (auto intro: obtain_shortest_path[of s x])
    with \<open>path x p\<^sub>2 u\<close> shortest_path_least[of s "p\<^sub>1'@p\<^sub>2" u] DSU show False
      by fastforce
  qed
  ultimately show "\<delta> s x = sum_list p\<^sub>1" by auto
  with DSU show "\<delta> s x < \<infinity>" using le_iff_add by fastforce
qed  
  
      
end


subsection \<open>Abstract Algorithm\<close>

type_synonym 'v estimate = "'v \<Rightarrow> enat"
text \<open>We fix a start node and a weighted graph\<close>
locale Dijkstra = WGraph w for w :: "('v) wgraph" +
  fixes s :: 'v
begin

text \<open>Relax all outgoing edges of node \<open>u\<close>\<close>
definition relax_outgoing :: "'v \<Rightarrow> 'v estimate \<Rightarrow> 'v estimate"
  where "relax_outgoing u D \<equiv> \<lambda>v. min (D v) (D u + w (u,v))"

text \<open>Initialization\<close>
definition "initD \<equiv> (\<lambda>_. \<infinity>)(s:=0)"
definition "initS \<equiv> {}"  
  
      
text \<open>Relaxing will never increase estimates\<close>
lemma relax_mono: "relax_outgoing u D v \<le> D v"
  by (auto simp: relax_outgoing_def)


definition "all_dnodes \<equiv> Set.insert s { v . \<exists>u. w (u,v)\<noteq>\<infinity> }"
definition "unfinished_dnodes S \<equiv> all_dnodes - S "

lemma unfinished_nodes_subset: "unfinished_dnodes S \<subseteq> all_dnodes"
  by (auto simp: unfinished_dnodes_def)

end  

subsubsection \<open>Invariant\<close>
text \<open>The invariant is defined as locale\<close>
  
locale Dijkstra_Invar = Dijkstra w s for w and s :: 'v +
  fixes D :: "'v estimate" and S :: "'v set"
  assumes upper_bound: \<open>\<delta> s u \<le> D u\<close> \<comment> \<open>\<open>D\<close> is a valid estimate\<close>
  assumes s_in_S: \<open>s\<in>S \<or> (D=(\<lambda>_. \<infinity>)(s:=0) \<and> S={})\<close> \<comment> \<open>The start node is 
    finished, or we are in initial state\<close>  
  assumes S_precise: "u\<in>S \<Longrightarrow> D u = \<delta> s u" \<comment> \<open>Finished nodes have precise 
    estimate\<close>
  assumes S_relaxed: \<open>v\<in>S \<Longrightarrow> D u \<le> \<delta> s v + w (v,u)\<close> \<comment> \<open>Outgoing edges of 
    finished nodes have been relaxed, using precise distance\<close>
begin

abbreviation (in Dijkstra) "D_invar \<equiv> Dijkstra_Invar w s"

text \<open>The invariant holds for the initial state\<close>  
theorem (in Dijkstra) invar_init: "D_invar initD initS"
  apply unfold_locales
  unfolding initD_def initS_def
  by (auto simp: relax_outgoing_def distance_direct)


text \<open>Relaxing some edges maintains the upper bound property\<close>    
lemma maintain_upper_bound: "\<delta> s u \<le> (relax_outgoing v D) u"
  apply (clarsimp simp: relax_outgoing_def upper_bound split: prod.splits)
  using triangle upper_bound add_right_mono dual_order.trans by blast

text \<open>Relaxing edges will not affect nodes with already precise estimates\<close>
lemma relax_precise_id: "D v = \<delta> s v \<Longrightarrow> relax_outgoing u D v = \<delta> s v"
  using maintain_upper_bound upper_bound relax_mono
  by (metis antisym)

text \<open>In particular, relaxing edges will not affect finished nodes\<close>  
lemma relax_finished_id: "v\<in>S \<Longrightarrow> relax_outgoing u D v = D v"
  by (simp add: S_precise relax_precise_id)  
      
text \<open>The least (finite) estimate among all nodes \<open>u\<close> not in \<open>S\<close> is already precise.
  This will allow us to add the node \<open>u\<close> to \<open>S\<close>. \<close>
lemma maintain_S_precise_and_connected:  
  assumes UNS: "u\<notin>S"
  assumes MIN: "\<forall>v. v\<notin>S \<longrightarrow> D u \<le> D v"
  shows "D u = \<delta> s u"
  text \<open>We start with a case distinction whether we are in the first 
    step of the loop, where we process the start node, or in subsequent steps,
    where the start node has already been finished.\<close>
proof (cases "u=s")  
  assume [simp]: "u=s" \<comment> \<open>First step of loop\<close>
  then show ?thesis using \<open>u\<notin>S\<close> s_in_S by simp
next
  assume \<open>u\<noteq>s\<close> \<comment> \<open>Later step of loop\<close>
  text \<open>The start node has already been finished\<close>   
  with s_in_S MIN have \<open>s\<in>S\<close> apply clarsimp using infinity_ne_i0 by metis
  
  show ?thesis
  text \<open>Next, we handle the case that \<open>u\<close> is unreachable.\<close>
  proof (cases \<open>\<delta> s u < \<infinity>\<close>)
    assume "\<not>(\<delta> s u < \<infinity>)" \<comment> \<open>Node is unreachable (infinite distance)\<close>
    text \<open>By the upper-bound property, we get \<open>D u = \<delta> s u = \<infinity>\<close>\<close>
    then show ?thesis using upper_bound[of u] by auto
  next
    assume "\<delta> s u < \<infinity>" \<comment> \<open>Main case: Node has finite distance\<close>
 
    text \<open>Consider a shortest path from \<open>s\<close> to \<open>u\<close>\<close>        
    obtain p where "path s p u" and DSU: "\<delta> s u = sum_list p"
      by (rule obtain_shortest_path)
    text \<open>It goes from inside \<open>S\<close> to outside \<open>S\<close>, so there must be an edge at the border.
      Let \<open>(x,y)\<close> be such an edge, with \<open>x\<in>S\<close> and \<open>y\<notin>S\<close>.\<close>
    from find_leave_edgeE[OF \<open>path s p u\<close> \<open>s\<in>S\<close> \<open>u\<notin>S\<close>] obtain p1 x y p2 where
      [simp]: "p = p1 @ w (x, y) # p2" 
      and DECOMP: "x \<in> S" "y \<notin> S" "path s p1 x" "path y p2 u" .
    text \<open>As prefixes of shortest paths are again shortest paths, the shortest 
          path to \<open>y\<close> ends with edge \<open>(x,y)\<close> \<close>  
    have DSX: "\<delta> s x = sum_list p1" and DSY: "\<delta> s y = \<delta> s x + w (x, y)"
      using shortest_path_prefix[of s p1 x "w (x,y)#p2" u] 
        and shortest_path_prefix[of s "p1@[w (x,y)]" y p2 u]
        and \<open>\<delta> s u < \<infinity>\<close> DECOMP 
        by (force simp: DSU)+
    text \<open>Upon adding \<open>x\<close> to \<open>S\<close>, this edge has been relaxed with the precise
       estimate for \<open>x\<close>. At this point the estimate for \<open>y\<close> has become 
       precise, too\<close>  
    with \<open>x\<in>S\<close> have "D y = \<delta> s y"  
      by (metis S_relaxed antisym_conv upper_bound)
    moreover text \<open>The shortest path to \<open>y\<close> is a prefix of that to \<open>u\<close>, thus 
      it shorter or equal\<close>
    have "\<dots> \<le> \<delta> s u" using DSU by (simp add: DSX DSY)
    moreover text \<open>The estimate for \<open>u\<close> is an upper bound\<close>
    have "\<dots> \<le> D u" using upper_bound by (auto)
    moreover text \<open>\<open>u\<close> was a node with smallest estimate\<close>
    have "\<dots> \<le> D y" using \<open>u\<notin>S\<close> \<open>y\<notin>S\<close> MIN by auto
    ultimately text \<open>This closed a cycle in the inequation chain. Thus, by 
      antisymmetry, all items are equal. In particular, \<open>D u = \<delta> s u\<close>, qed.\<close>
    show "D u = \<delta> s u" by simp
  qed    
qed
  
text \<open>A step of Dijkstra's algorithm maintains the invariant.
  More precisely, in a step of Dijkstra's algorithm, 
  we pick a node \<open>u\<notin>S\<close> with least finite estimate, relax the outgoing 
  edges of \<open>u\<close>, and add \<open>u\<close> to \<open>S\<close>.\<close>    
theorem maintain_D_invar:
  assumes UNS: "u\<notin>S"
  assumes UNI: "D u < \<infinity>"
  assumes MIN: "\<forall>v. v\<notin>S \<longrightarrow> D u \<le> D v"
  shows "D_invar (relax_outgoing u D) (Set.insert u S)"
  apply (cases \<open>s\<in>S\<close>)
  subgoal
    apply (unfold_locales)
    subgoal by (simp add: maintain_upper_bound)
    subgoal by simp
    subgoal 
      using maintain_S_precise_and_connected[OF UNS MIN] S_precise        
      by (auto simp: relax_precise_id) 
    subgoal
      using maintain_S_precise_and_connected[OF UNS MIN]
      by (auto simp: relax_outgoing_def S_relaxed min.coboundedI1)
    done
  subgoal
    apply unfold_locales
    using s_in_S UNI distance_direct 
    by (auto simp: relax_outgoing_def split: if_splits)
  done
  

text \<open>When the algorithm is finished, i.e., when there are 
  no unfinished nodes with finite estimates left,
  then all estimates are accurate.\<close>  
lemma invar_finish_imp_correct:
  assumes F: "\<forall>u. u\<notin>S \<longrightarrow> D u = \<infinity>"
  shows "D u = \<delta> s u"
proof (cases "u\<in>S")
  assume "u\<in>S" text \<open>The estimates of finished nodes are accurate\<close>
  then show ?thesis using S_precise by simp
next
  assume \<open>u\<notin>S\<close> text \<open>\<open>D u\<close> is minimal, and minimal estimates are precise\<close>
  then show ?thesis 
    using F maintain_S_precise_and_connected[of u] by auto
  
qed  
  
  
text \<open>A step decreases the set of unfinished nodes.\<close>
lemma unfinished_nodes_decr:
  assumes UNS: "u\<notin>S"
  assumes UNI: "D u < \<infinity>"
  shows "unfinished_dnodes (Set.insert u S) \<subset> unfinished_dnodes S"
proof -
  text \<open>There is a path to \<open>u\<close>\<close>
  from UNI have "\<delta> s u < \<infinity>" using upper_bound[of u] leD by fastforce
  
  text \<open>Thus, \<open>u\<close> is among \<open>all_dnodes\<close>\<close>
  have "u\<in>all_dnodes" 
  proof -
    obtain p where "path s p u" "sum_list p < \<infinity>"
      apply (rule obtain_shortest_path[of s u])
      using \<open>\<delta> s u < \<infinity>\<close> by auto
    with \<open>u\<notin>S\<close> show ?thesis 
      apply (cases p rule: rev_cases) 
      by (auto simp: Dijkstra.all_dnodes_def)
  qed
  text \<open>Which implies the proposition\<close>
  with \<open>u\<notin>S\<close> show ?thesis by (auto simp: unfinished_dnodes_def)
qed
  
        
end 

(*

  Define the algorithm and show it correct. If time suffices, look into refinement as well.

*)


end