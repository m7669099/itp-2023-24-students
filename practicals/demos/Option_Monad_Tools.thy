chapter \<open>Option monad and tools, from option-monad-demo and while-demo\<close> 
theory Option_Monad_Tools
imports 
  Main
  "HOL-Library.Monad_Syntax" (* To get the do-notation in Isabelle *)
  
begin


section \<open>Assertions\<close> 
  
(* Assert. Return () if condition is true, None otherwise. *)
  
definition "assert P \<equiv> if P then Some () else None"

lemma assert_simps[simp]:
  "assert False = None"
  "assert True = Some ()"
  by (auto simp: assert_def)

  
section \<open>Weakest Precondition\<close>  
  
  fun wp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "wp None Q = False"
  | "wp (Some x) Q = Q x"  
  
  lemma wp_bind[simp]: "wp (do { x\<leftarrow>m; f x }) Q = wp m (\<lambda>x. wp (f x) Q)"
    by (cases m) auto

  subsection \<open>Intro-Rules\<close>  
    
  named_theorems wp_intro
  
  lemma wp_returnI[wp_intro]: "Q a \<Longrightarrow> wp (Some a) Q" by simp
  lemma wp_bindI[wp_intro]: "wp m (\<lambda>x. wp (f x) Q) \<Longrightarrow> wp (do { x\<leftarrow>m; f x }) Q" by simp

  lemma wp_assertI[wp_intro]: "\<lbrakk>P; Q ()\<rbrakk> \<Longrightarrow> wp (assert P) Q" by simp
  
  (* Consequence rule: Weakening post-condition 
  
    If c ensures Q, and Q \<Longrightarrow> Q', then c ensures Q'
  *)
  lemma wp_cons: 
    assumes "wp c Q"
    assumes "\<And>s. Q s \<Longrightarrow> Q' s"
    shows "wp c Q'"
    using assms by (cases c) auto
  
    
  (* Plus case-splitting rules for better usability  *)
  
  lemma wp_case_prodI[wp_intro]:
    assumes "\<And>a b. \<lbrakk> p=(a,b) \<rbrakk> \<Longrightarrow> wp (f a b) Q"
    shows "wp (case p of (a,b) \<Rightarrow> f a b) Q"
    using assms by (cases p) auto
  

  lemma wp_if[wp_intro]:
    assumes "b \<Longrightarrow> wp c Q"
    assumes "\<not>b \<Longrightarrow> wp d Q"
    shows "wp (if b then c else d) Q"
    using assms by auto


section \<open>While Loops\<close>  

  context
    fixes b :: "'s \<Rightarrow> bool"
    fixes c :: "'s \<Rightarrow> 's option"
  begin
  
    partial_function (option) while :: "'s \<Rightarrow> 's option" where
      "while s = (
        if b s then do {
          s \<leftarrow> c s;
          while s
        } else Some s)" 

        
    lemma wp_while_aux:
      assumes WF: "wf V"
      assumes I0: "I s"
      assumes STEP: "\<And>s. \<lbrakk> I s; b s \<rbrakk> \<Longrightarrow> wp (c s) (\<lambda>s'. I s' \<and> (s',s)\<in>V)"
      shows "wp (while s) (\<lambda>s. I s \<and> \<not>b s)"
      using WF I0
    proof (induction s rule: wf_induct_rule)
      case (less s)
      
      note \<open>I s\<close> \<comment> \<open>Invariant holds for s\<close>
      
      show ?case proof (cases "b s")
        case True
        then show ?thesis
          apply (subst while.simps) 
          apply simp
          apply (rule wp_cons[OF STEP])
          apply (rule \<open>I s\<close>)
          apply assumption
          apply (rule less.IH)
          by auto
      next
        case False  
        then show ?thesis
          apply (subst while.simps) 
          by (simp add: \<open>I s\<close>)
      qed
    qed
        
  
    (* For better applicability, we combine the while-rule with a consequence rule: *)
    lemma wp_while:
      assumes WF: "wf V"
      assumes I0: "I s"
      assumes STEP: "\<And>s. \<lbrakk> I s; b s \<rbrakk> \<Longrightarrow> wp (c s) (\<lambda>s'. I s' \<and> (s',s)\<in>V)"
      assumes CONS: "\<And>s. \<lbrakk> I s; \<not>b s \<rbrakk> \<Longrightarrow> Q s"
      shows "wp (while s) Q"
      apply (rule wp_cons)
      apply (rule wp_while_aux[where I=I, OF WF I0 STEP])
      apply assumption
      apply assumption
      using CONS by blast
          
  end

  
  
section \<open>Monadic Fold\<close>  
  
thm fold_simps  
  
fun mfold :: "('a \<Rightarrow> 's \<Rightarrow> 's option) \<Rightarrow> 'a list \<Rightarrow> 's \<Rightarrow> 's option" where  
  "mfold f [] s = Some s" 
| "mfold f (x#xs) s = do { s \<leftarrow> f x s; mfold f xs s }"  

(* Invariant rule for monadic fold *)  
lemma wp_mfold:
  assumes I0: "I [] xs s"
  assumes STEP: "\<And>xs\<^sub>1 x xs\<^sub>2 s. \<lbrakk> xs=xs\<^sub>1@x#xs\<^sub>2; I xs\<^sub>1 (x#xs\<^sub>2) s \<rbrakk> \<Longrightarrow> wp (f x s) (\<lambda>s'. I (xs\<^sub>1@[x]) xs\<^sub>2 s')"
  assumes CONS: "\<And>s. I xs [] s \<Longrightarrow> Q s"
  shows "wp (mfold f xs s) Q"
proof -
  (* Generalization over xs\<^sub>1, the elements already processed. 
    We decided to apply the consequence rule later, so we show postcondition "I xs []".
    We could as well do that within the induction, showing Q directly.
  *)
  have "wp (mfold f xs\<^sub>2 s) (I xs [])" if "xs=xs\<^sub>1@xs\<^sub>2" "I xs\<^sub>1 xs\<^sub>2 s" for xs\<^sub>1 xs\<^sub>2 s
    using that
  proof (induction xs\<^sub>2 arbitrary: xs\<^sub>1 s)
    case Nil
    then show ?case by auto (* Note that "wp (Some x) Q \<longleftrightarrow> \<dots>" is in the simpset *)
  next
    case (Cons x xs\<^sub>2)
    
    (* We instantiate the STEP rule and the IH to our split of xs, 
      and register the resulting rules as wp_intro
    *)
    note [wp_intro] = wp_cons[OF STEP, OF \<open>xs=xs\<^sub>1@x#xs\<^sub>2\<close>]
    
    from Cons.IH[of "xs\<^sub>1@[x]"] Cons.prems 
    have IH: "I (xs\<^sub>1 @ [x]) xs\<^sub>2 s' \<Longrightarrow> wp (mfold f xs\<^sub>2 s') (I xs [])" for s' by auto
    note [wp_intro] = wp_cons[OF IH]
    
    show ?case 
      apply simp (* unfolds mfold definition *)
      apply (intro wp_intro) (* applies rule for f (STEP) and "mfold _ xs\<^sub>2" (IH), 
        which we registered as wp_intro. *)
      using Cons.prems by auto
    
  qed
  from this[of "[]" xs] I0 have "wp (mfold f xs s) (I xs [])" (* specialize to I [] xs *)
    by auto
  from wp_cons[OF this] CONS show ?thesis . (* Apply final consequence rule *)
qed

lemma mono_mfold[partial_function_mono]:
  assumes "\<And>x y. monotone option.le_fun option_ord (C x y)"
  shows "monotone option.le_fun option_ord (\<lambda>f. mfold (\<lambda>x y. C x y f) xs s)"
  apply (induction xs arbitrary: s)
  apply simp
  apply (intro partial_function_mono)
  apply simp
  apply (intro partial_function_mono assms)
  by simp
  
  
end

